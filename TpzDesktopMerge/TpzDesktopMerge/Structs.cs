﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.InteropServices;

namespace TpzDesktopMerge
{
    class Structs
    {
        [StructLayout(LayoutKind.Sequential)]
        public struct TrapezeLicenseInfo
        {
            public ushort size;
            public uint licenses;
            public ushort minHighwireVersion;
            public byte product;
            public expiryDate expiryDate;
            public string customer;
            public string location;
            public string key;
            public bool anyLicense;
        }

        [StructLayout(LayoutKind.Sequential)]
        public struct expiryDate
        {
            public byte day;
            public byte month;
            public byte year;
        } 

    }
}

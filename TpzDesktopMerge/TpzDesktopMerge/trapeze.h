/*
 * Onstream Trapeze
 *
 * Copyright 1997-2013 Onstream Systems Ltd
 */

/* On Windows:
     Requires windows.h
     Windows types can be used interchangeably with the Trapeze equivalent
     e.g. either DWORD or TrapezeDWORD can be used where either is required
     Define TRAPEZEDYNAMIC to use Trapeze dynamically, and call
     InitTrapezeDynamic() in trpzdyn.c to load Trapeze before calling any
     functions. Define TRAPEZEVERSION to the version required (default is
     the current version).
     e.g. #define TRAPEZEVERSION 0x2103
          declares and loads functions for Trapeze 2.1 Beta 3
   On Unix:
     Define NOTRAPEZEWINDOWSNAMES to disable name compatibility with
     Windows code.
 */

#ifndef _TRAPEZE_H_
#define _TRAPEZE_H_

#ifdef __cplusplus
extern "C" {
#endif /*__cplusplus*/

/* Change structure alignment to 8 */
#ifdef _MSC_VER
/* Visual Studio */
#pragma pack(push, 8)
#elif __GNUC__
#ifdef __APPLE__
/* GCC on Mac OS X */
#pragma pack(8)
#else
/* GCC on anything except Mac OS X */
#pragma pack(push, 8)
#endif /*__APPLE__*/
#elif __IBMC__
/* IBM Compiler on OS/390 */
#pragma pack(8)
#else
#error Unknown compiler
#endif

#ifdef _WIN32
/* Only available on Windows */

/* Type definitions */
#define TrapezeAPI                WINAPI
#define TrapezeBITMAPINFO         BITMAPINFO
#define TrapezeBITMAPINFOHEADER   BITMAPINFOHEADER
#define TrapezeBOOL               BOOL
#define TrapezeBSTR               BSTR
#define TrapezeBYTE               BYTE
#define TrapezeCOLORREF           COLORREF
#define TrapezeDWORD              DWORD
#ifdef _WIN64
#define TrapezeDWORD_PTR          UINT_PTR
#else
#define TrapezeDWORD_PTR          DWORD
#endif /*_WIN64*/
#define TrapezeGUID               GUID
#define TrapezeHGLOBAL            HGLOBAL
#define TrapezeLANGID             LANGID
#define TrapezeLONG               LONG
#define TrapezeLPBOOL             LPBOOL
#define TrapezeLPBITMAPINFO       LPBITMAPINFO
#define TrapezeLPBITMAPINFOHEADER LPBITMAPINFOHEADER
#define TrapezeLPBYTE             LPBYTE
#define TrapezeLPCGUID            LPCGUID
#define TrapezeLPCOLESTR          LPCOLESTR
#define TrapezeLPCOLORREF         LPCOLORREF
#define TrapezeLPCRECT            LPCRECT
#define TrapezeLPCSTR             LPCSTR
#define TrapezeLPCVOID            LPCVOID
#define TrapezeLPCWSTR            LPCWSTR
#define TrapezeLPDWORD            LPDWORD
#define TrapezeLPGUID             LPGUID
#define TrapezeLPINT              LPINT
#define TrapezeLPPOINT            LPPOINT
#define TrapezeLPRECT             LPRECT
#define TrapezeLPRGBQUAD          LPRGBQUAD
#ifdef _WIN64
#define TrapezeLPSIZE_T           PSIZE_T
#else
#define TrapezeLPSIZE_T           LPDWORD
#endif /*_WIN64*/
#define TrapezeLPSTR              LPSTR
#define TrapezeLPSYSTEMTIME       LPSYSTEMTIME
#define TrapezeLPVOID             LPVOID
#define TrapezeLPWORD             LPWORD
#define TrapezeLPWSTR             LPWSTR
#define TrapezePOINT              POINT
#define TrapezeRECT               RECT
#define TrapezeRGBQUAD            RGBQUAD
#ifdef _WIN64
#define TrapezeSIZE_T             SIZE_T
#else
#define TrapezeSIZE_T             DWORD
#endif /*_WIN64*/
#define TrapezeSYSTEMTIME         SYSTEMTIME
#define TrapezeUINT               UINT
#ifndef NOTRAPEZEVARIANT
#define TrapezeVARIANT            VARIANT
#define TrapezeVARTYPE            VARTYPE
#endif /*! NOTRAPEZEVARIANT*/
#define TrapezeWCHAR              WCHAR
#define TrapezeWORD               WORD

/* wParam for measureTrapezeMsg */
#define cancelTrapezeMeasure 0
#define measureTrapezePoint 1
#define measureTrapezeScale 2
#define measureTrapezeScaleLine 3

/* Units */
#define cmTrapezeUnit       0  /* Centimeters (1 cm = 10 mm) */
#define inchTrapezeUnit     1  /* Inches (1 inch = 25.4 mm) */
#define kmTrapezeUnit       2  /* Kilometers (1 km = 1000 m) */
#define meterTrapezeUnit    3  /* Meters (1 m = 1000 mm) */
#define metreTrapezeUnit    3  /* Meters (1 m = 1000 mm) */
#define mileTrapezeUnit     4  /* Miles (1 mile = 1760 yards) */
#define mmTrapezeUnit       5  /* Millimeters */
#define noTrapezeUnit       6  /* Unknown units */
#define originalTrapezeUnit 7  /* Original units */
#define yardTrapezeUnit     8  /* Yards (1 yard = 3 feet) */
#define ftTrapezeUnit       9  /* Feet (1 foot = 12 inches) */
#define chainTrapezeUnit    10 /* Chains (1 chain = 66 feet) */
#define rodTrapezeUnit      11 /* Rods (1 rod = 5.5 yards) */
#define furlongTrapezeUnit  12 /* Furlongs (1 furlong = 220 yards) */
#define linkTrapezeUnit     13 /* Links (1 link = 0.66 feet) */
#define micrometerTrapezeUnit 14 /* Micrometers (1 micrometer = 0.001 mm) */
#define decimeterTrapezeUnit 15 /* Decimeters (1 decimeter = 100 mm) */
#define dekameterTrapezeUnit 16 /* Dekameters (1 dekameter = 10000 mm) */
#define pointTrapezeUnit    17 /* Points (1 point = 1/72 inches) */

/* Area units */
#define acreTrapezeAreaUnit          0 /* Acres */
#define acreHectareTrapezeAreaUnit   1 /* Acres for imperial lengths, hectares
                                          for metric lengths */
#define areTrapezeAreaUnit           2 /* Ares */
#define hectareTrapezeAreaUnit       3 /* Hectares */
#define lengthSquaredTrapezeAreaUnit 4 /* Length unit squared */
#define perchTrapezeAreaUnit         5 /* Perches */
#define roodTrapezeAreaUnit          6 /* Roods */

/* Overlay image styles */
#define displayTrapezeOverlay 1
#define printTrapezeOverlay 2
#define opaqueTrapezeOverlay 4
#define transparentTrapezeOverlay 8
#define semitransparentTrapezeOverlay 16

/* Fit values */
#define fitTrapezeHeight          0 /* Fit to height */
#define fitTrapezeNone            1 /* No fitting */
#define fitTrapezeWidth           2 /* Fit to width */
#define fitTrapezeWindow          3 /* Fit to window */
#define fitTrapezeWindowNoEnlarge 4 /* Fit to window if larger than window,
                                       100% otherwise */

/* Default print range values */
#define currentTrapezePrintRange 0 /* Print selection/current page */
#define allTrapezePrintRange     1 /* Print all */
#define visibleTrapezePrintRange 2 /* Print visible area */

/* Image tools */
#define noTrapezeImageTool                     0 /* No tool */
#define zoomInTrapezeImageTool                 1 /* Zoom In tool */
#define zoomOutTrapezeImageTool                2 /* Zoom Out tool */
#define dragTrapezeImageTool                   3 /* Dragging tool */
#define selectTrapezeImageTool                 4 /* Rectangle Selection
                                                    tool */
#define selectZoomTrapezeImageTool             5 /* Zoom Area tool */
#define selectAnnotationTrapezeImageTool       6 /* Annotation Selection
                                                    tool */
#define arrowAnnotationTrapezeImageTool        7 /* Arrow Annotation tool */
#define filledRectAnnotationTrapezeImageTool   8 /* Filled Rectangle
                                                    Annotation tool */
#define highlighterAnnotationTrapezeImageTool  9 /* Highlighter Annotation
                                                    tool */
#define hollowRectAnnotationTrapezeImageTool   10 /* Hollow Rectangle
                                                     Annotation tool */
#define lineAnnotationTrapezeImageTool         11 /* Line Annotation tool */
#define pictureAnnotationTrapezeImageTool      12 /* Picture Annotation
                                                     tool */
#define polygonAnnotationTrapezeImageTool      13 /* Polygon Annotation
                                                     tool */
#define polylineAnnotationTrapezeImageTool     14 /* Polyline Annotation
                                                     tool */
#define rectAnnotationTrapezeImageTool         15 /* Rectangle Annotation
                                                     tool */
#define scribbleAnnotationTrapezeImageTool     16 /* Scribble Annotation
                                                     tool */
#define stickyNoteAnnotationTrapezeImageTool   17 /* Sticky Note Annotation
                                                     tool */
#define straightLineAnnotationTrapezeImageTool 18 /* Straight Line Annotation
                                                     tool */
#define textAnnotationTrapezeImageTool         19 /* Text Annotation tool */
#define magnifyTrapezeImageTool                20 /* Magnify tool */
#define otherTrapezeImageTool                  21 /* Other tool, defined in a
                                                     newer version of this
                                                     API */
#define measureTrapezeImageTool                22 /* Measurement tool */
#define calibrateTrapezeImageTool              23 /* Calibration tool */
#define ocrTextTrapezeImageTool                24 /* OCR Text Selection
                                                     tool */
#define stampAnnotationTrapezeImageTool        25 /* Stamp Annotation tool */
#define rectRedactionTrapezeImageTool          26 /* Rectangle Redaction
                                                     tool */
#define ellipseAnnotationTrapezeImageTool      27 /* Ellipse Annotation
                                                     tool */
#define filledEllipseAnnotationTrapezeImageTool 28 /* Filled Ellipse
                                                      Annotation tool */
#define hollowEllipseAnnotationTrapezeImageTool 29 /* Hollow Ellipse
                                                      Annotation tool */
#define ellipseRedactionTrapezeImageTool       30 /* Ellipse Redaction tool */
#define circleMeasureTrapezeImageTool          31 /* Circle measurement
                                                     tool */
#define scribbleRedactionTrapezeImageTool      32 /* Scribble Redaction
                                                     tool */
#define lightTableTrapezeImageTool             33 /* Light Table tool */
#define deskewTrapezeImageTool                 34 /* Deskew tool */
#define lineMeasurementAnnotationTrapezeImageTool 35 /* Line Measurement
                                                        Annotation tool */
#define recessionPlaneAnnotationTrapezeImageTool 36 /* Recession Plane
                                                       Annotation tool */
#define hConeOfVisionAnnotationTrapezeImageTool 37 /* Horizontal Cone Of
                                                      Vision Annotation
                                                      tool */

/* Default bookmark root level value */
#define defaultTrapezeBookmarkRootLevel 0xFFFFFFFF

/* Function sheet types */
#define trapezeDuplexFunctionSheet      0x00000001
#define trapezeJobSepFunctionSheet      0x00000002
#define trapezeNoRotateFunctionSheet    0x00000004
#define trapezePhotoModeFunctionSheet   0x00000008
#define trapezeRotateLeftFunctionSheet  0x00000010
#define trapezeRotateRightFunctionSheet 0x00000020
#define trapezeSimplexFunctionSheet     0x00000040
#define trapezeTextModeFunctionSheet    0x00000080
#define trapezeNewRecordExFunctionSheet 0x00000100
#define trapezeAllFunctionSheets        0xFFFFFFFF

/* Trapeze window properties
   Properties are set using setTrapezePropertyMsg and read using
   getTrapezePropertyMsg */

/* Changes the about box text, if licensed. */
#define aboutTextTrapezeProperty 94
/* Set: value = SetTrapezeAboutTextPropertyCPtr
*/

/* Gets the number of annotations in the current page. */
#define annotationCountTrapezeProperty 103
/* Get: value = LPDWORD
*/

/* Gets the number of annotation groups in the current page */
#define annotationGroupCountTrapezeProperty 108
/* Get: value = LPDWORD
*/

/* Determines the group(s) that any new annotations belong to */
#define annotationGroupsTrapezeProperty 109
/* Get: value = GetTrapezeAnnotationGroupsPtr
   Set: value = SetTrapezeAnnotationGroupsCPtr
*/

/* When set to TRUE, any annotations created by the user are created as
   redactions; annotations created by code can be created as annotations.
   Also, any stamps created are redactions. Default = FALSE. */
#define annotationRedactionTrapezeProperty 105
/* Get: value = LPBOOL
   Set: value = BOOL
*/

/* When set to TRUE, an audit trail of annotation changes is kept by sending
   annotationAuditTrapezeNotify notifications. Default = FALSE. */
#define annotationsAuditTrapezeProperty 133
/* Get: value = LPBOOL
   Set: value = BOOL
*/

/* Determines if annotations have been changed in the current document */
#define annotationsChangedTrapezeProperty 157
/* Get: value = LPBOOL
*/

/* Gets the number of selected annotations in the current page */
#define annotationSelCountTrapezeProperty 160
/* Get: value = LPDWORD
*/

/* When set to TRUE any annotations are read-only. Default = FALSE. This
   property is reset to the default when a document is opened. */
#define annotationsReadOnlyTrapezeProperty 87
/* Get: value = LPBOOL
   Set: value = BOOL
*/

/* When set to TRUE, annotations maintain create/modify times. When set to
   FALSE (the default), only annotations with create and/or modify times
   update modify times. */
#define annotationTimesTrapezeProperty 134
/* Get: value = LPBOOL
   Set: value = BOOL
*/

/* Gets or sets the area measurement unit preference, which is one of
   *TrapezeAreaUnit. */
#define areaUnitTrapezeProperty 117
/* Get: value = LPWORD
   Set: value = WORD
*/

/* When set to TRUE (the default) and preferences are set to not save changed
   thumbnails and a thumbnail is changed, the user is asked if thumbnail
   changes for the document should be saved. When set to FALSE, the user is
   never asked and changes are never saved. */
#define askSaveThumbnailChangesTrapezeProperty 115
/* Get: value = LPBOOL
   Set: value = BOOL
*/

/* When set to TRUE the window runs in asynchronous mode, where a message can
   return before the command it started has completed. In this mode, the
   caller should handle notifications to keep track of what is happening. When
   set to FALSE the window runs in synchronous mode, where a message will not
   return until it has completed. Default = FALSE. */
#define asyncTrapezeProperty 11
/* Get: value = LPBOOL
   Set: value = BOOL
*/

/* When set to TRUE, accelerators are processed automatically. When set to
   FALSE (the default), translateAcceleratorTrapezeMsg must be sent to handle
   accelerators. */
#define autoAccelTrapezeProperty 111
/* Get: value = LPBOOL
   Set: value = BOOL
*/

/* Gets the number of bookmarks in the current document. */
#define bookmarkCountTrapezeProperty 49
/* Get: value = LPDWORD
*/

/* Bookmark folders at and below this level are locked (can't be edited) and
   changes can only be made within folders at this level. Pages can't be at or
   below this level. The default is defaultTrapezeBookmarkRootLevel, which
   means no folders are locked, changes can be made anywhere and pages can be
   anywhere. */
#define bookmarkRootLevelTrapezeProperty 59
/* Get: value = LPDWORD
   Set: value = DWORD
*/

/* When set to TRUE any bookmarks are read-only. Default = FALSE. This
   property is reset to the default when a document is opened. */
#define bookmarksReadOnlyTrapezeProperty 73
/* Get: value = LPBOOL
   Set: value = BOOL
*/

/* Gets the number of selected bookmarks in the current document. */
#define bookmarkSelCountTrapezeProperty 74
/* Get: value = LPDWORD
*/

/* Gets or sets the bookmark pane width. Note: this property can change when a
   document is opened if the updatePluginViewTrapezeProperty property is
   TRUE. */
#define bookmarksWidthTrapezeProperty 33
/* Get: value = LPWORD
   Set: value = WORD
*/

/* Gets the busy state, which is FALSE if Trapeze is idle, TRUE if Trapeze is
   busy */
#define busyTrapezeProperty 156
/* Get: value = LPBOOL
*/

/* Gets the size of the DIB that will be returned by getTrapezeDIBMsg, or the
   size of the DIB available for use with getTrapezeDIBRectMsg. */
#define dibSizeTrapezeProperty 143
/* Get: value = LPPOINT
*/

/* When set to zero (the default), exemption codes are burned in. When set to
   one, exemption codes are burned in to the translucent copy and not burned
   in to the final copy. */
#define exemptionCodeBurnInTrapezeProperty 142
/* Get: value = LPBYTE
   Set: value = BYTE
*/

/* Gets or sets the units in the calibration popup/dialog, during calibration.
   It can be any of *TrapezeUnit, except originalTrapezeUnit. It can be set to
   originalTrapezeUnit, which will set it to the units stored in the image.
   Fails with ERROR_CANCELLED when not calibrating. While the final
   calibration dialog is showing, the value can be noTrapezeUnit for an image
   with no resolution or scale. */
#define calibratePopupUnitTrapezeProperty 120
/* Get: value = LPWORD
   Set: value = WORD
*/

/* When set to FALSE (the default), the annotation selection tool must be the
   current tool before an annotation can be selected. When set to TRUE,
   clicking in an annotation will change to the annotation selection tool and
   select the annotation. */
#define clickSelectAnnotationTrapezeProperty 124
/* Get: value = LPBOOL
   Set: value = BOOL
*/

/* When this property is TRUE, the Thumbnail menu/Copy Pages command will
   allow copying multiple pages to the clipboard and will use CF_TIFF as the
   clipboard format. When this property is FALSE (the default), the Thumbnail
   menu/Copy Pages command will only allow copying single pages and will use
   CF_DIB and CF_METAFILEPICT as the clipboard formats. */
#define copyPagesTIFFTrapezeProperty 149
/* Get: value = LPBOOL
   Set: value = BOOL
*/

#define customPrintRectTrapezeProperty 29
/* Get: value = LPRECT
   Set: value = LPCRECT
*/

/* Gets or sets the default print range for the print dialog. It can be any of
   *TrapezePrintRange (the default is currentTrapezePrintRange). */
#define defaultPrintRangeTrapezeProperty 131
/* Get: value = LPBYTE
   Set: value = BYTE
*/

/* When trapezeRedactionCommand is disabled, if this property is TRUE, any
   redaction stamps are disabled. If this property is FALSE (the default), any
   redaction stamps are treated as annotation stamps. When
   trapezeRedactionCommand is not disabled, this property is ignored. */
#define disableRedactionStampsTrapezeProperty 152
/* Get: value = LPBOOL
   Set: value = BOOL
*/

/* Determines if the document has been changed since being opened or saved.
   This property can be set to TRUE to mark an unchanged document as changed;
   it can be set to FALSE to mark a document as unchanged. */
#define documentChangedTrapezeProperty 65
/* Get: value = LPBOOL
   Set: value = BOOL
*/

/* Gets or sets the origin of the document, if it is different to the file
   used to open the document. This property is reset to the default ("")
   whenever a document is opened, which indicates that the file opened is the
   document origin. */
#define documentOriginTrapezeProperty 141
/* Get: value = GetTrapezeStringPropertyPtr
   Set: value = LPCSTR
*/

/* Gets the type of the currently open document. A returned type of
   errorTrapezeDocument indicates that no document is currently open. */
#define documentTypeTrapezeProperty 64
/* Get: value = LPDWORD
*/

/* When set to TRUE warnings are shown for incorrect but readable documents.
   When set to FALSE these warnings are not shown. */
#define documentWarningTrapezeProperty 68
/* Get: value = LPBOOL
   Set: value = BOOL
*/

/* When set to TRUE, image selections are dragged as text extracted from the
   image selection by using OCR. When set to FALSE (the default), image
   selections are dragged as images. */
#define dragImageTextTrapezeProperty 132
/* Get: value = LPBOOL
   Set: value = BOOL
*/

/* When set to TRUE (the default), the bookmark context menu is enabled. When
   set to FALSE the bookmark context menu is disabled, and if
   enableNotifyTrapezeProperty is set to TRUE a contextMenuTrapezeNotify
   notification is sent with pane set to trapezeBookmarkPane when the menu is
   requested. */
#define enableBookmarksMenuTrapezeProperty 40
/* Get: value = LPBOOL
   Set: value = BOOL
*/

/* When set to TRUE (the default), the image context menu is enabled. When set
   to FALSE the image context menu is disabled, and if
   enableNotifyTrapezeProperty is set to TRUE a contextMenuTrapezeNotify
   notification is sent with pane set to trapezeImagePane when the menu is
   requested. */
#define enableImageMenuTrapezeProperty 41
/* Get: value = LPBOOL
   Set: value = BOOL
*/

/* When set to TRUE (the default), the Trapeze window context menu is enabled.
   When set to FALSE the Trapeze window context menu is disabled, and if
   enableNotifyTrapezeProperty is set to TRUE a contextMenuTrapezeNotify
   notification is sent with pane set to trapezeTrapezePane when the menu is
   requested. */
#define enableMenuTrapezeProperty 48
/* Get: value = LPBOOL
   Set: value = BOOL
*/

/* When set to TRUE (the default), the Trapeze window will send various
   WM_NOTIFY notifications, defined below. When set to FALSE, the Trapeze
   window will not send any notifications. */
#define enableNotifyTrapezeProperty 45
/* Get: value = LPBOOL
   Set: value = BOOL
*/

/* When set to TRUE (the default), the thumbnail context menu is enabled. When
   set to FALSE the thumbnail context menu is disabled, and if
   enableNotifyTrapezeProperty is set to TRUE a contextMenuTrapezeNotify
   notification is sent with pane set to trapezeThumbnailPane when the menu is
   requested. */
#define enableThumbnailsMenuTrapezeProperty 42
/* Get: value = LPBOOL
   Set: value = BOOL
*/

/* When set to TRUE (the default), reduced color (or gray) images are drawn
   enhanced. When set to FALSE, quality is reduced but the images draw
   faster. */
#define enhanceColorTrapezeProperty 92
/* Get: value = LPBOOL
   Set: value = BOOL
*/

/* When set to TRUE (the default), reduced black and white (or 2 color)
   images are drawn enhanced. When set to FALSE, quality is reduced but the
   images draw faster. */
#define enhanceMonochromeTrapezeProperty 93
/* Get: value = LPBOOL
   Set: value = BOOL
*/

/* Gets/sets the exemption code action performed after creating a
   redaction. */
#define exemptionCodeActionTrapezeProperty 130
/* Get: value = GetTrapezeExemptionCodeActionPropertyPtr
   Set: value = GetTrapezeExemptionCodeActionPropertyCPtr
*/

/* When set to TRUE and the enableNotifyTrapezeProperty property is TRUE, the
   exemptionCodeActionTrapezeNotify notification is sent when appropriate. The
   default is FALSE. */
#define exemptionCodeActionNotifyTrapezeProperty 154
/* Get: value = LPBOOL
   Set: value = BOOL
*/

/* Gets the number of exemption code groups available. */
#define exemptionCodeGroupCountTrapezeProperty 129
/* Get: value = LPDWORD
*/

/* One of *TrapezeExemptionCodeSizeMode, indicating the method used to size
   exemption codes. The default is autoTrapezeExemptionCodeSizeMode. */
#define exemptionCodeSizeModeTrapezeProperty 135
/* Get: value = LPBYTE
   Set: value = BYTE
*/

/* This property adds extended function sheet types to the list of available
   function sheet types. Setting this property to 0 (the default) disables all
   extended function sheet types. Setting this property to a combination of
   trapeze*ExFunctionSheet values will allow use of the specified sheet
   types. */
#define exFunctionSheetTypesTrapezeProperty 140
/* Get: value = LPDWORD
   Set: value = DWORD
*/

/* When set to TRUE and the enableNotifyTrapezeProperty property is TRUE, the
   following notifications are sent when appropriate:
   deletePagesTrapezeNotify, insertPagesTrapezeNotify, movePagesTrapezeNotify.
   Default = FALSE. */
#define extendedPageNotifyTrapezeProperty 72
/* Get: value = LPBOOL
   Set: value = BOOL
*/

/* Sets the margins to leave when the window has the
   fillParentTrapezeWindowStyle style. rect->left specifies the left margin,
   in pixels, that the window will leave when filling its parent. rect->top,
   rect->right and rect->bottom specify the top, right and bottom margins,
   respectively. The default is (0, 0, 0, 0), which fills the entire
   parent. */
#define fillParentMarginsTrapezeProperty 83
/* Get: value = LPRECT
   Set: value = LPCRECT
*/

/* Gets and sets the fit mode, which is one of the fitTrapeze* values */
#define fitTrapezeProperty 0
/* Get: value = LPWORD
   Set: value = WORD
*/

#define generateThumbnailsTrapezeProperty 47
/* Get: value = LPBOOL
   Set: value = BOOL
*/

/* Returns TRUE if the current page contains redactions, FALSE if not. */
#define hasRedactionsTrapezeProperty 128
/* Get: value = LPBOOL
*/

/* When set to TRUE the Internet page is removed from the Preferences
   dialog. Default = FALSE. */
#define hideInternetPrefsTrapezeProperty 66
/* Get: value = LPBOOL
   Set: value = BOOL
*/

/* When set to TRUE, hides the "Start scribble annotation on pen down"
   preference. The tablet is opened when the window is shown, so to hide this
   preference the first time a tablet is found, create the window hidden, set
   this property to TRUE, then show the window. Default = FALSE. */
#define hideTabletScribbleAnnotationTrapezeProperty 123
/* Get: value = LPBOOL
   Set: value = BOOL
*/

/* Gets or sets the horizontal scroll bar position */
#define hScrollPosTrapezeProperty 2
/* Get: value = LPWORD
   Set: value = WORD
*/

/* Gets the horizontal scroll bar range */
#define hScrollRangeTrapezeProperty 4
/* Get: value = TrapezeScrollRangePtr
*/

#define imageBackColorTrapezeProperty 32
/* Get: value = TrapezeColorPtr
   Set: value = TrapezeColor
*/

/* When set to TRUE, an imageErrorTrapezeNotify notification is sent instead
   of displaying any image errors or warnings after opening a page. The
   default is FALSE. */
#define imageErrorNotifyTrapezeProperty 151
/* Get: value = LPBOOL
   Set: value = BOOL
*/

/* Gets the height of the current page, in pixels. */
#define imageHeightTrapezeProperty 52
/* Get: value = LPDWORD
*/

/* Gets or sets the current tool, which is one of the *TrapezeImageTool
   values. The tool cannot be set to otherTrapezeImageTool or
   stampAnnotationTrapezeImageTool. */
#define imageToolTrapezeProperty 30
/* Get: value = LPWORD
   Set: value = WORD
*/

#define imageWarningTrapezeProperty 58
/* Get: value = LPBOOL
   Set: value = BOOL
*/

/* Gets the width of the current page, in pixels. */
#define imageWidthTrapezeProperty 51
/* Get: value = LPDWORD
*/

#define invertTrapezeProperty 38
/* Get: value = LPBOOL
   Set: value = BOOL
*/

/* Gets the annotation number for the last created annotation, which can be
   used in messages such as deleteTrapezeAnnotationMsg that require an
   annotation number. Generally this message is sent immediately after
   insertTrapezeAnnotationMsg to get the number of the inserted annotation. */
#define lastAnnotationTrapezeProperty 107
/* Get: value = LPDWORD
*/

/* Gets the last edited exemption codes (the ones that will be used with the
   lastTrapezeExemptionCodeAction action). exemptionCodes->parent is ignored.
   exemptionCodes->exemptionCodes is overwritten; any existing value will not
   be freed, any returned value must be freed using GlobalFree() when it is no
   longer required. exemptionCodes->line is not changed. */
#define lastExemptionCodeTrapezeProperty 155
/* Get: value = TrapezeExemptionCodesPtr
*/

/* When this property is FALSE, Save As dialogs won't allow access to any
   local drives. When this property is TRUE (the default), Save As dialogs
   behave as normal. */
#define localSaveTrapezeProperty 61
/* Get: value = LPBOOL
   Set: value = BOOL
*/

#define locateBookmarkTrapezeProperty 44
/* Get: value = LPBOOL
   Set: value = BOOL
*/

/* When set to TRUE the magnifier shows a crosshair; when set to FALSE it
   doesn't */
#define magnifierCrosshairTrapezeProperty 71
/* Get: value = LPBOOL
   Set: value = BOOL
*/

/* Gets or sets the maximum font size, in points, used for an exemption code
   when exemptionCodeSizeModeTrapezeProperty is set to
   redactionWidthTrapezeExemptionCodeSizeMode. The default is 72. */
#define maxExemptionCodeFontSizeTrapezeProperty 136
/* Get: value = LPWORD
   Set: value = WORD
*/

/* Gets or sets the maximum allowed zoom, in percentage multiplied by 100.
   Default = 640000 (6400%) */
#define maxZoomTrapezeProperty 36
/* Get: value = LPDWORD
   Set: value = DWORD
*/

/* Gets or sets the units in the measurement popup, during measuring. It can
   be any of *TrapezeUnit except originalTrapezeUnit and noTrapezeUnit. It can
   be set to originalTrapezeUnit, which will set it to the units stored in the
   image, or fail with ERROR_INVALID_PARAMETER if there are no units stored in
   the image. Fails with ERROR_CANCELLED when not measuring. */
#define measurePopupUnitTrapezeProperty 119
/* Get: value = LPWORD
   Set: value = WORD
*/

/* Gets or sets the measurement unit preference, which is any of *TrapezeUnit
   except noTrapezeUnit. */
#define measureUnitTrapezeProperty 116
/* Get: value = LPWORD
   Set: value = WORD
*/

/* Gets or sets the minimum allowed zoom, in percentage multiplied by 100.
   The value zero indicates no minimum. Default = 0 (no minimum) */
#define minZoomTrapezeProperty 37
/* Get: value = LPDWORD
   Set: value = DWORD
*/

/* Gets or sets the color to mix annotations with before displaying when using
   mixedTrapezeAnnotationDisplayMode. 0xFFFFFFFF (the default) indicates no
   color is mixed with annotations during display. */
#define mixedAnnotationDisplayModeColorTrapezeProperty 121
/* Get: value = LPCOLORREF
   Set: value = COLORREF
*/

/* When set to TRUE (the default), the mouse wheel zooms the image. When set
   to FALSE, the mouse wheel scrolls the image. */
#define mouseWheelZoomTrapezeProperty 91
/* Get: value = LPBOOL
   Set: value = BOOL
*/

/* When set to TRUE multi-page documents show thumbnails when opened. When set
   to FALSE multi-page documents open with thumbnails hidden. */
#define multiPageDocumentThumbnailsTrapezeProperty 69
/* Get: value = LPBOOL
   Set: value = BOOL
*/

/* When set to TRUE any internet links are displayed as text rather than
   clickable links (e.g. the links in the About dialog). Default = FALSE. */
#define noInternetLinksTrapezeProperty 67
/* Get: value = LPBOOL
   Set: value = BOOL
*/

/* Gets/sets the ODMA dialog mode, one of *TrapezeODMADialogMode. The default
   is normalTrapezeODMADialogMode. */
#define odmaDialogModeTrapezeProperty 114
/* Get: value = LPBYTE
   Set: value = BYTE
*/

/* Gets the ODMA document id for the current document, if any. If the current
   document is not an ODMA document, an empty string is returned. */
#define odmaDocIDTrapezeProperty 137
/* Get: value = GetTrapezeStringPropertyPtr
*/

/* Gets/sets the name of the document to use when saving to ODMA. The default
   is an empty string. */
#define odmaSaveAsNameTrapezeProperty 138
/* Get: value = GetTrapezeStringPropertyPtr
   Set: value = LPCSTR
*/

/* Gets/sets ODMA preferences */
#define odmaSettingsTrapezeProperty 57
/* Get: value = GetTrapezeODMASettingsPropertyPtr
   Set: value = GetTrapezeODMASettingsPropertyCPtr
*/

/* When set to unknownTrapezeDocument (the default), an open allows any
   available type to be opened. When set to any other supported
   *TrapezeDocument value, opens will only allow that format to be opened, and
   will only display that format in open dialogs. */
#define openTypeTrapezeProperty 144
/* Get: value = LPDWORD
   Set: value = DWORD
*/

#define overlayImageColorTrapezeProperty 22
/* Get: value = TrapezeColorPtr
   Set: value = TrapezeColor
*/

#define overlayImageStyleTrapezeProperty 26
/* Get: value = LPDWORD
   Set: value = DWORD
*/

#define overlayTextColorTrapezeProperty 23
/* Get: value = TrapezeColorPtr
   Set: value = TrapezeColor
*/

#define overlayTextFontTrapezeProperty 25
/* Get: value = GetTrapezeFontPropertyPtr
   Set: value = GetTrapezeFontPropertyCPtr
*/

#define overlayTextRectTrapezeProperty 28
/* Get: value = LPRECT
   Set: value = LPCRECT
*/

#define overlayTextStyleTrapezeProperty 27
/* Get: value = LPDWORD
   Set: value = DWORD
*/

#define overlayTextTrapezeProperty 24
/* Get: value = GetTrapezeStringPropertyPtr
   Set: value = LPCSTR
*/

/* Gets the number of pages in the current document. */
#define pageCountTrapezeProperty 34
/* Get: value = LPDWORD
*/

/* Gets or sets the current page number in the current document. */
#define pageNoTrapezeProperty 35
/* Get: value = LPDWORD
   Set: value = DWORD
*/

/* Changes the panes shown in the preferences dialog. Zero (the default) shows
   all preference panes. Any other value, a combination of trapezePrefs*
   values (except trapezePrefsParent and trapezePrefsEx), limits the panes to
   those specified. */
#define preferencesTrapezeProperty 100
/* Get: value = LPDWORD
   Set: value = DWORD
*/

/* When set to TRUE the Trapeze window will always print the number of
   requested copies. When set to FALSE (the default) the Trapeze window will
   send one copy to the printer and request the printer driver to print
   copies, if the driver supports it. */
#define printCopiesTrapezeProperty 86
/* Get: value = LPBOOL
   Set: value = BOOL
*/

#define printerTrapezeProperty 21
/* Get: value = GetTrapezeStringPropertyPtr
   Set: value = LPCSTR
*/

/* This property enables barcode printing for printing job separation sheets
   from the Print Function Sheets command. The default is TRUE. */
#define printFunctionSheetBarcodesTrapezeProperty 79
/* Get: value = LPBOOL
   Set: value = BOOL
*/

/* This property specifies the sheet types that can be printed by the Print
   Function Sheets command. It is a combination of trapeze*FunctionSheet
   values. Setting this property to trapezeAllFunctionSheets (the default)
   enables printing of all available function sheet types. */
#define printFunctionSheetTypesTrapezeProperty 78
/* Get: value = LPDWORD
   Set: value = DWORD
*/

/* When set to TRUE and the enableNotifyTrapezeProperty property is TRUE, the
   printTrapezeNotify notification is sent whenever something is printed. The
   default is FALSE. */
#define printNotifyTrapezeProperty 85
/* Get: value = LPBOOL
   Set: value = BOOL
*/

/* When set to TRUE (the default), advanced print options are available if
   licensed. When set to FALSE, advanced print options are not available. */
#define printOptionsTrapezeProperty 102
/* Get: value = LPBOOL
   Set: value = BOOL
*/

/* This property specifies the current print settings. */
#define printSettingsTrapezeProperty 20
/* Get: value = GetTrapezePrintSettingsPropertyPtr
   Set: value = GetTrapezePrintSettingsPropertyCPtr
*/

/* This property specifies the size limit, in kilobytes, of a print job. If
   the total size of printed images in a print job exceed this size a warning
   is displayed, asking the user to confirm printing a large job and giving
   them the opportunity to cancel. Setting this property to zero (the default)
   disables this feature. */
#define printSizeLimitTrapezeProperty 63
/* Get: value = LPDWORD
   Set: value = DWORD
*/

/* When set to TRUE the Trapeze window will tell the printer to stretch any
   images being enlarged when printed. When set to FALSE (the default) the
   Trapeze window will do the enlarging and send the exact image to be printed
   to the printer. */
#define printStretchTrapezeProperty 95
/* Get: value = LPBOOL
   Set: value = BOOL
*/

/* When this property is FALSE, Print dialogs won't allow Print To File. When
   this property is TRUE (the default), Print dialogs allow Print To File as
   normal. */
#define printToFileTrapezeProperty 62
/* Get: value = LPBOOL
   Set: value = BOOL
*/

/* When set to TRUE and the enableNotifyTrapezeProperty property is TRUE, the
   progressTrapezeNotify notification is sent whenever Trapeze shows
   progress */
#define progressNotifyTrapezeProperty 159
/* Get: value = LPBOOL
   Set: value = BOOL
*/

/* When this property is TRUE, only RapidRedact documents can be opened over
   OTTP. When this property is FALSE (the default), any document can be opened
   over OTTP. */
#define rapidRedactOTTPTrapezeProperty 122
/* Get: value = LPBOOL
   Set: value = BOOL
*/

/* When this property is TRUE, RapidRedact files can be opened. When this
   property is FALSE (the default), RapidRedact files cannot be opened. */
#define rapidRedactTrapezeProperty 106
/* Get: value = LPBOOL
   Set: value = BOOL
*/

/* When this property is TRUE, deleted pages are marked as deleted by
   redaction. When this property is FALSE (the default), deleted pages are
   removed. */
#define redactDeletedPagesTrapezeProperty 98
/* Get: value = LPBOOL
   Set: value = BOOL
*/

/* Sets the text used on pages marked as deleted by redaction. Setting this
   property to NULL uses the default ("DELETED"). When this property is the
   default, getting this property will set str to NULL and leave size
   unchanged. */
#define redactionDeletedPageTextTrapezeProperty 146
/* Get: value = GetTrapezeStringPropertyPtr
   Set: value = LPCSTR
*/

/* When this property is TRUE (the default), all OCR text is removed from any
   page that is redacted. When this property is FALSE, only OCR text touching
   a redacted area is removed. */
#define removeOCRTextRedactTrapezeProperty 145
/* Get: value = LPBOOL
   Set: value = BOOL
*/

/* When this property is TRUE, all thumbnails are removed from any TIFF that
   is saved. When this property is FALSE (the default), thumbnails are saved
   as normal. */
#define removeTIFFThumbnailsTrapezeProperty 153
/* Get: value = LPBOOL
   Set: value = BOOL
*/

#define rotationTrapezeProperty 39
/* Get: value = LPWORD
   Set: value = WORD
*/

/* When this property is FALSE, Save dialogs won't allow BMP. The default is
   TRUE. */
#define saveAsBMPTrapezeProperty 76
/* Get: value = LPBOOL
   Set: value = BOOL
*/

/* When this property is TRUE, thumbnail changes will be saved; when it is
   FALSE thumbnail changes won't be saved. Note: this property is the
   preference setting, not the setting for the current document, which can be
   TRUE when this property is FALSE. */
#define saveChangedThumbnailsTrapezeProperty 80
/* Get: value = LPBOOL
   Set: value = BOOL
*/

/* When this property is TRUE, redactions can be saved back to the original
   document. When this property is FALSE (the default), redactions can only be
   saved to another document (save as, save copy as or save selection). */
#define saveRedactionsTrapezeProperty 81
/* Get: value = LPBOOL
   Set: value = BOOL
*/

/* When set to unknownTrapezeDocument (the default), a save allows the user to
   choose a type. When set to any other supported *TrapezeDocument value,
   saves will only save in that format. */
#define saveTypeTrapezeProperty 113
/* Get: value = LPDWORD
   Set: value = DWORD
*/

/* Gets the selected annotation on the current page. The returned value is
   one of:
   noTrapezeAnnotationSelected - No annotations are selected.
   multipleTrapezeAnnotationsSelected - More than one annotation is selected.
   any other value - The annotation number of the only selected annotation. */
#define selectedAnnotationTrapezeProperty 158
/* Get: value = LPDWORD
*/

/* Gets the selected bookmark in the current document. The returned value is
   one of:
   noTrapezeBookmarkSelected - No bookmarks are selected.
   multipleTrapezeBookmarksSelected - More than one bookmark is selected.
   any other value - The bookmark number of the only selected bookmark. */
#define selectedBookmarkTrapezeProperty 75
/* Get: value = LPDWORD
*/

/* Gets or sets the rectangle selection in the current page. This property
   returns an empty rectangle if the current image tool is not the rectangle
   selection tool, or if nothing is selected. Setting this property changes
   the current image tool to the rectangle selection tool, if it isn't
   already, then sets the selection, if possible. Setting this property to an
   empty rectangle removes any selection. */
#define selectRectTrapezeProperty 54
/* Get: value = LPRECT
   Set: value = LPCRECT
*/

/* Gets or sets the file name to use when sending a file by email. When set to
   an empty string (the default), the file name or document title, if
   available, is used. */
#define sendTitleTrapezeProperty 96
/* Get: value = GetTrapezeStringPropertyPtr
   Set: value = LPCSTR
*/

/* When set to TRUE, accelerators are shown in tooltips, if set in
   preferences. When set to FALSE (the default), accelerators are never shown
   in tooltips. */
#define showAccelTrapezeProperty 110
/* Get: value = LPBOOL
   Set: value = BOOL
*/

/* When set to TRUE (the default), using showTrapezeAnnotationGroupMsg will
   mark the document as modified. When set to FALSE, the document modified
   state is left as-is when showTrapezeAnnotationGroupMsg is used. */
#define showAnnotationGroupModifyTrapezeProperty 112
/* Get: value = LPBOOL
   Set: value = BOOL
*/

/* When set to TRUE (the default for a plugin window), a focus rectangle is
   displayed around the image pane when it has the focus. When set to FALSE
   (the default for a non-plugin window), the image fills the entire pane. */
#define showImageFocusTrapezeProperty 43
/* Get: value = LPBOOL
   Set: value = BOOL
*/

/* When set to TRUE (the default), the TRIM icon is displayed in TRIM
   preferences. When set to FALSE, the icon is hidden. */
#define showTRIMPrefsIconTrapezeProperty 118
/* Get: value = LPBOOL
   Set: value = BOOL
*/

/* When set to TRUE only a single bookmark or folder can be selected. When set
   to FALSE (the default) multiple bookmarks and/or folders can be
   selected. */
#define singleBookmarkSelectionTrapezeProperty 82
/* Get: value = LPBOOL
   Set: value = BOOL
*/

/* When set to TRUE single page documents show thumbnails when opened. When
   set to FALSE single page documents open with thumbnails hidden. */
#define singlePageDocumentThumbnailsTrapezeProperty 70
/* Get: value = LPBOOL
   Set: value = BOOL
*/

/* Gets a copy of the stamp menu. DestroyMenu() must be used to the destroy
   the menu when it is no longer required. Any commands selected from the menu
   should be forwarded to the Trapeze window the menu came from. The stamp
   menu can be changed by the user, so the returned menu should only be used
   short term. */
#define stampMenuTrapezeProperty 126
/* Get: value = HMENU*
*/

/* Gets or sets the current stamp. When getting, an empty string will be
   returned if the stamp tool is not selected. When setting, the stamp tool
   will be selected after the property has been set. Setting will fail with
   ERROR_INVALID_PARAMETER if the value is not the name of an existing
   stamp. */
#define stampNameTrapezeProperty 127
/* Get: value = GetTrapezeStringPropertyPtr
   Set: value = LPCSTR
*/

/* Gets or sets the tablet settings. The tablet is opened when the window is
   shown, so to disable the tablet, create the window hidden, set the tablet
   settings with disabled = TRUE, then show the window. */
#define tabletSettingsTrapezeProperty 56
/* Get: value = GetTrapezeTabletSettingsPropertyPtr
   Set: value = GetTrapezeTabletSettingsPropertyCPtr
*/

/* Gets the number of selected thumbnails in the current document. */
#define thumbnailSelCountTrapezeProperty 150
/* Get: value = LPDWORD
*/

/* Gets or sets the thumbnail size. */
#define thumbnailSizeTrapezeProperty 97
/* Get: value = LPPOINT
   Set: value = LPCPOINT
*/

/* Gets or sets the thumbnail pane width. Note: this property can change when
   a document is opened if the updatePluginViewTrapezeProperty property is
   TRUE. */
#define thumbnailsWidthTrapezeProperty 46
/* Get: value = LPWORD
   Set: value = WORD
*/

/* Gets or sets the thumbnail zoom, in percentage multiplied by 100.
   Thumbnails are all displayed zoomed by this value.
   Default = 10000 (100%) */
#define thumbnailZoomTrapezeProperty 31
/* Get: value = LPDWORD
   Set: value = DWORD
*/

/* Gets the location of the menu button on the toolbar, in screen coordinates.
   If the menu button is not visible, an empty rectangle is returned.
   Available in Trapeze 0x6570 or later. */
#define toolbarMenuRectTrapezeProperty 125
/* Get: value = LPRECT
*/

#ifdef UNICODE
#define translucentRedactionTTrapezeProperty \
  translucentRedactionWTrapezeProperty
#else
#define translucentRedactionTTrapezeProperty \
  translucentRedactionTrapezeProperty
#endif /*UNICODE*/

/* Change this property to a path to a file to write a translucent copy to
   whenever the document is redacted. This file is overwritten whenever the
   document is saved with redaction changes; if the document is never saved
   with redaction changes this file is not changed. This property is reset to
   the default ("") whenever a document is opened, which disables writing this
   file. */
#define translucentRedactionTrapezeProperty 90
/* Get: value = GetTrapezeStringPropertyPtr
   Set: value = LPCSTR
*/

/* Change this property to a path to a file to write a translucent copy to
   whenever the document is redacted (Unicode version). See
   translucentRedactionTrapezeProperty for details. */
#define translucentRedactionWTrapezeProperty 147
/* Get: value = GetTrapezeStringPropertyWPtr
   Set: value = LPCWSTR
*/

/* When this property is TRUE (the default), check out preferences are shown
   in the TRIM preferences dialog. */
#define trimCheckOutTrapezeProperty 101
/* Get: value = LPBOOL
   Set: value = BOOL
*/

/* When this property is TRUE (the default), TRIM and TopDrawer are available
   for use. When this property is FALSE, TRIM is available for use and
   TopDrawer is not. */
#define trimTopDrawerTrapezeProperty 99
/* Get: value = LPBOOL
   Set: value = BOOL
*/

/* When this property is TRUE, any OCR text is saved as all unprotected. When
   this property is FALSE (the default), any protection information is
   saved. */
#define unprotectOCRTextTrapezeProperty 148
/* Get: value = LPBOOL
   Set: value = BOOL
*/

/* When set to TRUE, any changes to thumbnails in a document containing
   thumbnails are saved, no matter what the other save thumbnail change
   properties and preferences are. When set to FALSE (the default), save
   thumbnail change properties and preferences determine whether thumbnail
   changes are saved. */
#define updateExistingThumbnailsTrapezeProperty 139
/* Get: value = LPBOOL
   Set: value = BOOL
*/

/* Change this property to FALSE to stop a plugin window updating the view
   from the preferences when a document is opened. The default is TRUE.
   Applies to a Trapeze plugin window only. */
#define updatePluginViewTrapezeProperty 60
/* Get: value = LPBOOL
   Set: value = BOOL
*/

/* Gets Trapeze version information. */
#define versionTrapezeProperty 10
/* Get: value = GetTrapezeStringPropertyPtr
*/

#define viewAnnotationsTrapezeProperty 53
/* Get: value = LPBOOL
   Set: value = BOOL
*/

/* Set to TRUE when bookmarks are visible; FALSE when they are hidden. For a
   plugin window with updatePluginViewTrapezeProperty set to TRUE, the value
   of this property will be changed when a document is opened. */
#define viewBookmarksTrapezeProperty 12
/* Get: value = LPBOOL
   Set: value = BOOL
*/

/* Set to TRUE when the image pane is visible; FALSE when it is hidden. For a
   plugin window with updatePluginViewTrapezeProperty set to TRUE, the value
   of this property will be changed when a document is opened. */
#define viewImageTrapezeProperty 13
/* Get: value = LPBOOL
   Set: value = BOOL
*/

/* Set to TRUE when the magnifier is visible; FALSE when it is hidden. */
#define viewMagnifierTrapezeProperty 84
/* Get: value = LPBOOL
   Set: value = BOOL
*/

/* Set to TRUE when the overview is visible; FALSE when it is hidden. */
#define viewOverviewTrapezeProperty 88
/* Get: value = LPBOOL
   Set: value = BOOL
*/

/* Set to TRUE when the pixel information popup is visible; FALSE when it is
   hidden. */
#define viewPixelInfoTrapezeProperty 89
/* Get: value = LPBOOL
   Set: value = BOOL
*/

/* Set to TRUE when the status bar is visible; FALSE when it is hidden. */
#define viewStatusBarTrapezeProperty 15
/* Get: value = LPBOOL
   Set: value = BOOL
*/

#define viewStatusBarBillingTrapezeProperty 50
/* Get: value = LPBOOL
   Set: value = BOOL
*/

#define viewStatusBarPageTrapezeProperty 16
/* Get: value = LPBOOL
   Set: value = BOOL
*/

#define viewStatusBarReadOnlyTrapezeProperty 17
/* Get: value = LPBOOL
   Set: value = BOOL
*/

#define viewStatusBarImageTrapezeProperty 18
/* Get: value = LPBOOL
   Set: value = BOOL
*/

#define viewStatusBarZoomTrapezeProperty 19
/* Get: value = LPBOOL
   Set: value = BOOL
*/

/* Set to TRUE when thumbnails are visible; FALSE when they are hidden. For a
   plugin window with updatePluginViewTrapezeProperty set to TRUE, the value
   of this property will be changed when a document is opened. */
#define viewThumbnailsTrapezeProperty 14
/* Get: value = LPBOOL
   Set: value = BOOL
*/

/* Set to TRUE when the toolbar is visible; FALSE when it is hidden. */
#define viewToolbarTrapezeProperty 77
/* Get: value = LPBOOL
   Set: value = BOOL
*/

/* Set to TRUE when the tools popup is visible; FALSE when it is hidden */
#define viewToolsTrapezeProperty 104
/* Get: value = LPBOOL
   Set: value = BOOL
*/

#define visibleRectTrapezeProperty 55
/* Get: value = LPRECT
*/

/* Gets or sets the vertical scroll bar position */
#define vScrollPosTrapezeProperty 3
/* Get: value = LPWORD
   Set: value = WORD
*/

/* Gets the vertical scroll bar range */
#define vScrollRangeTrapezeProperty 5
/* Get: value = TrapezeScrollRangePtr
*/

/* Gets the horizontal resolution of the current page. value->unit must be set
   to the units required before requesting this property; use
   originalTrapezeUnit to get the resolution in the units stored in the
   image. If the resolution has no units, the returned units will be
   noTrapezeUnit regardless of the requested units, and value->unit will be
   changed to noTrapezeUnit. If the resolution is undefined,
   value->denominator will be set to zero. Note that these units are different
   to those used by functions and structures that use
   *TrapezeTIFFResolutionScaleUnit values. */
#define xResolutionTrapezeProperty 6
/* Get: value = TrapezeResolutionScalePtr
*/

/* Gets the horizontal scale of the current page. value->unit must be set
   to the units required before requesting this property; use
   originalTrapezeUnit to get the resolution in the units stored in the
   image. If the scale has no units, the returned units will be noTrapezeUnit
   regardless of the requested units, and value->unit will be changed to
   noTrapezeUnit. If the scale is undefined, value->denominator will be set to
   zero. Note that these units are different to those used by functions and
   structures that use *TrapezeTIFFResolutionScaleUnit values. */
#define xScaleTrapezeProperty 8
/* Get: value = TrapezeResolutionScalePtr
*/

/* Gets the vertical resolution of the current page. value->unit must be set
   to the units required before requesting this property; use
   originalTrapezeUnit to get the resolution in the units stored in the
   image. If the resolution has no units, the returned units will be
   noTrapezeUnit regardless of the requested units, and value->unit will be
   changed to noTrapezeUnit. If the resolution is undefined,
   value->denominator will be set to zero. Note that these units are different
   to those used by functions and structures that use
   *TrapezeTIFFResolutionScaleUnit values. */
#define yResolutionTrapezeProperty 7
/* Get: value = TrapezeResolutionScalePtr
*/

/* Gets the vertical scale of the current page. value->unit must be set
   to the units required before requesting this property; use
   originalTrapezeUnit to get the resolution in the units stored in the
   image. If the scale has no units, the returned units will be noTrapezeUnit
   regardless of the requested units, and value->unit will be changed to
   noTrapezeUnit. If the scale is undefined, value->denominator will be set to
   zero. Note that these units are different to those used by functions and
   structures that use *TrapezeTIFFResolutionScaleUnit values. */
#define yScaleTrapezeProperty 9
/* Get: value = TrapezeResolutionScalePtr
*/

/* Gets or sets the current zoom, in percentage multiplied by 100. If the
   image is fitted, this value will contain the zoom used for fitting. Setting
   this value removes any fitting. */
#define zoomTrapezeProperty 1
/* Get: value = LPDWORD
   Set: value = DWORD
*/

/* Trapeze window scroll bars */
#define hTrapezeScrollBar 0 /* Horizontal scroll bar */
#define vTrapezeScrollBar 1 /* Vertical scroll bar */

/* Trapeze window scroll values */
#define scrollTrapezeDown 0 /* Scroll down one unit */
#define scrollTrapezeEnd  1 /* Scroll to end of scroll bar */
#define scrollTrapezeHome 2 /* Scroll to start of scroll bar */
#define scrollTrapezePgDn 3 /* Scroll down one page */
#define scrollTrapezePgUp 4 /* Scroll up one page */
#define scrollTrapezeUp   5 /* Scroll up one unit */

/* Trapeze window commands */
#define trapezeClearCommand              0 /* Edit/Delete command */
#define trapezeCopyCommand               1 /* Edit/Copy command */
#define trapezePasteCommand              2 /* Edit/Paste command */
#define trapezePropertiesCommand         3 /* Edit/Properties command */
#define trapezeSelectAllCommand          4 /* Edit/Select All command */
#define trapezeUndoCommand               5 /* Edit/Undo command */
#define trapezeZoomOtherCommand          6 /* Image/Zoom/Other command */
#define trapezePrintCommand              7 /* File/Print command */
#define trapezeZoomOutCommand            8 /* Image/Zoom Out command */
#define trapezeZoomSelectionCommand      9 /* Image/Zoom To Selection
                                              command */
#define trapezeZoomInCommand             10 /* Image/Zoom In command */
#define trapezeLocateBookmarkCommand     11 /* Page/Locate Page in Bookmarks
                                               command */
#define trapezeSaveAsCommand             12 /* File/Save As command */
#define trapezeSaveCommand               13 /* File/Save command */
#define trapezeHelpTopicsCommand         14 /* Help/Help Topics command */
#define trapezeViewToolbarCommand        15 /* View/Toolbar command */
#define trapezeScanAppendCommand         16 /* Page/Append/Scan Pages
                                               command */
#define trapezeScanAppendNoDialogCommand 17 /* Page/Append/Scan Pages command
                                               without a dialog */
#define trapezeSaveCopyAsCommand         18 /* File/Save Copy As command */
#define trapezePreferencesCommand        19 /* Edit/Preferences command */
#define trapezeMagnifyToolCommand        20 /* Tool/Magnify command */
#define trapezeInsertBeforeCommand       21 /* Page/Insert Before submenu */
#define trapezeInsertAfterCommand        22 /* Page/Insert After submenu */
#define trapezeAppendCommand             23 /* Page/Append submenu */
#define trapezeCloseCommand              24 /* File/Close command */
#define trapezeAppendFileCommand         25 /* Page/Append/Files command */
#define trapezeOCRBookmarkCommand        26 /* Edit/OCR/New Bookmark
                                               command */
#define trapezeRotatePageLeftCommand     27 /* Page/Rotate/Left command */
#define trapezeRotatePageFlipCommand     28 /* Page/Rotate/Flip command */
#define trapezeRotatePageRightCommand    29 /* Page/Rotate/Right command */
#define trapezeRotatePagesLeftCommand    30 /* Thumbnail menu/Rotate/Left
                                               command */
#define trapezeRotatePagesFlipCommand    31 /* Thumbnail menu/Rotate/Flip
                                               command */
#define trapezeRotatePagesRightCommand   32 /* Thumbnail menu/Rotate/Right
                                               command */
#define trapezeDeletePageCommand         33 /* Page/Delete command */
#define trapezeDeletePagesCommand        34 /* Thumbnail menu/Delete
                                               command */
#define trapezeToolbarMenuCommand        35 /* Toolbar/Menu command */
#define trapezeNewCommand                36 /* File/New command (application
                                               only) */
#define trapezeScanNewCommand            37 /* Toolbar/Scan New TIFF command
                                               (application only) */
#define trapezeOpenCommand               38 /* File/Open command (application
                                               only) */
#define trapezePrevPageCommand           39 /* Page/Previous command */
#define trapezeNextPageCommand           40 /* Page/Next command */
#define trapezeZoomCommand               41 /* Image/Zoom submenu and toolbar
                                               combo box */
#define trapezeRotateLeftCommand         42 /* Image/Rotate Left command */
#define trapezeRotateRightCommand        43 /* Image/Rotate Right command */
#define trapezeDragToolCommand           44 /* Tool/Dragging command */
#define trapezeZoomInToolCommand         45 /* Tool/Zoom In command */
#define trapezeZoomOutToolCommand        46 /* Tool/Zoom Out command */
#define trapezeSelectToolCommand         47 /* Tool/Rectangle Selection
                                               command */
#define trapezeSelectZoomToolCommand     48 /* Tool/Zoom Area command */
#define trapezeMeasureToolCommand        49 /* Tool/Measurement command */
#define trapezeCalibrateToolCommand      50 /* Tool/Calibration command */
#define trapezeSelectAnnotationToolCommand 51 /* Tool/Annotation/Redaction
                                                 Selection command */
#define trapezeImageToolCommand          52 /* Toolbar tool drop down */
#define trapezeAboutCommand              53 /* Help/About command */
#define trapezeViewBookmarksCommand      54 /* View/Bookmarks command */
#define trapezeViewThumbnailsCommand     55 /* View/Thumbnails command */
#define trapezeViewStatusBarCommand      56 /* View/Status Bar command */
#define trapezeViewAnnotationsCommand    57 /* View/Annotations command */
#define trapezeEnhanceMonochromeCommand  58 /* View/Enhance Black & White
                                               Display Quality command */
#define trapezeEnhanceColorCommand       59 /* View/Enhance Color command */
#define trapezeFullImageCommand          60 /* View/Full Image command */
#define trapezeViewPixelInfoCommand      61 /* View/Pixel Information
                                               command */
#define trapezeViewMagnifierCommand      62 /* View/Magnifier command */
#define trapezeViewOverviewCommand       63 /* View/Overview command */
#define trapezeFindCommand               64 /* Edit/Find command */
#define trapezeFindAgainCommand          65 /* Edit/Find Again command */
#define trapezeOCRDocumentCommand        66 /* Edit/OCR/Document command */
#define trapezeNewBookmarkFolderCommand  67 /* Edit/New Bookmark Folder
                                               command */
#define trapezeNewBookmarkCommand        68 /* Edit/New Bookmark command */
#define trapezeReplaceCommand            69 /* Page/Replace submenu */
#define trapezeViewImageCommand          70 /* View/Image command */
#define trapezeFindAgainReverseCommand   71 /* Find again backwards command */
#define trapezePrintFunctionSheetCommand 72 /* File/Print Function Sheets
                                               command */
#define trapezeSaveSelectionCommand      73 /* File/Save Selection command */
#define trapezeSavePageCommand           74 /* Image menu/Save Page command */
#define trapezeSendCommand               75 /* File/Send/Document command */
#define trapezeScanBeforeCommand         76 /* Page/Insert Before/Scan Pages
                                               command */
#define trapezeScanReplaceCommand        77 /* Page/Replace/Scan Pages
                                               command */
#define trapezeScanAfterCommand          78 /* Page/Insert After/Scan Pages
                                               command */
#define trapezeScanBeforePageCommand     79 /* Thumbnail menu/Insert Pages
                                               Before/Scan Pages command */
#define trapezeScanAfterPageCommand      80 /* Thumbnail menu/Insert Pages
                                               After/Scan Pages command */
#define trapezeScanReplacePagesCommand   81 /* Thumbnail menu/Replace/Scan
                                               Pages command */
#define trapezeRectRedactionToolCommand  82 /* Tool/Redaction/Rectangle
                                               command */
#define trapezeSetScaleCommand           83 /* Page/Set Scale command */
#define trapezePageSetupCommand          84 /* File/Page Setup command */
#define trapezeMovePageCommand           85 /* Page/Move command */
#define trapezeMovePagesCommand          86 /* Thumbnail menu/Move command */
#define trapezeRotatePageCommand         87 /* Page/Rotate submenu */
#define trapezeRotatePagesCommand        88 /* Thumbnail menu/Rotate
                                               submenu */
#define trapezeArrowAnnotationToolCommand 89 /* Tool/Annotation/Arrow
                                                command */
#define trapezeFilledRectAnnotationToolCommand 90 /* Tool/Annotation/
                                                     Filled Rectangle
                                                     command */
#define trapezeHighlighterAnnotationToolCommand 91 /* Tool/Annotation/
                                                      Highlighter command */
#define trapezeHollowRectAnnotationToolCommand 92 /* Tool/Annotation/
                                                     Hollow Rectangle
                                                     command */
#define trapezeLineAnnotationToolCommand 93 /* Tool/Annotation/Line command */
#define trapezePictureAnnotationToolCommand 94 /* Tool/Annotation/Picture
                                                  command */
#define trapezePolygonAnnotationToolCommand 95 /* Tool/Annotation/Polygon
                                                  command */
#define trapezePolylineAnnotationToolCommand 96 /* Tool/Annotation/Polyline
                                                   command */
#define trapezeRectAnnotationToolCommand 97 /* Tool/Annotation/Rectangle
                                               command */
#define trapezeScribbleAnnotationToolCommand 98 /* Tool/Annotation/Scribble
                                                   command */
#define trapezeStampAnnotationToolCommand 99 /* Tool/Annotation/Stamp
                                                command */
#define trapezeStickyNoteAnnotationToolCommand 100 /* Tool/Annotation/
                                                      Sticky Note command */
#define trapezeStraightLineAnnotationToolCommand 101 /* Tool/Annotation/
                                                        Straight Line
                                                        command */
#define trapezeTextAnnotationToolCommand 102 /* Tool/Annotation/Text
                                                command */
#define trapezeOCRTextToolCommand        103 /* Tool/OCR Text command */
#define trapezePrintSelectionCommand     104 /* Image menu/Print command */
#define trapezePrintPageCommand          105 /* Image menu/Print Page
                                                command */
#define trapezePrintPagesCommand         106 /* Bookmark menu/Print command
                                                and Thumbnail menu/Print
                                                command */
#define trapezeReplacePagesCommand       107 /* Thumbnail menu/Replace
                                                submenu */
#define trapezeInsertBeforePageCommand   108 /* Thumbnail menu/Insert Before
                                                submenu */
#define trapezeInsertAfterPageCommand    109 /* Thumbnail menu/Insert After
                                                submenu */
#define trapezeSendMenuCommand           110 /* File/Send submenu */
#define trapezeAnnotationsCommand        111 /* All annotation commands */
#define trapezeRedactionCommand          112 /* All redaction commands */
#define trapezeMeasureCommand            113 /* All measuring commands */
#define trapezeOCRCommand                114 /* All OCR commands */
#define trapezeEllipseAnnotationToolCommand 115 /* Tool/Annotation/Ellipse
                                                   command */
#define trapezeFilledEllipseAnnotationToolCommand 116 /* Tool/Annotation/
                                                         Filled Ellipse
                                                         command */
#define trapezeHollowEllipseAnnotationToolCommand 117 /* Tool/Annotation/
                                                         Hollow Ellipse
                                                         command */
#define trapezeEllipseRedactionToolCommand 118 /* Tool/Redaction/Ellipse
                                                  command */
#define trapezeCircleMeasureToolCommand  119 /* Tool/Circle Measurement
                                                command */
#define trapezeFavorite1Command          120 /* File/Favorites/1 command
                                                (application only) */
#define trapezeFavorite2Command          121 /* File/Favorites/2 command
                                                (application only) */
#define trapezeFavorite3Command          122 /* File/Favorites/3 command
                                                (application only) */
#define trapezeFavorite4Command          123 /* File/Favorites/4 command
                                                (application only) */
#define trapezeScribbleRedactionToolCommand 124 /* Tool/Redaction/Scribble
                                                   command */
#define trapezeSendSelectionCommand      125 /* File/Send/Selection command */
#define trapezeDropPagesCommand          126 /* Drag/drop pages into
                                                thumbnails */
#define trapezeDeleteThumbnailsCommand   127 /* Edit/Delete command in
                                                thumbnails */
#define trapezeExemptionCodeCommand      128 /* All exemption code commands */
#define trapezeEditExemptionCodesCommand 129 /* Edit/Exemption Codes
                                                command */
#define trapezeShowScanSettingsCommand   130 /* Edit/Preferences/Scanning/Show
                                                settings dialog before
                                                scanning preference */
#define trapezeBurnInCommand             131 /* Page/Make Annotations
                                                Permanent command */
#define trapezeCustom1Command            132 /* Custom 1 command (DLL only) */
#define trapezeCustom2Command            133 /* Custom 2 command (DLL only) */
#define trapezeCustom3Command            134 /* Custom 3 command (DLL only) */
#define trapezeCustom4Command            135 /* Custom 4 command (DLL only) */
#define trapezeScanSettingsCommand       136 /* File/Scan Settings command */
#define trapezeLocalSaveCopyAsCommand    137 /* File/Local Save Copy As
                                                command */
#define trapezeRotateFlipCommand         138 /* Image/Flip command */
#define trapezeGotoPageCommand           139 /* Page/Go To command */
#define trapezeStampsCommand             140 /* Tool/Annotation/Stamp/Stamps
                                                command */
#define trapezeCustomizeLineAnnotationCommand 141 /* Tool/Annotation/Line/
                                                     Customize command */
#define trapezeCustomizeRectAnnotationCommand 142 /* Tool/Annotation/
                                                     Rectangle/Customize
                                                     command */
#define trapezeCustomizeEllipseAnnotationCommand 143 /* Tool/Annotation/
                                                        Ellipse/Customize
                                                        command */
#define trapezeThumbnailZoom25Command    144 /* Edit/Preferences/Thumbnails/
                                                Zoom=25% */
#define trapezeThumbnailZoom50Command    145 /* Edit/Preferences/Thumbnails/
                                                Zoom=50% */
#define trapezeThumbnailZoom100Command   146 /* Edit/Preferences/Thumbnails/
                                                Zoom=100% */
#define trapezeThumbnailZoom200Command   147 /* Edit/Preferences/Thumbnails/
                                                Zoom=200% */
#define trapezeUndeletePageCommand       148 /* Page/Undelete command (DLL
                                                only) */
#define trapezeUndeletePagesCommand      149 /* Thumbnail menu/Undelete
                                                command (DLL only) */
#define trapezeAdminCommand              150 /* All administrator commands */
#define trapezeScanNewPDFCommand         151 /* Toolbar/Scan New PDF command
                                                (application only) */
#define trapezeCutCommand                152 /* Edit/Cut command */
#define trapezeDespeckleCommand          153 /* Page/Despeckle command */
#define trapezeLightTableToolCommand     154 /* Tool/Light Table command */
#define trapezeDeskewToolCommand         155 /* Tool/Deskew command */
#define trapezeAddPageNumbersCommand     156 /* Add Page Numbers command */
#define trapezeAddDateCommand            157 /* Add Date command */
#define trapezePrintPreviewCommand       158 /* File/Print Preview command */
#define trapezeCopyPagesCommand          159 /* Thumbnail menu/Copy Pages
                                                command */
#define trapezePasteAfterPageCommand     160 /* Thumbnail menu/Insert Pages
                                                After/Paste command */
#define trapezePasteBeforePageCommand    161 /* Thumbnail menu/Insert Pages
                                                Before/Paste command */
#define trapezePasteReplacePagesCommand  162 /* Thumbnail menu/Replace/Paste
                                                command */
#define trapezeCustom5Command            163 /* Custom 5 command (DLL only) */
#define trapezeCustom6Command            164 /* Custom 6 command (DLL only) */
#define trapezeCustom7Command            165 /* Custom 7 command (DLL only) */
#define trapezeCustom8Command            166 /* Custom 8 command (DLL only) */
#define trapezePasteAppendCommand        167 /* Page/Append/Paste command */
#define trapezeLineMeasurementAnnotationToolCommand 168 /* Tool/Annotation/
                                                           Line Measurement
                                                           command */
#define trapezeRecessionPlaneAnnotationToolCommand 169 /* Toolbar/Recession
                                                          Plane Annotation
                                                          Tool command */
#define trapezeHConeOfVisionAnnotationToolCommand 170 /* Toolbar/H Cone Of
                                                         Vision Annotation
                                                         Tool command */
#define trapezeCustomizeHConeOfVisionAnnotationCommand 171 /* Tool/Annotation/
                                                             H Cone Of Vision/
                                                              Customize
                                                              command */
#define trapezeOCRPageCommand            172 /* Page/OCR command */
#define trapezeOCRClipboardTextCommand   173 /* Edit/OCR/Copy Text */
#define trapezeText1Command              174 /* Text area 1 on toolbar (DLL
                                                only) */
#define trapezeText2Command              175 /* Text area 2 on toolbar (DLL
                                                only) */
#define trapezeText3Command              176 /* Text area 3 on toolbar (DLL
                                                only) */
#define trapezeText4Command              177 /* Text area 4 on toolbar (DLL
                                                only) */
#define trapezeRefreshCommand            178 /* Refresh command (Active
                                                document server only) */
#define trapezeLightTablePinCommand      179 /* Light Table popup/Set Pin
                                                Location command */
#define trapezeLightTablePinDownCommand  180 /* Light Table popup/Lock Pin
                                                Location command */
#define trapezeLightTableDragPointCommand 181 /* Light Table popup/Set Drag
                                                 Location command */
#define trapezeMagnifyZoomInCommand      182 /* Magnify popup/Zoom In
                                                command */
#define trapezeMagnifyZoomOutCommand     183 /* Magnify popup/Zoom Out
                                                command */
#define trapezeMagnifyCrosshairCommand   184 /* Magnify popup/Crosshair
                                                command */
#define trapezeScribbleEraserCommand     185 /* Scribble popup/Eraser
                                                command */
#define trapezeScribblePenCommand        186 /* Scribble popup/Pen command */
#define trapezeViewSelectedThumbnailsCommand 187 /* Thumbnail/View Selected
                                                    Thumbnails command */

/* Trapeze color */
typedef DWORD TrapezeColor, * TrapezeColorPtr;
/*
   0x00bbggrr = RGB color
   0x800000xx = GetSysColor(xx) color
*/

/* Trapeze resolution/scale information
   Resolution/scale value is (numerator / denominator) units */
typedef struct {
  DWORD denominator; /* Fraction denominator */
  DWORD numerator; /* Fraction numerator */
  WORD unit; /* Resolution/scale unit, one of *TrapezeUnit */
} TrapezeResolutionScaleRec, * TrapezeResolutionScalePtr;

/* Types for getting string property values */
#ifdef UNICODE
#define GetTrapezeStringPropertyTRec GetTrapezeStringPropertyWRec
#define GetTrapezeStringPropertyTPtr GetTrapezeStringPropertyWPtr
#else
#define GetTrapezeStringPropertyTRec GetTrapezeStringPropertyRec
#define GetTrapezeStringPropertyTPtr GetTrapezeStringPropertyPtr
#endif /*UNICODE*/

typedef struct {
  LPSTR str; /* Buffer to return string in, must be size characters */
  WORD size; /* Size of str, in characters, must be set before calling, for
                Trapeze version 0x6367 or later will be set to required size
                on return */
} GetTrapezeStringPropertyRec, * GetTrapezeStringPropertyPtr;

typedef struct {
  LPWSTR str; /* Buffer to return string in, must be size characters */
  WORD size; /* Size of str, in characters, must be set before calling, will
                be set to required size on return */
} GetTrapezeStringPropertyWRec, * GetTrapezeStringPropertyWPtr;

typedef struct {
  LPSTR str; /* Buffer to return string in, must be size characters */
  DWORD size; /* Size of str, in characters, must be set before calling, will
                 be set to required size on return */
} GetTrapezeLargeStringPropertyRec, * GetTrapezeLargeStringPropertyPtr;

/* Types for setting about box text property */
/* In all text fields below, text beginning with "mailto:" or "http://" will
   have that prefix removed and be displayed as a clickable link. */
typedef struct {
  WORD size; /* Size of this structure, in bytes */
  LPCSTR label1; /* First label, NULL = default ("Web site:") */
  LPCSTR text1; /* First field text, NULL = hide field */
  LPCSTR label2; /* Second label, NULL = default ("Email:") */
  LPCSTR text2; /* Second field text, NULL = hide field */
  LPCSTR label3; /* Third label, NULL = default ("Support:") */
  LPCSTR text3; /* Third field text, NULL = hide field */
  LPCSTR label4; /* Fourth label, NULL = hide field */
  LPCSTR text4; /* Fourth field text, NULL = hide field */
  LPCSTR label5; /* Fifth label, NULL = hide field */
  LPCSTR text5; /* Fifth field text, NULL = hide field */
} SetTrapezeAboutTextPropertyRec;
typedef const SetTrapezeAboutTextPropertyRec
  * SetTrapezeAboutTextPropertyCPtr;

/* Definitions for TRIM/TopDrawer */

#define trapezeTRIMNone      0x00000000 /* Don't use TRIM or TopDrawer */
#define trapezeTRIMTRIM      0x00000001 /* Use TRIM */
#define trapezeTRIMTopDrawer 0x00000002 /* Use TopDrawer */

/* Types for getting/setting print settings */
typedef struct {
  WORD size; /* Size of this structure, in bytes, must be set before getting
                or setting the property */
  DWORD marginUnits; /* Margin units, PSD_INHUNDREDTHSOFMILLIMETERS or
                        PSD_INTHOUSANDTHSOFINCHES */
  int pageHeight; /* y pages per piece of paper when < 0;
                     y pieces of paper per page when > 0 */
  int pageWidth; /* x pages per piece of paper when < 0;
                    x pieces of paper per page when > 0 */
  LPDEVMODE devMode; /* Printer information */
  LPDEVNAMES devNames; /* Printer identifier */
  RECT margins; /* Margins, in marginUnits */
  RECT printRect; /* Custom print area */
  WORD devModeSize; /* Size of devMode, in bytes. When getting, set to size
                       available in devMode before getting, changed to size
                       required/returned after getting. */
  WORD devNamesSize; /* Size of devNames, in bytes. When getting, set to size
                        available in devNames before getting, changed to size
                        required/returned after getting. */
  WORD style; /* Print style */
  BOOL bookmarkFolders; /* TRUE to print bookmark folder pages */
} GetTrapezePrintSettingsPropertyRec, * GetTrapezePrintSettingsPropertyPtr;
typedef const GetTrapezePrintSettingsPropertyRec
  * GetTrapezePrintSettingsPropertyCPtr;

/* Types for getting/setting tablet settings */
#define foundTrapezeTabletSetting         0x0001 /* Tablet has been found */
#define scribbleTrapezeTabletSetting      0x0002 /* Start scribble annotation
                                                    on pen down */
#define scribbleEraseTrapezeTabletSetting 0x0004 /* Start scribble erase on
                                                    eraser down */
#define noTrapezeTabletCursor ((BYTE) -1) /* Disable cursor */
typedef struct {
  WORD size; /* Size of this structure, in bytes */
  WORD flags; /* Flags from above */
  BYTE eraserCursor; /* Eraser cursor number, noTrapezeTabletCursor = none */
  BYTE penCursor; /* Pen cursor number, noTrapezeTabletCursor = none */
  BOOL disable; /* TRUE to disable tablet support */
} GetTrapezeTabletSettingsPropertyRec, * GetTrapezeTabletSettingsPropertyPtr;
typedef const GetTrapezeTabletSettingsPropertyRec
  * GetTrapezeTabletSettingsPropertyCPtr;

/* Types for getting/setting ODMA settings */
#define disableTrapezeODMASetting             0x00000001 /* Disable ODMA */
#define foundTrapezeODMASetting               0x00000002 /* ODMA has been
                                                            found on the
                                                            system */
#define insertDialogTrapezeODMASetting        0x00000004 /* Show ODMA dialog
                                                            for insert */
#define insertTextDialogTrapezeODMASetting    0x00000008 /* Show ODMA dialog
                                                            for text insert */
#define openDialogTrapezeODMASetting          0x00000010 /* Show ODMA dialog
                                                            for document
                                                            open */
#define openFileDialogTrapezeODMASetting      0x00000020 /* Use normal open
                                                            dialog on ODMA
                                                            cancel */
#define openPictureDialogTrapezeODMASetting   0x00000040 /* Show ODMA dialog
                                                            for picture
                                                            open */
#define saveFileDialogTrapezeODMASetting      0x00000080 /* Show normal save
                                                            dialog on ODMA
                                                            save cancel */
#define saveNewDialogTrapezeODMASetting       0x00000100 /* Show ODMA dialog
                                                            for document
                                                            save */
#define saveOptionsDialogTrapezeODMASetting   0x00000200 /* Show options
                                                            dialog before ODMA
                                                            save dialog */
#define saveSelectionDialogTrapezeODMASetting 0x00000400 /* Show ODMA dialog
                                                            for save
                                                            selection */
#define showErrorsTrapezeODMASetting          0x00000800 /* Show ODMA
                                                            errors */
#define showWarningsTrapezeODMASetting        0x00001000 /* Show ODMA
                                                            warnings */

typedef struct {
  WORD size; /* Size of this structure, in bytes */
  DWORD flags; /* Flags from above */
  char dms[9]; /* DMS, "" = default */
} GetTrapezeODMASettingsPropertyRec, * GetTrapezeODMASettingsPropertyPtr;
typedef const GetTrapezeODMASettingsPropertyRec
  * GetTrapezeODMASettingsPropertyCPtr;

/* ODMA dialog modes */
#define normalTrapezeODMADialogMode   0 /* Dialog shown is the same as last
                                           time */
#define odmaTrapezeODMADialogMode     1 /* ODMA dialog is shown first */
#define fileOnlyTrapezeODMADialogMode 2 /* File dialog is shown, ODMA dialog
                                           is not available */
#define odmaOnlyTrapezeODMADialogMode 3 /* ODMA dialog is shown, file dialog
                                           is not available */

/* Exemption code actions */
#define addTrapezeExemptionCodeAction    0 /* Show the add menu */
#define createTrapezeExemptionCodeAction 1 /* Create the redaction with
                                              exemption codes from buffer */
#define editTrapezeExemptionCodeAction   2 /* Show the exemption code dialog */
#define lastTrapezeExemptionCodeAction   3 /* Create the redaction with last
                                              edited exemption codes */
#define noTrapezeExemptionCodeAction     4 /* Do nothing */

/* Types for getting/setting the exemption code action */
typedef struct {
  BYTE action; /* Action, one of *TrapezeExemptionCodeAction */
  DWORD bufferSize; /* Ignored when setting, when getting specifies the size
                       of buffer, must be set before calling, will be set to
                       required size on return */
  LPSTR buffer; /* When action is createTrapezeExemptionCodeAction, is an
                   empty string terminated list of string pairs, each pair an
                   exemption code followed by a description. When getting,
                   must be bufferSize characters in size. */
} GetTrapezeExemptionCodeActionPropertyRec,
  * GetTrapezeExemptionCodeActionPropertyPtr;
typedef const GetTrapezeExemptionCodeActionPropertyRec
  * GetTrapezeExemptionCodeActionPropertyCPtr;

/* Types for getting/setting page setup */
typedef struct {
  WORD size;
  DWORD marginUnits;
  HGLOBAL devMode;
  HGLOBAL devNames;
  RECT margins;
} TrapezePageSetupRec, * TrapezePageSetupPtr;

/* Types for getting/setting fonts */
typedef struct {
  WORD size;
  BOOL italic;
  BOOL underline;
  BOOL strikeout;
  LPSTR fontName;
  WORD charSet;
  WORD fontNameSize;
  WORD height; /* Points * 100 */
  WORD weight;
} GetTrapezeFontPropertyRec, * GetTrapezeFontPropertyPtr;
typedef const GetTrapezeFontPropertyRec * GetTrapezeFontPropertyCPtr;

/* Types for replacing pages */
typedef struct {
  WORD size; /* Size of this structure, in bytes */
  DWORD pageNo; /* First page to be replaced (one based) */
  DWORD pageCount; /* Number of pages to be replaced, starting at pageNo */
  LPCSTR fileName; /* File to replace pages with; the pages are replaced with
                      all pages in this file */
} TrapezeReplacePagesRec;
typedef const TrapezeReplacePagesRec * TrapezeReplacePagesCPtr;

/* Types for save copy as */

typedef struct {
  WORD size; /* Size of this structure, in bytes */
  DWORD flags; /* A combination of page saving flags, except
                  deleteTrapezePagesAfterSave */
  LPCSTR fileName; /* File to save to */
  DWORD type; /* Format for saving, one of *TrapezeDocument, use
                 unknownTrapezeDocument to save in the current format */
} TrapezeSaveCopyAsExRec, * TrapezeSaveCopyAsExPtr;
typedef const TrapezeSaveCopyAsExRec * TrapezeSaveCopyAsExCPtr;

/* Types for saving bookmarks */
typedef struct {
  WORD size; /* Size of this structure, in bytes */
  DWORD bookmarkNo; /* Bookmark to save the page for; if it is a folder the
                       pages for all bookmarks under it will be saved. The
                       specified bookmarks must include some pages. */
  DWORD flags; /* A combination of bookmark saving flags from below */
  LPCSTR fileName; /* File to save to */
} TrapezeSaveBookmarkRec, * TrapezeSaveBookmarkPtr;

typedef struct {
  WORD size; /* Size of this structure, in bytes */
  DWORD bookmarkNo; /* Bookmark to save the page for; if it is a folder the
                       pages for all bookmarks under it will be saved. The
                       specified bookmarks must include some pages. */
  DWORD flags; /* A combination of bookmark saving flags from below */
  LPCWSTR fileName; /* File to save to */
} TrapezeSaveBookmarkWRec, * TrapezeSaveBookmarkWPtr;

/* Bookmark saving flags */
#define deleteTrapezeBookmarkAfterSave   0x00000001 /* The bookmarks and their
                                                       pages are deleted after
                                                       saving */
#define saveTrapezeBookmark              0x00000002 /* The pages are saved
                                                       with their bookmarks */
#define saveTrapezeBookmarkNoThumbnail   0x00000004 /* The pages are saved
                                                       without thumbnails */
#define saveTrapezeBookmarkNoAnnotations 0x00000008 /* The pages are saved
                                                       without annotations */
#define saveTrapezeBookmarkNoOCRText     0x00000010 /* The pages are saved
                                                       without OCR text */

/* Exemption code size modes */
/* The exemption code is sized to fit the exemption code text */
#define autoTrapezeExemptionCodeSizeMode           0
/* The exemption code is the width of its redaction, one line high, and the
   exemption code text is sized to fit the exemption code */
#define redactionWidthTrapezeExemptionCodeSizeMode 1

#define noTrapezeAnnotationSelected ((TrapezeDWORD) -2)

/* Annotation display modes */
/* Annotation is displayed normally */
#define normalTrapezeAnnotationDisplayMode 0x00
/* Annotation is mixed with what is under it, 50% each. This changes the way
   the annotation is displayed; printing and redacting are not affected. The
   annotation is only displayed this way until the current page is changed;
   mixed display mode is not saved or maintained across page changes. */
#define mixedTrapezeAnnotationDisplayMode  0x01
/* Annotation is hidden. The annotation is not displayed, printed or redacted.
   The annotation is saved as hidden in RapidRedact files, or removed during
   saving for any other file types. */
#define hiddenTrapezeAnnotationDisplayMode 0x02

/* Annotation preference blob */
typedef struct {
  DWORD blobSize; /* Size of blob, in bytes, ignored if blob is NULL. Will be
                     modified if preferences are changed in the dialog */
  HGLOBAL blob; /* Blob, NULL = default annotation. Will be modified if
                   preferences are changed in the dialog, must be freed using
                   GlobalFree() when no longer required. */
  HWND parent; /* Parent for dialog, NULL = use Trapeze window */
} TrapezeAnnotationPrefsBlobRec, * TrapezeAnnotationPrefsBlobPtr;

/* Types for saving pages */
#define trapezeSavePagesSize1 20 /* Versions 3.3 or later, includes up to
                                    fileName */
/* This structure requires Trapeze 3.3 or later */
typedef struct {
  WORD size; /* Size of this structure, in bytes */
  DWORD firstPageNo; /* First page to save (one based); if firstPageNo and
                        lastPageNo are both zero the entire document is
                        saved */
  DWORD lastPageNo; /* Last page to save (one based) */
  DWORD flags; /* A combination of page saving flags from below */
  LPCSTR fileName; /* File to save to */
  /* Anything below here requires Trapeze 6.75 or later */
  DWORD docType; /* File type to save as,
                    unknownTrapezeDocument = save in current format */
} TrapezeSavePagesRec, * TrapezeSavePagesPtr;

/* Page saving flags */
#define deleteTrapezePagesAfterSave   0x00000001 /* The pages (and any
                                                    bookmarks for them) are
                                                    deleted after saving */
#define saveTrapezePagesBookmarks     0x00000002 /* The pages are saved with
                                                    their bookmarks */
#define saveTrapezePagesNoAnnotations 0x00000004 /* The pages are saved
                                                    without annotations */
#define saveTrapezePagesNoThumbnails  0x00000008 /* The pages are saved
                                                    without thumbnails */
#define saveTrapezePagesNoOCRText     0x00000010 /* The pages are saved
                                                    without OCR text */
#define saveTrapezePagesCompress      0x00000020 /* Uncompressed pages are
                                                    saved using current
                                                    compression preferences */
/* Used with saveTrapezePagesNoAnnotations to also remove any redactions rather
   than burning them in */
#define saveTrapezePagesNoRedaction   0x00000040

/* Types for getting unit names */
typedef struct {
  LPSTR single;
  WORD singleSize;
  LPSTR plural;
  WORD pluralSize;
} TrapezeUnitStringsRec, * TrapezeUnitStringsPtr;

/* Print dialog flags */
/* Hide the "Print bookmark folders" check box */
#define trapezeHidePrintBookmarkFolders    0x00000001
/* Disable the "Print bookmark folders" check box */
#define trapezeDisablePrintBookmarkFolders 0x00000002
/* Check the "Print bookmark folders" check box */
#define trapezeCheckPrintBookmarkFolders   0x00000004
/* Disable the "Print annotations" check box */
#define trapezeDisablePrintAnnotations     0x00000008
/* Check the "Print annotations" check box */
#define trapezeCheckPrintAnnotations       0x00000010
/* Hide the "Print annotations" check box */
#define trapezeHidePrintAnnotations        0x00000020

/* Print flags */
#define printTrapezePageNums        0x00000001 /* Prints the pages between
                                                  startPage and endPage,
                                                  inclusive */
#define printTrapezeToScale         0x00000002 /* Prints to scale */
#define printTrapezeToResolution    0x00000004 /* Prints to resolution */
#define printTrapezeFitPage         0x00000008 /* Prints fitted to page */
#define printTrapezeStretchPage     0x00000010 /* Prints stretched to fill
                                                  page */
#define printTrapezeCustomFit       0x00000020 /* Prints fitted to custom
                                                  area */
#define printTrapezeCustomStretch   0x00000040 /* Prints stretched to fill
                                                  custom area */
#define printTrapezeAnnotations     0x00000080 /* Prints annotations */
#define printTrapezeBookmarkFolders 0x00000100 /* Prints bookmark folders */
#define printTrapezeMinMargins      0x00000200 /* Prints with minimum
                                                  margins */
#define printTrapezeSelectedImage   0x00000400 /* Prints the current image
                                                  selection */

/* Scanning Definitions and Types */

#define saveRestoreTrapezeScanFileKey ((HKEY) (TrapezeDWORD_PTR) 0x80001000)

#define noTrapezeScan               0x00000001L /* Don't scan, just show the
                                                   dialog */
#define noTrapezeScanDialog         0x00000002L /* Don't show the dialog, just
                                                   scan */
#define noTrapezeScanQueryFirstPage 0x00000004L /* The first page will be
                                                   scanned without asking the
                                                   user */
#define trapezeScanAutoDone         0x00000008L /* Selects "Done" every time
                                                   the "Continue Scan" dialog
                                                   would be shown */
#define trapezeScanForceJobSep      0x00000010L /* Force job separation */
#define noTrapezeScanDuplex         0x00000020L /* Disable duplex */
#define noTrapezeScanFunctionSheets 0x00000040L /* Disable function sheets */
#define noTrapezeScanMS61           0x00000080L /* Disable MS61 scanning -
                                                   only effective for the
                                                   first call to TrapezeScan()
                                                   in an application */
#define noTrapezeScanTWAIN          0x00000100L /* Disable TWAIN scanning -
                                                   only effective for the
                                                   first call to TrapezeScan()
                                                   in an application */
#define trapezeScanCallback         0x00000200L /* Use callback scanning */
#define noTrapezeScanCancel         0x00000400L /* Remove cancel button during
                                                   scanning */
#define trapezeScanDefaultSoftwareJobSep \
                                    0x00000800L /* Default to software
                                                   detected job separation */
#define noTrapezeScanBarcodes       0x00001000L /* Disable barcodes */
#define noTrapezeScanWIA            0x00002000L /* Disable WIA scanning -
                                                   only effective for the
                                                   first call to TrapezeScan()
                                                   in an application */
#define trapezeScanCallbackCanAdjustResolution \
                                    0x00004000L /* Callback can handle
                                        trapezeScanCallbackAdjustResolution */

#define trapezeScanCallbackJobSep         0x00000001 /* The page is a job
                                                        separator */
#define trapezeScanCallbackDetectJobSep   0x00000002 /* The callback function
                                                        needs to detect job
                                                        separators */
#define trapezeScanCallbackDetectBarcodes 0x00000004 /* The callback function
                                                        needs to detect
                                                        barcodes */
#define trapezeScanCallbackAdjustResolution 0x00000008 /* Adjust resolution of
                                                          previous page */

/* Scanning callback function
   Called for every page scanned or job separator detected during callback
   scanning. fileName is the path to a TIFF containing the scanned page, or
   NULL if the page is a scanner detected job separator with no image. The
   callback function is responsible for deleting fileName when it is no longer
   required. flags specifies information about the page:
   - trapezeScanCallbackJobSep indicates that the page is a scanner detected
     job separator. If fileName is NULL there is no image available, otherwise
     fileName contains the image for the job separator.
   - trapezeScanCallbackDetectJobSep indicates that the callback function
     needs to detect job separators (using FindTrapezeJobSeparator()).
   - trapezeScanCallbackDetectBarcodes indicates that the callback function
     needs to detect barcodes on job separators (using
     FindTrapezeJobSeparatorBarcodes()).
   - trapezeScanCallbackAdjustResolution indicates that the resolution for the
     previous page needs to be adjusted - lParam is a
     TrapezeScanCallbackAdjustResolutionCPtr, which contains the new
     resolution and the original lParam.
   lParam is the lParam value passed to TrapezeScan() in scanInfo. The return
   value is ERROR_SUCCESS to continue scanning, ERROR_CANCELLED to stop
   scanning (as if the user pressed Stop or the feeder is empty), 0xFFFFFFFF
   to cancel scanning (as if the user pressed Cancel), or any other error code
   to stop scanning and display an error to the user. */
typedef TrapezeDWORD (TrapezeAPI* TrapezeScanProc)(LPCSTR fileName,
                                                   TrapezeDWORD flags,
                                                   TrapezeLPVOID lParam);

typedef struct {
  TrapezeDWORD xDenominator; /* Horizontal fraction denominator */
  TrapezeDWORD xNumerator; /* Horizontal fraction numerator */
  TrapezeDWORD yDenominator; /* Vertical fraction denominator */
  TrapezeDWORD yNumerator; /* Vertical fraction numerator */
  TrapezeLPVOID lParam; /* lParam value passed to TrapezeScan() in scanInfo */
} TrapezeScanCallbackAdjustResolutionRec;
typedef const TrapezeScanCallbackAdjustResolutionRec
  * TrapezeScanCallbackAdjustResolutionCPtr;

/* Valid values for size from older versions of Trapeze */
#define trapezeScanSize1 32 /* Versions 2.1 or later, includes up to source */
#define trapezeScanSize2 36 /* Versions 4.3 or later, includes up to
                               orientation */
#define trapezeScanSize3 44 /* Versions 5.01 or later, includes up to
                               ignoreTWAINBitOrder */
#define trapezeScanSize4 52 /* Versions 5.25 or later, includes up to
                               lParam */

/* This structure requires Trapeze 2.1 or later */
typedef struct {
  WORD size; /* The size of this structure, in bytes */
  HGLOBAL settings; /* Scanner settings */
  HGLOBAL scanner; /* Scanner name */
  LPCSTR jobFolderName; /* &j will be replaced with the job number, &<char>
                           will be replaced with <char> */
  LPCSTR jobBookmarkName; /* &p will be replace with the document page number,
                             &j will be replaced with the job page number,
                             &<char> will be replace with <char> */
  WORD compression; /* Selected compression */
  WORD jobSeparation; /* Job separation mode */
  WORD scanAhead; /* Scan ahead mode */
  WORD scannerType; /* Scanner type */
  WORD side; /* Side mode */
  WORD source; /* Scan source */
  /* Anything below here requires Trapeze 4.3 or later */
  WORD functionSheet; /* Function sheet mode */
  WORD orientation; /* Orientation mode */
  /* Anything below here requires Trapeze 5.01 or later */
  BOOL checkTWAINBitOrder; /* TRUE to check bit order for TWAIN drivers */
  BOOL ignoreTWAINBitOrder; /* TRUE to use default bit order for TWAIN
                               drivers */
  /* Anything below here requires Trapeze 5.25 or later */
  TrapezeScanProc callbackProc; /* Callback function for each page,
                                   must be set when trapezeScanCallback is
                                   used */
  TrapezeLPVOID lParam; /* Parameter passed to callbackProc */
  /* Anything below here requires Trapeze 6.64 or later */
  BOOL blackZeroToWhiteZero; /* TRUE to convert black is zero monochrome
                                images to white is zero */
  BOOL closeTWAIN; /* TRUE to close the TWAIN driver between scans */
  BOOL ignoreDuplexFSBack; /* TRUE to ignore back of duplex software
                              detected function sheets */
  BOOL logTWAIN; /* TRUE to enable TWAIN logging */
  BOOL multiLevelBookmarks; /* TRUE to scan multi-level bookmarks */
  BOOL showDialog; /* TRUE to show dialog before scanning */
  BOOL showTWAINUI; /* TRUE to show TWAIN user interface when scanning */
  HGLOBAL twainLogFile; /* File to log TWAIN information to */
} TrapezeScanRec, * TrapezeScanPtr;

typedef struct TrapezeScannerStruct {
  int unused;
} * TrapezeScanner;
typedef TrapezeScanner * TrapezeScannerPtr;

/* Preferences flags */
#define trapezePrefsCompression    0x00000001 /* Compression */
#define trapezePrefsImage          0x00000002 /* Image */
#define trapezePrefsMeasure        0x00000004 /* Measuring */
#define trapezePrefsThumbnail      0x00000008 /* Thumbnails */
#define trapezePrefsExemptionCodes 0x00000010 /* Exemption codes */
#define trapezePrefsRedactionOnly  0x00000020 /* Redaction, no non-redaction
                                                 annotation types */
#define trapezePrefsParent         0x00000040 /* lParam specifies parent */
#define trapezePrefsTablet         0x00000080 /* Tablet */
#define trapezePrefsLanguage       0x00000100 /* Language */
#define trapezePrefsPrint          0x00000200 /* Printing */
#define trapezePrefsScan           0x00000400 /* Scanning */
#define trapezePrefsTRIM           0x00000800 /* TRIM */
#define trapezePrefsEx             0x00001000 /* lParam specifies a
                                                 TrapezePrefsExPtr */
#define trapezePrefsNoEllipseRedaction 0x00002000 /* No ellipse redaction
                                                     annotation type */
#define trapezePrefsODMA           0x00004000 /* ODMA */
#define trapezePrefsDespeckle      0x00008000 /* Despeckle */
#define trapezePrefsPDF            0x00010000 /* PDF */
#define trapezePrefsBookmarks      0x00020000 /* Bookmarks */

/* Preferences structures */
typedef struct {
  WORD size; /* Size of this structure, in bytes */
  HWND parent; /* Parent, if trapezePrefsParent is set in flags, otherwise the
                  Trapeze window is used */
  TrapezeScanPtr scanInfo; /* If the scanning pane is shown, specifies
                              scanning preferences to change, or NULL to
                              change the Trapeze window scanning
                              preferences. */
} TrapezePrefsExRec, * TrapezePrefsExPtr;

/* Types for opening a document */
typedef struct {
  WORD size; /* Size of this structure, in bytes */
  LPCSTR fileName; /* File to open, if NULL or empty ("") the open dialog is
                      displayed and readOnly is ignored; ERROR_CANCELLED will
                      be returned if the open dialog is cancelled. */
  BOOL readOnly; /* If TRUE the document is opened readOnly */
  DWORD pageNo; /* Initial page (one based) to open in the document, zero
                   opens the document with no page open */
} TrapezeOpenDocumentExRec, * TrapezeOpenDocumentExPtr;

/* Types for setting the overlay image */
typedef struct {
  WORD size;
  LPSTR fileName;
  LPBYTE data;
  TrapezeSIZE_T dataSize;
} TrapezeOverlayImageRec, * TrapezeOverlayImagePtr;

/* Flags for setTrapezeMessageBarTextMsg */
#define noShowTrapezeMessageBar 0x01 /* Don't show the message bar */

#define trapezeStampAdminUnicode 0x0001 /* fileName is a Unicode string */

/* Types for stamp administrator */
typedef struct {
  WORD size; /* Size of this structure, in bytes */
  WORD flags; /* Flags indicating which fields below are valid, a combination
                 of trapezeStampAdmin* values */
  HWND parent; /* Parent window for the dialog, NULL = no parent */
  LPCSTR fileName; /* Ini file to edit (will be created if it doesn't
                      exist) */
} TrapezeStampAdminRec, * TrapezeStampAdminPtr;
typedef const TrapezeStampAdminRec * TrapezeStampAdminCPtr;

/* Types for bookmark folder page ranges */
typedef struct {
  DWORD startPage; /* First page in range (one based) */
  DWORD endPage;   /* Last page in range (one based) */
} TrapezeBookmarkFolderPageRangeRec, * TrapezeBookmarkFolderPageRangePtr;
typedef const TrapezeBookmarkFolderPageRangeRec
  * TrapezeBookmarkFolderPageRangeCPtr;

/* Types for setting bookmark links */
typedef struct {
  LPCSTR menuItemText; /* Bookmark context menu item text */
  LPCSTR link; /* Link */
} SetTrapezeBookmarkLinkRec;
typedef const SetTrapezeBookmarkLinkRec * SetTrapezeBookmarkLinkCPtr;

/* Types for setting bookmark bitmaps */
/* Each bitmap is a 128 x 16 pixel bitmap with the specified number of colors,
   containing 8 16 x 16 pixel icons in the following order: closed folder,
   open folder, document page, locked closed folder, locked open folder,
   read-only closed folder, read-only open folder, read-only document page.
   The color with RGB value (255, 0, 255) in these bitmaps is not drawn.
   The 16 color bitmap is used for displays with <= 256 colors, the 256 color
   bitmap is used for displays with > 256 colors when themes are disabled, and
   the 24 bit bitmap is used for displays with > 256 colors when themes are
   enabled. The default bitmap will only be used if all bitmaps are set to
   NULL, otherwise the best bitmap available for the display will be used. */
typedef struct {
  WORD size; /* Size of this structure, in bytes */
  const BITMAPINFO* bitmap16; /* 16 color bitmap, NULL = use default */
  const BYTE* data16; /* 16 color bitmap data, NULL = use default */
  const BITMAPINFO* bitmap256; /* 256 color bitmap, NULL = use default */
  const BYTE* data256; /* 256 color bitmap data, NULL = use default */
  const BITMAPINFO* bitmap24; /* 24 bit color bitmap, NULL = use default */
  const BYTE* data24; /* 24 bit color bitmap data, NULL = use default */
} SetTrapezeBookmarkBitmapsRec;
typedef const SetTrapezeBookmarkBitmapsRec * SetTrapezeBookmarkBitmapsCPtr;

/* Types for getting bookmark links */
typedef struct {
  LPSTR menuItemText; /* Bookmark context menu item text */
  LPSTR link; /* Link */
  WORD menuItemTextSize; /* Size of menuItemText */
  WORD linkSize; /* Size of link */
} GetTrapezeBookmarkLinkRec, * GetTrapezeBookmarkLinkPtr;

/* Types for getting bookmark text */
typedef struct {
  BOOL description; /* TRUE to get description, FALSE to get name */
  LPSTR text; /* Name or description */
  WORD textSize; /* Size of text, in characters */
} GetTrapezeBookmarkTextRec, * GetTrapezeBookmarkTextPtr;

typedef struct {
  BOOL description; /* TRUE to get description, FALSE to get name */
  LPWSTR text; /* Name or description */
  WORD textSize; /* Size of text, in characters */
} GetTrapezeBookmarkTextWRec, * GetTrapezeBookmarkTextWPtr;

/* Information for getOCRTextTrapezeMsg and setOCRTextTrapezeMsg */
typedef struct {
  DWORD ocrTextSize; /* Size of ocrText, in bytes, ignored if ocrText is
                        NULL */
  HGLOBAL ocrText; /* OCR text data, NULL = no OCR text, must be destroyed
                      using GlobalFree() when it is no longer required */
} TrapezeOCRTextDataRec, * TrapezeOCRTextDataPtr;
typedef const TrapezeOCRTextDataRec * TrapezeOCRTextDataCPtr;

/* Information for appendTrapezeDIBMsg */
#define appendTrapezeDIBOCRText    0x0001 /* OCR text is valid */
#define appendTrapezeDIBDestroyDIB 0x0002 /* Destroy dib using GlobalFree()
                                             when done */
#define appendTrapezeDIBDestroyOCRText 0x0004 /* Destroy ocrText using
                                                 GlobalFree() when done */

typedef struct {
  WORD size; /* Size of this structure, in bytes */
  WORD flags; /* Flags indicating which fields below are valid, a combination
                 of appendTrapezeDIB* */
  HGLOBAL dib; /* DIB to append */
  HGLOBAL ocrText; /* OCR text to append, NULL = no OCR text, ignored if
                      appendTrapezeDIBOCRText is not set in flags */
  DWORD ocrTextSize; /* Size of ocrText, in bytes, ignored if
                        appendTrapezeDIBOCRText is not set in flags or ocrText
                        is NULL */
} AppendTrapezeDIBRec, * AppendTrapezeDIBPtr;
typedef const AppendTrapezeDIBRec * AppendTrapezeDIBCPtr;

/* Information for appendTrapezeDocumentExMsg */
/* All pages are compressed using compression preferences, even if already
   compressed. If this flag is not set, only uncompressed pages are compressed
   using compression preferences. */
#define appendTrapezeDocumentExForceCompression 0x0001
/* The append cannot be undone - note that in this case the append may clear
   the undo queue */
#define appendTrapezeDocumentExNoUndo           0x0002

typedef struct {
  WORD size; /* Size of this structure, in bytes */
  WORD flags; /* Flags indicating which fields below are valid, a combination
                 of appendTrapezeDocumentEx* values */
  LPCSTR fileName; /* File to append, all pages are appended */
} AppendTrapezeDocumentExRec, * AppendTrapezeDocumentExPtr;
typedef const AppendTrapezeDocumentExRec * AppendTrapezeDocumentExCPtr;

/* Types for saving the current page */
typedef struct {
  WORD size; /* Size of this structure, in bytes */
  WORD docType; /* File type to save as, one of bmpTrapezeDocument,
                   jpegTrapezeDocument or tiffTrapezeDocument, or
                   unknownTrapezeDocument = save in current format. */
  DWORD flags; /* A combination of page saving flags from TrapezeSavePagesRec
                  above, *TrapezePages* */
  LPCSTR fileName; /* File to save to */
  LPCVOID typeInfo; /* Information based on docType, NULL = use defaults.
                       If docType is:
                       bmpTrapezeDocument - must be a TrapezeBMPOptionsPtr
                       jpegTrapezeDocument - must be a TrapezeJPEGOptionsPtr
                       tiffTrapezeDocument - must be NULL
                       unknownTrapezeDocument - must be NULL */
} TrapezeSaveCurrentPageAsRec, * TrapezeSaveCurrentPageAsPtr;
typedef const TrapezeSaveCurrentPageAsRec * TrapezeSaveCurrentPageAsCPtr;

/* Types for setting annotation properties */
typedef struct {
  DWORD annotationNo; /* Annotation number */
  LPCSTR property; /* Name of property to set */
#ifdef NOTRAPEZEVARIANT
  LPCVOID value;
#else
  const VARIANT* value; /* New property value, type depends on property */
#endif /*NOTRAPEZEVARIANT*/
} SetTrapezeAnnotationPropertyRec, * SetTrapezeAnnotationPropertyPtr;
typedef const SetTrapezeAnnotationPropertyRec
  * SetTrapezeAnnotationPropertyCPtr;

/* Trapeze window panes */
#define trapezeBookmarkPane  0 /* Bookmark pane */
#define trapezeImagePane     1 /* Image pane */
#define trapezeThumbnailPane 2 /* Thumbnail pane */
#define trapezeTrapezePane   3 /* Trapeze window */

#define selectedTrapezeAnnotationNo ((DWORD) -1)

#define deleteTrapezeAnnotationAllPages ((DWORD) -1)

/* Flags for selectTrapezeAnnotationMsg */
#define selectTrapezeAnnotationSelect   0 /* Annotation is selected */
#define selectTrapezeAnnotationDeselect 0x01 /* Annotation is deselected */
#define selectTrapezeAnnotationExtend   0x02 /* Current selection is
                                                extended */
#define selectTrapezeAnnotationScroll   0x04 /* Annotation is scrolled into
                                                view */

#define trapezeMsgError   0 /* Probably means an incorrect message number */
#define trapezeMsgSuccess ((LRESULT) 0xFFFFFFFF) /* The message succeeded */

/* Trapeze window messages - all return an error code on failure or
   trapezeMsgSuccess on success. On failure (return value !=
   trapezeMsgSuccess) use GetTrapezeErrorMessage() to get the error message
   for the error. */
#define trapezeMsg (WM_USER)

/* Adds a new thumbnail tab containing the thumbnails for the document
   specified by fileName. tabName specifies the name of the tab. If fileName
   is NULL, tabName specifies the name of the tab for the original document
   thumbnails. If fileName is empty (""), the open dialog is displayed for the
   document to add. If tabName is NULL and fileName is not NULL, the name of
   the tab is set based on the document opened. */
#define addThumbnailTabTrapezeMsg    (trapezeMsg + 152)
/* wParam = (WPARAM) (LPCSTR) tabName
   lParam = (LPARAM) (LPCSTR) fileName
*/

/* Adds the annotation specified by annotationNo on the current page to the
   specified group. */
#define addTrapezeAnnotationGroupMsg (trapezeMsg + 153)
/* wParam = (WPARAM) (DWORD) annotationNo
   lParam = (LPARAM) (LPCSTR) group
*/

/* Shows the preference dialog for the annotation type specified by command.
   command must be one of trapezeArrowAnnotationToolCommand,
   trapezeFilledRectAnnotationToolCommand,
   trapezeHighlighterAnnotationToolCommand,
   trapezeHollowRectAnnotationToolCommand, trapezeLineAnnotationToolCommand,
   trapezeRectAnnotationToolCommand, trapezeStampAnnotationToolCommand,
   trapezeStickyNoteAnnotationToolCommand,
   trapezeStraightLineAnnotationToolCommand, trapezeTextAnnotationToolCommand,
   trapezeEllipseAnnotationToolCommand,
   trapezeFilledEllipseAnnotationToolCommand,
   trapezeHollowEllipseAnnotationToolCommand,
   trapezeEllipseRedactionToolCommand, trapezeRectRedactionToolCommand,
   trapezeLineMeasurementAnnotationToolCommand. If lParam is not zero, it
   specifies the parent for the preference dialog. */
#define annotationPrefsTrapezeMsg    (trapezeMsg + 99)
/* wParam = (WPARAM) (WORD) command
   lParam = (LPARAM) 0 or (LPARAM) (HWND) parent
*/

/* Shows the preference dialog for the annotation type specified by command
   with its data stored in blob. command must be
   trapezeRectRedactionToolCommand or trapezePictureAnnotationToolCommand. */
#define annotationPrefsBlobTrapezeMsg (trapezeMsg + 149)
/* wParam = (WPARAM) (WORD) command
   lParam = (LPARAM) (TrapezeAnnotationPrefsBlobPtr) blob
*/

/* Changes the annotation specified by annotationNo on the current page
   to/from a redaction. */
#define annotationRedactionTrapezeMsg (trapezeMsg + 82)
/* wParam = (WPARAM) (DWORD) annotationNo
   lParam = (LPARAM) (BOOL) redaction
*/

/* Displays the text dialog for the annotation specified by annotationNo on
   the current page. If the annotation type doesn't support text,
   ERROR_INVALID_PARAMETER is returned. annotationNo can be
   selectedTrapezeAnnotationNo to display the text dialog for the selected
   annotation; ERROR_INVALID_PARAMETER is returned if none or multiple
   annotations are selected. */
#define annotationTextPropertiesTrapezeMsg (trapezeMsg + 97)
/* wParam = (WPARAM) (DWORD) annotationNo
   lParam = 0
*/

/* Appends a DIB to the current document, according to information in
   appendInfo. */
#define appendTrapezeDIBMsg          (trapezeMsg + 165)
/* wParam = (WPARAM) 0
   lParam = (LPARAM) (AppendTrapezeDIBCPtr) appendInfo
*/

/* Appends all pages in the file specified by fileName to the current
   document. If forceCompression is TRUE, all pages are compressed using
   compression preferences, even if already compressed. If forceCompression
   is FALSE, only uncompressed pages are compressed using compression
   preferences. */
#define appendTrapezeDocumentMsg     (trapezeMsg + 40)
/* wParam = (WPARAM) (BOOL) forceCompression
   lParam = (LPARAM) (LPCSTR) fileName
*/

/* Appends a document to the current document. appendInfo specifies which
   document to append and how to append it. */
#define appendTrapezeDocumentExMsg   (trapezeMsg + 126)
/* wParam = (WPARAM) 0
   lParam = (LPARAM) (AppendTrapezeDocumentExCPtr) appendInfo
*/

/* Sets the check state of a command. command must be one of
   trapezeCustom*Command. If check is non-zero, command is checked, otherwise
   command is unchecked. */
#define checkTrapezeCommandMsg       (trapezeMsg + 80)
/* wParam = (WPARAM) (WORD) command
   lParam = (LPARAM) (BOOL) check
*/

/* Clears the selection for (deselects) the thumbnails from firstPage
   (one-based) through lastPage (one-based) in the current document. lastPage
   can be larger than the number of pages in the document; in this case the
   selection will be cleared through to the last page. */
#define clearTrapezeThumbnailSelectionMsg (trapezeMsg + 44)
/* wParam = (WPARAM) (DWORD) firstPage
   lParam = (LPARAM) (DWORD) lastPage
*/

/* Closes the current document. If update is TRUE, the user is asked if any
   changes need to be saved, otherwise any changes are lost. If update is TRUE
   and the user cancels the dialog, the return value is ERROR_CANCELLED. */
#define closeTrapezeDocumentMsg      (trapezeMsg + 3)
/* wParam = (WPARAM) (BOOL) update
   lParam = (LPARAM) 0
*/

/* Shows a custom annotation menu at the location specified by pos in screen
   coordinates. command specifies the command to display the menu for; it must
   be one of trapezeLineAnnotationToolCommand,
   trapezeRectAnnotationToolCommand or trapezeEllipseAnnotationToolCommand. */
#define customAnnotationMenuTrapezeMsg (trapezeMsg + 98)
/* wParam = (WPARAM) (WORD) command
   lParam = (LPARAM) (LPCPOINT) pos
*/

/* Deletes all annotations in the group specified by group from the current
   page (which also deletes the annotation group), if pageNo is zero. If
   pageNo is non-zero, the annotations are deleted from the page specified in
   pageNo, or all pages if pageNo is deleteTrapezeAnnotationAllPages. */
#define deleteTrapezeAnnotationGroupMsg (trapezeMsg + 89)
/* wParam = (WPARAM) (DWORD) pageNo
   lParam = (LPARAM) (LPCSTR) group
*/

/* Deletes the annotation specified by annotationNo from the current page.
   Following the delete, any existing annotation numbers must be assumed to be
   invalid. */
#define deleteTrapezeAnnotationMsg   (trapezeMsg + 26)
/* wParam = (WPARAM) (DWORD) annotationNo
   lParam = (LPARAM) 0
*/

/* Deletes the bookmark specified by bookmarkNo from the current document.
   Following the delete, any existing bookmark numbers must be assumed to be
   invalid. */
#define deleteTrapezeBookmarkMsg     (trapezeMsg + 23)
/* wParam = (WPARAM) (DWORD) bookmarkNo
   lParam = (LPARAM) 0
*/

/* Deletes pages from the current document, between firstPage and lastPage,
   inclusive. If successful and the enableNotifyTrapezeProperty property is
   TRUE, the documentCommandTrapezeNotify notification will be sent when the
   command is complete.
   If successful and the extendedPageNotifyTrapezeProperty property is TRUE,
   the deletePagesTrapezeNotify notification will also be sent when the
   command is complete.
   Any bookmarks for the deleted pages are also deleted. */
#define deleteTrapezePagesMsg        (trapezeMsg + 22)
/* wParam = (WPARAM) (DWORD) firstPage
   lParam = (LPARAM) (DWORD) lastPage
*/

/* Destroys find information returned by findOCRTextInfoTrapezeMsg. */
#define destroyOCRTextInfoTrapezeMsg (trapezeMsg + 68)
/* wParam = (WPARAM) 0
   lParam = (LPARAM) (TrapezeOCRTextInfo) findInfo
*/

/* Disables a command for the remainder of the life of the Trapeze window.
   command specifies the command to disable; currently it must be
   trapezeHelpTopicsCommand, trapezeViewToolbarCommand,
   trapezePreferencesCommand, trapezeInsertBeforeCommand,
   trapezeInsertAfterCommand, trapezeAppendCommand, trapezeSaveCommand,
   trapezeCloseCommand, trapezeOCRBookmarkCommand, trapezeReplaceCommand,
   trapezeSaveCopyAsCommand, trapezeSelectToolCommand,
   trapezeSaveSelectionCommand, trapezeSavePageCommand,
   trapezeMovePageCommand, trapezeMovePagesCommand, trapezeRotatePageCommand,
   trapezeRotatePagesCommand, trapezeSendCommand, trapezePrintCommand,
   trapezePrintSelectionCommand, trapezePrintPageCommand,
   trapezePrintPagesCommand, trapezeReplacePagesCommand,
   trapezeInsertBeforePageCommand, trapezeInsertAfterPageCommand,
   trapezeSendMenuCommand, trapezeAnnotationsCommand, trapezeRedactionCommand,
   trapezeMeasureCommand, trapezeOCRCommand, trapezeAboutCommand (if
   licensed), trapezeViewPixelInfoCommand, trapezeViewMagnifierCommand,
   trapezeViewOverviewCommand, trapezeMagnifyToolCommand,
   trapezeDropPagesCommand, trapezeDeleteThumbnailsCommand,
   trapezeExemptionCodeCommand, trapezeSendSelectionCommand,
   trapezeCustom1Command, trapezeCustom2Command, trapezeCustom3Command,
   trapezeCustom4Command, trapezeScribbleRedactionToolCommand,
   trapezeCustom5Command, trapezeCustom6Command, trapezeCustom7Command,
   trapezeCustom8Command, trapezeFindCommand, trapezeFindAgainCommand,
   trapezeFindAgainReverseCommand or trapezeDeletePagesCommand. */
#define disableTrapezeCommandMsg     (trapezeMsg + 34)
/* wParam = (WPARAM) (WORD) command
   lParam = (LPARAM) 0
*/

/* Enables a command that has been disabled using disableTrapezeCommandMsg.
   This message cannot enable a command that has been removed using
   removeTrapezeCommandMsg. */
#define enableTrapezeCommandMsg      (trapezeMsg + 87)
/* wParam = (WPARAM) (WORD) command
   lParam = (LPARAM) 0
*/

/* Shows an exemption code menu at the location specified by pos in screen
   coordinates. exemptionCodes->line is ignored.
   exemptionCodes->exemptionCodes is overwritten; any existing value will not
   be freed, any returned value must be freed using GlobalFree() when it is no
   longer required. On success, it will contain a single pair of strings
   terminated by an empty string; on failure, it will remain unchanged.
   exemptionCodes->parent is used as the parent for the menu and any dialogs.
   ERROR_CANCELLED is returned if the user cancels the menu. */
#define exemptionCodeMenuTrapezeMsg  (trapezeMsg + 114)
/* wParam = (WPARAM) (LPCPOINT) pos
   lParam = (LPARAM) (TrapezeExemptionCodesPtr) exemptionCodes
*/

/* Displays the options dialog for the exemption code group specified in
   exemptionCodeGroupName. If exemptionCodeGroupName is an empty string, the
   options dialog for a new exemption code group is displayed. */
#define exemptionCodeOptionsTrapezeMsg (trapezeMsg + 101)
/* wParam = (WPARAM) 0
   lParam = (LPARAM) (LPCSTR) exemptionCodeGroupName
*/

/* Displays an exemption codes dialog, allowing an exemption code list to be
   edited. */
#define exemptionCodesDialogTrapezeMsg (trapezeMsg + 105)
/* wParam = (WPARAM) 0
   lParam = (LPARAM) (TrapezeExemptionCodesPtr) exemptionCodes
*/

/* Expands or collapses the bookmark specified by bookmarkNo in the current
   document. bookmarkNo must specify a folder. bookmarkNo can be
   expandAllTrapezeBookmarks to expand or collapse all bookmarks. If expand is
   TRUE the bookmark is expanded, if FALSE it is collapsed. */
#define expandTrapezeBookmarkMsg    (trapezeMsg + 125)
/* wParam = (WPARAM) (DWORD) bookmarkNo
   lParam = (LPARAM) (BOOL) expand
*/

/* Finds barcodes in the current document. This message is identical to
   FindTrapezeBarcodes(), with the following exceptions:
   - Parameters are passed in the structure pointed to by find, with the same
     names as the function parameters. find->size must be set to the size of
     the structure before passing it.
   - The entire image is searched if rect is empty rather than NULL.
   - pageNo is used instead of dibData and dib, to specify the page to search.
   - The found barcodes are returned in find->barcodes.
   - The message returns an error code, so the special handling for no
     barcodes found is not required.
   See FindTrapezeBarcodes() for parameter details.
   */
#define findBarcodesTrapezeMsg       (trapezeMsg + 43)
/* wParam = (WPARAM) 0
   lParam = (LPARAM) (TrapezeFindBarcodesPtr) find
*/

/* Find highlights OCR text on the current page, according to information in
   findInfo. If findInfo is NULL, any highlighting is removed. */
#define findHighlightTrapezeMsg      (trapezeMsg + 174)
/* wParam = (WPARAM) 0
   lParam = (LPARAM) (TrapezeFindHighlightCPtr) findInfo
*/

/* Finds OCR text in the current document, according to information in
   findInfo. */
#define findOCRTextExTrapezeMsg      (trapezeMsg + 170)
/* wParam = (WPARAM) 0
   lParam = (LPARAM) (FindTrapezeOCRTextExCPtr) findInfo
*/

/* Finds all occurrences of text in the current document, returning data that
   can be queried on what was found. findWhat specifies what to find and how.
   findInfo is set to NULL if the text is not found in the document, or on
   error. findInfo must be destroyed using destroyOCRTextInfoTrapezeMsg when
   it is no longer required. */
#define findOCRTextInfoExTrapezeMsg  (trapezeMsg + 100)
/* wParam = (WPARAM) (TrapezeFindOCRTextInfoExCPtr) findWhat
   lParam = (LPARAM) (TrapezeOCRTextInfoPtr) findInfo
*/

/* Finds all occurrences of word in the current document, returning data that
   can be queried on what was found. findInfo is set to NULL if word is not
   found in the document, or on error. findInfo must be destroyed using
   destroyOCRTextInfoTrapezeMsg when it is no longer required. */
#define findOCRTextInfoTrapezeMsg    (trapezeMsg + 67)
/* wParam = (WPARAM) (LPCSTR) word
   lParam = (LPARAM) (TrapezeOCRTextInfoPtr) findInfo
*/

/* Finds OCR text in the current document. If pages is NULL, all pages are
   searched starting at the current page. If pages is not NULL, it is a zero
   terminated list of one-based page numbers to search, and each page in pages
   will be included in search hits even if none of the words can be found on
   the page. If pages is not NULL, the pages are searched in ascending order
   of page number, starting at the lowest numbered page. words is an empty
   string terminated list of words to search for. Any of these words results
   in a search hit. The search is not case-sensitive. This message with pages
   set to NULL and words set to a single word is the same as the Edit/Find
   command with "Find in" set to "OCR text (all pages)", "Find what" set to
   the word in words, "Match whole word only" on, "Match case" off, and
   "Direction" set to "Down". */
#define findOCRTextTrapezeMsg        (trapezeMsg + 51)
/* wParam = (WPARAM) (const DWORD*) pages
   lParam = (LPARAM) (LPCSTR) words
*/

/* Generates all missing thumbnails in the document, returning when they have
   all been generated. */
#define generateMissingThumbnailsTrapezeMsg (trapezeMsg + 118)
/* wParam = (WPARAM) 0
   lParam = (LPARAM) 0
*/

/* Gets a copy of the annotation data for the current page. The annotations
   are returned in annotations and their size is returned in size. On success,
   annotations is set to NULL if there are no annotations. The returned
   annotations must be freed using DestroyTrapezeAnnotations() when they are
   no longer required. */
#define getAnnotationsTrapezeMsg     (trapezeMsg + 115)
/* wParam = (WPARAM) (LPDWORD) size
   lParam = (LPARAM) (TrapezeAnnotationsPtr) annotations
*/

/* Gets the name of the exemption code group specified by
   exemptionCodeGroupNo (0 <= exemptionCodeGroupNo <
   exemptionCodeGroupCountTrapezeProperty). */
#define getExemptionCodeGroupNameTrapezeMsg (trapezeMsg + 102)
/* wParam = (WPARAM) (DWORD) exemptionCodeGroupNo
   lParam = (LPARAM) (GetTrapezeStringPropertyPtr) exemptionCodeGroupName
*/

/* Gets the next bookmark at the same level under the same parent as the
   bookmark specified by bookmarkNo in the current document. The number of the
   next bookmark is returned in nextNo, which is set to noNextTrapezeBookmark
   if there are no more bookmarks at the same level under the same parent. The
   first bookmark is bookmark number zero. */
#define getNextTrapezeBookmarkMsg    (trapezeMsg + 49)
/* wParam = (WPARAM) (DWORD) bookmarkNo
   lParam = (LPARAM) (LPDWORD) nextNo
*/

/* Gets the number of OCR text blocks on the current page. */
#define getOCRTextCountTrapezeMsg    (trapezeMsg + 74)
/* wParam = (WPARAM) 0
   lParam = (LPARAM) (LPDWORD) count
*/

/* Gets the number of occurrences of the word found by
   findOCRTextInfoTrapezeMsg. */
#define getOCRTextInfoCountTrapezeMsg (trapezeMsg + 69)
/* wParam = (WPARAM) (TrapezeOCRTextInfo) findInfo
   lParam = (LPARAM) (LPDWORD) count
*/

/* Gets the locations for an occurrence of the word found by
   findOCRTextInfoTrapezeMsg. findInfo, occurrence and rects must be filled in
   ocrTextInfo before sending this message. This message only works when the
   current page is the page for the specified occurrence of the word,
   otherwise callDisabledTrapezeError is returned. */
#define getOCRTextInfoRectsTrapezeMsg  (trapezeMsg + 72)
/* wParam = (WPARAM) 0
   lParam = (LPARAM) (GetTrapezeOCRTextInfoRectsPtr) ocrTextInfo
*/

/* Gets information about an occurrence of the word found by
   findOCRTextInfoTrapezeMsg. findInfo and occurrence must be filled in
   ocrTextInfo before sending this message. */
#define getOCRTextInfoTrapezeMsg     (trapezeMsg + 70)
/* wParam = (WPARAM) 0
   lParam = (LPARAM) (GetTrapezeOCRTextInfoPtr) ocrTextInfo
*/

/* Gets one of the locations for OCR text characters on the current page.
   charNo, ocrTextNo and rectNo must be filled in rectInfo before sending this
   message. */
#define getOCRTextRectTrapezeMsg     (trapezeMsg + 163)
/* wParam = (WPARAM) 0
   lParam = (LPARAM) (GetTrapezeOCRTextRectPtr) rectInfo
*/

/* Gets the locations for OCR text characters on the current page. charNo,
   ocrTextNo, count and rects must be filled in rects before sending this
   message. */
#define getOCRTextRectsTrapezeMsg    (trapezeMsg + 76)
/* wParam = (WPARAM) 0
   lParam = (LPARAM) (GetTrapezeOCRTextRectsPtr) rects
*/

/* Gets the text of an OCR text block on the current page. ocrTextNo, text
   and possibly textSize must be filled in before sending this message. */
#define getOCRTextTextTrapezeMsg     (trapezeMsg + 75)
/* wParam = (WPARAM) 0
   lParam = (LPARAM) (GetTrapezeOCRTextTextPtr) text
*/

/* Gets the OCR text for the page specified by pageNo in the current
   document. The OCR text and its size are returned in ocrText */
#define getOCRTextTrapezeMsg         (trapezeMsg + 166)
/* wParam = (WPARAM) (DWORD) pageNo
   lParam = (LPARAM) (TrapezeOCRTextDataPtr) ocrText
*/

/* Gets the creation time (GMT) for the annotation specified by annotationNo
   on the current page. If the annotation doesn't have a creation time,
   ERROR_CANCELLED is returned. */
#define getTrapezeAnnotationCreateTimeMsg (trapezeMsg + 107)
/* wParam = (WPARAM) (DWORD) annotationNo
   lParam = (LPARAM) (LPSYSTEMTIME) createTime
*/

/* Gets a copy of the DIB for the annotation specified by annotationNo on the
   current page. If the annotation type doesn't support pictures, dib is set
   to NULL. dib must be destroyed using GlobalFree() when it is no longer
   required. The returned DIB is a packed DIB. */
#define getTrapezeAnnotationDIBMsg (trapezeMsg + 85)
/* wParam = (WPARAM) (DWORD) annotationNo
   lParam = (LPARAM) (HGLOBAL*) dib
*/

/* Gets the display background color for the annotation specified by
   annotationNo on the current page. displayBackColor can be one of:
   0x00bbggrr = RGB color
   0x01bbggrr = transparent RGB color
   0xFFFFFFFF = no display background color (i.e. normal background)
   If the annotation type doesn't support a display background color,
   displayBackColor is set to 0xFFFFFFFF. */
#define getTrapezeAnnotationDisplayBackColorMsg (trapezeMsg + 112)
/* wParam = (WPARAM) (DWORD) annotationNo
   lParam = (LPARAM) (LPDWORD) displayBackColor
*/

/* Gets the numbers of the exemption code annotations associated with the
   annotation specified by annotationNo on the current page. exemptionCodes is
   a pointer to two DWORD values; the first is set to the number of the
   exemption code annotation, or to 0xFFFFFFFF if the annotation doesn't have
   an exemption code. The second DWORD value is set to the number of the
   exemption code line annotation, or to 0xFFFFFFFF if the annotation doesn't
   have an exemption code, or doesn't have an exemption code line. */
#define getTrapezeAnnotationExemptionCodesMsg (trapezeMsg + 109)
/* wParam = (WPARAM) (DWORD) annotationNo
   lParam = (LPARAM) (LPDWORD) exemptionCodes
*/

/* Gets the exemption codes for the annotation specified by annotationNo on
   the current page. The annotation must be a redaction.
   exemptionCodes->parent is ignored. exemptionCodes->exemptionCodes is
   overwritten; any existing value will not be freed, any returned value must
   be freed using GlobalFree() when it is no longer required.
   exemptionCodes->line is set to TRUE if the annotation has an exemption code
   line, or FALSE if not. */
#define getTrapezeAnnotationExemptionCodeStringsMsg (trapezeMsg + 111)
/* wParam = (WPARAM) (DWORD) annotationNo
   lParam = (LPARAM) (TrapezeExemptionCodesPtr) exemptionCodes
*/

/* Gets the name of an annotation group in the current page, based on the
   index in groupNo (0 <= groupNo < annotationGroupCountTrapezeProperty). */
#define getTrapezeAnnotationGroupMsg (trapezeMsg + 90)
/* wParam = (WPARAM) (DWORD) groupNo
   lParam = (LPARAM) (GetTrapezeStringPropertyPtr) group
*/

/* Gets the groups an annotation is in. annotationNo specifies the annotation
   on the current page. */
#define getTrapezeAnnotationGroupsMsg (trapezeMsg + 93)
/* wParam = (WPARAM) (DWORD) annotationNo
   lParam = (LPARAM) (GetTrapezeAnnotationGroupsPtr) groups
*/

/* Gets the modify time (GMT) for the annotation specified by annotationNo on
   the current page. If the annotation doesn't have a modify time,
   ERROR_CANCELLED is returned. */
#define getTrapezeAnnotationModifyTimeMsg (trapezeMsg + 108)
/* wParam = (WPARAM) (DWORD) annotationNo
   lParam = (LPARAM) (LPSYSTEMTIME) modifyTime
*/

/* Gets the bounding rectangle of the annotation specified by annotationNo on
   the current page. For annotation types not located by a rectangle
   (e.g. lines) the returned rectangle is the smallest rectangle containing
   all points of the annotation. */
#define getTrapezeAnnotationRectMsg (trapezeMsg + 96)
/* wParam = (WPARAM) (DWORD) annotationNo
   lParam = (LPARAM) (LPRECT) rect
*/

/* Gets the text for the annotation specified by annotationNo on the current
   page (can only handle text <= 64K in size). If the annotation type doesn't
   support text, an empty string is returned. */
#define getTrapezeAnnotationTextMsg (trapezeMsg + 79)
/* wParam = (WPARAM) (DWORD) annotationNo
   lParam = (LPARAM) (GetTrapezeStringPropertyPtr) text
*/

/* Gets the text for the annotation specified by annotationNo on the current
   page (can handle text > 64K in size). If the annotation type doesn't
   support text, an empty string is returned. */
#define getTrapezeAnnotationTextLargeMsg (trapezeMsg + 127)
/* wParam = (WPARAM) (DWORD) annotationNo
   lParam = (LPARAM) (GetTrapezeLargeStringPropertyPtr) text
*/

/* Gets the type of the annotation specified by annotationNo on the current
   page. The annotation type is one of *TrapezeAnnotationType. */
#define getTrapezeAnnotationTypeMsg (trapezeMsg + 110)
/* wParam = (WPARAM) (DWORD) annotationNo
   lParam = (LPARAM) (LPBYTE) annotationType
*/

/* Gets the first child of the bookmark specified by bookmarkNo in the current
   document. bookmarkNo must specify a folder. The number of the first child
   bookmark is returned in childNo, which is set to noTrapezeBookmarkChild if
   the bookmark has no children. bookmarkNo can be noTrapezeBookmarkParent to
   get the number of the first bookmark. The first bookmark is bookmark number
   zero. */
#define getTrapezeBookmarkChildMsg   (trapezeMsg + 48)
/* wParam = (WPARAM) (DWORD) bookmarkNo
   lParam = (LPARAM) (LPDWORD) childNo
*/

/* Gets the allowed page range for a bookmark folder. bookmarkNo must specify
   a bookmark folder, otherwise ERROR_INVALID_PARAMETER is returned. The
   default page range for a folder is 0-FFFFFFFF. */
#define getTrapezeBookmarkFolderPageRangeMsg (trapezeMsg + 28)
/* wParam = (WPARAM) (DWORD) bookmarkNo
   lParam = (LPARAM) (TrapezeBookmarkFolderPageRangePtr) pageRange
*/

/* Gets the link for a bookmark. bookmarkNo must specify a valid bookmark,
   otherwise ERROR_INVALID_PARAMETER is returned. All members of link must be
   initialized before calling this function. If either menuItemText or link
   are too small to hold the appropriate string, ERROR_INSUFFICIENT_BUFFER is
   returned and menuItemTextSize and linkSize are both set to the required
   size. If the bookmark has no link menuItemTextSize and linkSize will be
   changed to zero and menuItemText and link won't be changed. */
#define getTrapezeBookmarkLinkMsg    (trapezeMsg + 31)
/* wParam = (WPARAM) (DWORD) bookmarkNo
   lParam = (LPARAM) (GetTrapezeBookmarkLinkPtr) link
*/

/* Gets the page number for the bookmark specified by bookmarkNo in the
   current document. The page number is returned in pageNo, which is set to
   trapezeBookmarkFolder if the bookmark is a folder. */
#define getTrapezeBookmarkPageNoMsg  (trapezeMsg + 19)
/* wParam = (WPARAM) (DWORD) bookmarkNo
   lParam = (LPARAM) (LPDWORD) pageNo
*/

/* Gets the parent of the bookmark specified by bookmarkNo in the current
   document. The number of the parent bookmark is returned in parentNo, which
   is set to noTrapezeBookmarkParent if the bookmark has no parent (i.e. is at
   root level). */
#define getTrapezeBookmarkParentMsg  (trapezeMsg + 24)
/* wParam = (WPARAM) (DWORD) bookmarkNo
   lParam = (LPARAM) (LPDWORD) parentNo
*/

/* Gets the read-only state for a bookmark. bookmarkNo must specify a valid
   bookmark, otherwise ERROR_INVALID_PARAMETER is returned. */
#define getTrapezeBookmarkReadOnlyMsg (trapezeMsg + 33)
/* wParam = (WPARAM) (DWORD) bookmarkNo
   lParam = (LPARAM) (LPBOOL) readOnly
*/

/* Gets the name or description of a bookmark. bookmarkNo must specify a valid
   bookmark, otherwise ERROR_INVALID_PARAMETER is returned. All members of
   text must be initialized before calling this function. If text->description
   is FALSE, the text returned is the bookmark name, otherwise the bookmark
   description is returned. If text is too small to hold the appropriate
   string, ERROR_INSUFFICIENT_BUFFER is returned and textSize is set to the
   required size. */
#define getTrapezeBookmarkTextMsg    (trapezeMsg + 60)
/* wParam = (WPARAM) (DWORD) bookmarkNo
   lParam = (LPARAM) (GetTrapezeBookmarkTextPtr) text
*/

/* Gets the name or description of a bookmark. This is the wide string version
   of getTrapezeBookmarkTextMsg */
#define getTrapezeBookmarkTextWMsg   (trapezeMsg + 61)
/* wParam = (WPARAM) (DWORD) bookmarkNo
   lParam = (LPARAM) (GetTrapezeBookmarkTextWPtr) text
*/

/* Gets the DIB for the current page. If copy is TRUE, *dib is set to a copy
   of the DIB, which must be destroyed using GlobalFree() when it is no longer
   required. copy is FALSE is no longer supported, and will return
   callDisabledTrapezeError. The returned DIB is a packed DIB, and looks the
   same as the on-screen image (i.e. rotation/inversion/redaction has been
   applied). */
#define getTrapezeDIBMsg             (trapezeMsg + 10)
/* wParam = (WPARAM) (BOOL) copy
   lParam = (LPARAM) (HGLOBAL*) dib
*/

/* Gets the DIB for the specified rectangle of the current page. *dib is set
   to a copy of the DIB, which must be destroyed using GlobalFree() when it is
   no longer required. The returned DIB is a packed DIB, and is extracted from
   the on-screen image after rotation/inversion/redaction has been applied.
   Use dibSizeTrapezeProperty to find the maximum bounds of rect. */
#define getTrapezeDIBRectMsg         (trapezeMsg + 119)
/* wParam = (WPARAM) (LPCRECT) rect
   lParam = (LPARAM) (HGLOBAL*) dib
*/

/* Gets the value of a preference. pref specifies the name of the preference
   to get. The value is returned in value; its type depends on the
   preference. */
#define getTrapezePrefValueMsg       (trapezeMsg + 160)
/* wParam = (WPARAM) (LPCSTR) pref
   lParam = (LPARAM) (VARIANT*) value
*/

/* Gets the value of a property. property specifies the property to get the
   value from. The value is returned in value; its type depends on the
   property. */
#define getTrapezePropertyMsg        (trapezeMsg + 2)
/* wParam = (WPARAM) (WORD) property
   lParam = (LPARAM) value
*/

#define getTrapezeUnitStringsMsg     (trapezeMsg + 9)
/* wParam = (WPARAM) (WORD) unit
   lParam = (LPARAM) (TrapezeUnitStringsPtr) strings
*/

/* Determines if a page or document has annotations or redactions. If pageNo
   is zero, the current document is checked; if pageNo is ((DWORD) -1) the
   current page is checked, otherwise pageNo specifies the one based page
   number to check. Sets hasAnnotations to TRUE if the page or document has
   annotations or redactions, or FALSE if it doesn't. */
#define hasAnnotationsTrapezeMsg     (trapezeMsg + 168)
/* wParam = (WPARAM) (DWORD) pageNo
   lParam = (LPARAM) (LPBOOL) hasAnnotations
*/

/* Determines if a page or document has redactions. If pageNo is zero, the
   current document is checked; if pageNo is ((DWORD) -1) the current page is
   checked, otherwise pageNo specifies the one based page number to check.
   Sets hasRedactions to TRUE if the page or document has redactions, or
   FALSE if it doesn't. */
#define hasRedactionsTrapezeMsg      (trapezeMsg + 169)
/* wParam = (WPARAM) (DWORD) pageNo
   lParam = (LPARAM) (LPBOOL) hasRedactions
*/

/* Converts an image rectangle into a screen rectangle. If rect is empty, it
   is set to the screen coordinates of the image pane. */
#define imageToScreenRectTrapezeMsg  (trapezeMsg + 164)
/* wParam = (WPARAM) 0
   lParam = (LPARAM) (LPRECT) rect
*/

/* Inserts a new annotation onto the current page. annotationType is one of
   insert*TrapezeAnnotation. insertInfo depends on annotationType. */
#define insertTrapezeAnnotationMsg   (trapezeMsg + 27)
/* wParam = (WPARAM) annotationType
   lParam = (LPARAM) (LPCVOID) insertInfo
*/

/* Inserts all pages in the file specified by fileName at page pageNo in the
   current document. pageNo is a one-based page number. The first page from
   fileName becomes page pageNo; any existing pages at or above pageNo are
   moved up by the number of pages inserted. pageNo must be between one and
   (number of pages in the current document plus one), inclusive. */
#define insertTrapezeDocumentMsg     (trapezeMsg + 42)
/* wParam = (WPARAM) (DWORD) pageNo
   lParam = (LPARAM) (LPCSTR) fileName
*/

/* Sets selected to TRUE if the annotation specified by annotationNo in the
   current page is selected. Sets selected to FALSE if it is not selected. */
#define isTrapezeAnnotationSelectedMsg (trapezeMsg + 77)
/* wParam = (WPARAM) (DWORD) annotationNo
   lParam = (LPARAM) (LPBOOL) selected
*/

/* Sets selected to TRUE if the bookmark specified by bookmarkNo in the
   current document is selected. Sets selected to FALSE if it is not
   selected. */
#define isTrapezeBookmarkSelectedMsg (trapezeMsg + 47)
/* wParam = (WPARAM) (DWORD) bookmarkNo
   lParam = (LPARAM) (LPBOOL) selected
*/

/* Sets redactDeleted to TRUE if the page specified by pageNo (one based) is
   marked as deleted by redaction, or FALSE if not. If pageNo is ((DWORD) -1),
   the current page is checked. */
#define isTrapezePageRedactDeletedMsg (trapezeMsg + 161)
/* wParam = (WPARAM) (DWORD) pageNo
   lParam = (LPARAM) (LPBOOL) redactDeleted
*/

/* Checks if Trapeze is configured to use TRIM and/or TopDrawer. configured is
   set to trapezeTRIMNone if Trapeze is not configured to use TRIM or
   TopDrawer, otherwise trapezeTRIMTRIM if Trapeze is configured to use TRIM,
   and trapezeTRIMTopDrawer is set if Trapeze is configured to use TopDrawer. */
#define isTrapezeTRIMConfiguredMsg (trapezeMsg + 54)
/* wParam = (WPARAM) 0
   lParam = (LPARAM) (LPDWORD) configured
*/

#define measureTrapezeMsg            (trapezeMsg + 8)
/* wParam = (WPARAM) (WORD) measure
   lParam = (LPARAM) 0
*/

/* Creates a new PDF document */
#define newPDFTrapezeMsg             (trapezeMsg + 162)
/* wParam = (WPARAM) 0
   lParam = (LPARAM) (LPCSTR) title
*/

#ifdef UNICODE
#define newTIFFTTrapezeMsg newTIFFWTrapezeMsg
#else
#define newTIFFTTrapezeMsg newTIFFTrapezeMsg
#endif /*UNICODE*/

/* Creates a new TIFF document */
#define newTIFFTrapezeMsg            (trapezeMsg + 37)
/* wParam = (WPARAM) 0
   lParam = (LPARAM) (LPCSTR) title
*/

/* Creates a new TIFF document (Unicode version). See newTIFFTrapezeMsg for
   details */
#define newTIFFWTrapezeMsg           (trapezeMsg + 148)
/* wParam = (WPARAM) 0
   lParam = (LPARAM) (LPCWSTR) title
*/

/* Changes a command to notify instead of performing its normal function,
   using the commandTrapezeNotify notification code. command specifies the
   command to change; currently it must be trapezeNextPageCommand,
   trapezePrevPageCommand, trapezeHelpTopicsCommand, trapezeAboutCommand (if
   licensed), trapezeSendSelectionCommand, trapezePrintPagesCommand,
   trapezeZoomInCommand, trapezeZoomOutCommand,
   trapezeToolbarMenuCommand, trapezeFindCommand, trapezeFindAgainCommand or
   trapezeFindAgainReverseCommand. */
#define notifyTrapezeCommandMsg      (trapezeMsg + 52)
/* wParam = (WPARAM) (WORD) command
   lParam = (LPARAM) 0
*/

/* Opens all thumbnails in the document, returning when they have all been
   opened. */
#define openAllThumbnailsTrapezeMsg  (trapezeMsg + 117)
/* wParam = (WPARAM) 0
   lParam = (LPARAM) 0
*/

#ifdef UNICODE
#define openTrapezeDocumentTMsg openTrapezeDocumentWMsg
#else
#define openTrapezeDocumentTMsg openTrapezeDocumentMsg
#endif /*UNICODE*/

/* Opens a document. If readOnly is TRUE the document is opened readOnly.
   fileName specifies the file containing the document to open. If fileName is
   NULL or empty ("") the open dialog is displayed and readOnly is ignored;
   ERROR_CANCELLED will be returned if the open dialog is cancelled. */
#define openTrapezeDocumentMsg       (trapezeMsg)
/* wParam = (WPARAM) (BOOL) readOnly
   lParam = (LPARAM) (LPCSTR) fileName
*/

/* Opens a document (Unicode version). See openTrapezeDocumentMsg for
   details. */
#define openTrapezeDocumentWMsg      (trapezeMsg + 122)
/* wParam = (WPARAM) (BOOL) readOnly
   lParam = (LPARAM) (LPCWSTR) fileName
*/

/* Opens a document. openInfo specifies which document to open and how to open
   it. */
#define openTrapezeDocumentExMsg     (trapezeMsg + 18)
/* wParam = (WPARAM) 0
   lParam = (LPARAM) (TrapezeOpenDocumentExPtr) openInfo
*/

/* Opens a document from an IStream interface. The document is opened
   read-only. stream->AddRef() is called immediately, and stream->Release() is
   called when the document is closed. */
#define openTrapezeDocumentIStreamMsg (trapezeMsg + 155)
/* wParam = (WPARAM) 0
   lParam = (LPARAM) (LPSTREAM) stream
*/

#define openTrapezeMemoryDocumentMsg (trapezeMsg + 11)
/* wParam = (WPARAM) size
   lParam = (LPARAM) data
*/

/* Shows the Page Setup dialog */
#define pageSetupTrapezeMsg          (trapezeMsg + 6)
/* wParam = (WPARAM) 0
   lParam = (LPARAM) 0
*/

/* Shows the preferences. This differs from running trapezePreferencesCommand
   because only the preference panes specified by flags are shown. flags is a
   combination of trapezePrefs* values. If trapezePrefsParent is set in flags,
   parent specifies the parent for the preferences dialog, otherwise the
   Trapeze window is used. If trapezePrefsEx is set in flags, prefsEx
   specifies preference dialog information. */
#define preferencesTrapezeMsg        (trapezeMsg + 58)
/* wParam = (WPARAM) flags
   lParam = (LPARAM) 0 or (LPARAM) (HWND) parent or
            (LPARAM) (TrapezePrefsExPtr) prefsEx
*/

/* Displays the Print dialog to allow the user to print the document. flags
   can be a combination of trapeze*Print* values under print dialog flags
   above. */
#define printDialogTrapezeMsg        (trapezeMsg + 13)
/* wParam = (WPARAM) flags
   lParam = (LPARAM) 0
*/

/* Prints the document without showing a Print dialog, based on the
   information in printInfo. */
#define printTrapezeMsg              (trapezeMsg + 7)
/* wParam = (WPARAM) 0
   lParam = (LPARAM) (TrapezePrintPtr) printInfo
*/

/* Protects OCR text characters on the current page. If protectInfo is NULL
   all characters are changed to unprotected. */
#define protectOCRTextTrapezeMsg     (trapezeMsg + 151)
/* wParam = (WPARAM) 0
   lParam = (LPARAM) (ProtectTrapezeOCRTextCPtr) protectInfo
*/

/* Changes the current TIFF into a RapidRedact document, or the current
   RapidRedact document into a TIFF. */
#define rapidRedactTrapezeMsg        (trapezeMsg + 83)
/* wParam = 0
   lParam = 0
*/

/* Reloads exemption codes from the registry or ini file. */
#define reloadTrapezeExemptionCodesMsg (trapezeMsg + 103)
/* wParam = 0
   lParam = 0
*/

/* Reloads stamps from the registry or ini file. If fileName is not NULL, the
   stamps are loaded from that ini file. If fileName is NULL, customStamps is
   ignored, otherwise if customStamps is FALSE, the stamps are only loaded
   from that ini file, not the registry, and are all read-only; if
   customStamps is TRUE, stamps are also loaded from the registry if allowed
   by the ini file. */
#define reloadTrapezeStampsMsg       (trapezeMsg + 95)
/* wParam = (WPARAM) (BOOL) customStamps
   lParam = (LPARAM) (LPCSTR) fileName
*/

/* Reloads stamps according to the information in reloadInfo. */
#define reloadTrapezeStampsExMsg     (trapezeMsg + 158)
/* wParam = 0
   lParam = (LPARAM) (ReloadTrapezeStampsExCPtr) reloadInfo
*/

/* Removes a command for the remainder of the life of the Trapeze window.
   command specifies the command to remove; currently it must be
   trapezeHelpTopicsCommand, trapezeViewToolbarCommand,
   trapezePreferencesCommand, trapezeInsertBeforeCommand,
   trapezeInsertAfterCommand, trapezeAppendCommand, trapezeSaveCommand,
   trapezeCloseCommand, trapezeOCRBookmarkCommand, trapezeReplaceCommand,
   trapezeSaveCopyAsCommand, trapezeSelectToolCommand,
   trapezeSaveSelectionCommand, trapezeSavePageCommand,
   trapezeMovePageCommand, trapezeMovePagesCommand, trapezeRotatePageCommand,
   trapezeRotatePagesCommand, trapezeSendCommand, trapezePrintCommand,
   trapezePrintSelectionCommand, trapezePrintPageCommand,
   trapezePrintPagesCommand, trapezeReplacePagesCommand,
   trapezeInsertBeforePageCommand, trapezeInsertAfterPageCommand,
   trapezeSendMenuCommand, trapezeAnnotationsCommand, trapezeRedactionCommand,
   trapezeMeasureCommand, trapezeOCRCommand, trapezeAboutCommand (if
   licensed), trapezeViewPixelInfoCommand, trapezeViewMagnifierCommand,
   trapezeViewOverviewCommand, trapezeMagnifyToolCommand,
   trapezeExemptionCodeCommand, trapezeSendSelectionCommand,
   trapezeCustom1Command, trapezeCustom2Command, trapezeCustom3Command,
   trapezeCustom4Command, trapezeScribbleRedactionToolCommand,
   trapezeAdminCommand, trapezeCustom5Command, trapezeCustom6Command,
   trapezeCustom7Command, trapezeCustom8Command, trapezeDespeckleCommand or
   trapezeImageToolCommand. */
#define removeTrapezeCommandMsg      (trapezeMsg + 35)
/* wParam = (WPARAM) (WORD) command
   lParam = (LPARAM) 0
*/

/* Sets the text for trapezeText1Command, trapezeText2Command,
   trapezeText3Command or trapezeText4Command. */
#define setTrapezeCommandTextMsg     (trapezeMsg + 171)
/* wParam = (WPARAM) (WORD) command
   lParam = (LPARAM) (LPCSTR) text
*/

/* Replaces OCR text characters on the current page */
#define replaceOCRTextTextTrapezeMsg (trapezeMsg + 150)
/* wParam = (WPARAM) 0
   lParam = (LPARAM) (ReplaceTrapezeOCRTextTextCPtr) replaceInfo
*/

/* Replaces pages in the current document, based on information in
   replaceInfo. */
#define replaceTrapezePagesMsg       (trapezeMsg + 86)
/* wParam = (WPARAM) 0
   lParam = (LPARAM) (TrapezeReplacePagesCPtr) replaceInfo
*/

/* Saves a copy of the current document to fileName */
#define saveCopyAsTrapezeMsg         (trapezeMsg + 120)
/* wParam = (WPARAM) 0
   lParam = (LPARAM) (LPCSTR) fileName
*/

/* Saves a copy of the current document, according to the information in
   saveInfo. */
#define saveCopyAsExTrapezeMsg       (trapezeMsg + 156)
/* wParam = (WPARAM) 0
   lParam = (LPARAM) (TrapezeSaveCopyAsExCPtr) saveInfo
*/

/* Saves a copy of the current document to fileName. type specifies the format
   to use for saving, one of *TrapezeDocument. Use unknownTrapezeDocument to
   save in the current format. imageTypeTrapezeError is returned if type is a
   single page format and the document is multi-page. */
#define saveCopyAsTypeTrapezeMsg     (trapezeMsg + 121)
/* wParam = (WPARAM) (DWORD) type
   lParam = (LPARAM) (LPCSTR) fileName
*/

/* Saves the current document to fileName, and changes the current document to
   the new file */
#define saveAsTrapezeMsg             (trapezeMsg + 36)
/* wParam = (WPARAM) 0
   lParam = (LPARAM) (LPCSTR) fileName
*/

/* Saves the current document into TRIM or TopDrawer. If topDrawer is FALSE,
   the document is saved into TRIM, and ERROR_INVALID_PARAMETER is returned if
   Trapeze is not configured to use TRIM, or callDisabledTrapezeError is
   returned if TRIM is not available. If topDrawer is TRUE, the document
   is saved into TopDrawer, and ERROR_INVALID_PARAMETER is returned if Trapeze
   is not configured to use TopDrawer, or callDisabledTrapezeError is returned
   if TopDrawer is not available. ERROR_CANCELLED is returned if the user
   cancels the Save As. */
#define saveAsTrapezeTRIMMsg         (trapezeMsg + 56)
/* wParam = (WPARAM) (BOOL) topDrawer
   lParam = (LPARAM) 0
*/

/* Saves a copy of the current page of the current document. saveInfo
   specifies how the page is saved. If saveInfo is NULL the Save Copy As
   dialog is displayed. */
#define saveCurrentPageAsTrapezeMsg  (trapezeMsg + 38)
/* wParam = (WPARAM) 0
   lParam = (LPARAM) (TrapezeSaveCurrentPageAsCPtr) saveInfo
*/

/* Saves the currently selected bookmarks to fileName. flags is a combination
   of saveTrapeze* bookmark saving flags. If no bookmarks are selected,
   ERROR_NOT_FOUND is returned. */
#define saveSelectedTrapezeBookmarksMsg (trapezeMsg + 62)
/* wParam = (WPARAM) (DWORD) flags
   lParam = (LPARAM) (LPCSTR) fileName
*/

/* Saves the current image selection to fileName. If there is no image
   selection, the current page is saved. If there is no image for the current
   page, noImageTrapezeError is returned. type is a *TrapezeDocument value,
   indicating the format to save in, using current compression preferences. */
#define saveSelectedTrapezeImageMsg  (trapezeMsg + 84)
/* wParam = (WPARAM) (DWORD) type
   lParam = (LPARAM) (LPCSTR) fileName
*/

/* Saves the currently selected thumbnails to fileName. flags is a combination
   of saveTrapeze* page saving flags. If no thumbnails are selected,
   ERROR_NOT_FOUND is returned. */
#define saveSelectedTrapezeThumbnailsMsg (trapezeMsg + 157)
/* wParam = (WPARAM) (DWORD) flags
   lParam = (LPARAM) (LPCSTR) fileName
*/

/* Saves the pages for a bookmark, according to the information in
   saveInfo. */
#define saveTrapezeBookmarkMsg       (trapezeMsg + 20)
/* wParam = (WPARAM) 0
   lParam = (LPARAM) (TrapezeSaveBookmarkPtr) saveInfo
*/

/* Saves the pages for a bookmark, according to the information in saveInfo.
   This is the wide string version of saveTrapezeBookmarkMsg. */
#define saveTrapezeBookmarkWMsg      (trapezeMsg + 53)
/* wParam = (WPARAM) 0
   lParam = (LPARAM) (TrapezeSaveBookmarkWPtr) saveInfo
*/

/* Saves a range of pages from the current document, into a file of the same
   type as the current document */
#define saveTrapezePagesMsg          (trapezeMsg + 25)
/* wParam = (WPARAM) 0
   lParam = (LPARAM) (TrapezeSavePagesPtr) saveInfo
*/

/* Changes the position of a scroll bar. scrollBar specifies the scroll bar to
   change; it must be one of *TrapezeScrollBar. scrollCode specifies how to
   change the scroll bar; it must be one of scrollTrapeze*. */
#define scrollTrapezeMsg (trapezeMsg + 5)
/* wParam = (WPARAM) (WORD) scrollBar
   lParam = (LPARAM) (WORD) scrollCode
*/

/* Selects an occurrence of the word found by findOCRTextInfoTrapezeMsg.
   occurrence specifies the occurrence to select. If findInfo is NULL,
   occurrence is ignored and any selection is cleared. */
#define selectOCRTextInfoTrapezeMsg  (trapezeMsg + 71)
/* wParam = (WPARAM) (DWORD) occurrence
   lParam = (LPARAM) (TrapezeOCRTextInfo) findInfo
*/

/* Selects or deselects an annotation on the current page. flags is a
   combination of selectTrapezeAnnotation* values, specifying whether the
   annotation is selected or deselected, whether the current selection is
   extended, and whether the annotation is scrolled into view. */
#define selectTrapezeAnnotationMsg   (trapezeMsg + 172)
/* wParam = (WPARAM) (DWORD) annotationNo
   lParam = (LPARAM) (DWORD) flags
*/

/* Selects or deselects all annotations in the group specified by group on
   the current page. If groups is NULL, all annotations on the current page
   are selected/deselected. select specifies whether the annotations are
   selected or deselected. If extend is TRUE, the current selection is
   extended by the changes, otherwise all annotations are deselected first. */
#define selectTrapezeAnnotationGroupMsg (trapezeMsg + 91)
/* wParam = (WPARAM) MAKEWPARAM(select, extend)
   lParam = (LPARAM) (LPCSTR) group
*/

/* Selects a bookmark, ensures it is visible and gives it the focus,
   deselecting all other bookmarks. bookmarkNo must specify a valid bookmark,
   otherwise ERROR_INVALID_PARAMETER is returned. If open is TRUE, the page
   for the bookmark (or the first page in a bookmark folder) is opened. */
#define selectTrapezeBookmarkMsg (trapezeMsg + 21)
/* wParam = (WPARAM) (DWORD) bookmarkNo
   lParam = (LPARAM) (BOOL) open
*/

/* Replaces the annotations for the current page with those specified in
   annotations. size specifies the size of the data in annotations. If
   annotations is NULL or size is zero, all annotations are removed from the
   current page. */
#define setAnnotationsTrapezeMsg     (trapezeMsg + 116)
/* wParam = (WPARAM) (DWORD) size
   lParam = (LPARAM) (TrapezeAnnotations) annotations
*/

/* Sets the OCR text for the page specified by pageNo in the current
   document, replacing any existing OCR text for the page. The OCR text and
   its size are specified in ocrText. If ocrText is NULL, pageNo is ignored
   and OCR text is removed from all pages. */
#define setOCRTextTrapezeMsg         (trapezeMsg + 167)
/* wParam = (WPARAM) (DWORD) pageNo
   lParam = (LPARAM) (TrapezeOCRTextDataCPtr) ocrText
*/

/* Sets the display background color for the annotation specified by
   annotationNo on the current page. displayBackColor can be one of:
   0x00bbggrr = RGB color
   0x01bbggrr = transparent RGB color
   0xFFFFFFFF = no display background color (i.e. normal background)
   If the annotation type doesn't support a display background color,
   ERROR_INVALID_PARAMETER is returned. */
#define setTrapezeAnnotationDisplayBackColorMsg (trapezeMsg + 113)
/* wParam = (WPARAM) (DWORD) annotationNo
   lParam = (LPARAM) (DWORD) displayBackColor
*/

/* Changes the display mode for the annotation specified by annotationNo on
   the current page. mode is a combination of *TrapezeAnnotationDisplayMode
   values. */
#define setTrapezeAnnotationDisplayModeMsg (trapezeMsg + 73)
/* wParam = (WPARAM) (DWORD) annotationNo
   lParam = (LPARAM) (BYTE) mode
*/

/* Sets the exemption codes for the annotation specified by annotationNo on
   the current page. The annotation must be a redaction.
   exemptionCodes->parent is ignored. Any existing exemption codes for the
   annotation are replaced. */
#define setTrapezeAnnotationExemptionCodesMsg (trapezeMsg + 106)
/* wParam = (WPARAM) (DWORD) annotationNo
   lParam = (LPARAM) (TrapezeExemptionCodesCPtr) exemptionCodes
*/

/* Sets the groups belonged to by the annotation specified by annotationNo on
   the current page. groups specifies the groups for the annotation to belong
   to, or NULL for the default group ("[Untitled]"). Annotation groups are
   case-sensitive. */
#define setTrapezeAnnotationGroupsMsg (trapezeMsg + 88)
/* wParam = (WPARAM) (DWORD) annotationNo
   lParam = (LPARAM) (SetTrapezeAnnotationGroupsCPtr) groups
*/

/* Sets the text for the annotation specified by annotationNo on the current
   page, and changes the text style for the annotation to hidden. text
   specifies the text for the annotation. */
#define setTrapezeAnnotationHiddenTextMsg (trapezeMsg + 78)
/* wParam = (WPARAM) (DWORD) annotationNo
   lParam = (LPARAM) (LPCSTR) text
*/

/* Sets a property of an annotation */
#define setTrapezeAnnotationPropertyMsg (trapezeMsg + 123)
/* wParam = (WPARAM) 0
   lParam = (LPARAM) (SetTrapezeAnnotationPropertyCPtr) setInfo
*/

/* Sets the bitmap for a bookmark. bookmarkNo must specify a valid bookmark,
   otherwise ERROR_INVALID_PARAMETER is returned. bitmapNo specifies a bitmap
   set using setTrapezeBookmarkBitmapsMsg. If bitmapNo is set to
   defaultTrapezeBookmarkBitmapNo or an invalid bitmap number, the default
   bitmap is used. */
#define setTrapezeBookmarkBitmapMsg (trapezeMsg + 63)
/* wParam = (WPARAM) (DWORD) bookmarkNo
   lParam = (LPARAM) (DWORD) bitmapNo
*/

/* Sets the bitmaps available for bookmarks. bitmapNo specifies the bitmap
   number to set; this number can be used with setTrapezeBookmarkBitmapMsg to
   use the bitmap for a bookmark. The first use of this message must use zero
   for bitmapNo, and each subsequent call can only use a maximum of one larger
   than the previous largest value (i.e. there can't be gaps in the bitmaps
   set). This message takes a copy of the bitmaps, so the bitmaps passed in
   can be destroyed after using this message. If bitmapNo specifies a bitmap
   that has previously been set, the specified bitmaps replace the current
   ones. */
#define setTrapezeBookmarkBitmapsMsg (trapezeMsg + 64)
/* wParam = (WPARAM) (DWORD) bitmapNo
   lParam = (LPARAM) (SetTrapezeBookmarkBitmapsCPtr) bitmaps
*/

/* Sets the allowed page range for a bookmark folder. bookmarkNo must specify
   a bookmark folder, otherwise ERROR_INVALID_PARAMETER is returned. The
   default page range for a folder is 0-FFFFFFFF. */
#define setTrapezeBookmarkFolderPageRangeMsg (trapezeMsg + 29)
/* wParam = (WPARAM) (DWORD) bookmarkNo
   lParam = (LPARAM) (TrapezeBookmarkFolderPageRangeCPtr) pageRange
*/

/* Sets the link for a bookmark. bookmarkNo must specify a valid bookmark,
   otherwise ERROR_INVALID_PARAMETER is returned. Set link to NULL or
   either of menuItemText or link to NULL or an empty string to remove the
   link for a bookmark. */
#define setTrapezeBookmarkLinkMsg (trapezeMsg + 30)
/* wParam = (WPARAM) (DWORD) bookmarkNo
   lParam = (LPARAM) (SetTrapezeBookmarkLinkCPtr) link
*/

/* Sets the name of a bookmark. bookmarkNo must specify a valid bookmark,
   otherwise ERROR_INVALID_PARAMETER is returned. */
#define setTrapezeBookmarkNameMsg (trapezeMsg + 59)
/* wParam = (WPARAM) (DWORD) bookmarkNo
   lParam = (LPARAM) (LPCSTR) name
*/

/* Sets the read-only state for a bookmark. bookmarkNo must specify a valid
   bookmark, otherwise ERROR_INVALID_PARAMETER is returned. */
#define setTrapezeBookmarkReadOnlyMsg (trapezeMsg + 32)
/* wParam = (WPARAM) (DWORD) bookmarkNo
   lParam = (LPARAM) (BOOL) readOnly
*/

/* Sets the bookmarks for the current document, replacing any existing
   bookmarks, and marking the bookmarks as changed. If size is zero,
   bookmarks is a TrapezeBookmarks value, otherwise size specifies the size
   and bookmarks is an HGLOBAL containing file storage format bookmark
   data. */
#define setTrapezeBookmarksChangedMsg (trapezeMsg + 50)
/* wParam = (WPARAM) (DWORD) size
   lParam = (LPARAM) bookmarks
*/

/* Sets the bookmarks for the current document, replacing any existing
   bookmarks. If size is zero, bookmarks is a TrapezeBookmarks value,
   otherwise size specifies the size and bookmarks is an HGLOBAL containing
   file storage format bookmark data. */
#define setTrapezeBookmarksMsg       (trapezeMsg + 41)
/* wParam = (WPARAM) (DWORD) size
   lParam = (LPARAM) bookmarks
*/

/* Sets the focus to the specified Trapeze pane, which must be one of
   trapezeBookmarkPane, trapezeImagePane or trapezeThumbnailPane. */
#define setTrapezeFocusMsg           (trapezeMsg + 104)
/* wParam = (WPARAM) pane
   lParam = (LPARAM) 0
*/

/* Sets the message bar text for a trapeze window. If text is NULL the message
   bar is removed. flags is a combination of *TrapezeMessageBar values */
#define setTrapezeMessageBarTextMsg  (trapezeMsg + 173)
/* wParam = (WPARAM) (DWORD) flags
   lParam = (LPARAM) (LPCSTR) text
*/

#define setTrapezeOverlayImageMsg    (trapezeMsg + 12)
/* wParam = (WPARAM) 0
   lParam = (LPARAM) (TrapezeOverlayImagePtr) overlayInfo
*/

/* Sets the value of a preference. pref specifies the name of the preference
   to set. value specifies the new value of the preference; its type depends
   on the preference.
   Special values supported here are:
   Annotations\Rect Redaction
     Used to set all rectangle redaction preferences at once; the value is a
     blob (VT_BLOB) or an array of bytes (VT_UI1 | VT_ARRAY). This blob can be
     created using the data from annotationPrefsBlobTrapezeMsg with command =
     trapezeRectRedactionToolCommand.
 */
#define setTrapezePrefValueMsg       (trapezeMsg + 159)
/* wParam = (WPARAM) (LPCSTR) pref
   lParam = (LPARAM) (const VARIANT*) value
*/

/* Sets the value of a property. property specifies the property to set. value
   specifies the new value of property; its type depends on the property. */
#define setTrapezePropertyMsg        (trapezeMsg + 1)
/* wParam = (WPARAM) (WORD) property
   lParam = (LPARAM) value
*/

/* Sets the status bar text for a trapeze window. If text is NULL the status
   bar text is cleared. */
#define setTrapezeStatusBarTextMsg   (trapezeMsg + 16)
/* wParam = (WPARAM) 0
   lParam = (LPARAM) (LPCSTR) text
*/

/* Sets the focused thumbnail in the current document to the thumbnail
   specified by pageNo (one-based). If scroll is TRUE, the thumbnail will be
   scrolled into view if necessary. */
#define setTrapezeThumbnailFocusMsg  (trapezeMsg + 46)
/* wParam = (WPARAM) (BOOL) scroll
   lParam = (LPARAM) (DWORD) pageNo
*/

/* Sets the selection for (selects) the thumbnails from firstPage (one-based)
   through lastPage (one-based) in the current document. lastPage can be
   larger than the number of pages in the document; in this case the selection
   will be set through to the last page. */
#define setTrapezeThumbnailSelectionMsg (trapezeMsg + 45)
/* wParam = (WPARAM) (DWORD) firstPage
   lParam = (LPARAM) (DWORD) lastPage
*/

/* Shows or hides all annotations in the group specified by group. If group is
   NULL, all annotations are shown or hidden. */
#define showTrapezeAnnotationGroupMsg (trapezeMsg + 92)
/* wParam = (WPARAM) (BOOL) show
   lParam = (LPARAM) (LPCSTR) group
*/

/* Shows or hides a toolbar tool. command specifies the tool to show or hide;
   currently it must be trapezeMagnifyToolCommand. show is TRUE to show the
   tool or FALSE to hide it. */
#define showTrapezeToolbarToolMsg    (trapezeMsg + 39)
/* wParam = (WPARAM) (WORD) command
   lParam = (LPARAM) (BOOL) show
*/

/* Shows the stamp administration dialog. fileName specifies the ini file to
   edit. */
#define stampAdminTrapezeMsg         (trapezeMsg + 154)
/* wParam = (WPARAM) 0
   lParam = (LPARAM) (LPCSTR) fileName
*/

/* Shows the stamps menu at location (x, y) in screen coordinates. */
#define stampsMenuTrapezeMsg         (trapezeMsg + 81)
/* wParam = (WPARAM) (int) x
   lParam = (LPARAM) (int) y
*/

/* Processes menu accelerator-key messages from the application's message
   queue. message is a WM_KEYDOWN or WM_SYSKEYDOWN message to be translated.
   translated is set to FALSE if the message was not processed, or TRUE if the
   message was processed and should be ignored by the caller. */
#define translateAcceleratorTrapezeMsg (trapezeMsg + 94)
/* wParam = (WPARAM) (LPBOOL) translated
   lParam = (LPARAM) (LPMSG) message
*/

/* Runs a command, if it is enabled. command specifies the command to run; it
   must be one of the trapeze*Command values. If the command is disabled
   callDisabledTrapezeError is returned. The command cannot be
   trapezeInsertBeforeCommand, trapezeInsertAfterCommand,
   trapezeAppendCommand, trapezeOCRBookmarkCommand, trapezeReplaceCommand,
   trapezeNewCommand, trapezeScanNewCommand, trapezeOpenCommand,
   trapezeZoomCommand, trapezeImageToolCommand, trapezeRotatePageCommand,
   trapezeRotatePagesCommand, trapezeStampAnnotationToolCommand,
   trapezeReplacePagesCommand, trapezeInsertBeforePageCommand,
   trapezeInsertAfterPageCommand, trapezeSendMenuCommand,
   trapezeAnnotationsCommand, trapezeRedactionCommand, trapezeMeasureCommand,
   trapezeOCRCommand, trapezeDropPagesCommand, trapezeToolbarMenuCommand,
   trapezeExemptionCodeCommand, trapezeAdminCommand, trapezeScanNewPDFCommand,
   trapezeText1Command, trapezeText2Command, trapezeText3Command,
   trapezeText4Command or one of trapezeFavorite*Command. If force is TRUE,
   the command is run even if it has been disabled using
   disableTrapezeCommandMsg or removed using removeTrapezeCommandMsg. */
#define trapezeCommandMsg            (trapezeMsg + 14)
/* wParam = (WPARAM) (WORD) command
   lParam = (LPARAM) (BOOL) force
*/

/* Returns the enabled state of a command. command specifies the command to
   check; it must be one of the trapeze*Command values. If the command is
   enabled, enabled is set to TRUE, otherwise it is set to FALSE. */
#define trapezeCommandEnabledMsg     (trapezeMsg + 15)
/* wParam = (WPARAM) (WORD) command
   lParam = (LPARAM) (LPBOOL) enabled
*/

/* Shows the system information dialog. parent specifies the parent for the
   dialog. */
#define trapezeSystemInfoMsg         (trapezeMsg + 66)
/* wParam = (WPARAM) 0
   lParam = (LPARAM) (HWND) parent
*/

#define trapezeThumbnailSelectedMsg  (trapezeMsg + 17)
/* wParam = (WPARAM) (DWORD) pageNo
   lParam = (LPARAM) (LPBOOL) selected
*/

/* Shows the thumbnail size preferences dialog. parent specifies the parent
   for the dialog. ERROR_CANCELLED is returned if the user cancels the dialog. */
#define trapezeThumbnailSizePrefsMsg (trapezeMsg + 65)
/* wParam = (WPARAM) 0
   lParam = (LPARAM) (HWND) parent
*/

/* Shows the TRIM preferences dialog, allowing the user to configure Trapeze
   for use with TRIM and/or TopDrawer. allowed specifies trapezeTRIMTRIM
   and/or trapezeTRIMTopDrawer, indicating which configurations are allowed.
   On success, Trapeze will be configured to use one of the allowed
   configurations, and restart is set to TRUE if Trapeze must be restarted for
   the new configurations to take effect. ERROR_CANCELLED is returned if the
   user cancels the dialog. When restart is set to TRUE, the user has not been
   told by Trapeze that it needs to be restarted. */
#define trapezeTRIMPrefsMsg          (trapezeMsg + 55)
/* wParam = (WPARAM) (DWORD) allowed
   lParam = (LPARAM) (LPBOOL) restart
*/

/* Tells Trapeze it is being used as the TRIM viewer, causing it to remove
   commands not wanted by the TRIM viewer */
#define trimViewerTrapezeMsg         (trapezeMsg + 57)
/* wParam = 0
   lParam = 0
*/

/* Writes any changed preferences to the registry */
#define writeTrapezePrefsMsg         (trapezeMsg + 124)
/* wParam = (WPARAM) 0
   lParam = (LPARAM) 0
*/

/* Changes the current zoom. If rect is not NULL, percentage is ignored and
   rect specifies the rectangle to zoom to. The zoom is set to the highest
   zoom keeping all of rect visible and centered. If rect is NULL, the current
   zoom is multiplied by percentage multiplied by 100.
   e.g. SendMessage(trapezeWindow, zoomTrapezeMsg, 5000, (LPARAM) NULL) will
     change the current zoom to half of its current value. */
#define zoomTrapezeMsg               (trapezeMsg + 4)
/* wParam = (WPARAM) (DWORD) percentage
   lParam = (LPARAM) (LPRECT) rect
*/

/* Trapeze window styles */
/* The Trapeze window is a plugin window */
#define pluginTrapezeWindowStyle             0x0001L
/* The Trapeze window starts and runs in a new thread, closing the thread when
   the window is destroyed. The window must be destroyed using
   DestroyTrapezeWindow(). */
#define newThreadTrapezeWindowStyle          0x0002L
/* The Trapeze window automatically sizes itself to fill its parent window */
#define fillParentTrapezeWindowStyle         0x0004L
/* The Trapeze window disables annotation modification */
#define noAnnotateTrapezeWindowStyle         0x0008L
/* The Trapeze window disables redaction */
#define noRedactTrapezeWindowStyle           0x0010L
/* The Trapeze window disables page modification */
#define noPageModifyTrapezeWindowStyle       0x0020L
/* The Trapeze window disables toolbar customization */
#define noToolbarCustomizeTrapezeWindowStyle 0x0040L
/* The Trapeze window creates its own tooltips, rather than global tooltips
   created in the thread of the first Trapeze window created */
#define ownTooltipsTrapezeWindowStyle        0x0080L
/* The Trapeze window is intended to run with TRIM */
#define trimTrapezeWindowStyle               0x0100L
/* The Trapeze window disables bookmarks */
#define noBookmarksTrapezeWindowStyle        0x0200L
/* The Trapeze window is a Unicode window */
#define unicodeTrapezeWindowStyle            0x0400L
/* The Trapeze window disables drag/drop */
#define noDragDropTrapezeWindowStyle         0x0800L

#define noSizeGripTrapezeStatusBarStyle 0x00000001L

#define trapezeStatusBarMsg (WM_USER)

#define showProgressTrapezeStatusBarMsg (trapezeStatusBarMsg)
/* wParam = (WPARAM) (BOOL) show
   lParam = (LPARAM) 0
   Returns 0
*/

#define getProgressBarTrapezeStatusBarMsg (trapezeStatusBarMsg + 1)
/* wParam = (WPARAM) 0
   lParam = (LPARAM) 0
   Returns (HWND) progressBar
*/

#define documentOpenTrapezeStatusBarMsg (trapezeStatusBarMsg + 2)
/* wParam = (WPARAM) (BOOL) open
   lParam = (LPARAM) 0
   Returns 0
*/

#define readWriteTrapezeStatus  0
#define readOnlyTrapezeStatus   1
#define noReadOnlyTrapezeStatus 2

#define readOnlyTrapezeStatusBarMsg (trapezeStatusBarMsg + 3)
/* wParam = (WPARAM) (BYTE) readOnly
   lParam = (LPARAM) 0
   Returns 0
*/

#define rotationTrapezeStatusBarMsg (trapezeStatusBarMsg + 4)
/* wParam = (WPARAM) (WORD) rotation
   lParam = (LPARAM) 0
   Returns 0
*/

#define zoomTrapezeStatusBarMsg (trapezeStatusBarMsg + 5)
/* wParam = (WPARAM) (DWORD) zoom
   lParam = (LPARAM) 0
   Returns 0
*/

#define pageTrapezeStatusBarMsg (trapezeStatusBarMsg + 6)
/* wParam = (WPARAM) (DWORD) pageNo
   lParam = (LPARAM) (DWORD) pageCount
   Returns 0
*/

#define showPartTrapezeStatusBarMsg (trapezeStatusBarMsg + 7)
/* wParam = (WPARAM) (BYTE) part
   lParam = (LPARAM) (BOOL) show
   Returns 0
*/

#define invertTrapezeStatusBarMsg (trapezeStatusBarMsg + 8)
/* wParam = (WPARAM) (BOOL) invert
   lParam = (LPARAM) 0
   Returns 0
*/

#define billingOffTrapezeStatus 0
#define billingOnTrapezeStatus  1
#define noBillingTrapezeStatus  2

#define billingTrapezeStatusBarMsg (trapezeStatusBarMsg + 9)
/* wParam = (WPARAM) (BYTE) billing
   lParam = (LPARAM) 0
   Returns 0
*/

#define draftOffTrapezeStatus 0
#define draftOnTrapezeStatus  1
#define noDraftTrapezeStatus  2

#define draftTrapezeStatusBarMsg (trapezeStatusBarMsg + 10)
/* wParam = (WPARAM) (BYTE) draft
   lParam = (LPARAM) 0
   Returns 0
*/

#define secureOffTrapezeStatus       0
#define secureOnTrapezeStatus        1
#define secureOffHiddenTrapezeStatus 2
#define secureOnHiddenTrapezeStatus  3

#define secureTrapezeStatusBarMsg (trapezeStatusBarMsg + 11)
/* wParam = (WPARAM) (BYTE) secure
   lParam = (LPARAM) 0
   Returns 0
*/

#define noTrapezeStatusBarPart       0
#define statusTrapezeStatusBarPart   1
#define imageTrapezeStatusBarPart    2
#define zoomTrapezeStatusBarPart     3
#define pageTrapezeStatusBarPart     4
#define readOnlyTrapezeStatusBarPart 5
#define billingTrapezeStatusBarPart  6
#define secureTrapezeStatusBarPart   7

typedef struct {
  NMHDR hdr;
  WORD part;
} DblClkTrapezeStatusBarNotifyRec, * DblClkTrapezeStatusBarNotifyPtr;

/* Toolbar bitmaps */
#define trapezeToolbarBitmap 0 /* Toolbar tools */
#define rotateLeftTrapezeToolbarBitmapNo  0 /* Rotate left */
#define rotateRightTrapezeToolbarBitmapNo 1 /* Rotate right */
#define helpTopicsTrapezeToolbarBitmapNo  2 /* Help topics */
#define zoomInTrapezeToolbarBitmapNo      3 /* Zoom in */
#define zoomOutTrapezeToolbarBitmapNo     4 /* Zoom out */
#define setScaleTrapezeToolbarBitmapNo    5 /* Set scale */
#define scanNewTrapezeToolbarBitmapNo     6 /* Scan new */
#define aboutTrapezeToolbarBitmapNo       7 /* About */
#define prevPageTrapezeToolbarBitmapNo    8 /* Previous page */
#define nextPageTrapezeToolbarBitmapNo    9 /* Next page */

#define trapezeImageToolsToolbarBitmap 1 /* Image tools */
#define dragToolTrapezeToolbarBitmapNo         0 /* Dragging tool */
#define zoomInToolTrapezeToolbarBitmapNo       1 /* Zoom in tool */
#define zoomOutToolTrapezeToolbarBitmapNo      2 /* Zoom out tool */
#define selectToolTrapezeToolbarBitmapNo       3 /* Rectangle selection
                                                    tool */
#define selectZoomToolTrapezeToolbarBitmapNo   4 /* Zoom area tool */
#define measureToolTrapezeToolbarBitmapNo      5 /* Measurement tool */
#define calibrateToolTrapezeToolbarBitmapNo    6 /* Calibration tool */
#define annotationToolTrapezeToolbarBitmapNo   7 /* Rectangle annotation
                                                    tool */
#define selectAnnotationTrapezeToolbarBitmapNo 8 /* Annotation selection
                                                    tool */
#define hollowRectAnnotationToolTrapezeToolbarBitmapNo 9 /* Hollow rectangle
                                                            annotation tool */
/* Filled rectangle annotation tool */
#define filledRectAnnotationToolTrapezeToolbarBitmapNo 10
/* Highlighter annotation tool */
#define highlighterAnnotationToolTrapezeToolbarBitmapNo 11
/* Sticky note annotation tool */
#define stickyNoteAnnotationToolTrapezeToolbarBitmapNo 12
#define textAnnotationToolTrapezeToolbarBitmapNo 13 /* Text annotation tool */
/* Straight line annotation tool */
#define straightLineAnnotationToolTrapezeToolbarBitmapNo 14
#define arrowAnnotationToolTrapezeToolbarBitmapNo 15 /* Arrow annotation
                                                        tool */
#define lineAnnotationToolTrapezeToolbarBitmapNo 16 /* Line annotation tool */
#define pictureAnnotationToolTrapezeToolbarBitmapNo 17 /* Picture annotation
                                                          tool */
#define scribbleAnnotationToolTrapezeToolbarBitmapNo 18 /* Scribble annotation
                                                           tool */
#define polylineAnnotationToolTrapezeToolbarBitmapNo 19 /* Polyline annotation
                                                           tool */
#define polygonAnnotationToolTrapezeToolbarBitmapNo 20 /* Polygon annotation
                                                          tool */
#define stampAnnotationToolTrapezeToolbarBitmapNo 21 /* Stamp annotation
                                                        tool */
#define magnifyToolTrapezeToolbarBitmapNo      22 /* Magnify tool */
#define ocrTextToolTrapezeToolbarBitmapNo      23 /* OCR text selection
                                                     tool */
#define rectRedactionToolTrapezeToolbarBitmapNo 24 /* Rectangle redaction
                                                      tool */
#define hollowEllipseAnnotationToolTrapezeToolbarBitmapNo 25 /* Hollow ellipse
                                                                annotation
                                                                tool */
#define filledEllipseAnnotationToolTrapezeToolbarBitmapNo 26 /* Filled ellipse
                                                                annotation
                                                                tool */
#define ellipseAnnotationToolTrapezeToolbarBitmapNo 27 /* Ellipse annotation
                                                          tool */
#define ellipseRedactionToolTrapezeToolbarBitmapNo 28 /* Ellipse redaction
                                                         tool */
#define circleMeasureToolTrapezeToolbarBitmapNo    29 /* Circle measurement
                                                         tool */
#define scribbleRedactionToolTrapezeToolbarBitmapNo 30 /* Scribble redaction
                                                          tool */
#define lightTableToolTrapezeToolbarBitmapNo   31 /* Light table tool */
#define deskewToolTrapezeToolbarBitmapNo       32 /* Deskew tool */
/* Line measurement annotation tool */
#define lineMeasurementAnnotationToolTrapezeToolbarBitmapNo 33
/* Recession plane annotation tool */
#define recessionPlaneAnnotationToolTrapezeToolbarBitmapNo 34

/* Color button messages */
#define getColorTrapezeColorButtonMsg WM_USER
/* wParam = 0
   lParam = 0
   Return = (COLORREF) color
*/
#define setColorTrapezeColorButtonMsg (WM_USER + 1)
/* wParam = (WPARAM) (COLORREF) color
   lParam = (LPARAM) (BOOL) redraw
   Return = 0
*/

#else /*_WIN32*/
/* Only available on Unix */

/* Type definitions */
#define TrapezeAPI
typedef int            TrapezeBOOL;
typedef unsigned char  TrapezeBYTE;
typedef unsigned long  TrapezeCOLORREF;
typedef TrapezeCOLORREF * TrapezeLPCOLORREF;
typedef TrapezeBYTE    * TrapezeLPBYTE;
typedef const char     * TrapezeLPCSTR;
typedef const void     * TrapezeLPCVOID;
typedef unsigned long  TrapezeDWORD;
#ifdef _MSC_VER
/* Visual Studio */
#ifdef _M_IX86
typedef unsigned long  TrapezeDWORD_PTR;
typedef unsigned long  TrapezeUINT_PTR;
#else
#error Unknown processor
#endif
typedef __int64        TrapezeLONGLONG;
#else
#error Unknown compiler
#endif
typedef struct TrapezeHGLOBALStruct {
  int unused;
} * TrapezeHGLOBAL;
typedef long           TrapezeHRESULT;
typedef unsigned short TrapezeLANGID;
typedef long           TrapezeLONG;
typedef TrapezeBOOL    * TrapezeLPBOOL;
typedef TrapezeDWORD   * TrapezeLPDWORD;
typedef int            * TrapezeLPINT;
typedef char           * TrapezeLPSTR;
typedef long           TrapezeLRESULT;
typedef size_t         TrapezeSIZE_T;
typedef TrapezeSIZE_T  * TrapezeLPSIZE_T;
typedef unsigned short TrapezeWCHAR;
typedef TrapezeWCHAR * TrapezeBSTR;
typedef const TrapezeWCHAR * TrapezeLPCOLESTR;
typedef const TrapezeWCHAR * TrapezeLPCWSTR;
typedef TrapezeWCHAR * TrapezeLPWSTR;
typedef unsigned short TrapezeWORD;
typedef TrapezeWORD    * TrapezeLPWORD;
typedef void           * TrapezeLPVOID;
typedef unsigned int   TrapezeUINT;
typedef unsigned int   * TrapezeLPUINT;
typedef unsigned short TrapezeUSHORT;
#ifndef NOTRAPEZEVARIANT
typedef short          TrapezeVARIANT_BOOL;
typedef unsigned short TrapezeVARTYPE;
#endif /*! NOTRAPEZEVARIANT*/
typedef int (* TrapezeFARPROC)();
typedef struct TrapezeGUIDStruct {
  TrapezeDWORD Data1;
  TrapezeWORD Data2;
  TrapezeWORD Data3;
  TrapezeBYTE Data4[8];
} TrapezeGUID, * TrapezeLPGUID;
typedef const TrapezeGUID * TrapezeLPCGUID;
typedef struct TrapezePointStruct {
  TrapezeLONG x;
  TrapezeLONG y;
} TrapezePOINT, * TrapezeLPPOINT;
typedef struct TrapezeRectStruct {
  TrapezeLONG left;
  TrapezeLONG top;
  TrapezeLONG right;
  TrapezeLONG bottom;
} TrapezeRECT, * TrapezeLPRECT;
typedef const TrapezeRECT * TrapezeLPCRECT;
typedef struct TrapezeRGBQUADStruct {
  TrapezeBYTE rgbBlue;
  TrapezeBYTE rgbGreen;
  TrapezeBYTE rgbRed;
  TrapezeBYTE rgbReserved;
} TrapezeRGBQUAD, * TrapezeLPRGBQUAD;
typedef struct TrapezeBITMAPINFOHEADERStruct {
  TrapezeDWORD biSize;
  TrapezeLONG biWidth;
  TrapezeLONG biHeight;
  TrapezeWORD biPlanes;
  TrapezeWORD biBitCount;
  TrapezeDWORD biCompression;
  TrapezeDWORD biSizeImage;
  TrapezeLONG biXPelsPerMeter;
  TrapezeLONG biYPelsPerMeter;
  TrapezeDWORD biClrUsed;
  TrapezeDWORD biClrImportant;
} TrapezeBITMAPINFOHEADER, * TrapezeLPBITMAPINFOHEADER;
typedef struct TrapezeBITMAPINFOStruct {
  TrapezeBITMAPINFOHEADER bmiHeader;
  TrapezeRGBQUAD bmiColors[1];
} TrapezeBITMAPINFO, * TrapezeLPBITMAPINFO;
typedef struct TrapezeSYSTEMTIMEStruct {
  TrapezeWORD wYear;
  TrapezeWORD wMonth;
  TrapezeWORD wDayOfWeek;
  TrapezeWORD wDay;
  TrapezeWORD wHour;
  TrapezeWORD wMinute;
  TrapezeWORD wSecond;
  TrapezeWORD wMilliseconds;
}	TrapezeSYSTEMTIME, * TrapezeLPSYSTEMTIME;
#ifndef NOTRAPEZEVARIANT
typedef struct TrapezeCYStruct {
  TrapezeLONGLONG int64;
} TrapezeCY;
typedef struct TrapezeVARIANTStruct {
#define TrapezeVT_I2   2
#define TrapezeVT_I4   3
#define TrapezeVT_BSTR 8
#define TrapezeVT_BOOL 11
#define TrapezeVT_UI1  17
  TrapezeVARTYPE vt;
  TrapezeUSHORT wReserved1;
  TrapezeUSHORT wReserved2;
  TrapezeUSHORT wReserved3;
  union {
    TrapezeBYTE bVal;
    short iVal;
    TrapezeLONG lVal;
    float fltVal;
    double dblVal;
    TrapezeVARIANT_BOOL boolVal;
    TrapezeLONG scode;
    TrapezeCY cyVal;
    double date;
    TrapezeBSTR bstrVal;
    TrapezeLPVOID pdecVal;
    TrapezeLPVOID punkVal;
    TrapezeLPVOID pdispVal;
    TrapezeLPVOID parray;
    TrapezeLPBYTE pbVal;
    short* piVal;
    TrapezeLONG* plVal;
    float* pfltVal;
    double* pdblVal;
    short* pboolVal;
    TrapezeLONG* pscode;
    TrapezeCY* pcyVal;
    double* pdate;
    TrapezeBSTR* pbstrVal;
    TrapezeLPVOID* ppunkVal;
    TrapezeLPVOID* ppdispVal;
    TrapezeLPVOID* pparray;
    struct TrapezeVARIANTStruct* pvarVal;
    TrapezeLPVOID byref;
  };
} TrapezeVARIANT;
#endif /*! NOTRAPEZEVARIANT*/
#ifndef NOTRAPEZEWINDOWSNAMES
#define BITMAPINFO         TrapezeBITMAPINFO
#define BITMAPINFOHEADER   TrapezeBITMAPINFOHEADER
#define BOOL               TrapezeBOOL
#define BSTR               TrapezeBSTR
#define BYTE               TrapezeBYTE
#define COLORREF           TrapezeCOLORREF
#define DWORD              TrapezeDWORD
#define DWORD_PTR          TrapezeDWORD_PTR
#define FARPROC            TrapezeFARPROC
#define GUID               TrapezeGUID
#define HGLOBAL            TrapezeHGLOBAL
#define HRESULT            TrapezeHRESULT
#define LANGID             TrapezeLANGID
#define LONG               TrapezeLONG
#define LPBITMAPINFO       TrapezeLPBITMAPINFO
#define LPBITMAPINFOHEADER TrapezeLPBITMAPINFOHEADER
#define LPBOOL             TrapezeLPBOOL
#define LPBYTE             TrapezeLPBYTE
#define LPCGUID            TrapezeLPCGUID
#define LPCOLESTR          TrapezeLPCOLESTR
#define LPCOLORREF         TrapezeLPCOLORREF
#define LPCRECT            TrapezeLPCRECT
#define LPCSTR             TrapezeLPCSTR
#define LPCVOID            TrapezeLPCVOID
#define LPCWSTR            TrapezeLPCWSTR
#define LPDWORD            TrapezeLPDWORD
#define LPGUID             TrapezeLPGUID
#define LPINT              TrapezeLPINT
#define LPPOINT            TrapezeLPPOINT
#define LPRECT             TrapezeLPRECT
#define LPRGBQUAD          TrapezeLPRGBQUAD
#define LPSTR              TrapezeLPSTR
#define LPSYSTEMTIME       TrapezeLPSYSTEMTIME
#define LPUINT             TrapezeLPUINT
#define LPVOID             TrapezeLPVOID
#define LPWORD             TrapezeLPWORD
#define LPWSTR             TrapezeLPWSTR
#define LRESULT            TrapezeLRESULT
#define POINT              TrapezePOINT
#define POINTDEFINED
#define RECT               TrapezeRECT
#define RGBQUAD            TrapezeRGBQUAD
#define SIZE_T             TrapezeSIZE_T
#define SYSTEMTIME         TrapezeSYSTEMTIME
#define SYSTEMTIMEDEFINED
#define UINT               TrapezeUINT
#define UINT_PTR           TrapezeUINT_PTR
#define USHORT             TrapezeUSHORT
#ifndef NOTRAPEZEVARIANT
#define VARIANT            TrapezeVARIANT
#define VARIANT_BOOL       TrapezeVARIANT_BOOL
#define VARTYPE            TrapezeVARTYPE
#endif /*! NOTRAPEZEVARIANT*/
#define VT_BOOL            TrapezeVT_BOOL
#define VT_BSTR            TrapezeVT_BSTR
#define VT_I2              TrapezeVT_I2
#define VT_I4              TrapezeVT_I4
#define VT_UI1             TrapezeVT_UI1
#define WCHAR              TrapezeWCHAR
#define WINAPI             TrapezeAPI
#define WORD               TrapezeWORD
#endif /*! NOTRAPEZEWINDOWSNAMES*/

#endif /*_WIN32*/

/* Available on Windows and Unix */

/* Progress function
   Called periodically by various functions. percentage specifies the
   percentage of the operation that has been completed (0-100). If percentage
   is >100 then the progress is unknown and/or the function is busy doing an
   unknown length operation, such as waiting for network data. lParam is the
   lParam value passed to the function calling the progress function. The
   return value is TRUE to continue the operation, or FALSE to fail the
   function calling the progress function with ERROR_CANCELLED. */
typedef TrapezeBOOL (TrapezeAPI* TrapezeProgressProc)(TrapezeWORD percentage,
                                                      TrapezeLPVOID lParam);

typedef struct TrapezeAnnotationsStruct {
  int unused;
} * TrapezeAnnotations;
typedef TrapezeAnnotations * TrapezeAnnotationsPtr;

/* Resolution/scale units */
#define noTrapezeTIFFResolutionScaleUnit      1 /* No units, resolutions
                                                   indicate the aspect
                                                   ratio */
#define inchTrapezeTIFFResolutionScaleUnit    2 /* Inches */
#define cmTrapezeTIFFResolutionScaleUnit      3 /* Centimeters */
#define yardTrapezeTIFFResolutionScaleUnit    4 /* Yards */
#define meterTrapezeTIFFResolutionScaleUnit   5 /* Meters */
#define metreTrapezeTIFFResolutionScaleUnit   5 /* Meters */
#define mileTrapezeTIFFResolutionScaleUnit    6 /* Miles */
#define kmTrapezeTIFFResolutionScaleUnit      7 /* Kilometers */
#define mmTrapezeTIFFResolutionScaleUnit      8 /* Millimeters */
#define ftTrapezeTIFFResolutionScaleUnit      9 /* Feet */
#define chainTrapezeTIFFResolutionScaleUnit   10 /* Chains */
#define rodTrapezeTIFFResolutionScaleUnit     11 /* Rods */
#define furlongTrapezeTIFFResolutionScaleUnit 12 /* Furlongs */
#define linkTrapezeTIFFResolutionScaleUnit    13 /* Links */
#define micrometerTrapezeTIFFResolutionScaleUnit 14 /* Micrometers */
#define decimeterTrapezeTIFFResolutionScaleUnit 15 /* Decimeters */
#define dekameterTrapezeTIFFResolutionScaleUnit 16 /* Dekameters */
#define pointTrapezeTIFFResolutionScaleUnit   17 /* Points */

/* TIFF resolution/scale. X value is (xNumerator / xDenominator) units.
   Y value is (yNumerator / yDenominator) units */
typedef struct {
  TrapezeDWORD xDenominator; /* Horizontal fraction denominator */
  TrapezeDWORD xNumerator; /* Horizontal fraction numerator */
  TrapezeDWORD yDenominator; /* Vertical fraction denominator */
  TrapezeDWORD yNumerator; /* Vertical fraction numerator */
  TrapezeWORD unit; /* Unit from above, one of
                       noTrapezeTIFFResolutionScaleUnit,
                       inchTrapezeTIFFResolutionScaleUnit or
                       cmTrapezeTIFFResolutionScaleUnit for a resolution, or
                       any of *TrapezeTIFFResolutionScaleUnit for a scale */
} TrapezeTIFFResolutionScaleRec, * TrapezeTIFFResolutionScalePtr;
typedef const TrapezeTIFFResolutionScaleRec * TrapezeTIFFResolutionScaleCPtr;

/* Layer information */
typedef struct {
  TrapezeCOLORREF color; /* Layer color, 0x00bbggrr = RGB color */
  TrapezeLPCSTR fileName; /* File containing the layer image */
} TrapezeLayerRec, * TrapezeLayerPtr;

/* Annotation burn in flags */
#define increaseColorsTrapezeBurnIn 0x00000001 /* Increase colors if
                                                  necessary */
#define inPlaceTrapezeBurnIn        0x00000002 /* Burn in in-place if
                                                  possible */
#define redactionsTrapezeBurnIn     0x00000004 /* Burn in redactions only */
#define showAllTrapezeBurnIn        0x00000008 /* Burn in any annotations
                                                  hidden by group */

/* Data for BurnInTrapezeAnnotationsEx() */
typedef struct {
  TrapezeWORD size; /* Size of this structure, in bytes */
  TrapezeWORD rotation; /* Rotation that will be applied to DIB after burn in,
                           0, 90, 180 or 270 */
  TrapezeAnnotations annotations; /* annotations and annotationsSize must be a
                                     value returned by
                                     GetTrapezeTIFFAnnotations(),
                                     InsertTrapezeAnnotation() or
                                     LoadTrapezeAnnotations() */
  TrapezeDWORD annotationsSize; /* Size of annotations, in bytes */
  TrapezeBYTE* dibData; /* Data for DIB pixels */
  TrapezeBITMAPINFO* dib; /* DIB information */
  /* flags is a combination of:
     - increaseColorsTrapezeBurnIn, if necessary, the number of colors in the
       DIB is increased so that there is no annotation color loss during the
       burn in. If this flag is not set, the nearest available colors from the
       image are used.
     - inPlaceTrapezeBurnIn, to, if possible, burn the annotations into the
       passed in DIB rather than returning a new DIB.
     - redactionsTrapezeBurnIn, to only burn in redactions from annotations.
       After the burn in, the burned in redactions are removed from
       annotations. When this flag is set, annotationsSize is modified to
       reflect the new size of annotations. If annotationSize is set to zero,
       all annotations were redactions and annotations should be freed using
       DestroyTrapezeAnnotations() immediately.
     - showAllTrapezeBurnIn, to burn in any annotations hidden by group. */
  TrapezeDWORD flags;
} BurnInTrapezeAnnotationsExRec, * BurnInTrapezeAnnotationsExPtr;

/* Types for getting annotation groups */
typedef struct {
  TrapezeDWORD count; /* Number of strings, 0 = use default ("[Untitled]") */
  TrapezeDWORD size; /* Size of groups, in characters, must be set before
                        calling, will be set to required size on return */
  TrapezeLPSTR groups; /* Buffer to return groups in, must be size characters,
                          count strings are returned */
} GetTrapezeAnnotationGroupsRec;
typedef GetTrapezeAnnotationGroupsRec * GetTrapezeAnnotationGroupsPtr;

/* Types for setting annotation groups */
typedef struct {
  TrapezeDWORD count; /* Number of strings, 0 = use default ("[Untitled]") */
  TrapezeLPCSTR groups; /* Groups, count strings */
} SetTrapezeAnnotationGroupsRec;
typedef const SetTrapezeAnnotationGroupsRec * SetTrapezeAnnotationGroupsCPtr;

/* Types for editing/setting exemption codes */
typedef struct {
  TrapezeBOOL line; /* TRUE to show a line between the exemption code and its
                       redaction */
  TrapezeHGLOBAL exemptionCodes; /* A GlobalAlloc() handle containing pairs of
                                    strings terminated by an empty string.
                                    NULL indicates an empty list. This handle
                                    must be freed using GlobalFree() when it
                                    is no longer required. If this handle is
                                    changed by a call, the old handle will
                                    have been freed. */
#ifdef _WIN32
  HWND parent; /* Parent for dialogs */
#endif /*_WIN32*/
} TrapezeExemptionCodesRec, * TrapezeExemptionCodesPtr;
typedef const TrapezeExemptionCodesRec * TrapezeExemptionCodesCPtr;

/* Types for inserting annotations */
/* Data is InsertRectangleTrapezeAnnotationPtr */
#define insertRectangleTrapezeAnnotation          0x00000000
/* Data is InsertPictureTrapezeAnnotationPtr */
#define insertPictureTrapezeAnnotation            0x00000001
/* Data is InsertRectangleTrapezeRedactionPtr, requires Trapeze 6.41 or
   later */
#define insertRectangleRedactionTrapezeAnnotation 0x00000002
/* Data is InsertEllipseTrapezeRedactionPtr, requires Trapeze 8.0 Beta 6 or
   later */
#define insertEllipseRedactionTrapezeAnnotation   0x00000003
/* Data is InsertStampTrapezeAnnotationPtr, requires Trapeze 8.0 Beta 8 or
   later */
#define insertStampTrapezeAnnotation              0x00000004

/* Rectangle annotation border styles */
#define noRectTrapezeAnnotationBorderStyle      0 /* No border */
#define lineRectTrapezeAnnotationBorderStyle    1 /* Line border */
#define noteRectTrapezeAnnotationBorderStyle    2 /* Sticky note border */
#define raisedRectTrapezeAnnotationBorderStyle  3 /* Raised border */
#define shadowRectTrapezeAnnotationBorderStyle  4 /* Shadow border */
#define sunkenRectTrapezeAnnotationBorderStyle  5 /* Sunken border */

/* Rectangle annotation font styles */
#define boldRectTrapezeAnnotationFontStyle      0x01 /* Bold font */
#define italicRectTrapezeAnnotationFontStyle    0x02 /* Italic font */
#define strikeoutRectTrapezeAnnotationFontStyle 0x04 /* Strikeout font */
#define underlineRectTrapezeAnnotationFontStyle 0x08 /* Underline font */
#define hiddenRectTrapezeAnnotationFontStyle    0x10 /* Hidden font */
#define centerRectTrapezeAnnotationFontStyle    0x20 /* Text is horizontally
                                                        centered */
#define vCenterRectTrapezeAnnotationFontStyle   0x40 /* Text is vertically
                                                        centered */
#define rightRectTrapezeAnnotationFontStyle     0x80 /* Text is right
                                                        aligned */

/* Annotation redaction values */
#define noTrapezeAnnotationRedaction         0 /* Not a redaction */
#define annotationTrapezeAnnotationRedaction 1 /* An annotation changed into a
                                                  redaction */
#define redactionTrapezeAnnotationRedaction  2 /* A redaction, cannot be
                                                  changed back into an
                                                  annotation */

/* Annotation background styles */
#define solidTrapezeAnnotationBackStyle  0 /* Solid */
#define back75TrapezeAnnotationBackStyle 1 /* Background is 75% backColor,
                                              25% backColor2 */
#define back50TrapezeAnnotationBackStyle 2 /* Background is 50% backColor,
                                              50% backColor2 */
#define back25TrapezeAnnotationBackStyle 3 /* Background is 25% backColor,
                                              75% backColor2 */
#define backDXHatchAnnotationBackStyle   4 /* Diagonal crosshatch */

/* Annotation size/location values */
/* Size/location defined as described in rect, locationBorder is ignored */
#define rectTrapezeAnnotationSizeLocation         0
/* Size is size of text, location is top left, moved right and down by
   locationBorder pixels, rect is ignored */
#define topLeftTrapezeAnnotationSizeLocation      1
/* Size is size of text, location is top center, moved down by locationBorder
   pixels, rect is ignored */
#define topCenterTrapezeAnnotationSizeLocation    2
/* Size is size of text, location is top right, moved left and down by
   locationBorder pixels, rect is ignored */
#define topRightTrapezeAnnotationSizeLocation     3
/* Size is size of text, location is center right, moved left by
   locationBorder pixels, rect is ignored */
#define centerRightTrapezeAnnotationSizeLocation  4
/* Size is size of text, location is bottom right, moved left and up by
   locationBorder pixels, rect is ignored */
#define bottomRightTrapezeAnnotationSizeLocation  5
/* Size is size of text, location is bottom center, moved up by locationBorder
   pixels, rect is ignored */
#define bottomCenterTrapezeAnnotationSizeLocation 6
/* Size is size of text, location is bottom left, moved right and up by
   locationBorder pixels, rect is ignored */
#define bottomLeftTrapezeAnnotationSizeLocation   7
/* Size is size of text, location is center left, moved right by
   locationBorder pixels, rect is ignored */
#define centerLeftTrapezeAnnotationSizeLocation   8
/* Size is size of text, location is center, locationBorder and rect are
   ignored */
#define centerCenterTrapezeAnnotationSizeLocation 9

/* exemptionCodes is valid (annotation must be a redaction) */
#define insertRectangleTrapezeAnnotationExemptionCodes 0x00000001
/* createTime is valid */
#define insertRectangleTrapezeAnnotationCreateTime     0x00000002
/* imageSize is valid */
#define insertRectangleTrapezeAnnotationImageSize      0x00000004
/* All strings are Unicode */
#define insertRectangleTrapezeAnnotationUnicode        0x00000008
/* When using insertTrapezeAnnotationMsg and the text doesn't fit in the
   annotation, auto-create a new page and continue the annotation */
#define insertRectangleTrapezeAnnotationNewPage        0x00000010
/* newPageChar is valid */
#define insertRectangleTrapezeAnnotationNewPageChar    0x00000020

#define insertRectangleTrapezeAnnotationSize1 48 /* Versions 3.3 or later,
                                                    includes up to text */
#define insertRectangleTrapezeAnnotationSize2 52 /* Versions 6.31 Beta 3 or
                                                    later, includes up to
                                                    redaction */
#define insertRectangleTrapezeAnnotationSize3 68 /* Versions 6.79 or later,
                                                    includes up to
                                                    locationBorder */
#define insertRectangleTrapezeAnnotationSize4 76 /* Versions 6.8 or later,
                                                    includes up to rotation */
#define insertRectangleTrapezeAnnotationSize5 88 /* Versions 7.56 or later,
                                                    includes up to
                                                    createTime */
#define insertRectangleTrapezeAnnotationSize6 100 /* Versions 8.11 or later,
                                                     includes up to
                                                     imageSize */

/* Structure for inserting a rectangle annotation */
typedef struct {
  TrapezeWORD size; /* Size of this structure, in bytes */
  TrapezeWORD borderStyle; /* A *RectTrapezeAnnotationBorderStyle value from
                              above, ignored if redaction is
                              redactionTrapezeAnnotationRedaction */
  TrapezeWORD borderWidth; /* Border width, in image pixels, ignored if
                              redaction is
                              redactionTrapezeAnnotationRedaction */
  TrapezeDWORD borderColor; /* Border color, 0x00bbggrr = RGB,
                               0x01bbggrr = transparent RGB,
                               0xFFFFFFFF = none, ignored if redaction is
                               redactionTrapezeAnnotationRedaction */
  TrapezeBYTE fontCharSet; /* Font character set, ignored if redaction is
                              redactionTrapezeAnnotationRedaction */
  TrapezeBYTE fontStyle; /* Any combination of *RectTrapezeAnnotationFontStyle
                            values from above, ignored if redaction is
                            redactionTrapezeAnnotationRedaction */
  TrapezeWORD fontSize; /* Font size, in points (1/72 inch), ignored if
                           redaction is redactionTrapezeAnnotationRedaction */
  TrapezeDWORD fontColor; /* Font color, 0x00bbggrr = RGB,
                             0x01bbggrr = transparent RGB,
                             0xFFFFFFFF = none or hidden,
                             ignored if redaction is
                             redactionTrapezeAnnotationRedaction */
  TrapezeLPCSTR fontName; /* Font name, can be NULL for no text or hidden
                             text, ignored if redaction is
                             redactionTrapezeAnnotationRedaction. This is a
                             LPCWSTR if
                             insertRectangleTrapezeAnnotationUnicode is set in
                             flags. */
  TrapezeRECT rect; /* Position and size of annotation, must be within the
                       image. If rect.right is -1, the width of the inserted
                       text is used as the annotation width; if
                       rect.left + width > imageWidth then rect.left is
                       adjusted; if width > imageWidth then rect.left and
                       width are adjusted. If rect.bottom is -1, same for
                       height as for width above. */
  TrapezeDWORD backColor; /* Background color, 0x00bbggrr = RGB,
                             0x01bbggrr = transparent RGB, 0xFFFFFFFF = none
                             If redaction is
                             redactionTrapezeAnnotationRedaction, this must be
                             a solid color (0x00bbggrr) and the nearest color
                             in the image will be used. */
  TrapezeLPCSTR text; /* Text for annotation, NULL = no text, ignored if
                         redaction is redactionTrapezeAnnotationRedaction.
                         This is a LPCWSTR if
                         insertRectangleTrapezeAnnotationUnicode is set in
                         flags. */
  /* Anything below here requires Trapeze 6.31 Beta 3 or later */
  TrapezeBYTE redaction; /* Redaction value, one of
                            *TrapezeAnnotationRedaction */
  /* Anything below here requires Trapeze 6.79 or later */
  TrapezeDWORD backStyle; /* Background style, one of
                             *TrapezeAnnotationBackStyle, ignored if backColor
                             is 0xFFFFFFFF */
  TrapezeDWORD backColor2; /* Second background color, 0x00bbggrr = RGB, used
                              when backStyle is not
                              solidTrapezeAnnotationBackStyle */
  TrapezeDWORD sizeLocation; /* Size/location, one of
                                *TrapezeAnnotationSizeLocation */
  TrapezeDWORD locationBorder; /* Border for location, used for some values of
                                  sizeLocation */
  /* Anything below here requires Trapeze 6.8 or later */
  TrapezeDWORD displayBackColor; /* Color to use instead of backColor for
                                    display only, 0x00bbggrr = RGB,
                                    0x01bbggrr = transparent RGB,
                                    0xFFFFFFFF = use backColor, ignored if no
                                    background */
  TrapezeBYTE rotateWithImage; /* TRUE to rotate with image, ignored if
                                  redaction is
                                  redactionTrapezeAnnotationRedaction */
  TrapezeWORD rotation; /* Rotation, 0, 90, 180 or 270, ignored if redaction
                           is redactionTrapezeAnnotationRedaction */
  /* Anything below here requires Trapeze 7.56 or later */
  TrapezeDWORD flags; /* Flags indicating which fields below are valid, a
                         combination of insertRectangleTrapezeAnnotation*
                         values */
  /* Exemption codes, ignored if
     insertRectangleTrapezeAnnotationExemptionCodes is not set in flags. The
     strings in exemptionCodes are Unicode strings if
     insertRectangleTrapezeAnnotationUnicode is set in flags. */
  TrapezeExemptionCodesCPtr exemptionCodes;
  /* Creation time (GMT), ignored if
     insertRectangleTrapezeAnnotationCreateTime is not set in flags */
  const TrapezeSYSTEMTIME* createTime;
  /* Anything below here requires Trapeze 8.11 or later */
  /* Image size, ignored if insertRectangleTrapezeAnnotationImageSize is not
     set in flags. Used for auto-sizing with InsertTrapezeAnnotation(),
     otherwise ignored. */
  struct {
    TrapezeDWORD width; /* Width of image */
    TrapezeDWORD height; /* Height of image */
    TrapezeTIFFResolutionScaleCPtr resolution; /* Image resolution */
  } imageSize;
  /* Anything below here requires Trapeze 8.2 Beta 8 or later */
  TrapezeWCHAR newPageChar; /* New page character, ignored if
                               insertRectangleTrapezeAnnotationNewPageChar is
                               not set in flags or if not using
                               insertTrapezeAnnotationMsg. This character in
                               text will auto-create a new page and continue
                               the annotation. */
} InsertRectangleTrapezeAnnotationRec, * InsertRectangleTrapezeAnnotationPtr;
typedef const InsertRectangleTrapezeAnnotationRec
  * InsertRectangleTrapezeAnnotationCPtr;

#define insertPictureTrapezeAnnotationSize1 32 /* Versions 6.36 Beta 4 or
                                                  later */
#define insertPictureTrapezeAnnotationSize2 40 /* Versions 7.51 or later */

/* Picture annotation styles */
#define transparentPictureTrapezeAnnotationStyle 1 /* Picture is
                                                      transparent */
#define fitPictureTrapezeAnnotationStyle         2 /* Picture is fitted, not
                                                      stretched to fill */
#define negativePictureTrapezeAnnotationStyle    4 /* Picture is inverted */
#define blobPictureTrapezeAnnotationStyle        8 /* Picture comes from blob
                                                      and blobSize */
/* exemptionCodes is valid (picture must be a redaction) */
#define exemptionCodesPictureTrapezeAnnotationStyle 16
#define createTimePictureTrapezeAnnotationStyle  32 /* createTime is valid */

/* Picture annotation orientation values */
/* Picture is not rotated */
#define normalPictureTrapezeAnnotationOrientation    1
/* Picture is rotated 180 degrees before displaying */
#define rotate180PictureTrapezeAnnotationOrientation 3
/* Picture is rotated 90 degrees clockwise before displaying */
#define rotate90PictureTrapezeAnnotationOrientation  6
/* Picture is rotated 90 degrees anticlockwise before displaying */
#define rotate270PictureTrapezeAnnotationOrientation 8

/* Structure for inserting a picture annotation */
typedef struct {
  TrapezeWORD size; /* Size of this structure, in bytes */
  TrapezeWORD style; /* A combination of *PictureTrapezeAnnotationStyle values
                        from above, all except
                        blobPictureTrapezeAnnotationStyle,
                        exemptionCodesPictureTrapezeAnnotationStyle and
                        createTimePictureTrapezeAnnotationStyle are ignored if
                        blobPictureTrapezeAnnotationStyle is set */
  TrapezeWORD orientation; /* A *PictureTrapezeAnnotationOrientation value
                              from above; ignored if
                              blobPictureTrapezeAnnotationStyle is set in
                              style */
  TrapezeRECT rect; /* Position and size of annotation, must be within the
                       image. If rect.right is -1, the width of the inserted
                       image is used as the annotation width; if
                       rect.left + width > imageWidth then rect.left is
                       adjusted; if width > imageWidth then rect.left and
                       width are adjusted. If rect.bottom is -1, same for
                       height as for width above. */
  TrapezeLPCSTR fileName; /* File containing image, NULL or "" = get image
                             from clipboard; ignored if
                             blobPictureTrapezeAnnotationStyle is set in
                             style */
  TrapezeDWORD imageNo; /* One based image number to use, ignored for single
                           page image types or if
                           blobPictureTrapezeAnnotationStyle is set in
                           style */
  /* Anything below here requires Trapeze 7.51 or later */
  TrapezeHGLOBAL blob; /* Blob for picture. blob is created using
                          annotationPrefsBlobTrapezeMsg. Ignored if
                          blobPictureTrapezeAnnotationStyle is not set in
                          style */
  TrapezeDWORD blobSize; /* Size of blob, in bytes, ignored if
                            blobPictureTrapezeAnnotationStyle is not set in
                            style */
  /* Anything below here requires Trapeze 7.56 or later */
  /* Exemption codes, ignored if exemptionCodesPictureTrapezeAnnotationStyle
     is not set in style */
  TrapezeExemptionCodesCPtr exemptionCodes;
  /* Creation time (GMT), ignored if createTimePictureTrapezeAnnotationStyle
     is not set in style */
  const TrapezeSYSTEMTIME* createTime;
} InsertPictureTrapezeAnnotationRec, * InsertPictureTrapezeAnnotationPtr;
typedef const InsertPictureTrapezeAnnotationRec
  * InsertPictureTrapezeAnnotationCPtr;

#define insertRectangleTrapezeRedactionSize1 20 /* Versions 6.41 or later */
#define insertRectangleTrapezeRedactionSize2 24 /* Versions 6.8 or later */
#define insertRectangleTrapezeRedactionSize3 36 /* Versions 7.5 Beta 1 or
                                                   later */

#define insertRectangleTrapezeRedactionBlob 0x00000001 /* blob and blobSize
                                                          are valid */
/* exemptionCodes is valid */
#define insertRectangleTrapezeRedactionExemptionCodes 0x00000002
/* createTime is valid */
#define insertRectangleTrapezeRedactionCreateTime     0x00000004

/* Structure for inserting a rectangle redaction - requires Trapeze 6.41 or
   later */
/* Color etc comes from rectangle redaction preferences, or from blob and
   blobSize if insertRectangleTrapezeRedactionBlob is set in flags */
typedef struct {
  TrapezeWORD size; /* Size of this structure, in bytes */
  TrapezeRECT rect; /* Position and size of annotation, must be within the
                       image */
  /* Anything below here requires Trapeze 6.8 or later */
  TrapezeDWORD displayBackColor; /* Color to use instead of backColor for
                                    display only, 0x00bbggrr = RGB,
                                    0x01bbggrr = transparent RGB,
                                    0xFFFFFFFF = use backColor */
  /* Anything below here requires Trapeze 7.5 Beta 1 or later */
  TrapezeDWORD flags; /* Flags indicating which fields below are valid, a
                         combination of insertRectangleTrapezeRedaction*
                         values */
  TrapezeHGLOBAL blob; /* Blob for color etc, or NULL for defaults. blob is
                          created using annotationPrefsBlobTrapezeMsg. Ignored
                          if insertRectangleTrapezeRedactionBlob is not set in
                          flags */
  TrapezeDWORD blobSize; /* Size of blob, in bytes, ignored if blob is NULL or
                            insertRectangleTrapezeRedactionBlob is not set in
                            flags */
  /* Anything below here requires Trapeze 7.58 or later */
  /* Exemption codes, ignored if insertRectangleTrapezeRedactionExemptionCodes
     is not set in flags */
  TrapezeExemptionCodesCPtr exemptionCodes;
  /* Creation time (GMT), ignored if insertRectangleTrapezeRedactionCreateTime
     is not set in flags */
  const TrapezeSYSTEMTIME* createTime;
} InsertRectangleTrapezeRedactionRec, * InsertRectangleTrapezeRedactionPtr;
typedef const InsertRectangleTrapezeRedactionRec
  * InsertRectangleTrapezeRedactionCPtr;

/* Structure for inserting an ellipse redaction - requires Trapeze 8.0 Beta 6
   or later */
/* Color etc comes from ellipse redaction preferences */
typedef struct {
  TrapezeWORD size; /* Size of this structure, in bytes */
  TrapezeWORD flags; /* Flags indicating which fields below are valid,
                        currently must be zero */
  TrapezeRECT rect; /* Position and size of annotation, must be within the
                       image */
} InsertEllipseTrapezeRedactionRec, InsertEllipseTrapezeRedactionPtr;
typedef const InsertEllipseTrapezeRedactionRec
  * InsertEllipseTrapezeRedactionCPtr;

#define insertStampTrapezeAnnotationUnicode 0x0001 /* stampName is a Unicode
                                                      string */

/* Structure for inserting a stamp - requires Trapeze 8.0 Beta 8 or later */
typedef struct {
  TrapezeWORD size; /* Size of this structure, in bytes */
  TrapezeWORD flags; /* Flags indicating which fields below are valid, a
                        combintation of insertStampTrapezeAnnotation*
                        values */
  TrapezePOINT pos; /* Location for the stamp, in image coordinates. This is
                       ignored for stamps that include a position. */
  TrapezeLPCSTR stampName; /* Stamp to insert, NULL or "" will insert the
                              current stamp. ERROR_INVALID_PARAMETER is
                              returned if stampName is invalid or stampName is
                              NULL or "" and there is no current stamp */
} InsertStampTrapezeAnnotationRec, InsertStampTrapezeAnnotationPtr;
typedef const InsertStampTrapezeAnnotationRec
  * InsertStampTrapezeAnnotationCPtr;

/* Types for JPEG compression */

/* JPEG processes */
#define baselineTrapezeJPEGProcess        0 /* Baseline DCT with Huffman
                                               coding */
#define losslessHuffmanTrapezeJPEGProcess 1 /* Lossless with Huffman coding */

typedef struct {
  TrapezeWORD size; /* Size of this structure, in bytes */
  TrapezeBYTE process; /* JPEG process, one of *TrapezeJPEGProcess,
                          default = baselineTrapezeJPEGProcess */
  TrapezeBYTE quality; /* Quality, 0 = worst to 100 = best, default = 75 */
} TrapezeJPEG8OptionsRec, * TrapezeJPEG8OptionsPtr;
typedef const TrapezeJPEG8OptionsRec * TrapezeJPEG8OptionsCPtr;

/* JPEG color spaces */
#define ycbcrTrapezeJPEGColorSpace   0 /* YCbCr */
#define cmykTrapezeJPEGColorSpace    1 /* CMYK */
#define grayTrapezeJPEGColorSpace    2 /* Grayscale */
#define rgbTrapezeJPEGColorSpace     3 /* RGB */
#define unknownTrapezeJPEGColorSpace 4 /* Unknown */
#define ycckTrapezeJPEGColorSpace    5 /* YCCK */

typedef struct {
  TrapezeWORD size; /* Size of this structure, in bytes */
  TrapezeBYTE colorSpace; /* Color space, one of *TrapezeJPEGColorSpace,
                             default = ycbcrTrapezeJPEGColorSpace,
                             currently must be ycbcrTrapezeJPEGColorSpace */
  TrapezeBYTE process; /* JPEG process, one of *TrapezeJPEGProcess,
                          default = baselineTrapezeJPEGProcess */
  TrapezeBYTE quality; /* Quality, 0 = worst to 100 = best, default = 75 */
  TrapezeBYTE ycbcrSubSamplingH; /* Cb and Cr horizontal sampling frequency,
                                    1 = 1, 2 = 1/2, 4 = 1/4, default = 2,
                                    currently must be 1 when process is
                                    losslessHuffmanTrapezeJPEGProcess */
  TrapezeBYTE ycbcrSubSamplingV; /* Cb and Cr vertical sampling frequency,
                                    1 = 1, 2 = 1/2, 4 = 1/4, default = 1,
                                    currently must be 1 when process is
                                    losslessHuffmanTrapezeJPEGProcess */
} TrapezeJPEG24OptionsRec, * TrapezeJPEG24OptionsPtr;
typedef const TrapezeJPEG24OptionsRec * TrapezeJPEG24OptionsCPtr;

typedef struct {
  TrapezeWORD size; /* Size of this structure, in bytes */
  TrapezeJPEG8OptionsPtr jpeg8; /* 8 bit gray JPEG options, NULL = use
                                   defaults */
  TrapezeJPEG24OptionsPtr jpeg24; /* 24 bit color JPEG options, NULL = use
                                     defaults */
} TrapezeJPEGOptionsRec, * TrapezeJPEGOptionsPtr;
typedef const TrapezeJPEGOptionsRec * TrapezeJPEGOptionsCPtr;

/* Annotation save formats */
#define trapezeTrapezeAnnotationFormat 0 /* Trapeze annotation file */

/* Annotations Definitions and Types */

#ifdef _WIN64
#define burnInTrapezeAnnotationsInPlace  ((TrapezeHGLOBAL) 0xFFFFFFFFFFFFFFFF)
#else
#define burnInTrapezeAnnotationsInPlace  ((TrapezeHGLOBAL) 0xFFFFFFFF)
#endif /*_WIN64*/
#define getTrapezeAnnotationsCountFailed ((TrapezeDWORD) -1)
#define getTrapezeAnnotationTextFailed   ((TrapezeDWORD) -1)
#define insertTrapezeAnnotationFailed    ((TrapezeDWORD) -1)

/* Annotation load warnings */
#define trapezeAnnotationTypeWarning    0x00000001 /* Unsupported annotation
                                                      type */
#define trapezeAnnotationStyleWarning   0x00000002 /* Unsupported annotation
                                                      style */
#define trapezeAnnotationUnicodeWarning 0x00000004 /* Unsupported Unicode
                                                      annotation */

/* Flags for TrapezeAnnotationsFlags() */
/* Annotations have redactions */
#define redactionTrapezeAnnotationFlag  1
/* Annotations contain a page delete redaction */
#define pageDeleteTrapezeAnnotationFlag 2
/* An error occurred */
#define errorTrapezeAnnotationFlag     ((TrapezeDWORD) -1)

/* Annotation types */
#define ellipseTrapezeAnnotationType           0x00 /* Ellipse */
#define exemptionCodeLineTrapezeAnnotationType 0x01 /* Exemption code line */
#define exemptionCodeTrapezeAnnotationType     0x02 /* Exemption code */
#define lineTrapezeAnnotationType              0x03 /* Line */
#define pictureTrapezeAnnotationType           0x04 /* Picture */
#define polylineTrapezeAnnotationType          0x05 /* Polyline/scribble */
#define rectangleTrapezeAnnotationType         0x06 /* Rectangle/text */
#define lineMeasurementTrapezeAnnotationType   0x07 /* Line measurement */
#define recessionPlaneTrapezeAnnotationType    0x08 /* Recession plane */
#define pageDeleteTrapezeAnnotationType        0x09 /* Page delete
                                                       (rectangle/text) */
#define hConeOfVisionTrapezeAnnotationType     0x0a /* Horizontal cone of
                                                       vision */

/* Document types */
#define bmpTrapezeDocument            0 /* Windows or OS/2 bitmap */
#define gifTrapezeDocument            1 /* GIF */
#define jpegTrapezeDocument           2 /* JPEG */
#define ottpTrapezeDocument           3 /* OTTP URL */
#define pcxTrapezeDocument            4 /* PCX */
#define pngTrapezeDocument            5 /* PNG */
#define tiffTrapezeDocument           6 /* TIFF */
#define trapezeFileTrapezeDocument    7 /* Trapeze File */
#define unknownTrapezeDocument        8 /* Unknown */
#define pdfTrapezeDocument            9 /* PDF */
#define dicomTrapezeDocument          10 /* DICOM */
#define iconTrapezeDocument           11 /* Icon */
#define cursorTrapezeDocument         12 /* Cursor */
#define calsTrapezeDocument           13 /* CALS */
#define pnmTrapezeDocument            14 /* PNM (PBM/PGM/PPM) */
#define rapidRedactTrapezeDocument    15 /* RapidRedact file */
#define jbig2TrapezeDocument          16 /* JBIG2 */
#define rapidRedactPDFTrapezeDocument 17 /* RapidRedact PDF */
#define dwgTrapezeDocument            18 /* DWG */
#define dxfTrapezeDocument            19 /* DXF */
#define wdpTrapezeDocument            20 /* Windows Media Photo */
#define hdpTrapezeDocument            20 /* HD Photo, previously known as
                                            Windows Media Photo */
#define tgaTrapezeDocument            21 /* Truevision TGA */
#define mdiTrapezeDocument            22 /* Microsoft Document Imaging */
#define dwfTrapezeDocument            23 /* DWF */
#define wsqTrapezeDocument            24 /* WSQ */
#define iHeadTrapezeDocument          25 /* IHead */
#define dgnTrapezeDocument            26 /* DGN */
#define errorTrapezeDocument          ((TrapezeDWORD) -1) /* An error
                                                             occurred */

/* Color formats */
#define blackAndWhiteTrapezeColorType 0 /* Black and white */
#define palette2TrapezeColorType      1 /* 2 colors */
#define gray16TrapezeColorType        2 /* 16 grays */
#define palette16TrapezeColorType     3 /* 16 colors */
#define gray256TrapezeColorType       4 /* 256 grays */
#define palette256TrapezeColorType    5 /* 256 colors */
#define rgb24TrapezeColorType         6 /* 24 bit RGB color */
#define ycbcr24TrapezeColorType       7 /* 24 bit YCbCr color */
#define otherTrapezeColorType         8 /* None of the other defined values -
                                           any unknown values should be
                                           treated as equivalent to this to
                                           allow for future additions to this
                                           list */
#define errorTrapezeColorType         ((TrapezeDWORD) -1) /* An error
                                                             occurred */

/* Convert to black and white using 2x2 dither, giving 5 gray levels */
#define changeTrapezeDIBColorBlackAndWhiteDither2x2 ((TrapezeDWORD) -2)

/* Compression types */
#define noTrapezeCompression          0  /* None */
#define packBitsTrapezeCompression    1  /* Macintosh RLE */
#define g4TrapezeCompression          2  /* CCITT Group 4 (T.6) */
#define unknownTrapezeCompression     3  /* Unknown (none of the others
                                                     here) */
#define g3TrapezeCompression          4  /* CCITT Group 3 (T.4) */
#define lzwTrapezeCompression         5  /* LZW */
#define jpeg6TrapezeCompression       6  /* JPEG 6 */
#define jpeg7TrapezeCompression       7  /* JPEG 7 */
#define ccitt1DWTrapezeCompression    8  /* CCITT modified Huffman RLE with
                                            word alignment */
#define thunderscanTrapezeCompression 9  /* Thunderscan RLE */
#define ccitt1DTrapezeCompression     10 /* CCITT modified Huffman RLE */
#define jbig2TrapezeCompression       11 /* JBIG2 compression */

/* OCR Text Definitions and Types */

#define getTrapezeOCRTextCountFailed ((TrapezeDWORD) -1)
#define getTrapezeOCRTextRectsFailed ((TrapezeDWORD) -1)
#define getTrapezeOCRTextTextFailed  ((TrapezeDWORD) -1)

typedef struct TrapezeOCRTextStruct {
  int unused;
} * TrapezeOCRText;
typedef TrapezeOCRText * TrapezeOCRTextPtr;

typedef struct TrapezeOCRTextInfoStruct {
  int unused;
} * TrapezeOCRTextInfo;
typedef TrapezeOCRTextInfo * TrapezeOCRTextInfoPtr;

/* Valid values for size from older versions of Trapeze */
#define trapezeFindOCRTextInfoExRecSize1 16 /* Versions 6.41 or later,
                                               includes up to caseSensitive */

#define trapezeFindOCRTextInfoExPageRange 0x00000001 /* firstPageNo and
                                                        lastPageNo are
                                                        valid */

typedef struct {
  TrapezeWORD size; /* Size of this structure, in bytes */
  TrapezeLPCSTR text; /* Text to find */
  TrapezeBOOL wholeWord; /* TRUE to find whole words, FALSE to find
                            anywhere */
  TrapezeBOOL caseSensitive; /* TRUE for case-sensitive search */
  /* Anything below here requires Trapeze 7.5 or later */
  DWORD flags; /* Flags indicating which fields below are valid, a combination
                  of trapezeFindOCRTextInfoEx* values */
  DWORD firstPageNo; /* First page to search (one based). Ignored if
                        trapezeFindOCRTextInfoExPageRange is not set in
                        flags */
  DWORD lastPageNo; /* Last page to search (one based). Ignored if
                       trapezeFindOCRTextInfoExPageRange is not set in
                       flags */
} TrapezeFindOCRTextInfoExRec, * TrapezeFindOCRTextInfoExPtr;
typedef const TrapezeFindOCRTextInfoExRec * TrapezeFindOCRTextInfoExCPtr;

/* Structure for getOCRTextInfoTrapezeMsg */
typedef struct {
  TrapezeDWORD blockNo; /* Returns the OCR text block containing the word */
  TrapezeDWORD charNo; /* Returns the character number of the start of the
                          word within the OCR text block */
  TrapezeDWORD occurrence; /* Occurrence of the word to get information for */
  TrapezeDWORD pageNo; /* Returns the page containing the word */
  TrapezeOCRTextInfo findInfo; /* Information returned by
                                  findOCRTextInfoTrapezeMsg */
} GetTrapezeOCRTextInfoRec, * GetTrapezeOCRTextInfoPtr;

/* Structure for getOCRTextInfoRectsTrapezeMsg */
typedef struct {
  TrapezeDWORD count; /* Number of rectangles */
  TrapezeDWORD occurrence; /* Occurrence of the word to get information for */
  TrapezeLPRECT rects; /* Rectangles, must be large enough to hold the number
                          of rectangles being returned, NULL = only count is
                          returned. */
  TrapezeOCRTextInfo findInfo; /* Information returned by
                                  findOCRTextInfoTrapezeMsg */
} GetTrapezeOCRTextInfoRectsRec, * GetTrapezeOCRTextInfoRectsPtr;

/* Structure for getOCRTextTextTrapezeMsg */
typedef struct {
  TrapezeDWORD ocrTextNo; /* Zero based block number, must be less than the
                             value returned by getOCRTextCountTrapezeMsg,
                             otherwise ERROR_FILE_NOT_FOUND is returned */
  TrapezeDWORD textSize; /* Specifies the size of text when called if text is
                            not NULL, on return is set to the length of the
                            text, not including the NULL terminator */
  TrapezeLPSTR text; /* Buffer for returned text, or NULL to query text
                        length */
} GetTrapezeOCRTextTextRec, * GetTrapezeOCRTextTextPtr;

/* Structure for getOCRTextRectTrapezeMsg */
typedef struct {
  TrapezeDWORD charNo; /* Character number within the block */
  TrapezeDWORD count; /* When called, specifies the number of characters
                         (starting at charNo) to get rectangles for. */
  TrapezeDWORD ocrTextNo; /* Zero based block number, must be less than the
                             value returned by getOCRTextCountTrapezeMsg,
                             otherwise ERROR_FILE_NOT_FOUND is returned */
  TrapezeDWORD rectNo; /* Zero based rectangle number, must be less than the
                          number of rectangles, otherwise ERROR_FILE_NOT_FOUND
                          is returned. Use getOCRTextRectsTrapezeMsg with
                          rects->rects set to NULL to get the number of
                          rectangles. */
  TrapezeRECT rect; /* Returned rectangle */
} GetTrapezeOCRTextRectRec, * GetTrapezeOCRTextRectPtr;

/* Structure for getOCRTextRectsTrapezeMsg */
typedef struct {
  TrapezeDWORD charNo; /* Character number within the block */
  TrapezeDWORD count; /* When called, specifies the number of characters
                         (starting at charNo) to get rectangles for. On return
                         is set to the number of rectangles. */
  TrapezeDWORD ocrTextNo; /* Zero based block number, must be less than the
                             value returned by getOCRTextCountTrapezeMsg,
                             otherwise ERROR_FILE_NOT_FOUND is returned */
  TrapezeLPRECT rects; /* Rectangles, must be large enough to hold the number
                          of rectangles being returned, NULL = only count is
                          returned. */
} GetTrapezeOCRTextRectsRec, * GetTrapezeOCRTextRectsPtr;

/* Structure for protectOCRTextTrapezeMsg */
typedef struct {
  TrapezeDWORD charNo; /* Character number within the block; ignored if rect
                          is not NULL */
  TrapezeDWORD ocrTextNo; /* Zero based block number, must be less than the
                             value returned by getOCRTextCountTrapezeMsg,
                             otherwise ERROR_FILE_NOT_FOUND is returned;
                             ignored if rect is not NULL */
  TrapezeDWORD count; /* The number of characters to protect; ignored if rect
                         is not NULL */
  TrapezeLPCRECT rect; /* Rectangle containing characters to protect, or NULL
                          to use ocrTextNo, charNo and count. Any characters
                          fully inside this rectangle are protected. */
} ProtectTrapezeOCRTextRec, * ProtectTrapezeOCRTextPtr;
typedef const ProtectTrapezeOCRTextRec * ProtectTrapezeOCRTextCPtr;

/* Structure for replaceOCRTextTextTrapezeMsg */
typedef struct {
  TrapezeDWORD charNo; /* Character number within the block */
  TrapezeDWORD ocrTextNo; /* Zero based block number, must be less than the
                             value returned by getOCRTextCountTrapezeMsg,
                             otherwise ERROR_FILE_NOT_FOUND is returned */
  TrapezeLPCSTR text; /* Text to replace characters with; the number of
                         characters replaced is the number of characters in this
                         string */
} ReplaceTrapezeOCRTextTextRec, * ReplaceTrapezeOCRTextTextPtr;
typedef const ReplaceTrapezeOCRTextTextRec * ReplaceTrapezeOCRTextTextCPtr;

/* Structure for findHighlightTrapezeMsg */
#define trapezeFindHighlightExtend 0x0001 /* Extend current selection */
#define trapezeFindHighlightColor  0x0002 /* color is valid */

typedef struct {
  WORD size; /* Size of this structure, in bytes */
  WORD flags; /* Flags indicating which fields below are valid, a combination
                 of trapezeFindHighlight* values */
  DWORD ocrTextNo; /* Zero based block number, must be less than the
                      value returned by getOCRTextCountTrapezeMsg,
                      otherwise ERROR_FILE_NOT_FOUND is returned */
  DWORD charNo; /* Character number within block */
  DWORD count; /* Number of characters to highlight */
  COLORREF color; /* Color to use for highlighting, 0xffffffff = use default
                     color from preferences, ignored if
                     trapezeFindHighlightColor is not set in flags */
} TrapezeFindHighlightRec, * TrapezeFindHighlightPtr;
typedef const TrapezeFindHighlightRec * TrapezeFindHighlightCPtr;

/* Structure for findOCRTextExTrapezeMsg */

#define findTrapezeOCRTextExHighlightAll 0x0001 /* Highlights all search hits
                                                   on the page */
#define findTrapezeOCRTextExColor        0x0002 /* color is valid */

typedef struct {
  WORD size; /* Size of this structure, in bytes */
  WORD flags; /* Flags indicating which fields below are valid, a combination
                 of findTrapezeOCRTextEx* values */
  const DWORD* pages; /* Pages to search, NULL = search all pages starting at
                         the current page, not NULL = zero terminated list of
                         one-based page numbers to search, and each page in
                         pages will be included in search hits even if none of
                         the words can be found on the page. If pages is not
                         NULL, the pages are searched in ascending order of
                         page number, starting at the lowest numbered page. */
  LPCSTR words; /* An empty string terminated list of words to search for. Any
                   of these words results in a search hit. The search is not
                   case-sensitive */
  COLORREF color; /* The color to use for search hits, 0xffffffff = use
                     default color from preferences, ignored if
                     findTrapezeOCRTextExColor is not set in flags */
} FindTrapezeOCRTextExRec, * FindTrapezeOCRTextExPtr;
typedef const FindTrapezeOCRTextExRec * FindTrapezeOCRTextExCPtr;

/* Structure for reloadTrapezeStampsExMsg */
#define reloadTrapezeStampsExUnicode 0x0001 /* fileName is a Unicode string */
/* If fileName is not NULL, if reloadTrapezeStampsExCustom is not set, the
   stamps are only loaded from that ini file, not the registry, and are all
   read-only; if reloadTrapezeStampsExCustom is set, stamps are also loaded
   from the registry if allowed by the ini file */
#define reloadTrapezeStampsExCustom  0x0002
/* If reloadTrapezeStampsExMerge is set, any ini menu is read-only and
   followed by any custom menu; if reloadTrapezeStampsExMerge is not set, any
   custom menu replaces any ini menu */
#define reloadTrapezeStampsExMerge   0x0004

typedef struct {
  WORD size; /* Size of this structure, in bytes */
  WORD flags; /* Flags indicating which fields below are valid, a combination
                 of reloadTrapezeStampsEx* values */
  LPCSTR fileName; /* NULL or ini file to load stamps from */
} ReloadTrapezeStampsExRec, * ReloadTrapezeStampsExPtr;
typedef const ReloadTrapezeStampsExRec * ReloadTrapezeStampsExCPtr;

/* Bookmark Definitions and Types */

#define expandAllTrapezeBookmarks        ((TrapezeDWORD) -1)
#define getNextTrapezeBookmarkFailed     ((TrapezeDWORD) -2)
#define getTrapezeBookmarkChildFailed    ((TrapezeDWORD) -2)
#define getTrapezeBookmarkCountFailed    ((TrapezeDWORD) -1)
#define getTrapezeBookmarkPageNoFailed   ((TrapezeDWORD) -1)
#define getTrapezeBookmarkParentFailed   ((TrapezeDWORD) -2)
#define getTrapezeBookmarkTextFailed     ((TrapezeDWORD) -1)
#define insertTrapezeBookmarkFailed      ((TrapezeDWORD) -1)
#define multipleTrapezeBookmarksSelected ((TrapezeDWORD) -2)
#define noNextTrapezeBookmark            ((TrapezeDWORD) -1)
#define noTrapezeBookmarkChild           ((TrapezeDWORD) -1)
#define noTrapezeBookmarkParent          ((TrapezeDWORD) -1)
#define noTrapezeBookmarkSelected        ((TrapezeDWORD) -1)

#define defaultTrapezeBookmarkBitmapNo ((TrapezeDWORD) -1)
#define trapezeBookmarkFolder          0

typedef struct TrapezeBookmarksStruct {
  int unused;
} * TrapezeBookmarks;
typedef TrapezeBookmarks * TrapezeBookmarksPtr;

/* Debug log function */
typedef void (TrapezeAPI* TrapezeDebugLogProc)(TrapezeLPCSTR message,
                                               TrapezeLPVOID lParam);

/* Debug log function flags */
#define noLFTrapezeDebugLogProc 0x00000001 /* Any trailing LF is removed from
                                              the message */

/* OCR Definitions */

#define defaultTrapezeOCREngine 0 /* Any available engine */
#define drsTrapezeOCREngine     1 /* DRS (ReadIRIS) */
#define tigerTrapezeOCREngine   2 /* Tiger (Cuneiform) */
#define nuanceTrapezeOCREngine  3 /* Nuance */

#define ocrTrapezeText    0x00 /* Plain text, lines CRLF separated */
#define ocrTrapezeOCRText 0x01 /* OCR text blob, same as that returned by
                                  GetTrapezeTIFFOCRText() */

/* Function called by OCRTrapezeTIFFEx() when a page cannot be OCRed. ifdNo
   specifies the IFD being OCRed. error specifies the error that has occurred.
   errorMessage specifies the message for the error. lParam is the lParam
   parameter passed to OCRTrapezeTIFFEx(). The return value is TRUE to ignore
   the error and continue with the remaining pages, or FALSE to stop OCRing;
   OCRTrapezeTIFFEx() will fail with the specified error. */
typedef TrapezeBOOL (TrapezeAPI* OCRTrapezeTIFFExErrorProc)(TrapezeDWORD
                                                            ifdNo,
                                                            TrapezeDWORD
                                                            error,
                                                            TrapezeLPCSTR
                                                            errorMessage,
                                                            TrapezeLPVOID
                                                            lParam);

/* Valid values for size from older versions of Trapeze */
#define ocrTrapezeTIFFExRecSize1 24 /* Versions 5.3 Beta 5 or later, includes
                                       up to errorProc */
#define ocrTrapezeTIFFExRecSize2 36 /* Versions 7.53 or later, includes up to
                                       lastPageNo */

#define ocrTrapezeTIFFExPageRange 0x00000001 /* firstPageNo and lastPageNo
                                                are valid */
#define ocrTrapezeTIFFExPageList  0x00000002 /* pageList and pageListCount are
                                                valid */
#define ocrTrapezeTIFFExPageFix   0x00000004 /* Fix any page numbering
                                                problems while OCRing
                                                (slower), version 8.392 or
                                                later */

/* TIFF OCR information */
typedef struct {
  TrapezeWORD size; /* Size of this structure, in bytes */
  TrapezeLPCSTR fileName; /* TIFF to OCR */
  TrapezeBOOL replace; /* TRUE to replace existing OCR text */
  TrapezeProgressProc progressProc; /* Progress function, NULL = none */
  TrapezeLPVOID lParam; /* Parameter for progressProc and errorProc */
  OCRTrapezeTIFFExErrorProc errorProc; /* Error function, NULL = none */
  /* Anything below here requires Trapeze 7.53 or later */
  TrapezeDWORD flags; /* Flags indicating which fields below are valid, a
                         combination of ocrTrapezeTIFFEx* values */
  TrapezeDWORD firstPageNo; /* First page to OCR (one based). Ignored if
                               ocrTrapezeTIFFExPageRange is not set in
                               flags */
  TrapezeDWORD lastPageNo; /* Last page to OCR (one based). Ignored if
                              ocrTrapezeTIFFExPageRange is not set in flags */
  /* Anything below here requires Trapeze 7.56 or later */
  TrapezeDWORD pageListCount; /* Number of pages in pageList, ignored if
                                 ocrTrapezeTIFFExPageList is not set in
                                 flags */
  const TrapezeDWORD* pageList; /* Pages to OCR (one based), pageListCount
                                   specifies the number of entries in this
                                   list, ignored if ocrTrapezeTIFFExPageList
                                   is not set in flags */
} OCRTrapezeTIFFExRec, * OCRTrapezeTIFFExPtr;

/* TIFF Definitions and Types */

/* File access modes */
#ifdef _WIN32
#define trapezeReadAccess  GENERIC_READ  /* Open file for reading */
#define trapezeWriteAccess GENERIC_WRITE /* Open file for writing */
#else /*_WIN32*/
#define trapezeReadAccess  0x01 /* Open file for reading */
#define trapezeWriteAccess 0x02 /* Open file for writing */
#endif /*_WIN32*/

/* Returned when GetTrapezeTIFFDocumentPageCount() fails */
#define getTrapezeTIFFDocumentPageCountFailed ((TrapezeDWORD) -1)
#define getTrapezeTIFFIFDSizeFailed           ((TrapezeSIZE_T) -1)
#define setMemoryTrapezeTIFFSizeFailed        ((TrapezeSIZE_T) -1)

#define setMemoryTrapezeTIFFAutoGrow          ((TrapezeSIZE_T) -1)
#define setMemoryTrapezeTIFFNoChange          ((TrapezeSIZE_T) -2)

typedef struct TrapezeTIFFStruct {
  int unused;
} * TrapezeTIFF;
typedef TrapezeTIFF * TrapezeTIFFPtr;

/* Flags for AppendTrapezeOpenTIFFEx() */
/* Change annotations */
#define appendTrapezeOpenTIFFExAnnotations      0x00000001
/* Change bookmarks */
#define appendTrapezeOpenTIFFExBookmarks        0x00000002
/* Change NewSubfileType */
#define appendTrapezeOpenTIFFExNewSubfileType   0x00000004
/* Change page number */
#define appendTrapezeOpenTIFFExPageNumber       0x00000008
/* Change scale */
#define appendTrapezeOpenTIFFExScale            0x00000010
/* Change resolution */
#define appendTrapezeOpenTIFFExResolution       0x00000020
/* Change compression */
#define appendTrapezeOpenTIFFExCompression      0x00000040
/* Change date/time */
#define appendTrapezeOpenTIFFExDateTime         0x00000080
/* Use compression options when appendTrapezeOpenTIFFExCompression is set */
#define appendTrapezeOpenTIFFExCompressOptions  0x00000100
/* Call progressProc during append */
#define appendTrapezeOpenTIFFExProgress         0x00000200
/* Return paramError for ERROR_INVALID_PARAMETER failure */
#define appendTrapezeOpenTIFFExParamError       0x00000400
/* Append JPEG as type 6 rather than type 7 */
#define appendTrapezeOpenTIFFExJPEG6            0x00000800
/* Change OCR text */
#define appendTrapezeOpenTIFFExOCRText          0x00001000
/* Change document name */
#define appendTrapezeOpenTIFFExDocName          0x00002000
/* Change image description */
#define appendTrapezeOpenTIFFExImageDescription 0x00004000
/* Change make */
#define appendTrapezeOpenTIFFExMake             0x00008000
/* Change model */
#define appendTrapezeOpenTIFFExModel            0x00010000
/* Change software */
#define appendTrapezeOpenTIFFExSoftware         0x00020000
/* Change artist */
#define appendTrapezeOpenTIFFExArtist           0x00040000
/* Change host computer */
#define appendTrapezeOpenTIFFExHostComputer     0x00080000
/* Change copyright */
#define appendTrapezeOpenTIFFExCopyright        0x00100000
/* Remove any orientation */
#define appendTrapezeOpenTIFFExRemoveOrientation 0x00200000
/* All strings are Unicode */
#define appendTrapezeOpenTIFFExUnicode          0x00400000
/* srcDIB is a pointer to a packed DIB, rather than a handle */
#define appendTrapezeOpenTIFFExSrcDIBPtr        0x00800000
/* Set orientation tag (doesn't rotate data) */
#define appendTrapezeOpenTIFFExOrientation      0x01000000

/* Parameter errors for AppendTrapezeOpenTIFFEx() */
/* This error will never be returned - set paramError to this value and set
   appendTrapezeOpenTIFFExParamError before calling AppendTrapezeOpenTIFFEx()
   to determine if the error is because the structure size is invalid. */
#define appendTrapezeOpenTIFFExStructSizeError          0
/* More or less than one of srcFileName, srcTIFF and srcDIB is non-NULL */
#define appendTrapezeOpenTIFFExSrcError                 1
/* An invalid flag was used */
#define appendTrapezeOpenTIFFExFlagError                2
/* The source IFD number is invalid */
#define appendTrapezeOpenTIFFExSrcIDFNoError            3
/* Bookmarks can only be written to the first IFD */
#define appendTrapezeOpenTIFFExBookmarkIFDError         4
/* An invalid NewSubfileType flag was used */
#define appendTrapezeOpenTIFFExNewSubfileTypeError      5
/* Compression is invalid */
#define appendTrapezeOpenTIFFExCompressionError         6
/* Date/time is invalid */
#define appendTrapezeOpenTIFFExDateTimeError            7
/* JPEG options are invalid */
#define appendTrapezeOpenTIFFExJPEGOptionsError         8
/* compressOptions must be NULL for all except JPEG */
#define appendTrapezeOpenTIFFExCompressOptionsError     9
/* Compression is invalid for the source image color type */
#define appendTrapezeOpenTIFFExCompressColorError       10
/* Scale is invalid */
#define appendTrapezeOpenTIFFExScaleError               11
/* Scale units are invalid */
#define appendTrapezeOpenTIFFExScaleUnitError           12
/* Resolution is invalid */
#define appendTrapezeOpenTIFFExResolutionError          13
/* Resolution units are invalid */
#define appendTrapezeOpenTIFFExResolutionUnitError      14
/* Structure size is invalid for specified flags */
#define appendTrapezeOpenTIFFExStructSizeFlagsError     15
/* srcDIB contains invalid data */
#define appendTrapezeOpenTIFFExSrcDIBError              16
/* Orientation is invalid */
#define appendTrapezeOpenTIFFExOrientationError         17
/* Annotations are invalid */
#define appendTrapezeOpenTIFFExAnnotationError          18

typedef struct {
  TrapezeBYTE day; /* Day (1-31) */
  TrapezeBYTE hour; /* Hour (0-23) */
  TrapezeBYTE minute; /* Minute (0-59) */
  TrapezeBYTE month; /* Month (1-12) */
  TrapezeBYTE second; /* Second (0-59) */
  TrapezeWORD year; /* Year */
} TrapezeTIFFDateTimeRec, * TrapezeTIFFDateTimePtr;

/* TIFF appending information.
   Exactly one of srcFileName, srcTIFF or srcDIB must be non-NULL.
   If srcFileName is a JPEG and appendTrapezeOpenTIFFExCompression is not set,
   the JPEG is appended to the TIFF as-is, without being recompressed. If
   appendTrapezeOpenTIFFExJPEG6 is set, the JPEG is append as a type 6 TIFF
   JPEG, otherwise it is appended as a type 7 TIFF JPEG.
   When appendTrapezeOpenTIFFExCompression and
   appendTrapezeOpenTIFFExCompressOptions are both set, compressOptions
   can be NULL to use default compression, otherwise it depends on
   the value of compression:
     - jpeg6TrapezeCompression or jpeg7TrapezeCompression, compressOptions is
       a TrapezeJPEGOptionsPtr
     - any other value, compressOptions must be NULL
   */
typedef struct {
  TrapezeWORD size; /* Size of this structure, in bytes */
  TrapezeLPCSTR srcFileName; /* File containing source TIFF or JPEG,
                                NULL = none */
  TrapezeTIFF srcTIFF; /* Source TIFF, NULL = none */
  TrapezeDWORD srcIFDNo; /* Source IFD number (one based) when srcFileName or
                            srcTIFF not NULL (ignored if srcFileName is a
                            JPEG) */
  TrapezeDWORD flags; /* Flags from above specifying fields below, a
                         combination of appendTrapezeOpenTIFFEx* values */
  TrapezeAnnotations annotations; /* NULL = remove, used when
                                     appendTrapezeOpenTIFFExAnnotations is
                                     set */
  TrapezeDWORD annotationsSize; /* Size of annotations, in bytes, used when
                                   annotations is not NULL and
                                   appendTrapezeOpenTIFFExAnnotations is
                                   set */
  TrapezeTIFFResolutionScaleCPtr scale; /* NULL = remove, used when
                                           appendTrapezeOpenTIFFExScale is
                                           set */
  TrapezeHGLOBAL bookmarks; /* NULL = remove, used when
                               appendTrapezeOpenTIFFExBookmarks is set */
  TrapezeDWORD bookmarksSize; /* Size of bookmarks, in bytes, used when
                                 bookmarks is not NULL and
                                 appendTrapezeOpenTIFFExBookmarks is set */
  TrapezeDWORD pageNo; /* 0 based, ((pageNo >= pageCount) and
                          (pageCount != 0)) = remove, used when
                          appendTrapezeOpenTIFFExPageNumber is set */
  TrapezeDWORD pageCount; /* 0 = unknown, used when
                             appendTrapezeOpenTIFFExPageNumber is set */
  TrapezeDWORD newSubfileType; /* One of *TrapezeNewSubfileType, used when
                                  appendTrapezeOpenTIFFExNewSubfileType is
                                  set */
  TrapezeTIFFResolutionScaleCPtr resolution; /* Can't be removed, used when
                                             appendTrapezeOpenTIFFExResolution
                                                is set */
  TrapezeWORD compression; /* One of *TrapezeCompression, used when
                              appendTrapezeOpenTIFFExCompression is set */
  TrapezeHGLOBAL srcDIB; /* Source DIB, NULL = none */
  TrapezeTIFFDateTimePtr dateTime; /* NULL or (year = 0) = remove, used when
                                      appendTrapezeOpenTIFFExDateTime is
                                      set */
  TrapezeLPCVOID compressOptions; /* Compression options, used when
                                     appendTrapezeOpenTIFFExCompression and
                                     appendTrapezeOpenTIFFExCompressOptions
                                     are set - see above for details */
  TrapezeProgressProc progressProc; /* If not NULL and
                                       appendTrapezeOpenTIFFExProgress is set,
                                       called periodically during the append
                                       with lParam passed to it */
  TrapezeLPVOID lParam; /* Parameter passed to progressProc, used when
                           appendTrapezeOpenTIFFExProgress is set and
                           progressProc is not NULL */
  TrapezeDWORD paramError; /* Set when appendTrapezeOpenTIFFExParamError is
                              set and the function fails with
                              ERROR_INVALID_PARAMETER. It is set to one of
                              appendTrapezeOpenTIFFEx*Error. This value will
                              remain unchanged if the structure size is
                              incorrect. */
  TrapezeOCRText ocrText; /* NULL = remove, used when
                             appendTrapezeOpenTIFFExOCRText is set */
  TrapezeDWORD ocrTextSize; /* Size of OCRText, in bytes, used when ocrText is
                               not NULL and appendTrapezeOpenTIFFExOCRText is
                               set */
  TrapezeLPCSTR docName; /* NULL or "" = remove, used when
                            appendTrapezeOpenTIFFExDocName is set */
  TrapezeLPCSTR imageDescription; /* NULL or "" = remove, used when
                                     appendTrapezeOpenTIFFExImageDescription
                                     is set */
  TrapezeLPCSTR make; /* NULL or "" = remove, used when
                         appendTrapezeOpenTIFFExMake is set */
  TrapezeLPCSTR model; /* NULL or "" = remove, used when
                          appendTrapezeOpenTIFFExModel is set */
  TrapezeLPCSTR software; /* NULL or "" = remove, used when
                             appendTrapezeOpenTIFFExSoftware is set */
  TrapezeLPCSTR artist; /* NULL or "" = remove, used when
                           appendTrapezeOpenTIFFExArtist is set */
  TrapezeLPCSTR hostComputer; /* NULL or "" = remove, used when
                                 appendTrapezeOpenTIFFExHostComputer is set */
  TrapezeLPCSTR copyright; /* NULL or "" = remove, used when
                              appendTrapezeOpenTIFFExCopyright is set */
  /* Anything below here requires Trapeze 8.21 or later */
  TrapezeWORD orientation; /* One of *TrapezeOrientation from below, used when
                              appendTrapezeOpenTIFFExOrientation is set */
} AppendTrapezeOpenTIFFExRec, * AppendTrapezeOpenTIFFExPtr;
typedef const AppendTrapezeOpenTIFFExRec * AppendTrapezeOpenTIFFExCPtr;

/* TIFF Document Types */

#define getTrapezeTIFFDocumentIFDCountFailed 0

typedef struct TrapezeTIFFDocumentStruct {
  int unused;
} * TrapezeTIFFDocument;
typedef TrapezeTIFFDocument * TrapezeTIFFDocumentPtr;

#define openTrapezeTIFFDocumentExUnicode  0x0001 /* fileName is a Unicode
                                                    string */
/* Internal annotations are only read if no .art file is present, cannot be
   set if openTrapezeTIFFDocumentExArtMerge is set */
#define openTrapezeTIFFDocumentExArtFirst 0x0002
/* Internal annotations are merged with .art file annotations, cannot be set
   if openTrapezeTIFFDocumentExArtFirst is set */
#define openTrapezeTIFFDocumentExArtMerge 0x0004

typedef struct {
  TrapezeWORD size; /* Size of this structure, in bytes */
  TrapezeWORD flags; /* Flags, a combination of openTrapezeTIFFDocumentEx*
                        values */
  TrapezeLPCSTR fileName; /* File to open */
} OpenTrapezeTIFFDocumentExRec, * OpenTrapezeTIFFDocumentExPtr;
typedef const OpenTrapezeTIFFDocumentExRec * OpenTrapezeTIFFDocumentExCPtr;

#define reducedTrapezeNewSubfileType      0x00000001L
#define pageTrapezeNewSubfileType         0x00000002L
#define transparencyTrapezeNewSubfileType 0x00000004L

/* The 0th row represents the visual top of the image, and the 0th column
   represents the visual left-hand side */
#define topLeftTrapezeOrientation     1
/* The 0th row represents the visual top of the image, and the 0th column
   represents the visual right-hand side */
#define topRightTrapezeOrientation    2
/* The 0th row represents the visual bottom of the image, and the 0th column
   represents the visual right-hand side */
#define bottomRightTrapezeOrientation 3
/* The 0th row represents the visual bottom of the image, and the 0th column
   represents the visual left-hand side */
#define bottomLeftTrapezeOrientation  4
/* The 0th row represents the visual left-hand side of the image, and the 0th
   column represents the visual top */
#define leftTopTrapezeOrientation     5
/* The 0th row represents the visual right-hand side of the image, and the 0th
   column represents the visual top */
#define rightTopTrapezeOrientation    6
/* The 0th row represents the visual right-hand side of the image, and the 0th
   column represents the visual bottom */
#define rightBottomTrapezeOrientation 7
/* The 0th row represents the visual left-hand side of the image, and the 0th
   column represents the visual bottom */
#define leftBottomTrapezeOrientation  8

#define whiteZeroTrapezePhotoInt    0
#define blackZeroTrapezePhotoInt    1
#define rgbTrapezePhotoInt          2
#define paletteTrapezePhotoInt      3
#define transparencyTrapezePhotoInt 4
#define separatedTrapezePhotoInt    5
#define ycbcrTrapezePhotoInt        6
#define cielabTrapezePhotoInt       8
#define itulabTrapezePhotoInt       10

/* Valid values for size from older versions of Trapeze */
#define trapezeTIFFDocumentIFDInfoSize1 24 /* Versions 2.0 or later, includes
                                              up to photoInt */
#define trapezeTIFFDocumentIFDInfoSize2 28 /* Versions 6.51 or later, includes
                                              up to orientationSwap */

/* This structure requires Trapeze 2.0 or later */
typedef struct {
  TrapezeDWORD size; /* Size of this structure, in bytes */
  TrapezeDWORD pageNo; /* Page number (one based) */
  TrapezeDWORD width; /* Image width */
  TrapezeDWORD height; /* Image height */
  TrapezeDWORD newSubfileType; /* One of *TrapezeNewSubfileType from above */
  TrapezeDWORD photoInt; /* Color space, one of *TrapezePhotoInt from above */
  /* Anything below here requires Trapeze 6.51 or later */
  TrapezeBOOL orientationSwap; /* TRUE if width and height have been swapped
                                  because of orientation */
  /* Anything below here requires Trapeze 7.17 or later */
  TrapezeDWORD orientation; /* One of *TrapezeOrientation from above */
} TrapezeTIFFDocumentIFDInfoRec, * TrapezeTIFFDocumentIFDInfoPtr;

/* Trapeze File Types */

typedef struct TrapezeFileStruct {
  int unused;
} * TrapezeFile;
typedef TrapezeFile * TrapezeFilePtr;

/* Barcode Types */

typedef struct TrapezeBarcodeStruct {
  int unused;
} * TrapezeBarcodes;

/* Types for finding barcodes */
typedef struct {
  TrapezeWORD size; /* Size of this structure, in bytes */
  TrapezeRECT rect; /* Rectangle to search, empty = entire image */
  TrapezeDWORD pageNo; /* One-based page number to search, 0 = current page */
  TrapezeDWORD flags; /* A combination of findTrapezeBarcode* values */
  int xIncrement; /* Horizontal search jump */
  int yIncrement; /* Vertical search jump */
  TrapezeWORD searchHeight; /* Maximum search height, zero = no limit */
  TrapezeWORD minHeight; /* Minimum height for bars */
  TrapezeWORD maxCount; /* Maximum number of barcodes, zero = no limit */
  TrapezeBarcodes barcodes; /* Found barcodes, NULL = none or error */
  /* Anything below here requires Trapeze 8.05 or later */
  TrapezeWORD minWidth; /* Minimum number of characters in a barcode. Ignored
                           if findTrapezeBarcodeMinWidth is not set in
                           flags */
} TrapezeFindBarcodesRec, * TrapezeFindBarcodesPtr;

/* Barcode flags */

/* Finds right way up barcodes (left to right) */
#define findTrapezeBarcode0                       0x00000001
/* Finds 90 degree rotated barcodes (top to bottom) */
#define findTrapezeBarcode90                      0x00000002
/* Finds upside down barcodes (right to left) */
#define findTrapezeBarcode180                     0x00000004
/* Finds 270 degree rotated barcodes (bottom to top) */
#define findTrapezeBarcode270                     0x00000008
/* Finds Code 39 barcodes */
#define findTrapezeBarcodeCode39                  0x00000010
/* Finds Extended Code 39 barcodes - only used if findTrapezeBarcodeCode39 is
   set */
#define findTrapezeBarcodeExtendedCode39          0x00000020
/* Finds Code 39 or Extended Code 39 barcodes with a checksum - only used if
   findTrapezeBarcodeCode39 is set */
#define findTrapezeBarcodeCode39Checksum          0x00000040
/* Finds Interleaved 2 of 5 barcodes */
#define findTrapezeBarcodeInterleaved2Of5         0x00000080
/* Finds Interleaved 2 of 5 barcodes with a checksum - only used if
   findTrapezeBarcodeInterleaved2Of5 is set */
#define findTrapezeBarcodeInterleaved2Of5Checksum 0x00000100
/* Finds barcodes with invalid characters, otherwise they are ignored */
#define findTrapezeBarcodeInvalidChars            0x00000200
/* Finds UPC A barcodes */
#define findTrapezeBarcodeUPCA                    0x00000400
/* Finds UPC E barcodes */
#define findTrapezeBarcodeUPCE                    0x00000800
/* Finds EAN 8 barcodes */
#define findTrapezeBarcodeEAN8                    0x00001000
/* Finds EAN 13 barcodes */
#define findTrapezeBarcodeEAN13                   0x00002000
/* Finds Code 128 barcodes */
#define findTrapezeBarcodeCode128                 0x00004000
/* Finds Codabar barcodes */
#define findTrapezeBarcodeCodabar                 0x00008000
/* Finds barcodes without quiet zones, otherwise they are ignored */
#define findTrapezeBarcodeNoQuietZones            0x00010000
/* minWidth is valid */
#define findTrapezeBarcodeMinWidth                0x00020000

/* Barcode definitions */
#define getTrapezeBarcodeCountFailed     ((TrapezeDWORD) -1)
#define getTrapezeBarcodeDirectionFailed ((TrapezeDWORD) 0)
#define getTrapezeBarcodeTextFailed      ((TrapezeDWORD) -1)
#define getTrapezeBarcodeTypeFailed      ((TrapezeDWORD) 0)

/* License definitions and types */

#define annotationsTrapezeLicense      0x00000001
#define annotationModifyTrapezeLicense 0x00000002
#define barcodesTrapezeLicense         0x00000004
#define bookmarkModifyTrapezeLicense   0x00000008
#define bookmarksTrapezeLicense        0x00000010
#define evaluationTrapezeLicense       0x00000020
#define findTrapezeLicense             0x00000040
#define folderModifyTrapezeLicense     0x00000080
#define fullDllTrapezeLicense          0x00000100
#define hideUnlicensedTrapezeLicense   0x00000200
#define licensedTrapezeLicense         0x00000400
#define lrfTrapezeLicense              0x00000800
#define lzwTrapezeLicense              0x00001000
#define measuringTrapezeLicense        0x00002000
#define ms61TrapezeLicense             0x00004000
#define odmaTrapezeLicense             0x00008000
#define ottpTrapezeLicense             0x00010000
#define pageModifyTrapezeLicense       0x00020000
#define pasteboardTrapezeLicense       0x00040000
#define printOptionsTrapezeLicense     0x00080000
#define saveSelectionTrapezeLicense    0x00100000
#define thumbnailModifyTrapezeLicense  0x00200000
#define twainTrapezeLicense            0x00400000
#define silentOneTrapezeLicense        0x00800000
#define noLocalSaveTrapezeLicense      0x01000000
#define ocrTrapezeLicense              0x02000000
#define iManageTrapezeLicense          0x04000000
#define functionSheetsTrapezeLicense   0x08000000
#define pdfTrapezeLicense              0x10000000
#define dicomTrapezeLicense            0x20000000
#define trimTrapezeLicense             0x40000000
#define redactionTrapezeLicense        0x80000000

#define onstreamTrapezeProductName 0 /* Onstream Trapeze */
#define trapezeLiteProductName     1 /* Trapeze Desktop */
#define trapezeProProductName      2 /* Trapeze Desktop */
#define trapezeViewProductName     3 /* Trapeze Lite */
#define idoxImageViewerProductName 4 /* IDOX Image Viewer */
#define customTrapezeProductName   5 /* Onstream Trapeze or custom */

/* Valid values for size from older versions of Trapeze */
#define trapezeLicenseSize1 16 /* Versions 3.4 or later, includes up to
                                  expiryDate */
#define trapezeLicenseSize2 28 /* Versions 6.0 Beta 4 or later, includes up to
                                  key */

/* Structure for GetTrapezeLicense() - requires Trapeze 3.4 or later */
typedef struct {
  TrapezeWORD size; /* Size of this structure, in bytes */
  TrapezeDWORD licenses; /* License flags, from above */
  TrapezeWORD minHighwireVersion; /* 0xabcd, a = major, b = minor, c = bugfix,
                                     d = release no */
  TrapezeBYTE product; /* Product name, from above */
  struct {
    TrapezeBYTE day; /* 1-31, or 0 = no expiry */
    TrapezeBYTE month; /* 1-12, or 0 = no expiry */
    TrapezeWORD year; /* 0-65535 */
  } expiryDate; /* Last day before expiry */
  /* Anything below here requires Trapeze 6.0 Beta 4 or later */
  TrapezeLPCSTR customer; /* Customer of license to check, NULL = check
                             current license */
  TrapezeLPCSTR location; /* Location of license to check, NULL = check
                             current license */
  TrapezeLPCSTR key; /* Key of license to check, NULL = check current
                        license */
  /* Anything below here requires Trapeze 6.43 Beta 4 or later */
  TrapezeBOOL anyLicense; /* TRUE to return information on any license, FALSE
                             to return information for a DLL license only */
} TrapezeLicenseRec, * TrapezeLicensePtr;

/* Photometric interpretation values */
#define whiteZeroTrapezeTIFFPhotoInt    0 /* Black & white/gray with white at
                                             zero, black at 2**n-1 */
#define blackZeroTrapezeTIFFPhotoInt    1 /* Black & white/gray with black at
                                             zero, white at 2**n-1 */
#define rgbTrapezeTIFFPhotoInt          2 /* Each pixel is an RGB color */
#define paletteTrapezeTIFFPhotoInt      3 /* Each pixel is a palette entry */
#define transparencyTrapezeTIFFPhotoInt 4 /* Image is a transparency */
#define separatedTrapezeTIFFPhotoInt    5 /* Each pixel is a separated color
                                             (typically CMYK) */
#define ycbcrTrapezeTIFFPhotoInt        6 /* Each pixel is a YCbCr color */
#define cielabTrapezeTIFFPhotoInt       8 /* Each pixel is a CIE L*a*b*
                                             color */
#define unknownTrapezeTIFFPhotoInt      ((TrapezeDWORD) 0xFFFFFFFE)
#define errorTrapezeTIFFPhotoInt        ((TrapezeDWORD) 0xFFFFFFFF)

/* Types for BMP compression */

typedef struct {
  TrapezeWORD size; /* Size of this structure, in bytes */
  TrapezeBOOL os2; /* TRUE for an OS/2 bitmap, FALSE for a Windows bitmap,
                      default = FALSE */
} TrapezeBMP1OptionsRec, * TrapezeBMP1OptionsPtr;
typedef const TrapezeBMP1OptionsRec * TrapezeBMP1OptionsCPtr;

typedef struct {
  TrapezeWORD size; /* Size of this structure, in bytes */
  TrapezeBOOL os2; /* TRUE for an OS/2 bitmap, FALSE for a Windows bitmap,
                      default = FALSE */
  TrapezeBOOL rle; /* TRUE for RLE compression, FALSE for no compression,
                      default = FALSE. Must be FALSE if os2 is TRUE. */
} TrapezeBMP4OptionsRec, * TrapezeBMP4OptionsPtr;
typedef const TrapezeBMP4OptionsRec * TrapezeBMP4OptionsCPtr;

typedef struct {
  TrapezeWORD size; /* Size of this structure, in bytes */
  TrapezeBOOL os2; /* TRUE for an OS/2 bitmap, FALSE for a Windows bitmap,
                      default = FALSE */
  TrapezeBOOL rle; /* TRUE for RLE compression, FALSE for no compression,
                      default = FALSE. Must be FALSE if os2 is TRUE. */
} TrapezeBMP8OptionsRec, * TrapezeBMP8OptionsPtr;
typedef const TrapezeBMP8OptionsRec * TrapezeBMP8OptionsCPtr;

typedef struct {
  TrapezeWORD size; /* Size of this structure, in bytes */
  TrapezeBOOL os2; /* TRUE for an OS/2 bitmap, FALSE for a Windows bitmap,
                      default = FALSE */
} TrapezeBMP24OptionsRec, * TrapezeBMP24OptionsPtr;
typedef const TrapezeBMP24OptionsRec * TrapezeBMP24OptionsCPtr;

typedef struct {
  TrapezeWORD size; /* Size of this structure, in bytes */
  TrapezeBMP1OptionsPtr bmp1; /* 1 bit BMP options, NULL = use defaults */
  TrapezeBMP4OptionsPtr bmp4; /* 4 bit BMP options, NULL = use defaults */
  TrapezeBMP8OptionsPtr bmp8; /* 1 bit BMP options, NULL = use defaults */
  TrapezeBMP24OptionsPtr bmp24; /* 24 bit BMP options, NULL = use defaults */
} TrapezeBMPOptionsRec, * TrapezeBMPOptionsPtr;
typedef const TrapezeBMPOptionsRec * TrapezeBMPOptionsCPtr;

/* Types for TIFF compression */

typedef struct {
  TrapezeWORD size; /* Size of this structure, in bytes */
  TrapezeBOOL reverseBits; /* TRUE for reverse the bit order, FALSE for normal
                              bit order (default = FALSE) */
} TrapezeTIFFNoCompressionOptionsRec, * TrapezeTIFFNoCompressionOptionsPtr;
typedef const TrapezeTIFFNoCompressionOptionsRec
  * TrapezeTIFFNoCompressionOptionsCPtr;

typedef struct {
  TrapezeWORD size; /* Size of this structure, in bytes */
  TrapezeBOOL blackZeroToWhiteZero; /* TRUE to write black is zero images as
                                       white is zero */
  TrapezeBOOL reverseBits; /* TRUE for reverse the bit order, FALSE for normal
                              bit order (default = FALSE) */
} TrapezeTIFFG4CompressionOptionsRec, * TrapezeTIFFG4CompressionOptionsPtr;
typedef const TrapezeTIFFG4CompressionOptionsRec
  * TrapezeTIFFG4CompressionOptionsCPtr;

typedef struct {
  TrapezeWORD size; /* Size of this structure, in bytes */
  TrapezeWORD parameterK; /* The number of 2D lines (K - 1) following each 1D
                             line, 0 = use default, or 2-9999 (default = 0).
                             Ignored if coding2D is FALSE. */
  TrapezeBOOL blackZeroToWhiteZero; /* TRUE to write black is zero images as
                                       white is zero */
  TrapezeBOOL coding2D; /* TRUE to enable 2D encoding, FALSE for 1D encoding
                           (default = FALSE) */
  TrapezeBOOL fillBits; /* TRUE to add fill bits to make EOLs end on a byte
                           boundary, FALSE for no fill bits
                           (default = FALSE). */
  TrapezeBOOL reverseBits; /* TRUE for reverse the bit order, FALSE for normal
                              bit order (default = FALSE) */
} TrapezeTIFFG3CompressionOptionsRec, * TrapezeTIFFG3CompressionOptionsPtr;
typedef const TrapezeTIFFG3CompressionOptionsRec
  * TrapezeTIFFG3CompressionOptionsCPtr;

typedef struct {
  TrapezeWORD size; /* Size of this structure, in bytes */
  TrapezeBOOL blackZeroToWhiteZero; /* TRUE to write black is zero images as
                                       white is zero */
  TrapezeBOOL reverseBits; /* TRUE for reverse the bit order, FALSE for normal
                              bit order (default = FALSE) */
} TrapezeTIFFCCITT1DCompressionOptionsRec,
  * TrapezeTIFFCCITT1DCompressionOptionsPtr;
typedef const TrapezeTIFFCCITT1DCompressionOptionsRec
  * TrapezeTIFFCCITT1DCompressionOptionsCPtr;

typedef struct {
  TrapezeWORD size; /* Size of this structure, in bytes */
  TrapezeWORD compression; /* Compression, one of noTrapezeCompression,
                              packBitsTrapezeCompression,
                              g4TrapezeCompression, g3TrapezeCompression,
                              lzwTrapezeCompression, jpeg6TrapezeCompression,
                              jpeg7TrapezeCompression or
                              ccitt1DTrapezeCompression. Must be appropriate
                              for the image being compressed. The default
                              depends on the image:
                              2 colors, default is g4TrapezeCompression.
                              16 grays or colors, default is
                                lzwTrapezeCompression if LZW is licensed
                                or packBitsTrapezeCompression if LZW is not
                                licensed.
                              256 grays, default is jpeg7TrapezeCompression.
                              256 colors, default is
                                lzwTrapezeCompression if LZW is licensed
                                or packBitsTrapezeCompression if LZW is not
                                licensed.
                              24 bit color, default is
                                jpeg7TrapezeCompression.
                              Anything else, default is
                                lzwTrapezeCompression if LZW is licensed
                                or packBitsTrapezeCompression if LZW is not
                                licensed. */
  TrapezeLPCVOID options; /* Specifies additional information based on
                             compression, or NULL to use defaults. When
                             compression is:
                             noTrapezeCompression, options is NULL, or a
                               TrapezeTIFFNoCompressionOptionsPtr only if the
                               image has two colors.
                             packBitsTrapezeCompression, options must be NULL.
                             g4TrapezeCompression, options is NULL or a
                               TrapezeTIFFG4CompressionOptionsPtr. The image
                               must have two colors.
                             g3TrapezeCompression, options is NULL or a
                               TrapezeTIFFG3CompressionOptionsPtr. The image
                               must have two colors.
                             lzwTrapezeCompression, options must be NULL.
                             jpeg6TrapezeCompression or
                             jpeg7TrapezeCompression, options is NULL or a
                               TrapezeJPEGOptionsPtr. If the image has <= 256
                               colors and all pixels are gray it will be saved
                               as an 8 bit gray JPEG, otherwise it will be
                               saved as a 24 bit color JPEG.
                             ccitt1DTrapezeCompression, options is NULL or a
                               TrapezeTIFFCCITT1DCompressionOptionsPtr. The
                               image must have two colors. */
} TrapezeTIFFOptionsRec, * TrapezeTIFFOptionsPtr;
typedef const TrapezeTIFFOptionsRec * TrapezeTIFFOptionsCPtr;

/* DICOM Definitions and Types */

#ifdef _WIN64
#define undefinedTrapezeDICOMLength 0xFFFFFFFFFFFFFFFF /* Pixel data has
                                                          undefined length */
#define noTrapezeDICOMPixelData     0xFFFFFFFFFFFFFFFE /* No pixel data to
                                                          remove */
#else
#define undefinedTrapezeDICOMLength 0xFFFFFFFF /* Pixel data has undefined
                                                  length */
#define noTrapezeDICOMPixelData     0xFFFFFFFE /* No pixel data to remove */
#endif /*_WIN64*/

#define getTrapezeDICOMDataRaw  0x00000001 /* The data set is returned as a
                                              DICOM data set */
#define getTrapezeDICOMDataText 0x00000002 /* The data set is returned as a
                                              NULL terminated string for
                                              display */

typedef struct TrapezeDICOMStruct {
  int unused;
} * TrapezeDICOM;

#define trapezeDICOMInfoPixelDataLength 0x00000001 /* Get pixel data length */

typedef struct {
  TrapezeWORD size; /* Size of this structure, in bytes */
  TrapezeWORD compression; /* Compression, one of *TrapezeCompression;
                              jpeg6TrapezeCompression and
                              jpeg7TrapezeCompression are equivalent and
                              indicate DICOM JPEG compression;
                              packBitsTrapezeCompression indicates DICOM RLE
                              compression */
  TrapezeDWORD flags; /* Flags indicating what information is required, a
                         combination of trapezeDICOMInfo* values */
  TrapezeDWORD frameCount; /* Number of frames in the DICOM file, 0 = no image
                              data. pixelDataLength, scale, width, height and
                              dibColorType are only returned if
                              frameCount > 0. */
  TrapezeSIZE_T pixelDataLength; /* Size of pixel data, in bytes, returned if
                                    trapezeDICOMInfoPixelDataLength flag is
                                    set */
  TrapezeTIFFResolutionScaleRec scale; /* Scale information, denominator=0 or
                                          numerator=0 indicates invalid
                                          value */
  TrapezeWORD width; /* Image width, in pixels */
  TrapezeWORD height; /* Image height, in pixels */
  TrapezeDWORD dibColorType; /* Color format that a returned DIB will have,
                                one of *TrapezeColorType,
                                errorTrapezeColorType indicates unsupported
                                image type */
} TrapezeDICOMInfoRec, * TrapezeDICOMInfoPtr;

/* DIB reading information */

#define readTrapezeDIBExUnicode 0x00000001 /* All strings are Unicode */
#define readTrapezeDIBExBurnIn  0x00000002 /* Annotations are burned in */

#define readTrapezeDIBExSize1 28 /* Versions 6.4 or later, includes up to
                                    lParam */
#define readTrapezeDIBExSize2 32 /* Versions 8.2 Beta 6 or later, includes up
                                    to flags */

typedef struct {
  TrapezeWORD size; /* Size of this structure, in bytes */
  TrapezeLPCSTR fileName; /* File to read the image from. This is a
                             TrapezeLPCWSTR if readTrapezeDIBExUnicode is set
                             in flags. */
  TrapezeDWORD pageNo; /* Page number (one based) of the document to read the
                          image from; for a TIFF the full size image for the
                          page will be retrieved */
  TrapezeDWORD offset; /* Offset within the file to start reading the image
                          from */
  TrapezeDWORD offsetHigh; /* High 32 bits of offset within the file to start
                              reading the image from; on Windows 95/98/Me,
                              this must be zero */
  TrapezeProgressProc progressProc; /* If not NULL, it is called periodically
                                       with lParam passed to it; if it returns
                                       FALSE the function fails with the error
                                       ERROR_CANCELLED */
  TrapezeLPVOID lParam; /* Parameter passed to progressProc */
  /* Anything below here requires Trapeze 8.2 Beta 6 or later */
  TrapezeDWORD flags; /* Flags indicating which fields below are valid, a
                         combination of readTrapezeDIBEx* values */
  /* Anything below here requires Trapeze 8.725 or later */
  TrapezeDWORD burnInFlags; /* A combination of *TrapezeBurnIn values, as
                               described in BurnInTrapezeAnnotationsExRec,
                               ignored if readTrapezeDIBExBurnIn is not set in
                               flags */
} ReadTrapezeDIBExRec;
typedef const ReadTrapezeDIBExRec * ReadTrapezeDIBExCPtr;

/* Ignore any orientation */
#define readTrapezeOpenTIFFDIBExIgnoreOrientation 0x00000001
/* Annotations are burned in */
#define readTrapezeOpenTIFFDIBExBurnIn            0x00000002

#define readTrapezeOpenTIFFDIBExSize1 24 /* Versions 6.51 or later, includes
                                            up to flags */

typedef struct {
  TrapezeWORD size; /* Size of this structure, in bytes */
  TrapezeTIFF tiff; /* TIFF to read the DIB from */
  TrapezeDWORD ifdNo; /* The one-based IFD number to read the image from */
  TrapezeProgressProc progressProc; /* If progressProc is not NULL, it is
                                       called periodically with lParam passed
                                       to it; if it returns FALSE the function
                                       fails with the error
                                       ERROR_CANCELLED. */
  TrapezeLPVOID lParam; /* Parameter passed to progressProc */
  TrapezeDWORD flags; /* Flags, a combination of readTrapezeOpenTIFFDIBEx*
                         values */
  /* Anything below here requires Trapeze 8.725 or later */
  TrapezeDWORD burnInFlags; /* A combination of *TrapezeBurnIn values, as
                               described in BurnInTrapezeAnnotationsExRec,
                               ignored if readTrapezeOpenTIFFDIBExBurnIn is
                               not set in flags */
} ReadTrapezeOpenTIFFDIBExRec;
typedef const ReadTrapezeOpenTIFFDIBExRec * ReadTrapezeOpenTIFFDIBExCPtr;

/* Flags for ResizeTrapezeDIBExRec */
#define resizeTrapezeDIBExEnhance 0x0001 /* Average to get reduced pixels when
                                            reducing */

/* Structure for ResizeTrapezeDIBEx() */
typedef struct {
  TrapezeWORD size; /* Size of this structure, in bytes */
  TrapezeWORD flags; /* Flags, a combination of resizeTrapezeDIBEx* values */
  TrapezeDWORD width; /* Width of the resized DIB, in pixels */
  TrapezeDWORD height; /* Height of the resized DIB, in pixels */
  const TrapezeBYTE* dibData; /* Source DIB data */
  const TrapezeBITMAPINFO* dib; /* Source DIB */
  TrapezeProgressProc progressProc; /* If not NULL, this function is called
                                       periodically with lParam passed to it;
                                       if it returns FALSE the function fails
                                       with the error ERROR_CANCELLED */
  TrapezeLPVOID lParam; /* Parameter passed to progressProc */
} ResizeTrapezeDIBExRec, *ResizeTrapezeDIBExPtr;
typedef const ResizeTrapezeDIBExRec * ResizeTrapezeDIBExCPtr;

/* Tag reading information */

#define readTrapezeOpenTIFFASCIITagFailed ((TrapezeDWORD) -1)

#define documentNameTrapezeTIFFASCIITag     269 /* Document name */
#define imageDescriptionTrapezeTIFFASCIITag 270 /* Image description */
#define makeTrapezeTIFFASCIITag             271 /* Scanner make */
#define modelTrapezeTIFFASCIITag            272 /* Scanner model */
#define pageNameTrapezeTIFFASCIITag         285 /* Page name */
#define softwareTrapezeTIFFASCIITag         305 /* Software */
#define dateTimeTrapezeTIFFASCIITag         306 /* Date/time */
#define artistTrapezeTIFFASCIITag           315 /* Artist */
#define hostComputerTrapezeTIFFASCIITag     316 /* Host computer */
#define inkNamesTrapezeTIFFASCIITag         333 /* Ink names */
#define targetPrinterTrapezeTIFFASCIITag    337 /* Target printer */
#define copyrightTrapezeTIFFASCIITag        33432 /* Copyright */

/* OCR text reading information */

/* OCR text locations are rotated to match orientation */
#define getTrapezeTIFFOCRTextExApplyOrientation 0x0001

typedef struct {
  TrapezeWORD size; /* Size of this structure, in bytes */
  TrapezeWORD flags; /* Flags, a combination of getTrapezeTIFFOCRTextEx*
                        values */
  TrapezeTIFF tiff; /* The TIFF to retrieve the OCR text from */
  TrapezeDWORD ifdNo; /* The one based IFD number within the TIFF to retrieve
                         the OCR text from */
  TrapezeOCRText ocrText; /* OCR text, set on return. This is set to NULL if
                             there is no OCR text. Must be freed using
                             DestroyTrapezeOCRText() when it is no longer
                             required */
  TrapezeDWORD ocrTextSize; /* Size of ocrText, in bytes, set on return */
} GetTrapezeTIFFOCRTextExRec, * GetTrapezeTIFFOCRTextExPtr;

/* PDF Definitions and Types */

#define trapezePDFPageAll           0x00 /* All page data */
#define trapezePDFPageThumbnail     0x01 /* Thumbnail data only */
#define trapezePDFPageNoAnnotations 0x02 /* All page data except
                                            annotations */
#define trapezePDFPageDIB           0x04 /* DIB */
#define trapezePDFPageJPEG          0x08 /* DIB or JPEG */
#define trapezePDFPageThumbnailDIB  0x10 /* DIB for thumbnail */
#define trapezePDFPageOCRText       0x20 /* OCR text */
#define trapezePDFPageResolution    0x40 /* Return resolution */

/* Resolution structure for GetTrapezePDFPageData() */
typedef struct {
  TrapezeSIZE_T size; /* Size of returned data */
  TrapezeTIFFResolutionScaleRec resolution; /* Resolution */
} TrapezePDFPageDataResolutionRec, * TrapezePDFPageDataResolutionPtr;

#define getTrapezePDFPageCountFailed ((TrapezeDWORD) -1)

#define convertTrapezePDFToTIFFAllPages 0

#define trapezeNoPDFDither  0 /* No dithering */
#define trapeze2x2PDFDither 1 /* 2x2 dither, giving 5 gray levels */

/* Special color for converting PDF to TIFF - automatic with a maximum of
   256 grays */
#define auto256GrayTrapezePDFToTIFFColorType ((TrapezeBYTE) 255)
/* Special color for converting PDF to TIFF - black & white with 2x2 dither */
#define bw2x2DitherTrapezePDFToTIFFColorType ((TrapezeBYTE) 254)

/* Structure for compression options in ConvertTrapezePDFToTIFFExRec */
typedef struct {
  TrapezeDWORD size; /* Size of this structure, in bytes */
  /* Any below can be NULL for default options */
  /* Required if blackWhiteCompressionTrapezePDFOption is set in options.
     If blackWhiteCompression is:
     noTrapezeCompression, blackWhite is NULL, or a
       TrapezeTIFFNoCompressionOptionsPtr.
     packBitsTrapezeCompression or lzwTrapezeCompression, blackWhite must be
       NULL.
     g4TrapezeCompression, blackWhite is NULL or a
       TrapezeTIFFG4CompressionOptionsPtr.
     g3TrapezeCompression, blackWhite is NULL or a
       TrapezeTIFFG3CompressionOptionsPtr.
     ccitt1DTrapezeCompression, blackWhite is NULL or a
       TrapezeTIFFCCITT1DCompressionOptionsPtr. */
  TrapezeLPCVOID blackWhite;
  /* Required if gray256CompressionTrapezePDFOption is set in options.
     If gray256Compression is:
     noTrapezeCompression, packBitsTrapezeCompression or
       lzwTrapezeCompression, gray256 must be NULL.
     jpeg6TrapezeCompression or jpeg7TrapezeCompression, gray256 is NULL or a
       TrapezeJPEG8OptionsPtr. */
  TrapezeLPCVOID gray256;
  /* Required if rgb24CompressionTrapezePDFOption is set in options.
     If rgb24Compression is:
     noTrapezeCompression, packBitsTrapezeCompression or
       lzwTrapezeCompression, rgb24 must be NULL.
     jpeg6TrapezeCompression or jpeg7TrapezeCompression, rgb24 is NULL or a
       TrapezeJPEG24OptionsPtr. */
  TrapezeLPCVOID rgb24;
  TrapezeLPCVOID palette2; /* Required if palette2CompressionTrapezePDFOption2
                              is set in options2. Otherwise, same as for
                              blackWhite. */
  /* Required if gray16CompressionTrapezePDFOption2 is set in options2.
     Currently gray16 must be NULL. */
  TrapezeLPCVOID gray16;
  /* Required if palette16CompressionTrapezePDFOption2 is set in options2.
     Currently palette16 must be NULL. */
  TrapezeLPCVOID palette16;
  /* Required if palette256CompressionTrapezePDFOption2 is set in options2.
     Currently palette256 must be NULL. */
  TrapezeLPCVOID palette256;
} TrapezePDFToTIFFCompressionOptionsRec,
  * TrapezePDFToTIFFCompressionOptionsPtr;
typedef const TrapezePDFToTIFFCompressionOptionsRec
  * TrapezePDFToTIFFCompressionOptionsCPtr;

/* Valid values for size from older versions of Trapeze */
#define convertTrapezePDFToTIFFExSize1 40 /* Versions 6.51 or later, includes
                                             up to lParam */
#define convertTrapezePDFToTIFFExSize2 44 /* Versions 6.57 or later, includes
                                             up to dither */
#define convertTrapezePDFToTIFFExSize3 52 /* Versions 7.24 or later, includes
                                             up to palette256Compression */
#define convertTrapezePDFToTIFFExSize4 56 /* Versions 7.3 Beta 1 or later,
                                             includes up to
                                             compressionOptions */
#define convertTrapezePDFToTIFFExSize5 60 /* Versions 7.42 Beta 2 or later,
                                             includes up to optionFlags2 */

/* Structure for ConvertTrapezePDFToTIFFEx() - requires Trapeze 6.51 or
   later */
typedef struct {
  TrapezeWORD size; /* Size of this structure, in bytes */
  TrapezeWORD options; /* Conversion options, any not specified use default,
                          0 = all use defaults. A combination of
                          colorTypeTrapezePDFOption,
                          resolutionTrapezePDFOption,
                          pdfTextToTIFFTrapezePDFOption,
                          colorOptimizeTrapezePDFOption,
                          blackWhiteCompressionTrapezePDFOption,
                          gray256CompressionTrapezePDFOption,
                          rgb24CompressionTrapezePDFOption,
                          openPasswordTrapezePDFOption,
                          ditherTrapezePDFOption,
                          rapidRedactPDFTrapezePDFOption,
                          safeModeTrapezePDFOption,
                          noRapidRedactPDFPasswordTrapezePDFOption and
                          options2TrapezePDFOption. */
  TrapezeWORD optionFlags; /* Flags indicating option values, a combination of
                              tiffOCRTextToPDFTrapezePDFOption,
                              pdfTextToTIFFTrapezePDFOption,
                              tiffToPDFATrapezePDFOption,
                              colorOptimizeTrapezePDFOption and
                              safeModeTrapezePDFOption, required if any of
                              these are set in options */
  TrapezeBYTE colorType; /* Color type, one of blackAndWhiteTrapezeColorType,
                            gray256TrapezeColorType, rgb24TrapezeColorType,
                            otherTrapezeColorType (for automatic),
                            palette16TrapezeColorType,
                            auto256GrayTrapezePDFToTIFFColorType,
                            bw2x2DitherTrapezePDFToTIFFColorType, required
                            if colorTypeTrapezePDFOption is set in options */
  TrapezeBYTE resolutionUnits; /* Resolution units, one of cmTrapezeUnit or
                                  inchTrapezeUnit, required if
                                  resolutionTrapezePDFOption is set in
                                  options */
  TrapezeDWORD resolution; /* Resolution multiplied by 1000, required if
                              resolutionTrapezePDFOption is set in options */
  TrapezeBYTE blackWhiteCompression; /* Compression, one of
                                        *TrapezeCompression, required if
                                        blackWhiteCompressionTrapezePDFOption
                                        is set in options */
  TrapezeBYTE gray256Compression; /* Compression, one of
                                     *TrapezeCompression, required if
                                     gray256CompressionTrapezePDFOption is set
                                     in options */
  TrapezeBYTE rgb24Compression; /* Compression, one of *TrapezeCompression,
                                   required if
                                   rgb24CompressionTrapezePDFOption is set in
                                   options */
  TrapezeLPCSTR openPassword; /* Password for opening PDFs, required if
                                 openPasswordTrapezePDFOption is set in
                                 options */
  TrapezeLPCSTR srcFileName; /* PDF to convert */
  TrapezeDWORD pageNo; /* One based page number to convert, or
                          convertTrapezePDFToTIFFAllPages to convert all pages
                          in the PDF */
  TrapezeLPCSTR destFileName; /* Location to write the TIFF */
  TrapezeProgressProc progressProc; /* Progress function called periodically,
                                       NULL = none */
  TrapezeLPVOID lParam; /* Parameter for progressProc() */
  /* Anything below here requires Trapeze 6.57 or later */
  TrapezeDWORD dither; /* Dither mode, one of trapeze*PDFDither, required if
                          ditherTrapezePDFOption is set in options, ignored if
                          color type is
                          bw2x2DitherTrapezePDFToTIFFColorType */
  /* Anything below here requires Trapeze 7.24 or later */
  TrapezeDWORD options2; /* More conversion options, a combination of
                            *TrapezePDFOption2, required if
                            options2TrapezePDFOption is set in options */
  TrapezeBYTE palette2Compression; /* Compression, one of *TrapezeCompression,
                                      required if
                                      palette2CompressionTrapezePDFOption2 is
                                      set in options2 */
  TrapezeBYTE gray16Compression; /* Compression, one of *TrapezeCompression,
                                    required if
                                    gray16CompressionTrapezePDFOption2 is set
                                    in options2 */
  TrapezeBYTE palette16Compression; /* Compression, one of
                                       *TrapezeCompression, required if
                                       palette16CompressionTrapezePDFOption2
                                       is set in options2 */
  TrapezeBYTE palette256Compression; /* Compression, one of
                                        *TrapezeCompression, required if
                                        palette256CompressionTrapezePDFOption2
                                        is set in options2 */
  /* Anything below here requires Trapeze 7.3 Beta 1 or later */
  /* Compression options, required if compressionOptionsTrapezePDFOption2 is
     set in options2 */
  TrapezePDFToTIFFCompressionOptionsCPtr compressionOptions;
  /* Anything below here requires Trapeze 7.42 Beta 2 or later */
  TrapezeDWORD optionFlags2; /* More flags indicating option values, a
                                combination of *TrapezePDFOption2 */
  /* Anything below here requires Trapeze 7.56 or later */
  TrapezeDWORD firstTextPageNo; /* First page to convert text for (one based),
                                   required if
                                   pdfTextToTIFFPageRangeTrapezePDFOption2 is
                                   set in options2 */
  TrapezeDWORD lastTextPageNo; /* Last page to convert text for (one based),
                                  required if
                                  pdfTextToTIFFPageRangeTrapezePDFOption2 is
                                  set in options2 */
  TrapezeDWORD textPageListCount; /* Number of pages in textPageList, required
                                     if pdfTextToTIFFPageListTrapezePDFOption2
                                     is set in options2 */
  const TrapezeDWORD* textPageList; /* Pages to convert text for (one based),
                                       textPageListCount specifies the number
                                       of entries in this list, required if
                                       pdfTextToTIFFPageListTrapezePDFOption2
                                       is set in options2 */
} ConvertTrapezePDFToTIFFExRec;
typedef const ConvertTrapezePDFToTIFFExRec * ConvertTrapezePDFToTIFFExCPtr;

typedef struct TrapezePDFStruct {
  int unused;
} * TrapezePDF;

/* Change resolution to the values in resolution and resolutionUnits (default
   is 300 dpi: resolution = 300000, resolutionUnits = inchTrapezeUnit) */
#define resolutionTrapezePDFOption               0x0001
/* Change color type to the value in colorType (default is
   rgb24TrapezeColorType) */
#define colorTypeTrapezePDFOption                0x0002
/* Change this option to the value of this flag in flags. When set (the
   default), AppendTrapezePDFPage(), ConvertTrapezeTIFFToPDF() and
   ConvertTrapezeTIFFToPDFEx() will convert any OCR text in a TIFF to hidden
   (searchable) text in the PDF. When not set, the output PDF will not have
   any text. */
#define tiffOCRTextToPDFTrapezePDFOption         0x0004
/* Change this option to the value of this flag in flags. When set, the number
   of colors in the output image is reduced to the minimum. The default is not
   set. */
#define colorOptimizeTrapezePDFOption            0x0008
/* Change the compression for 24 bit color images to the value in compression.
   (default is packBitsTrapezeCompression). Only one of the compression
   options can be changed per SetTrapezePDFOptions() call. */
#define rgb24CompressionTrapezePDFOption         0x0010
/* Change the compression for 256 gray images to the value in compression
   (default is packBitsTrapezeCompression). Only one of the compression
   options can be changed per SetTrapezePDFOptions() call. */
#define gray256CompressionTrapezePDFOption       0x0020
/* Change the compression for black & white images to the value in compression
   (default is g4TrapezeCompression). Only one of the compression options can
   be changed per SetTrapezePDFOptions() call. */
#define blackWhiteCompressionTrapezePDFOption    0x0040
/* Change this option to the value of this flag in flags. When set (the
   default), any text in the PDF will be converted into OCR text in the output
   TIFF. */
#define pdfTextToTIFFTrapezePDFOption            0x0080
/* Change the password for opening PDFs to the value in openPassword */
#define openPasswordTrapezePDFOption             0x0100
/* Change the dither mode to the value in dither (default is
   trapezeNoPDFDither) */
#define ditherTrapezePDFOption                   0x0200
/* Converts the PDF to a RapidRedact PDF rather than a TIFF */
#define rapidRedactPDFTrapezePDFOption           0x0400
/* Stops any password being written to a RapidRedact PDF */
#define noRapidRedactPDFPasswordTrapezePDFOption 0x0800
/* OpenTrapezePDF() opens with an object list (default is off) */
#define objectListTrapezePDFOption               0x1000
/* Change this option to the value of this flag in flags. When set, PDF to
   TIFF conversion is run in a separate process. The default is not set. */
#define safeModeTrapezePDFOption                 0x2000
/* Change this option to the value of this flag in flags. When set,
   ConvertTrapezeTIFFToPDF() and ConvertTrapezeTIFFToPDFEx() will convert the
   TIFF to PDF/A. When not set (the default), the output PDF will be a
   standard PDF. */
#define tiffToPDFATrapezePDFOption               0x4000
/* Change the options set in options2 */
#define options2TrapezePDFOption                 0x8000

/* Change the compression for 2 color images to the value in compression
   (default is g4TrapezeCompression). Only one of the compression options can
   be changed per SetTrapezePDFOptions() call. */
#define palette2CompressionTrapezePDFOption2   0x00000001
/* Change the compression for 16 gray images to the value in compression
   (default is packBitsTrapezeCompression). Only one of the compression
   options can be changed per SetTrapezePDFOptions() call. */
#define gray16CompressionTrapezePDFOption2     0x00000002
/* Change the compression for 16 color images to the value in compression
   (default is packBitsTrapezeCompression). Only one of the compression
   options can be changed per SetTrapezePDFOptions() call. */
#define palette16CompressionTrapezePDFOption2  0x00000004
/* Change the compression for 256 color images to the value in compression
   (default is packBitsTrapezeCompression). Only one of the compression
   options can be changed per SetTrapezePDFOptions() call. */
#define palette256CompressionTrapezePDFOption2 0x00000008
/* Change compression options for any compression being changed */
#define compressionOptionsTrapezePDFOption2    0x00000010
/* Change this option to the value of this flag in flags2. When set (the
   default), ConvertTrapezeTIFFToPDF() and ConvertTrapezeTIFFToPDFEx() will
   copy any bookmarks to the PDF. When not set, the output PDF will not have
   any bookmarks. */
#define tiffBookmarksToPDFTrapezePDFOption2    0x00000020
/* Change this option to the value of this flag in flags2. When set (the
   default), ConvertTrapezeTIFFToPDF() and ConvertTrapezeTIFFToPDFEx() will
   copy any thumbnails to the PDF. When not set, the output PDF will not have
   any thumbnails. */
#define tiffThumbnailsToPDFTrapezePDFOption2   0x00000040
/* Change this option to the value of this flag in flags2. When set (the
   default), any bookmarks in the PDF will be converted into bookmarks in the
   output TIFF. When not set, the output TIFF will not have any bookmarks.
   Available in Trapeze 0x7421 or later. */
#define pdfBookmarksToTIFFTrapezePDFOption2    0x00000080
/* Only converts text in the PDF into OCR text in the output TIFF for a
   specified page range. Available in Trapeze 7.56 or later. */
#define pdfTextToTIFFPageRangeTrapezePDFOption2 0x00000100
/* Only converts text in the PDF into OCR text in the output TIFF for a
   specified list of pages. Available in Trapeze 7.56 or later. */
#define pdfTextToTIFFPageListTrapezePDFOption2  0x00000200
/* Change this option to the value of this flag in flags2. When set and PDF
   text is being converted to OCR text, the OCR text will be in physical
   layout order. When not set (the default), the OCR text will be in the order
   it appears in the PDF. Available in Trapeze 8.39 or later. */
#define pdfTextToTIFFPhysicalLayoutModeTrapezePDFOption2 0x00000400
/* When set in options2 in ConvertTrapezePDFToTIFFExRec, all strings passed to
   ConvertTrapezePDFToTIFFEx() are Unicode. Available in Trapeze 8.392 or
   later. */
#define unicodeTrapezePDFOption2                0x00000800
/* Change this option to the value of this flag in flags2. When set any
   existing compression in the PDF is used in the TIFF, where possible. When
   not set (the default) or the existing compression can't be kept, the
   current compression for the image type is used. Available in Trapeze 8.41
   or later. */
#define keepCompressionTrapezePDFOption2        0x00001000
/* Change this option to the value of this flag in flags2. When set TIFFs are
   always written with normal orientation. When not set (the default), any
   rotated PDF pages are written to TIFF with an orientation tag. Available in
   Trapeze 8.417 or later. */
#define noOrientationTrapezePDFOption2          0x00002000
/* Change this option to the value of this flag in flags2. When set pages are
   always converted to colorType. When not set (the default), any whole page
   images are extracted as-is. Available in Trapeze 8.744 or later. */
#define forceColorTypeTrapezePDFOption2         0x00004000

/* Valid values for size from older versions of Trapeze */
#define trapezePDFOptionsSize1 16 /* Versions 6.43 Beta 2 or later, includes
                                     up to compression */
#define trapezePDFOptionsSize2 20 /* Versions 6.43 Beta 3 or later, includes
                                     up to openPassword */
#define trapezePDFOptionsSize3 24 /* Versions 6.57 or later, includes up to
                                     dither */
#define trapezePDFOptionsSize4 28 /* Versions 7.24 or later, includes up to
                                     options2 */
#define trapezePDFOptionsSize5 32 /* Versions 7.3 Beta 1 or later, includes up
                                     to compressionOptions */

/* This structure requires Trapeze 6.43 Beta 2 or later */
typedef struct {
  TrapezeWORD size; /* Size of this structure, in bytes */
  TrapezeWORD options; /* PDF options to change, a combination of
                          *TrapezePDFOption, except
                          rapidRedactPDFTrapezePDFOption and
                          noRapidRedactPDFPasswordTrapezePDFOption */
  TrapezeWORD flags; /* Flags indicating option values, a combination of
                        *TrapezePDFOption */
  TrapezeBYTE colorType; /* Color type, one of blackAndWhiteTrapezeColorType,
                            gray256TrapezeColorType, rgb24TrapezeColorType,
                            otherTrapezeColorType (for automatic),
                            palette16TrapezeColorType,
                            auto256GrayTrapezePDFToTIFFColorType,
                            bw2x2DitherTrapezePDFToTIFFColorType */
  TrapezeBYTE resolutionUnits; /* Resolution units, one of cmTrapezeUnit or
                                  inchTrapezeUnit */
  TrapezeDWORD resolution; /* Resolution multiplied by 1000 */
  TrapezeBYTE compression; /* Compression, one of *TrapezeCompression */
  /* Anything below here requires Trapeze 6.43 Beta 3 or later */
  TrapezeLPCSTR openPassword; /* Password for opening PDFs */
  /* Anything below here requires Trapeze 6.57 or later */
  TrapezeDWORD dither; /* Dither mode, one of trapeze*PDFDither,
                          color type bw2x2DitherTrapezePDFToTIFFColorType will
                          override this */
  /* Anything below here requires Trapeze 7.24 or later */
  TrapezeDWORD options2; /* More PDF options to change, a combination of
                            *TrapezePDFOption2, except
                            unicodeTrapezePDFOption2 */
  /* Anything below here requires Trapeze 7.3 Beta 1 or later */
  /* Compression options, used when compressionOptionsTrapezePDFOption2 is
     set in options2. NULL = default compression options, otherwise when
     compression is:
     noTrapezeCompression, compressionOptions is NULL, or a
       TrapezeTIFFNoCompressionOptionsPtr only if the compression being set is
       blackWhiteCompressionTrapezePDFOption or
       palette2CompressionTrapezePDFOption2.
     packBitsTrapezeCompression, compressionOptions must be NULL.
     g4TrapezeCompression, compressionOptions is NULL or a
       TrapezeTIFFG4CompressionOptionsPtr.
     g3TrapezeCompression, compressionOptions is NULL or a
       TrapezeTIFFG3CompressionOptionsPtr.
     lzwTrapezeCompression, compressionOptions must be NULL.
     jpeg6TrapezeCompression or jpeg7TrapezeCompression, compressionOptions is
       NULL or:
         a TrapezeJPEG8OptionsPtr, if the compression being set is
           gray256CompressionTrapezePDFOption.
         a TrapezeJPEG24OptionsPtr, if the compression being set is
           rgb24CompressionTrapezePDFOption.
     ccitt1DTrapezeCompression, compressionOptions is NULL or a
       TrapezeTIFFCCITT1DCompressionOptionsPtr. */
  TrapezeLPCVOID compressionOptions;
  /* Anything below here requires Trapeze 7.33 or later */
  TrapezeDWORD flags2; /* More flags indicating option values, a combination
                          of *TrapezePDFOption2 */
} TrapezePDFOptionsRec;
typedef const TrapezePDFOptionsRec * TrapezePDFOptionsCPtr;

/* When set in options, the value of this flag in optionFlags is used. When
   set, ConvertTrapezeTIFFToPDFEx() will convert the TIFF to PDF/A. When not
   set the output PDF will be a standard PDF. */
#define pdfaTrapezeTIFFToPDFOption          0x0001
/* When set in options, the value of this flag in optionFlags is used. When
   set, ConvertTrapezeTIFFToPDFEx() will convert any OCR text in a TIFF to
   hidden (searchable) text in the PDF. When not set, the output PDF will not
   have any text. */
#define ocrTextTrapezeTIFFToPDFOption       0x0002
/* When set in options, xmpProperties is used to set XMP properties */
#define xmpPropertiesTrapezeTIFFToPDFOption 0x0004
/* When set in options, all strings are Unicode */
#define unicodeTrapezeTIFFToPDFOption       0x0008
/* When set in options, xmpPaddingSize is used to set the XMP padding size */
#define xmpPaddingSizeTrapezeTIFFToPDFOption 0x0010
/* When set in options, progressProc is called periodically with lParam passed
   to it */
#define progressTrapezeTIFFToPDFOption       0x0020
/* When set in options, options2 and optionFlags2 are used */
#define options2TrapezeTIFFToPDFOption      0x8000

/* Valid values for size from older versions of Trapeze */
#define convertTrapezeTIFFToPDFExSize1 20 /* Versions 7.3 Beta 2 or later,
                                             includes up to xmpProperties */
#define convertTrapezeTIFFToPDFExSize2 28 /* Versions 7.33 or later */
#define convertTrapezeTIFFToPDFExSize3 32 /* Versions 8.33 or later */

/* Structure for ConvertTrapezeTIFFToPDFEx() - requires Trapeze 7.3 Beta 2 or
   later */
typedef struct {
  TrapezeWORD size; /* Size of this structure, in bytes */
  TrapezeWORD options; /* Conversion options, any not specified use default,
                          0 = all use defaults. A combination of
                          *TrapezeTIFFToPDFOption values */
  TrapezeWORD optionFlags; /* Flags indicating option values, a combination of
                              pdfaTrapezeTIFFToPDFOption and
                              ocrTextTrapezeTIFFToPDFOption, required if any
                              of these are set in options */
  TrapezeLPCSTR srcFileName; /* Source TIFF */
  TrapezeLPCSTR destFileName; /* Destination for PDF */
  TrapezeLPCSTR xmpProperties; /* XMP properties, required if
                                  xmpPropertiesTrapezeTIFFToPDFOption is set
                                  in options. This must contain one or more
                                  rdf:Description elements */
  /* Anything below here requires Trapeze 7.33 or later */
  TrapezeDWORD options2; /* More PDF options to change, a combination of
                            *TrapezePDFOption2, except
                            unicodeTrapezePDFOption2, required if
                            options2TrapezeTIFFToPDFOption is set in
                            options */
  TrapezeDWORD optionFlags2; /* More flags indicating option values, a
                                combination of *TrapezePDFOption2, required if
                                options2TrapezeTIFFToPDFOption is set in
                                options */
  /* Anything below here requires Trapeze 8.33 or later */
  TrapezeDWORD xmpPaddingSize; /* XMP padding size, required if
                                  xmpPaddingSizeTrapezeTIFFToPDFOption is set
                                  in options */
  /* Anything below here requires Trapeze 9.196 or later */
  TrapezeProgressProc progressProc; /* Progress function, required if
                                       progressTrapezeTIFFToPDFOption is set
                                       in options */
  TrapezeLPVOID lParam; /* Parameter for progressProc, required if
                           progressTrapezeTIFFToPDFOption is set in options */
} ConvertTrapezeTIFFToPDFExRec, * ConvertTrapezeTIFFToPDFExPtr;
typedef const ConvertTrapezeTIFFToPDFExRec * ConvertTrapezeTIFFToPDFExCPtr;

/* Structure for CompactTrapezePDF() - requires Trapeze 8.622 or later */
typedef struct {
  TrapezeWORD size; /* Size of this structure, in bytes */
  TrapezeWORD flags; /* Flags indicating how to compact, currently must be
                        zero */
  TrapezeLPCSTR srcFileName; /* PDF to compact */
  TrapezeLPCSTR destFileName; /* Output file, NULL = modify the file in
                                 srcFileName */
} CompactTrapezePDFRec, * CompactTrapezePDFPtr;
typedef const CompactTrapezePDFRec * CompactTrapezePDFCPtr;

/* OTTP Definitions and Types */

#ifdef _WIN32

#define openTrapezeOTTPConnectionFailed ((DWORD) -1)

typedef struct TrapezeOTTPStruct {
  int unused;
} * TrapezeOTTP;

#endif /*_WIN32*/

#ifdef _WIN32
/* Only available on Windows */

/* Types for printing */
typedef struct {
  WORD size; /* Size of this structure, in bytes */
  DWORD flags; /* A combination of printTrapeze* values from above */
  WORD startPage; /* First page to print, valid when printTrapezePageNums is
                     set */
  WORD endPage; /* Last page to print, valid when printTrapezePageNums is
                   set */
  WORD copies; /* Number of copies to print */
  short pageWidth; /* x pages per piece of paper when < 0;
                      x pieces of paper per page when > 0 */
  short pageHeight; /* y pages per piece of paper when < 0;
                       y pieces of paper per page when > 0 */
} TrapezePrintRec, * TrapezePrintPtr;

typedef struct {
  DWORD startPage; /* First page in range (one based) */
  DWORD endPage;   /* Last page in range (one based) */
} PrintTrapezeDocumentPageRangeRec, * PrintTrapezeDocumentPageRangePtr;
typedef const PrintTrapezeDocumentPageRangeRec
  * PrintTrapezeDocumentPageRangeCPtr;

/* Valid values for size from older versions of Trapeze */
#define printTrapezeDocumentExRecSize1 24 /* Versions 3.4 or later,
                                             includes up to printSizeLimit */
#define printTrapezeDocumentExRecSize2 28 /* Versions 4.01 or later,
                                             includes up to flags */
#define printTrapezeDocumentExRecSize3 60 /* Versions 8.5 Beta 13 or later,
                                             includes up to photocopyAdjust */

/* PrintTrapezeDocumentEx flags */
/* Show print dialog for all pages, pageRangeCount must be zero */
#define printTrapezeDocumentExDialog          0x00000001
/* Hides Print To File in the print dialog */
#define printTrapezeDocumentExHidePrintToFile 0x00000002
/* Disables local saving in the Print To File dialog */
#define printTrapezeDocumentExNoLocalSave     0x00000004
/* fileName is a TrapezeTIFFDocument */
#define printTrapezeDocumentExTIFFDoc         0x00000008
/* settings is valid */
#define printTrapezeDocumentExSettings        0x00000010
/* copies is valid */
#define printTrapezeDocumentExCopies          0x00000020
/* collate bit is valid in values */
#define printTrapezeDocumentExCollate         0x00000040
/* invert bit is valid in values */
#define printTrapezeDocumentExInvert          0x00000080
/* rotate is valid */
#define printTrapezeDocumentExRotate          0x00000100
/* annotations bit is valid in values */
#define printTrapezeDocumentExAnnotations     0x00000200
/* print as image bit is valid in values */
#define printTrapezeDocumentExAsImage         0x00000400
/* auto-orientation bit is valid in values */
#define printTrapezeDocumentExAutoOrientation 0x00000800
/* dither bit is valid in values */
#define printTrapezeDocumentExDither          0x00001000
/* jpeg8 bit is valid in values */
#define printTrapezeDocumentExJPEG8           0x00002000
/* jpeg24 bit is valid in values */
#define printTrapezeDocumentExJPEG24          0x00004000
/* minMargins bit is valid in values */
#define printTrapezeDocumentExMinMargins      0x00008000
/* stretch bit is valid in values */
#define printTrapezeDocumentExStretch         0x00010000
/* photocopy mode bit is valid in values */
#define printTrapezeDocumentExPhotocopyMode   0x00020000
/* jpeg8Options is valid */
#define printTrapezeDocumentExJPEG8Options    0x00040000
/* jpeg24Options is valid */
#define printTrapezeDocumentExJPEG24Options   0x00080000
/* photocopyAdjustUnits and photocopyAdjust are valid */
#define printTrapezeDocumentExPhotocopyAdjust 0x00100000
/* toBW bit is valid in values */
#define printTrapezeDocumentExToBW            0x00200000
/* lowestResolution bit is valid in values */
#define printTrapezeDocumentExLowestResolution 0x00400000
/* progressProc and lParam are valid */
#define printTrapezeDocumentExProgress        0x00800000

typedef struct {
  WORD size; /* Size of this structure, in bytes */
  HWND parent; /* Parent window for dialogs */
  LPCSTR fileName; /* File to print */
  DWORD pageRangeCount; /* Number of page ranges in pageRanges, must be zero
                           if printTrapezeDocumentExDialog flag is set */
  PrintTrapezeDocumentPageRangeCPtr pageRanges; /* pageRangeCount page
                                                   ranges */
  DWORD printSizeLimit; /* Print size limit, in kilobytes, as for the
                           printSizeLimitTrapezeProperty property */
  DWORD flags; /* Flags from above specifying how to print */
   /* Print settings, ignored if printTrapezeDocumentExSettings is not set in
      flags */
  GetTrapezePrintSettingsPropertyCPtr settings;
  WORD copies; /* Number of copies to print, 1 is used if
                  printTrapezeDocumentExCopies is not set in flags */
  WORD rotate; /* 0 = don't rotate, 90, 180 or 270 = rotate all pages by 90,
                  180 or 270 degrees clockwise */
  DWORD values; /* Boolean values, ignored if no values bits are set in
                   flags */
  /* 8-bit JPEG options, NULL = use defaults, ignored if
     printTrapezeDocumentExJPEG8Options is not set in flags */
  TrapezeJPEG8OptionsCPtr jpeg8Options;
  /* 24-bit JPEG options, NULL = use defaults, ignored if
     printTrapezeDocumentExJPEG24Options is not set in flags */
  TrapezeJPEG8OptionsCPtr jpeg24Options;
  DWORD photocopyAdjustUnits; /* Photocopy adjust units,
                                 PSD_INHUNDREDTHSOFMILLIMETERS or
                                 PSD_INTHOUSANDTHSOFINCHES */
  POINT photocopyAdjust; /* Adjustment, positive = left/up,
                            negative = right/down */
  TrapezeProgressProc progressProc; /* Progress function, NULL = none */
  LPVOID lParam; /* Parameter for progressProc */
} PrintTrapezeDocumentExRec, * PrintTrapezeDocumentExPtr;

/* Trapeze window notifications */
#define trapezeNotify                   0 /* First notification value */

/* NM_KILLFOCUS - sent when the Trapeze window loses focus */
/* NM_SETOFOCUS - sent when the Trapeze window gains focus */

/* Annotation audit notification */
#define trapezeAnnotationAuditDeleted       0xFFFFFFFF
#define trapezeAnnotationAuditChangeDeleted 0xFFFFFFFE
#define trapezeAnnotationAuditAdded         0xFFFFFFFF
typedef struct {
  NMHDR hdr; /* Standard WM_NOTIFY header */
  DWORD oldAnnotationNo; /* Annotation before the change, or
                            trapezeAnnotationAuditAdded for a new
                            annotation. If the annotation is being deleted,
                            this annotation will have been added to the hidden
                            [Deleted] group. If the annotation is being
                            changed, this annotation will have been added to
                            the hidden [Changed] group. */
  DWORD newAnnotationNo; /* Annotation after the change, or
                            trapezeAnnotationAuditDeleted if the annotation is
                            being deleted, or
                            trapezeAnnotationAuditChangeDeleted if the
                            annotation is being deleted after being
                            changed. */
} TrapezeAnnotationAuditNotifyRec, * TrapezeAnnotationAuditNotifyPtr;

/* Sent when an annotation is modified and annotationsAuditTrapezeProperty is
   TRUE */
#define annotationAuditTrapezeNotify (trapezeNotify + 36)
/* lParam = (LPARAM) (TrapezeAnnotationAuditNotifyPtr) notifyInfo
   Return = ignored
*/

/* Annotation selection operation notification */
#define multipleTrapezeAnnotationsSelected 0xFFFFFFFF
typedef struct {
  NMHDR hdr; /* Standard WM_NOTIFY header */
  DWORD annotationNo; /* Annotation being processed, or
                         multipleTrapezeAnnotationsSelected if multiple
                         annotations are selected */
} TrapezeAnnotationSelectNotifyRec, * TrapezeAnnotationSelectNotifyPtr;

/* Sent when an annotation has been created */
#define annotationCreatedTrapezeNotify (trapezeNotify + 31)
/* lParam = (LPARAM) (TrapezeAnnotationSelectNotifyPtr) notifyInfo
   Return = ignored
*/

/* Sent when the user is about to begin dragging annotations, to see if the
   drag is allowed. */
#define beginAnnotationDragTrapezeNotify (trapezeNotify + 24)
/* lParam = (LPARAM) (TrapezeAnnotationSelectNotifyPtr) notifyInfo
   Return = 0 to allow drag, non-zero to disallow
*/

/* Drag notification */
typedef struct {
  NMHDR hdr; /* Standard WM_NOTIFY header */
  BYTE pane; /* Pane starting a drag, one of trapeze*Pane */
} TrapezeDragNotifyRec, * TrapezeDragNotifyPtr;

/* Sent when a drag is beginning and the enableNotifyTrapezeProperty is
   TRUE */
#define beginDragTrapezeNotify (trapezeNotify + 30)
/* lParam = (LPARAM) (TrapezeDragNotifyPtr) notifyInfo
   Return = (BOOL) ignore
*/

/* Sent when the bookmark selection has changed */
#define bookmarkSelChangeTrapezeNotify  (trapezeNotify + 17)

/* Can bookmark selection change notification */
typedef struct {
  NMHDR hdr; /* Standard WM_NOTIFY header */
  DWORD bookmark; /* Currently selected bookmark,
                     noTrapezeBookmarkSelected = none,
                     multipleTrapezeBookmarksSelected = more than one,
                     any other value = the bookmark number of the only
                     selected bookmark */
  DWORD pageNo; /* Page number for the selected bookmark, if bookmark is not
                   noTrapezeBookmarkSelected or
                   multipleTrapezeBookmarksSelected, 0 = selected bookmark is
                   a folder */
} TrapezeCanBookmarkSelChangeNotifyRec,
  * TrapezeCanBookmarkSelChangeNotifyPtr;

/* Sent to query if the user is allowed to view/edit annotation properties. */
#define canAnnotationPropertiesTrapezeNotify (trapezeNotify + 25)
/* lParam = (LPARAM) (TrapezeAnnotationSelectNotifyPtr) notifyInfo
   Return = 0 to allow properties, 1 to disallow, 2 to allow viewing only
*/

/* Sent to query if the user is allowed to resize an annotation. */
#define canAnnotationSizeTrapezeNotify (trapezeNotify + 26)
/* lParam = (LPARAM) (TrapezeAnnotationSelectNotifyPtr) notifyInfo
   Return = 0 to allow resizing, non-zero to disallow
*/

/* Sent before the bookmark selection changes, to see if it can be changed.
   setTrapezeBookmarksChangedMsg and setTrapezeBookmarksMsg cannot be used
   while inside this notification. */
#define canBookmarkSelChangeTrapezeNotify (trapezeNotify + 20)
/* lParam = (LPARAM) (TrapezeCanBookmarkSelChangeNotifyPtr) notifyInfo
   Return = 0 to allow change, non-zero to disallow
*/

/* Can page change notification */
typedef struct {
  NMHDR hdr; /* Standard WM_NOTIFY header */
  DWORD oldPageNo; /* Current page number (one based) */
  DWORD newPageNo; /* New page number (one based) */
} TrapezeCanPageChangeNotifyRec, * TrapezeCanPageChangeNotifyPtr;

/* Sent before a page changes, to see if it can be changed. */
#define canPageChangeTrapezeNotify      (trapezeNotify + 21)
/* lParam = (LPARAM) (TrapezeCanPageChangeNotifyPtr) notifyInfo
   Return = 0 to allow change, non-zero to disallow
*/

#define closeDocumentTrapezeNotify      (trapezeNotify + 12)

/* Command completion notification */
typedef struct {
  NMHDR hdr; /* Standard WM_NOTIFY header */
  WORD command; /* Command that was completed */
  BOOL success; /* TRUE if the command was successful, FALSE if it failed.
                   Generally, if success is FALSE, the user will have been
                   advised of the failure. */
} TrapezeCommandCompleteNotifyRec, * TrapezeCommandCompleteNotifyPtr;

/* Sent when a command has completed. */
#define commandCompleteTrapezeNotify    (trapezeNotify + 27)
/* lParam = (LPARAM) (TrapezeCommandCompleteNotifyPtr) notifyInfo
   Return = ignored
*/

/* Command notification */
typedef struct {
  NMHDR hdr; /* Standard WM_NOTIFY header */
  WORD command; /* Command that was selected */
} TrapezeCommandNotifyRec, * TrapezeCommandNotifyPtr;

/* Sent when a command changed to notify using notifyTrapezeCommandMsg has
   been selected. */
#define commandTrapezeNotify            (trapezeNotify + 19)
/* lParam = (LPARAM) (TrapezeCommandNotifyPtr) notifyInfo
   Return = ignored
*/

/* Context menu notification */
typedef struct {
  NMHDR hdr; /* Standard WM_NOTIFY header */
  BYTE pane; /* Pane that was requested to show the menu, one of
                trapeze*Pane */
  POINT pos; /* Mouse location, in screen coordinates, (-1, -1) = menu
                generated from the keyboard */
} TrapezeContextMenuNotifyRec, * TrapezeContextMenuNotifyPtr;

/* Sent when a disabled context menu has been requested */
#define contextMenuTrapezeNotify        (trapezeNotify + 23)
/* lParam = (LPARAM) (TrapezeContextMenuNotifyPtr) notifyInfo
   Return = ignored
*/

/* Context menu text notification */
typedef struct {
  NMHDR hdr; /* Standard WM_NOTIFY header */
  BYTE pane; /* Pane that was requested to show the menu, one of
                trapeze*Pane */
  WORD command; /* Command, one of trapeze*Command */
  WORD textSize; /* Size of text buffer */
  LPSTR text; /* Text for command, can be modified */
} TrapezeContextMenuTextNotifyRec, * TrapezeContextMenuTextNotifyPtr;

/* Sent to allow modification of context menu text */
#define contextMenuTextTrapezeNotify    (trapezeNotify + 38)
/* lParam = (LPARAM) (TrapezeContextMenuTextNotifyPtr) notifyInfo
   Return = ignored
*/

/* Page delete notification */
typedef struct {
  NMHDR hdr; /* Standard WM_NOTIFY header */
  DWORD count; /* Number of pages deleted. Can be zero indicating some pages
                  were deleted but an error occurred during the notify. */
  DWORD pages[1]; /* count page numbers indicating the deleted pages. Each
                     page number is the page number before deleting any
                     pages. */
} TrapezeDeletePagesNotifyRec, * TrapezeDeletePagesNotifyPtr;

/* Sent when pages have been deleted from the current document and both the
   enableNotifyTrapezeProperty and extendedPageNotifyTrapezeProperty
   properties are TRUE. */
#define deletePagesTrapezeNotify        (trapezeNotify + 13)
/* lParam = (LPARAM) (TrapezeDeletePagesNotifyPtr) notifyInfo
   Return = ignored
*/

/* Page deleting annotations notification */
typedef struct {
  NMHDR hdr; /* Standard WM_NOTIFY header */
  DWORD pageNo; /* The page number of the page about to be deleted, before any
                   pages are deleted. */
  TrapezeAnnotations annotations; /* The annotations for the page being
                                     deleted, NULL = none. These are owned by
                                     Trapeze and must not be used after the
                                     notification returns. These can be used
                                     anywhere that annotations returned by
                                     GetTrapezeTIFFAnnotations() or
                                     InsertTrapezeAnnotation() can be used, */
  DWORD annotationsSize; /* Size of annotations */
} TrapezeDeletingPageAnnotationsNotifyRec,
  * TrapezeDeletingPageAnnotationsNotifyPtr;

/* Sent when requested by a deletingPageTrapezeNotify notification. */
#define deletingPageAnnotationsTrapezeNotify (trapezeNotify + 41)
/* lParam = (LPARAM) (TrapezeDeletingPageAnnotationsNotifyPtr) notifyInfo
   Return = ignored
*/

/* Page deleting notification */
typedef struct {
  NMHDR hdr; /* Standard WM_NOTIFY header */
  DWORD pageNo; /* The page number of the page about to be deleted, before any
                   pages are deleted. */
} TrapezeDeletingPageNotifyRec, * TrapezeDeletingPageNotifyPtr;

/* Sent when a page is about to be deleted from the current document and the
   enableNotifyTrapezeProperty property is TRUE. */
#define deletingPageTrapezeNotify       (trapezeNotify + 40)
/* lParam = (LPARAM) (TrapezeDeletingPageNotifyPtr) notifyInfo
   Return = 0 to continue, 1 to send a deletingPageAnnotationsTrapezeNotify
            notification before continuing
*/

#define documentCommandTrapezeNotify    (trapezeNotify + 10)

/* Double click notification */

#define trapezeLeftButton   0 /* Left mouse button */
#define trapezeMiddleButton 1 /* Middle mouse button */
#define trapezeRightButton  2 /* Right mouse button */

typedef struct {
  NMHDR hdr; /* Standard WM_NOTIFY header */
  BYTE button; /* Button that was double clicked, one of trapeze*Button */
  BYTE pane; /* Pane that was double clicked, one of trapeze*Pane */
  BYTE shift; /* Shift flags, indicating which shift keys are down, a
                 combination of trapeze*Shift values */
  POINT pos; /* Mouse location, relative to the upper left corner of the
                pane */
} TrapezeDoubleClickNotifyRec, * TrapezeDoubleClickNotifyPtr;

/* Sent when a mouse button is double clicked and the
   enableNotifyTrapezeProperty property is TRUE. */
#define doubleClickTrapezeNotify        (trapezeNotify + 34)
/* lParam = (LPARAM) (TrapezeDoubleClickNotifyPtr) notifyInfo
   Return = 0 to process the double click, non-zero to ignore it
*/

/* Exemption code action notification */

typedef struct {
  NMHDR hdr; /* Normal WM_NOTIFY header */
  BYTE action; /* Action, one of *TrapezeExemptionCodeAction, this can be
                  changed before returning to change the action */
  DWORD annotationNo; /* Annotation being processed */
  LPCSTR buffer; /* When action is createTrapezeExemptionCodeAction, is an
                    empty string terminated list of string pairs, each pair an
                    exemption code followed by a description. This can be
                    changed to change the exemption code(s) that will be added
                    using createTrapezeExemptionCodeAction; it must be changed
                    to a value that will remain valid after the notification
                    returns. If action is changed to
                    createTrapezeExemptionCodeAction from another value,
                    buffer must also be changed. */
} TrapezeExemptionCodeActionNotifyRec, * TrapezeExemptionCodeActionNotifyPtr;

#define exemptionCodeActionTrapezeNotify (trapezeNotify + 48)
/* lParam = (LPARAM) (TrapezeExemptionCodeActionNotifyPtr) notifyInfo
   Return = ignored
*/

/* File retrieval notification */

typedef struct {
  NMHDR hdr; /* Standard WM_NOTIFY header */
  BOOL isTempFile; /* TRUE if the file is a temporary file */
  LPCSTR fileName; /* File location */
  LPCSTR origin; /* Document that was opened */
} TrapezeFileRetrieveNotifyRec, * TrapezeFileRetrieveNotifyPtr;

/* Sent when a file has been retrieved from TRIM, ODMA, WebDAV etc, and the
   enableNotifyTrapezeProperty property is TRUE. */
#define fileRetrieveTrapezeNotify       (trapezeNotify + 37)
/* lParam = (LPARAM) (TrapezeFileRetrieveNotifyPtr) notifyInfo
   Return = 0 to continue opening, 1 to cancel opening, 2 to cancel opening
            without deleting the temporary file. 2 can only be returned when
            isTempFile is TRUE, and then the receiver of the notification is
            responsible for deleting the temporary file.
*/

/* Help notification */
typedef struct {
  NMHDR hdr; /* Standard WM_NOTIFY header */
  DWORD contextId; /* Help context identifier, 0 = contents */
  HWND control; /* NULL for normal help, non-NULL indicates a control to show
                   help for in a pop-up window. This field is only present for
                   Trapeze version 0x6590 or later. */
} TrapezeHelpNotifyRec, * TrapezeHelpNotifyPtr;

#define helpTrapezeNotify               (trapezeNotify + 32)
/* lParam = (LPARAM) (TrapezeHelpNotifyPtr) notifyInfo
   Return = 0 to show normal help, non-zero to do nothing
*/

#ifdef UNICODE
#define TrapezeGetStampFieldTNotifyPtr TrapezeGetStampFieldWNotifyPtr
#define TrapezeGetStampFieldTNotifyRec TrapezeGetStampFieldWNotifyRec
#define getStampFieldTTrapezeNotify getStampFieldWTrapezeNotify
#else
#define TrapezeGetStampFieldTNotifyPtr TrapezeGetStampFieldNotifyPtr
#define TrapezeGetStampFieldTNotifyRec TrapezeGetStampFieldNotifyRec
#define getStampFieldTTrapezeNotify getStampFieldTrapezeNotify
#endif /*UNICODE*/

/* Stamp field notification */
typedef struct {
  NMHDR hdr; /* Standard WM_NOTIFY header */
  LPCSTR fieldName; /* The name of the field being requested */
  LPSTR text; /* The text to be inserted into the field. If the text to be
                 returned exceeds textSize characters in size, change text to
                 point to a private buffer, which must remain valid when this
                 notification returns. */
  LPSTR errorText; /* The text to display on error. Initially this contains an
                      empty string (""). If it is changed to a non-empty
                      string, the field insert fails and this error is
                      displayed. If the error to be returned exceeds
                      errorTextSize characters in size, change errorText to
                      point to a private buffer, which must remain valid when
                      this notification returns. */
  WORD textSize; /* The size of the buffer pointed to by text */
  WORD errorTextSize; /* The size of the buffer pointed to by errorText */
} TrapezeGetStampFieldNotifyRec, * TrapezeGetStampFieldNotifyPtr;

/* Sent when a stamp containing a dllfield to be replaced has been used. */
#define getStampFieldTrapezeNotify      (trapezeNotify + 42)
/* lParam = (LPARAM) (TrapezeGetStampFieldNotifyPtr) notifyInfo
   Return = ignored
*/

/* Stamp field notification (Unicode version) */
typedef struct {
  NMHDR hdr; /* Standard WM_NOTIFY header */
  LPCWSTR fieldName; /* The name of the field being requested */
  LPWSTR text; /* The text to be inserted into the field. If the text to be
                  returned exceeds textSize characters in size, change text to
                  point to a private buffer, which must remain valid when this
                  notification returns. */
  LPWSTR errorText; /* The text to display on error. Initially this contains an
                       empty string (""). If it is changed to a non-empty
                       string, the field insert fails and this error is
                       displayed. If the error to be returned exceeds
                       errorTextSize characters in size, change errorText to
                       point to a private buffer, which must remain valid when
                       this notification returns. */
  WORD textSize; /* The size of the buffer pointed to by text */
  WORD errorTextSize; /* The size of the buffer pointed to by errorText */
} TrapezeGetStampFieldWNotifyRec, * TrapezeGetStampFieldWNotifyPtr;

/* Sent when a stamp containing a dllfield to be replaced has been used
   (Unicode version). */
#define getStampFieldWTrapezeNotify     (trapezeNotify + 43)
/* lParam = (LPARAM) (TrapezeGetStampFieldWNotifyPtr) notifyInfo
   Return = ignored
*/

/* Image error/warning notification */

/* Image violates the specification */
#define trapezeImageErrorImageWarning             0x00000001
/* Unsupported annotation type */
#define trapezeImageErrorAnnotationTypeWarning    0x00000002
/* Unsupported annotation style */
#define trapezeImageErrorAnnotationStyleWarning   0x00000004
/* Unsupported Unicode annotation */
#define trapezeImageErrorAnnotationUnicodeWarning 0x00000008
/* Some other warning - any unknown flag should be treated the same as this */
#define trapezeImageErrorOtherWarning             0x00000010

typedef struct {
  NMHDR hdr; /* Normal WM_NOTIFY header */
  DWORD error; /* Error code, ERROR_SUCCESS = warnings only */
  DWORD warnings; /* Warning flags, a combination of
                     trapezeImageError*Warning values */
} TrapezeImageErrorNotifyRec, * TrapezeImageErrorNotifyPtr;

#define imageErrorTrapezeNotify         (trapezeNotify + 47)
/* lParam = (LPARAM) (TrapezeImageErrorNotifyPtr) notifyInfo
   Return = ignored
*/

/* Sent when the image selection has changed */
#define imageSelChangeTrapezeNotify     (trapezeNotify + 18)
/* lParam = (LPARAM) (LPNMHDR) notifyInfo
   Return = ignored
*/

/* Image tool change notification */
typedef struct {
  NMHDR hdr; /* Standard WM_NOTIFY header */
  WORD tool; /* New tool, one of *TrapezeImageTool */
} TrapezeImageToolChangeNotifyRec, * TrapezeImageToolChangeNotifyPtr;

/* Sent when the image tool has changed. */
#define imageToolChangeTrapezeNotify    (trapezeNotify + 28)
/* lParam = (LPARAM) (TrapezeImageToolChangeNotifyPtr) notifyInfo
   Return = ignored
*/

/* Page insert notification */
typedef struct {
  NMHDR hdr; /* Standard WM_NOTIFY header */
  DWORD count; /* Number of pages inserted */
  DWORD pages[1]; /* count page numbers indicating the inserted pages. Each
                     page number is the page number after the previous pages
                     have been inserted. i.e. the second page number in the
                     list is the page number after the first page has been
                     inserted. */
} TrapezeInsertPagesNotifyRec, * TrapezeInsertPagesNotifyPtr;

/* Sent when pages have been inserted into the current document and both the
   enableNotifyTrapezeProperty and extendedPageNotifyTrapezeProperty
   properties are TRUE. */
#define insertPagesTrapezeNotify        (trapezeNotify + 14)
/* lParam = (LPARAM) (TrapezeInsertPagesNotifyPtr) notifyInfo
   Return = ignored
*/

/* Key press notification */

#define trapezeShiftShift   1 /* Shift key is down */
#define trapezeControlShift 2 /* Control key is down */
#define trapezeAltShift     4 /* Alt key is down */

typedef struct {
  NMHDR hdr; /* Standard WM_NOTIFY header */
  BYTE pane; /* Pane that received the key press, one of trapeze*Pane */
  BYTE shift; /* Shift flags, indicating which shift keys are down, a
                 combination of trapeze*Shift values */
  WORD keyCode; /* Virtual key code */
} TrapezeKeyDownNotifyRec, * TrapezeKeyDownNotifyPtr;

/* Sent when a key is pressed and the enableNotifyTrapezeProperty is TRUE */
#define keyDownTrapezeNotify            (trapezeNotify + 29)
/* lParam = (LPARAM) (TrapezeKeyDownNotifyPtr) notifyInfo
   Return = (BOOL) ignore
*/

/* Light table save notification */
typedef struct {
  NMHDR hdr; /* Standard WM_NOTIFY header */
  WORD fileNameSize; /* Size of fileName buffer */
  LPSTR fileName; /* File to save to, can be modified, "" = show save
                     dialog */
} TrapezeLightTableSaveNotifyRec, * TrapezeLightTableSaveNotifyPtr;

/* Sent when the light table save button is clicked. */
#define lightTableSaveTrapezeNotify     (trapezeNotify + 44)
/* lParam = (LPARAM) (TrapezeLightTableSaveNotifyPtr) notifyInfo
   Return = (BOOL) cancel
*/

/* MDI activate notification */
typedef struct {
  NMHDR hdr;
  HWND deactivate;
  HWND activate;
} TrapezeMDIActivateNotifyRec, * TrapezeMDIActivateNotifyPtr;

#define mdiActivateTrapezeNotify        (trapezeNotify + 4)

/* Measure popup notification */
typedef struct {
  NMHDR hdr; /* Standard WM_NOTIFY header */
  BOOL calibrate; /* TRUE if calibrating, FALSE if measuring */
  BOOL dialog; /* When calibrate is TRUE, TRUE if showing the final
                  calibration dialog, FALSE if showing the popup */
  BOOL hasScale; /* TRUE if the page contains scale information */
  WORD areaUnit; /* Area units, one of *TrapezeAreaUnit if calibrate is
                    FALSE */
  WORD unit; /* Units, one of *TrapezeUnit */
} TrapezeMeasurePopupNotifyRec, * TrapezeMeasurePopupNotifyPtr;

/* Sent when the popup is shown because measuring or calibration has
   started, sent by Trapeze 0x6521 or later */
#define measurePopupTrapezeNotify       (trapezeNotify + 35)

/* Measure point notification */
typedef struct {
  NMHDR hdr; /* Standard WM_NOTIFY header */
  DWORD error;
  POINT pt;
} TrapezeMeasurePointNotifyRec, * TrapezeMeasurePointNotifyPtr;

#define measureTrapezePointNotify       (trapezeNotify)

/* Measure scale line notification */
typedef struct {
  NMHDR hdr; /* Standard WM_NOTIFY header */
  DWORD error;
  DWORD numerator;
  DWORD denominator;
  WORD unit;
} TrapezeMeasureScaleLineNotifyRec, * TrapezeMeasureScaleLineNotifyPtr;

#define measureTrapezeScaleLineNotify   (trapezeNotify + 1)

/* Measure unit change notification */
typedef struct {
  NMHDR hdr; /* Standard WM_NOTIFY header */
  BOOL area; /* TRUE if the area units have changed, FALSE if length units
                have changed */
  WORD unit; /* New units, one of *TrapezeUnit if area is FALSE, one of
                *TrapezeAreaUnit if area is TRUE */
  /* Anything below here is present with Trapeze 0x6521 or later */
  BOOL calibrate; /* TRUE if calibrating (area will be FALSE), FALSE if
                     measuring */
} TrapezeMeasureUnitNotifyRec, * TrapezeMeasureUnitNotifyPtr;

/* Sent when units are changed while measuring or calibrating */
#define measureUnitTrapezeNotify        (trapezeNotify + 33)
/* lParam = (LPARAM) (TrapezeMeasureUnitNotifyPtr) notifyInfo
   Return = ignored
*/

/* Page move notification */
typedef struct {
  DWORD srcPageNo; /* Source page number, one based */
  DWORD destPageNo; /* Destination page number, one based */
} TrapezeMovePageRec, * TrapezeMovePagePtr;

typedef struct {
  NMHDR hdr; /* Standard WM_NOTIFY header */
  DWORD count; /* Number of pages moved. Can be zero indicating some pages
                  were moved but an error occurred during the notify. */
  TrapezeMovePageRec pages[1]; /* count entries indicating the moved pages.
                                  Each entry indicates the source and
                                  destination page numbers. Each page number
                                  is the page number after the previous pages
                                  have been moved i.e. the second set of page
                                  numbers in the list are after the first set
                                  has been moved. The destination page number
                                  is the page number before the source page
                                  has been removed, and can be one larger than
                                  the number of pages, indicating a move to
                                  after the last page. */
} TrapezeMovePagesNotifyRec, * TrapezeMovePagesNotifyPtr;

/* Sent when pages have been moved in the current document and both the
   enableNotifyTrapezeProperty and extendedPageNotifyTrapezeProperty
   properties are TRUE. */
#define movePagesTrapezeNotify          (trapezeNotify + 15)
/* lParam = (LPARAM) (TrapezeMovePagesNotifyPtr) notifyInfo
   Return = ignored
*/

/* Open document notification */
typedef struct {
  NMHDR hdr;
  DWORD error;
} TrapezeOpenDocumentNotifyRec, * TrapezeOpenDocumentNotifyPtr;

#define openDocumentTrapezeNotify       (trapezeNotify + 8)

/* Page number notification */
typedef struct {
  NMHDR hdr; /* Normal WM_NOTIFY header */
  DWORD oldPageNo; /* Page number before page change, zero = none */
  DWORD newPageNo; /* Page number after page change */
  DWORD error; /* Error opening page, ERROR_SUCCESS = none */
} TrapezePageNoNotifyRec, * TrapezePageNoNotifyPtr;

/* Sent when the current page has changed and the enableNotifyTrapezeProperty
   is TRUE. */
#define pageNoTrapezeNotify             (trapezeNotify + 6)
/* lParam = (LPARAM) (TrapezePageNoNotifyPtr) notifyInfo
   Return = ignored
*/

#define postTrapezeDialogBoxNotify      (trapezeNotify + 3)

#define preTrapezeDialogBoxNotify       (trapezeNotify + 2)

/* Print types */
#define printTrapezeTypeDocument       0 /* Printed pages of a document */
#define printTrapezeTypeFunctionSheets 1 /* Printed function sheets */

/* Print notification */
typedef struct {
  NMHDR hdr; /* Normal WM_NOTIFY header */
  BYTE type; /* Print type, one of printTrapezeType*; new values could be
                added in the future */
  DWORD copies; /* Number of copies printed */
  DWORD error; /* Error code, ERROR_SUCCESS = printed successfully */
  DWORD pageCount; /* Number of pages printed */
} TrapezePrintNotifyRec, * TrapezePrintNotifyPtr;

/* Sent when something has been printed and both the
   enableNotifyTrapezeProperty and printNotifyTrapezeProperty properties are
   TRUE. */
#define printTrapezeNotify              (trapezeNotify + 22)

/* Progress notification */
typedef struct {
  NMHDR hdr; /* Normal WM_NOTIFY header */
  WORD percentage; /* Percentage of operation complete, 0-100 */
} TrapezeProgressNotifyRec, * TrapezeProgressNotifyPtr;

#define progressTrapezeNotify           (trapezeNotify + 50)
/* lParam = (LPARAM) (TrapezeProgressNotifyPtr) notifyInfo
   Return = (BOOL) cancel
*/

/* Save document complete notification */
typedef struct {
  NMHDR hdr; /* Normal WM_NOTIFY header */
  WORD command; /* Save command that has completed, one of:
                   trapezeSaveAsCommand, trapezeSaveCommand,
                   trapezeSaveCopyAsCommand, trapezeSaveSelectionCommand,
                   trapezeSavePageCommand, trapezeLocalSaveCopyAsCommand. */
  BOOL saveFailed; /* TRUE if the save failed; this field is only present for
                      Trapeze version 0x5303 or later. The user will have
                      already been told of the reason for the failure. */
} TrapezeSaveDocumentNotifyRec, * TrapezeSaveDocumentNotifyPtr;

/* Sent when a save command has completed and the enableNotifyTrapezeProperty
   is TRUE. */
#define saveDocumentTrapezeNotify       (trapezeNotify + 11)
/* lParam = (LPARAM) (TrapezeSaveDocumentNotifyPtr) notifyInfo
   Return = ignored
*/

/* Scroll notification */
typedef struct {
  NMHDR hdr; /* Normal WM_NOTIFY header */
  BYTE pane; /* Pane being scrolled, one of trapeze*Pane */
} TrapezeScrollNotifyRec, * TrapezeScrollNotifyPtr;

#define scrollTrapezeNotify             (trapezeNotify + 49)
/* lParam = (LPARAM) (TrapezeScrollNotifyPtr) notifyInfo
   Return = ignored
*/

/* Pane show/hide notification */
typedef struct {
  NMHDR hdr; /* Normal WM_NOTIFY header */
  BYTE pane; /* Pane being shown or hidden, one of trapeze*Pane */
  BOOL show; /* TRUE if the pane is being shown, FALSE if it is being
                hidden */
} TrapezeShowPaneNotifyRec, * TrapezeShowPaneNotifyPtr;

#define showPaneTrapezeNotify           (trapezeNotify + 46)
/* lParam = (LPARAM) (TrapezeShowPaneNotifyPtr) notifyInfo
   Return = (BOOL) cancel
*/

#define sizeTrapezeNotify               (trapezeNotify + 5)

#ifdef UNICODE
#define TrapezeStatusTextTNotifyPtr TrapezeStatusTextWNotifyPtr
#define TrapezeStatusTextTNotifyRec TrapezeStatusTextWNotifyRec
#define statusTextTTrapezeNotify statusTextWTrapezeNotify
#else
#define TrapezeStatusTextTNotifyPtr TrapezeStatusTextNotifyPtr
#define TrapezeStatusTextTNotifyRec TrapezeStatusTextNotifyRec
#define statusTextTTrapezeNotify statusTextTrapezeNotify
#endif /*UNICODE*/

/* Status bar text notification */
typedef struct {
  NMHDR hdr; /* Standard WM_NOTIFY header */
  WORD command; /* Command, one of trapeze*Command */
  WORD textSize; /* Size of text buffer */
  LPSTR text; /* Text for command, can be modified */
} TrapezeStatusTextNotifyRec, * TrapezeStatusTextNotifyPtr;

/* Sent to allow modification of status bar command text */
#define statusTextTrapezeNotify         (trapezeNotify + 39)
/* lParam = (LPARAM) (TrapezeStatusTextNotifyPtr) notifyInfo
   Return = ignored
*/

/* Status bar text notification (Unicode version) */
typedef struct {
  NMHDR hdr; /* Standard WM_NOTIFY header */
  WORD command; /* Command, one of trapeze*Command */
  WORD textSize; /* Size of text buffer */
  LPWSTR text; /* Text for command, can be modified */
} TrapezeStatusTextWNotifyRec, * TrapezeStatusTextWNotifyPtr;

/* Sent to allow modification of status bar command text (Unicode version) */
#define statusTextWTrapezeNotify        (trapezeNotify + 45)
/* lParam = (LPARAM) (TrapezeStatusTextWNotifyPtr) notifyInfo
   Return = ignored
*/

#define thumbnailSelChangeTrapezeNotify (trapezeNotify + 9)

/* Toolbar show/hide information */
typedef struct {
  NMHDR hdr; /* Standard WM_NOTIFY header */
  BOOL visible; /* TRUE if the toolbar has been shown, FALSE if it has been
                   hidden */
} TrapezeViewToolbarNotifyRec, * TrapezeViewToolbarNotifyPtr;

/* Sent when the toolbar has been shown or hidden and the
   enableNotifyTrapezeProperty is TRUE. */
#define viewToolbarTrapezeNotify        (trapezeNotify + 16)
/* lParam = (LPARAM) (TrapezeViewToolbarNotifyPtr) notifyInfo
   Return = ignored
*/

#define zoomTrapezeNotify               (trapezeNotify + 7)

typedef struct {
  WORD min;
  WORD max;
} TrapezeScrollRangeRec, * TrapezeScrollRangePtr;

#endif /*_WIN32*/

/* JPEG Definitions and Types */

#define compressedSizeTrapezeJPEGInfo 0x0001 /* Set to get compressed size,
                                                requires parsing the whole
                                                JPEG */
#define mcuDataTrapezeJPEGInfo        0x0002 /* Set to determine if MCU data
                                                can be extracted, may require
                                                parsing the whole JPEG */

typedef struct TrapezeJPEGStruct {
  int unused;
} * TrapezeJPEG;

typedef struct {
  TrapezeWORD size; /* Size of this structure, in bytes */
  TrapezeWORD flags; /* Flags indicating what to read, a combination of
                        *TrapezeJPEGInfo */
  TrapezeWORD width; /* Width of the JPEG, in pixels */
  TrapezeWORD height; /* Height of the JPEG, in pixels */
  TrapezeBOOL canGetMCUData; /* TRUE if GetTrapezeJPEGMCUData() or
                                GetTrapezeJPEGMCUDataProgress() can be called
                                for this JPEG, returned if
                                mcuDataTrapezeJPEGInfo was set in flags */
  TrapezeSIZE_T compressedSize; /* Compressed size of JPEG, returned if
                                   compressedSizeTrapezeJPEGInfo was set in
                                   flags */
  TrapezeTIFFResolutionScaleRec resolution; /* Resolution */
  TrapezeBYTE colorSpace; /* Color space, one of *TrapezeJPEGColorSpace */
  TrapezeBYTE sampleCount; /* Number of samples in the image */
  TrapezeBYTE samplePrecision; /* Number of bits per sample */
  TrapezeBYTE mcuWidth; /* Width of an MCU, in pixels */
  TrapezeBYTE mcuHeight; /* Height of an MCU, in pixels */
} TrapezeJPEGInfoRec, * TrapezeJPEGInfoPtr;

typedef struct {
  TrapezeWORD size; /* Size of this structure, in bytes */
  TrapezeWORD type; /* Type of MCU data, can be 1 or 2 */
  TrapezeLPCRECT rect; /* Rectangle, NULL = get compression information */
  TrapezeLPVOID lParam; /* Parameter for progressProc() */
  TrapezeProgressProc progressProc; /* Progress function, NULL = none */
} GetTrapezeJPEGMCUDataRec, * GetTrapezeJPEGMCUDataPtr;
typedef const GetTrapezeJPEGMCUDataRec * GetTrapezeJPEGMCUDataCPtr;

/* Job separator definitions */

#define trapezeJobSeparatorNotFound 0 /* Job separator not found */
#define trapezeJobSeparatorFound    1 /* Job separator found */
#define trapezeJobSeparatorError    2 /* An error occurred */

/* Debug definitions */

#define dllTrapezeDebug     0x00000001 /* DLL call debug messages */
#define memoryTrapezeDebug  0x00000002 /* Memory debug messages */
#define stackTrapezeDebug   0x00000004 /* Stack debug messages */
#define warningTrapezeDebug 0x00000008 /* Warning debug messages */
/* On = no breaks, off = debug break if in a debugger and an uninitialized
   value is passed to a function or message */
#define noUninitializedBreakTrapezeDebug 0x00000010

/* Document type capabilities */

#define trapezeDocTypeCapsFailed   0xFFFFFFFF
#define trapezeDocTypeCapsOpen     0x00000001 /* The document type can be
                                                 opened */
#define trapezeDocTypeCapsAnnotate 0x00000002 /* The document type can be
                                                 annotated */
#define trapezeDocTypeCapsRedact   0x00000004 /* The document type can be
                                                 redacted */

/* DIB drawing modes */
#define copyTrapezeDIBMode        0 /* Pixels are copied */
#define transparentTrapezeDIBMode 1 /* Pixels drawn transparently (and'ed) */
#define mixTrapezeDIBMode         2 /* Pixels are drawn mixed (averaged) */

/* Exif definitions */

#define getTrapezeExifInfoIFD0       0x0000 /* Get information from IFD 0
                                               (primary image) */
#define getTrapezeExifInfoIFD1       0x0001 /* Get information from IFD 1
                                               (thumbnail) */
#define getTrapezeExifInfoExifIFD0   0x0002 /* Get information from Exif
                                               IFD from IFD 0 (primary
                                               image) */
#define getTrapezeExifInfoGPSIFD0    0x0003 /* Get information from GPS IFD
                                               from IFD 0 (primary image) */
#define getTrapezeExifInfoIFD0Exif   0x0004 /* Get information from
                                               IFD 0 (primary image), or from
                                               Exif IFD from IFD 0 if not
                                               present in IFD 0 */
/* Text in parentheses indicates the correct location of the data */
#define getTrapezeExifInfoSoftware   0x0008 /* Get software (IFD) */
#define getTrapezeExifInfoDateTime   0x0010 /* Get date/time (IFD) */
#define getTrapezeExifInfoTag        0x0020 /* Get tag by number specified in
                                               tagNo (depends on tagNo) */
#define getTrapezeExifInfoDateTimeOriginal 0x0040 /* Get date/time of original
                                                     data generation
                                                     (Exif IFD) */
#define getTrapezeExifInfoDateTimeDigitized 0x0080 /* Get date/time of digital
                                                      data generation
                                                      (Exif IFD) */
#define getTrapezeExifInfoNextTag    0x0100 /* When getTrapezeExifInfoTag is
                                               set, if tagNo is not found,
                                               finds the next highest tag and
                                               changes tagNo to indicate the
                                               tag found */
#define getTrapezeExifInfoAnyType    0x0200 /* When getTrapezeExifInfoTag is
                                               set, will return tag types not
                                               valid for Exif */

#define byteTrapezeExifTagType      1 /* 8-bit unsigned integer */
#define asciiTrapezeExifTagType     2 /* 8-bit bytes containing 7-bit ASCII
                                         codes, terminated with '\0' */
#define shortTrapezeExifTagType     3 /* 16-bit (2-byte) unsigned integer */
#define longTrapezeExifTagType      4 /* 32-bit (4-byte) unsigned integer */
#define rationalTrapezeExifTagType  5 /* Two LONGs. The first LONG is the
                                         numerator and the second LONG is the
                                         denominator */
#define undefinedTrapezeExifTagType 7 /* 8-bit byte that can take any value
                                         depending on the field definition */
#define slongTrapezeExifTagType     9 /* 32-bit (4-byte) signed integer */
#define srationalTrapezeExifTagType 10 /* Two SLONGs. The first SLONG is the
                                          numerator and the second SLONG is the
                                          denominator */

typedef struct {
  TrapezeWORD size; /* Size of this structure, in bytes, must be set before
                       calling GetTrapezeExifInfo() */
  TrapezeWORD flags; /* Flags specifying what information to get, a
                        combination of getTrapezeExifInfo* values, must be set
                        before calling GetTrapezeExifInfo() */
  TrapezeHGLOBAL exifData; /* Exif data, retrieved using
                              ReadTrapezeExifData() */
  TrapezeSIZE_T exifDataSize; /* Size of exifData, retrieved using
                                 ReadTrapezeExifData() */
  TrapezeHGLOBAL software; /* Software, contains a NULL terminated string, set
                              when getTrapezeExifInfoSoftware is set in flags,
                              NULL = not found, must be freed using using
                              GlobalFree() when no longer required */
  char dateTime[20]; /* Date/time, in the format "YYYY:MM:DD HH:MM:SS", with a
                        NULL terminator, set when getTrapezeExifInfoDateTime
                        is set in flags, "" (empty string) = not found */
  TrapezeWORD tagNo; /* Tag number, specified when getTrapezeExifInfoTag is
                        set in flags, to specify which tag to retrieve data
                        for */
  TrapezeBYTE tagType; /* Type for tag data, returned when
                          getTrapezeExifInfoTag is set in flags, one of
                          *TrapezeExifTagType */
  TrapezeDWORD tagCount; /* Number of values in tag data, returned when
                            getTrapezeExifInfoTag is set in flags. This is the
                            number of values, not the number of bytes.
                            e.g. for type asciiTrapezeExifTagType, count is
                            the length of the string, including the NULL
                            terminator; for shortTrapezeExifTagType, count is
                            the number of WORDs in the data array */
  TrapezeHGLOBAL tagData; /* Tag data, contains an array of values, set when
                             getTrapezeExifInfoTag is set in flags, NULL = not
                             found, must be freed using using
                             GlobalFree() when no longer required */
  char dateTimeOriginal[20]; /* Date/time of original data generation, in the
                                format "YYYY:MM:DD HH:MM:SS", with a NULL
                                terminator, set when
                                getTrapezeExifInfoDateTimeOriginal is set in
                                flags, "" (empty string) = not found */
  char dateTimeDigitized[20]; /* Date/time of digital data generation, in the
                                 format "YYYY:MM:DD HH:MM:SS", with a NULL
                                 terminator, set when
                                 getTrapezeExifInfoDateTimeDigitized is set in
                                 flags, "" (empty string) = not found */
} GetTrapezeExifInfoRec, * GetTrapezeExifInfoPtr;

/* Flags for DespeckleTrapezeDIB() */
#define despeckleTrapezeDIBBlack   0x01 /* Remove black speckles */
#define despeckleTrapezeDIBWhite   0x02 /* Remove white speckles */
#define despeckleTrapezeDIBInPlace 0x04 /* Despeckle the DIB in-place */

/* Initialization definitions */

#ifdef _WIN32
#define initTrapezeNoOLE 0x00000001 /* Don't use OLE */
#define initTrapezeNoGUI 0x00000002 /* Don't enable GUI functions */
#endif /*_WIN32*/

#ifdef _WIN32
#if ! defined(_WIN32_WINNT) || (_WIN32_WINNT < 0x0400) || \
    ! defined(__WINCRYPT_H__)
#define PCCERT_CONTEXT LPCVOID
#endif /*! defined(_WIN32_WINNT) || (_WIN32_WINNT < 0x0400) ||
         ! defined(__WINCRYPT_H__)*/
#endif /*_WIN32*/

#ifdef TRAPEZEDYNAMIC
#include "trpzdyn.h"
#else

/* Annotation Functions
   ==================== */

/* BurnInTrapezeAnnotations
   Burns annotations into a DIB. annotations and annotationsSize must be
   values returned by GetTrapezeTIFFAnnotations(),
   InsertTrapezeAnnotation() or LoadTrapezeAnnotations(). dibData is the data
   for the DIB pixels. dib contains information about the DIB. flags is a
   combination of:
   - increaseColorsTrapezeBurnIn, if necessary, the number of colors in the
     DIB is increased so that there is no annotation color loss during the
     burn in. If this flag is not set, the nearest available colors from the
     image are used.
   - inPlaceTrapezeBurnIn, to, if possible, burn the annotations into the
     passed in DIB rather than returning a new DIB.
   - redactionsTrapezeBurnIn, to only burn in redactions from annotations.
     After the burn in, the burned in redactions are removed from annotations.
     When this flag is set, annotationsSize is a TrapezeLPDWORD, which is
     modified to reflect the new size of annotations. If annotationSize is set
     to zero, all annotations were redactions and annotations should be freed
     using DestroyTrapezeAnnotations() immediately.
   - showAllTrapezeBurnIn, to burn in any annotations hidden by group.
   The return value is a handle to a new DIB, burnInTrapezeAnnotationsInPlace,
   or NULL on failure. On failure, use GetLastError() to get the cause of the
   failure. burnInTrapezeAnnotationsInPlace is returned if
   inPlaceTrapezeBurnIn was set and the annotations were burned into the
   passed in DIB; in that case the data pointed to by dibData will have been
   modified. If a new DIB is returned, it must be freed using GlobalFree()
   when it is no longer required. */
extern TrapezeHGLOBAL TrapezeAPI BurnInTrapezeAnnotations(TrapezeAnnotations
                                                          annotations,
                                                          TrapezeDWORD_PTR
                                                          annotationsSize,
                                                          const TrapezeBYTE*
                                                          dibData,
                                                          const
                                                          TrapezeBITMAPINFO*
                                                          dib,
                                                          TrapezeDWORD flags);

/* BurnInTrapezeAnnotationsEx
   Burns annotations into a DIB, according to information in burnInInfo. The
   return value is a handle to a new DIB, burnInTrapezeAnnotationsInPlace, or
   NULL on failure. On failure, use GetLastError() to get the cause of the
   failure. burnInTrapezeAnnotationsInPlace is returned if
   inPlaceTrapezeBurnIn was set and the annotations were burned into the
   passed in DIB; in that case the data pointed to by dibData will have been
   modified. If a new DIB is returned, it must be freed using GlobalFree()
   when it is no longer required. */
extern TrapezeHGLOBAL TrapezeAPI BurnInTrapezeAnnotationsEx(
                                                 BurnInTrapezeAnnotationsExPtr
                                                            burnInInfo);

/* DestroyTrapezeAnnotations
   Destroys annotations returned by GetTrapezeTIFFAnnotations() or
   InsertTrapezeAnnotation(). The return value is TRUE on success, or FALSE on
   failure. On failure, use GetLastError() to get the cause of the error.
   annotations must not be used in any other Trapeze calls after this function
   has been called. */
extern TrapezeBOOL TrapezeAPI DestroyTrapezeAnnotations(TrapezeAnnotations
                                                        annotations);

/* GetTrapezeAnnotationData
   Converts annotations into file storage format. annotations must be created
   using InsertTrapezeAnnotation(), GetTrapezeTIFFAnnotations() or
   LoadTrapezeAnnotations(). Before calling, size must be set to the size of
   annotations. On return, size is set to the size of the annotation data.
   format specifies the format to use; one of trapeze*AnnotationFormat. The
   return value is a handle containing the data on success, or NULL on
   failure. On failure, use GetLastError() to get the cause of the error. */
extern TrapezeHGLOBAL TrapezeAPI GetTrapezeAnnotationData(TrapezeAnnotations
                                                          annotations,
                                                          TrapezeLPDWORD size,
                                                          TrapezeBYTE format);

/* GetTrapezeAnnotationGroups
   Gets the groups an annotation is in. annotations and annotationsSize must be
   values returned by GetTrapezeTIFFAnnotations() or InsertTrapezeAnnotation().
   annotationNo specifies the zero based number of the annotation to retrieve
   the groups from. annotationNo must be less than the value returned by
   GetTrapezeAnnotationsCount(), otherwise the function will fail with error
   ERROR_FILE_NOT_FOUND. groups->size must be set before calling this
   function. The return value is TRUE on success, or FALSE on failure. On
   failure, use GetLastError() to get the cause of the error. */
extern BOOL TrapezeAPI GetTrapezeAnnotationGroups(TrapezeAnnotations
                                                  annotations,
                                                  TrapezeDWORD
                                                  annotationsSize,
                                                  TrapezeDWORD annotationNo,
                                                 GetTrapezeAnnotationGroupsPtr
                                                  groups);

/* GetTrapezeAnnotationProperty
   Gets a property of an annotation. annotations and annotationsSize must be
   values returned by GetTrapezeTIFFAnnotations() or
   InsertTrapezeAnnotation(). annotationNo specifies the zero based number of
   the annotation to retrieve the property from. annotationNo must be less
   than the value returned by GetTrapezeAnnotationsCount(), otherwise the
   function will fail with error ERROR_FILE_NOT_FOUND. property specifies the
   name of the property to get. All annotations have the property "Type",
   which has type VT_UI1; its value is one of *TrapezeAnnotationType. Other
   properties depend on the annotation type. The value is returned in value;
   its type depends on the property. The return value is TRUE on success, or
   FALSE on failure. On failure, use GetLastError() to get the cause of the
   error. */
extern TrapezeBOOL TrapezeAPI GetTrapezeAnnotationProperty(TrapezeAnnotations
                                                           annotations,
                                                           TrapezeDWORD
                                                           annotationsSize,
                                                           TrapezeDWORD
                                                           annotationNo,
                                                           TrapezeLPCSTR
                                                           property,
#ifdef NOTRAPEZEVARIANT
                                                           TrapezeLPVOID
                                                           value
#else
                                                           TrapezeVARIANT*
                                                           value
#endif /*NOTRAPEZEVARIANT*/
                                                           );

/* GetTrapezeAnnotationsCount
   Returns the number of annotations. annotations and annotationsSize must be
   values returned by GetTrapezeTIFFAnnotations() or
   InsertTrapezeAnnotation(). The return value is
   getTrapezeAnnotationsCountFailed if an error occurred. On failure, use
   GetLastError() to get the cause of the error. */
extern TrapezeDWORD TrapezeAPI GetTrapezeAnnotationsCount(TrapezeAnnotations
                                                          annotations,
                                                          TrapezeDWORD
                                                          annotationsSize);

/* GetTrapezeAnnotationText
   Gets the text of an annotation. annotations and annotationsSize must be
   values returned by GetTrapezeTIFFAnnotations() or
   InsertTrapezeAnnotation(). annotationNo specifies the zero based number of
   the annotation to retrieve the text from. annotationNo must be less than
   the value returned by GetTrapezeAnnotationsCount(), otherwise the function
   will fail with error ERROR_FILE_NOT_FOUND. The text is returned in text if
   text is not NULL; if text is NULL the text is not returned. If text is not
   NULL textSize specifies the size of text, in characters. The return value
   is the length of the annotation text, in characters, not including the NULL
   terminator, or getTrapezeAnnotationTextFailed on failure. On failure, use
   GetLastError() to get the cause of the error. */
extern TrapezeDWORD TrapezeAPI GetTrapezeAnnotationText(TrapezeAnnotations
                                                        annotations,
                                                        TrapezeDWORD
                                                        annotationsSize,
                                                        TrapezeDWORD
                                                        annotationNo,
                                                        TrapezeLPSTR text,
                                                        TrapezeDWORD
                                                        textSize);

/* InsertTrapezeAnnotation
   Inserts a new annotation into annotations. If *annotations is NULL, new
   annotations are created, otherwise *annotations and *annotationsSize must
   be values returned by GetTrapezeTIFFAnnotations() or
   InsertTrapezeAnnotation(). annotationType is one of
   insert*TrapezeAnnotation. insertInfo depends on annotationType. The return
   value is the number of the new annotation, or insertTrapezeAnnotationFailed
   on failure. On failure, use GetLastError() to get the cause of the error.
   On success, *annotations may be changed to a new value; if it is, the old
   value is no longer valid and must not be destroyed. The returned
   annotations must be freed using DestroyTrapezeAnnotations() when they are
   no longer required.
   Note: because there is no image associated with these annotations, Trapeze
   cannot perform operations or checks that require image information, such as
   auto-sizing annotations, or checking that annotations locations are
   valid. */
extern TrapezeDWORD TrapezeAPI InsertTrapezeAnnotation(TrapezeAnnotations*
                                                       annotations,
                                                       TrapezeLPDWORD
                                                       annotationsSize,
                                                       TrapezeDWORD
                                                       annotationType,
                                                       TrapezeLPCVOID
                                                       insertInfo);

/* LoadTrapezeAnnotations
   Loads annotations from a file for use in other annotation functions.
   fileName specifies the file to read the annotations from, created using
   SaveTrapezeAnnotations(), or an .art file. For an .art file, pageNo
   specifies the page number (one based) to read, otherwise pageNo is ignored.
   The size of the annotations is returned in annotationsSize. If warnings is
   not NULL, it is set to zero for no warnings, or a combination of
   trapezeAnnotation*Warning values indicating any warnings. The return value
   is not NULL on success or NULL on failure. On failure, use GetLastError()
   to get the cause of the error. */
extern TrapezeAnnotations TrapezeAPI LoadTrapezeAnnotations(TrapezeLPCSTR
                                                            fileName,
                                                            TrapezeDWORD
                                                            pageNo,
                                                            TrapezeLPDWORD
                                                            annotationsSize,
                                                            TrapezeLPDWORD
                                                            warnings);

/* SaveTrapezeAnnotations
   Saves annotations to a file. annotations and annotationsSize must be
   annotations created using InsertTrapezeAnnotation(),
   GetTrapezeTIFFAnnotations() or LoadTrapezeAnnotations(). fileName specifies
   the file to write the annotations to, for subsequent use with
   LoadTrapezeAnnotations(). format specifies the format to use, one of
   trapeze*AnnotationFormat. The return value is TRUE on success or FALSE on
   failure. On failure, use GetLastError() to get the cause of the error. */
extern TrapezeBOOL TrapezeAPI SaveTrapezeAnnotations(TrapezeAnnotations
                                                     annotations,
                                                     TrapezeDWORD
                                                     annotationsSize,
                                                     TrapezeLPCSTR fileName,
                                                     TrapezeBYTE format);

/* SetTrapezeAnnotationGroups
   Sets the groups belonged to by an annotation. annotations and
   annotationsSize must be values returned by GetTrapezeTIFFAnnotations() or
   InsertTrapezeAnnotation(). annotationNo specifies the zero based number of
   the annotation to set the groups on. annotationNo must be less than the
   value returned by GetTrapezeAnnotationsCount(), otherwise the function will
   fail with error ERROR_FILE_NOT_FOUND. groups specifies the groups for the
   annotation to belong to, or NULL for the default group ("[Untitled]").
   Annotation groups are case-sensitive. The return value is TRUE on success,
   or FALSE on failure. On failure, use GetLastError() to get the cause of the
   error. */
extern TrapezeBOOL TrapezeAPI SetTrapezeAnnotationGroups(TrapezeAnnotations*
                                                         annotations,
                                                         TrapezeLPDWORD
                                                         annotationsSize,
                                                         TrapezeDWORD
                                                         annotationNo,
                                                SetTrapezeAnnotationGroupsCPtr
                                                         groups);

/* SetTrapezeAnnotationProperty
   Sets a property of an annotation. annotations and annotationsSize must be
   values returned by GetTrapezeTIFFAnnotations() or
   InsertTrapezeAnnotation(). annotationNo specifies the zero based number of
   the annotation to set the property on. annotationNo must be less than the
   value returned by GetTrapezeAnnotationsCount(), otherwise the function will
   fail with error ERROR_FILE_NOT_FOUND. property specifies the name of the
   property to set. The property "Type" cannot be set. Other properties depend
   on the annotation type. value specifies the new value of the property; its
   type depends on the property. The return value is TRUE on success, or FALSE
   on failure. On failure, use GetLastError() to get the cause of the
   error. */
extern TrapezeBOOL TrapezeAPI SetTrapezeAnnotationProperty(TrapezeAnnotations*
                                                           annotations,
                                                           TrapezeLPDWORD
                                                           annotationsSize,
                                                           TrapezeDWORD
                                                           annotationNo,
                                                           TrapezeLPCSTR
                                                           property,
#ifdef NOTRAPEZEVARIANT
                                                           TrapezeLPCVOID
                                                           value
#else
                                                           const
                                                           TrapezeVARIANT*
                                                           value
#endif /*NOTRAPEZEVARIANT*/
                                                           );

/* TrapezeAnnotationsFlags
   Gets information about annotations. annotations and annotationsSize must be
   values returned by GetTrapezeTIFFAnnotations() or
   InsertTrapezeAnnotation(). flags specifies the information to check for, a
   combination of *TrapezeAnnotationFlag values, except
   errorTrapezeAnnotationFlag. The return value is all or some of the values
   specified in flags, indicating what requested information is found in the
   annotations. On failure, the return value is errorTrapezeAnnotationFlag. On
   failure, use GetLastError() to get the cause of the error. */
extern TrapezeDWORD TrapezeAPI TrapezeAnnotationsFlags(TrapezeAnnotations
                                                       annotations,
                                                       TrapezeDWORD
                                                       annotationsSize,
                                                       TrapezeDWORD flags);

/* TrapezeAnnotationsHaveRedactions
   Returns TRUE if annotations contains redactions, FALSE if not or on error.
   annotations and annotationsSize must be values returned by
   GetTrapezeTIFFAnnotations() or InsertTrapezeAnnotation(). The last error
   value is not updated on success, so to determine success or failure for a
   FALSE return value, call SetLastError(ERROR_SUCCESS) before calling
   TrapezeAnnotationsHaveRedactions(), then call GetLastError() to get the
   cause of the error. If GetLastError() returns ERROR_SUCCESS then the
   annotations don't contain redactions, otherwise an error occurred. */
extern TrapezeBOOL TrapezeAPI
TrapezeAnnotationsHaveRedactions(TrapezeAnnotations annotations,
                                 TrapezeDWORD annotationsSize);

/* Barcode Functions
   ================= */

/* DestroyTrapezeBarcodes
   Destroys barcodes returned by FindTrapezeBarcodes(),
   FindTrapezeBarcodesEx() or findBarcodesTrapezeMsg. The return value is TRUE
   on success, or FALSE on failure. On failure, use GetLastError() to get the
   cause of the error. barcodes must not be used in any other Trapeze calls
   after this function has been called. */
extern TrapezeBOOL TrapezeAPI DestroyTrapezeBarcodes(TrapezeBarcodes
                                                     barcodes);

/* FindTrapezeBarcodes
   Finds barcodes in a DIB. rect specifies the rectangle to be searched. If
   rect is NULL the entire DIB is searched. dibData is the data for the DIB
   pixels. dib contains information about the DIB. flags specifies the
   barcodes to search for. xIncrement specifies the horizontal search jump
   when searching for 90 or 270 degree rotated barcodes. xIncrement needs to
   be small enough that barcodes aren't skipped (maximum of half the barcode
   width), but very small values will decrease speed. xIncrement is ignored if
   flags doesn't contain findTrapezeBarcode90 or findTrapezeBarcode270.
   yIncrement specifies the vertical search jump when searching for 0 or 180
   degree rotated barcodes, as for xIncrement. yIncrement is ignored if
   flags doesn't contain findTrapezeBarcode0 or findTrapezeBarcode180.
   searchHeight specifies the maximum search height when finding bars. Larger
   values will increase accuracy but reduce speed. If searchHeight is zero
   there is no limit on the height when finding bars. minHeight specifies the
   minimum height for bars - any bars shorter are ignored. minHeight cannot be
   larger than 2 * searchHeight + 1. maxCount specifies the maximum number of
   barcodes to be found. The search will stop when either maxCount barcodes
   have been found or the entire image has been searched. If maxCount is zero
   there is no limit on the number of barcodes. The return value is any found
   barcodes. If the return value is NULL, either no barcodes were found or an
   error occured. The last error value is not updated on success, so to
   determine success or failure for a NULL return value, call
   SetLastError(ERROR_SUCCESS) before calling FindTrapezeBarcodes(), then call
   GetLastError() to get the cause of the error. If GetLastError() returns
   ERROR_SUCCESS then no barcodes were found, otherwise an error occurred. The
   return value must be freed using DestroyTrapezeBarcodes() when it is no
   longer required.  */
extern TrapezeBarcodes TrapezeAPI FindTrapezeBarcodes(TrapezeLPCRECT rect,
                                                      const TrapezeBYTE*
                                                      dibData,
                                                      const TrapezeBITMAPINFO*
                                                      dib,
                                                      TrapezeDWORD flags,
                                                      int xIncrement,
                                                      int yIncrement,
                                                      TrapezeWORD
                                                      searchHeight,
                                                      TrapezeWORD minHeight,
                                                      TrapezeWORD maxCount);

/* FindTrapezeBarcodesEx
   Finds barcodes in a DIB, according to information in findInfo. dibData and
   dib specify the DIB to search. findInfo specifies barcode searching
   information, or NULL to use defaults and search the entire DIB.
   findInfo->rect specifies the rectangle to be searched. If findInfo->rect is
   empty the entire DIB is searched. findInfo->pageNo is ignored.
   findInfo->barcodes is ignored and not changed. The return value is any
   found barcodes. If the return value is NULL, either no barcodes were found
   or an error occured. The last error value is not updated on success, so to
   determine success or failure for a NULL return value, call
   SetLastError(ERROR_SUCCESS) before calling FindTrapezeBarcodesEx(), then
   call GetLastError() to get the cause of the error. If GetLastError()
   returns ERROR_SUCCESS then no barcodes were found, otherwise an error
   occurred. The return value must be freed using DestroyTrapezeBarcodes()
   when it is no longer required. */
extern TrapezeBarcodes TrapezeAPI FindTrapezeBarcodesEx(const TrapezeBYTE*
                                                        dibData,
                                                      const TrapezeBITMAPINFO*
                                                        dib,
                                                        TrapezeFindBarcodesPtr
                                                        findInfo);

/* GetTrapezeBarcodeCount
   Returns the number of barcodes found. barcodes is a value returned by
   FindTrapezeBarcodes(). The return value is the number of barcodes in
   barcodes, or getTrapezeBarcodeCountFailed on failure. On failure, use
   GetLastError() to get the cause of the error. */
extern TrapezeDWORD TrapezeAPI GetTrapezeBarcodeCount(TrapezeBarcodes
                                                      barcodes);

/* GetTrapezeBarcodeDirection
   Gets the direction of a found barcode. barcodes is a value returned by
   FindTrapezeBarcodes(). barcodeNo specifies the barcode to retrieve the
   direction for, starting at zero. The return value is one of the
   findTrapezeBarcode* direction flags, or getTrapezeBarcodeDirectionFailed on
   failure. On failure, use GetLastError() to get the cause of the failure. */
extern TrapezeDWORD TrapezeAPI GetTrapezeBarcodeDirection(TrapezeBarcodes
                                                          barcodes,
                                                          TrapezeDWORD
                                                          barcodeNo);

/* GetTrapezeBarcodeRect
   Gets the location of a found barcode. barcodes is a value returned by
   FindTrapezeBarcodes(). barcodeNo specifies the barcode to retrieve the
   location for, starting at zero. The location is returned in rect. The
   return value is TRUE for success or FALSE for failure. On failure, use
   GetLastError() to get the cause of the failure. */
extern TrapezeBOOL TrapezeAPI GetTrapezeBarcodeRect(TrapezeBarcodes barcodes,
                                                    TrapezeDWORD barcodeNo,
                                                    TrapezeLPRECT rect);
/* GetTrapezeBarcodeText
   Gets the text of a found barcode. barcodes is a value returned by
   FindTrapezeBarcodes(). barcodeNo specifies the barcode to retrieve the text
   for, starting at zero. The text is returned in text if text is not NULL.
   textSize specifies the size of text, in characters. If text is NULL the
   function returns the length of the text and textSize is ignored. The return
   value is the length of the text (without the NULL terminator) on success,
   getTrapezeBarcodeTextFailed on failure. On failure, use GetLastError() to
   get the cause of the failure. On success, the returned length is the entire
   length of the text, which may be larger than what was actually returned in
   text. */
extern TrapezeDWORD TrapezeAPI GetTrapezeBarcodeText(TrapezeBarcodes barcodes,
                                                     TrapezeDWORD barcodeNo,
                                                     TrapezeLPSTR text,
                                                     TrapezeDWORD textSize);

/* GetTrapezeBarcodeType
   Gets the type of a found barcode. barcodes is a value returned by
   FindTrapezeBarcodes(). barcodeNo specifies the barcode to retrieve the type
   for, starting at zero. The return value is a combination of the
   findTrapezeBarcode* type flags, or getTrapezeBarcodeTypeFailed on failure.
   On failure, use GetLastError() to get the cause of the failure. */
extern TrapezeDWORD TrapezeAPI GetTrapezeBarcodeType(TrapezeBarcodes barcodes,
                                                     TrapezeDWORD barcodeNo);

/* Bookmark Functions
   ================== */

/* ChangeTrapezeBookmark
   Changes a bookmark. bookmarks must be a pointer to bookmarks created using
   CreateTrapezeBookmarks(), GetTrapezeTIFFDocumentBookmarks() or
   LoadTrapezeBookmarks(). bookmarkNo is the identifier of the bookmark to
   change. pageNo is the page number (one based) of the bookmark, or
   ignored when changing a bookmark folder. name is the name of the
   bookmark. description is the name of the bookmark. The return value is TRUE
   on success, or FALSE on failure. On failure, use GetLastError() to get the
   cause of the error. */
extern TrapezeBOOL TrapezeAPI ChangeTrapezeBookmark(TrapezeBookmarksPtr
                                                    bookmarks,
                                                    TrapezeDWORD bookmarkNo,
                                                    TrapezeDWORD pageNo,
                                                    TrapezeLPCSTR name,
                                                    TrapezeLPCSTR
                                                    description);

/* CreateTrapezeBookmarks
   Creates bookmarks. The return value is not NULL on success or NULL on
   failure. On failure, use GetLastError() to get the cause of the error. */
extern TrapezeBookmarks TrapezeAPI CreateTrapezeBookmarks(void);

/* DeleteTrapezeBookmark
   Deletes a bookmark. bookmarks is a value returned by
   CreateTrapezeBookmarks(), GetTrapezeTIFFDocumentBookmarks() or
   LoadTrapezeBookmarks(). bookmarkNo is the bookmark to delete. If the
   bookmark is a folder all child bookmarks will also be deleted. The return
   value is TRUE on success, or FALSE on failure. On failure, use
   GetLastError() to get the cause of the error.
   Following the delete, any existing bookmark numbers must be assumed to be
   invalid. */
extern TrapezeBOOL TrapezeAPI DeleteTrapezeBookmark(TrapezeBookmarksPtr
                                                    bookmarks,
                                                    TrapezeDWORD bookmarkNo);

/* DestroyTrapezeBookmarks
   Destroys bookmarks created with CreateTrapezeBookmarks(),
   GetTrapezeTIFFDocumentBookmarks() or LoadTrapezeBookmarks(). The return
   value is TRUE on success or FALSE on failure. On failure, use
   GetLastError() to get the cause of the error. */
extern TrapezeBOOL TrapezeAPI DestroyTrapezeBookmarks(TrapezeBookmarks
                                                      bookmarks);

/* GetNextTrapezeBookmark
   Gets the next bookmark at the same level under the same parent. bookmarks
   is a value returned by CreateTrapezeBookmarks(),
   GetTrapezeTIFFDocumentBookmarks() or LoadTrapezeBookmarks(). bookmarkNo
   specifies the bookmark to retrieve the next bookmark for, starting at zero.
   The return value is getNextTrapezeBookmarkFailed on failure, or
   noNextTrapezeBookmark or the zero based bookmark number on success. On
   failure, use GetLastError() to get the cause of the error. */
extern TrapezeDWORD TrapezeAPI GetNextTrapezeBookmark(TrapezeBookmarks
                                                      bookmarks,
                                                      TrapezeDWORD
                                                      bookmarkNo);

/* GetTrapezeBookmarkChild
   Gets the first child of a bookmark. bookmarks is a value returned by
   CreateTrapezeBookmarks(), GetTrapezeTIFFDocumentBookmarks() or
   LoadTrapezeBookmarks(). bookmarkNo specifies the bookmark to retrieve the
   first child for, starting at zero; it must specify a bookmark folder.
   bookmarkNo can be noTrapezeBookmarkParent to get the first bookmark. The
   return value is getTrapezeBookmarkChildFailed on failure, or
   noTrapezeBookmarkChild or the zero based bookmark number on success. On
   failure, use GetLastError() to get the cause of the error. */
extern TrapezeDWORD TrapezeAPI GetTrapezeBookmarkChild(TrapezeBookmarks
                                                       bookmarks,
                                                       TrapezeDWORD
                                                       bookmarkNo);

/* GetTrapezeBookmarkCount
   Returns the number of bookmarks. bookmarks is a value returned by
   CreateTrapezeBookmarks(), GetTrapezeTIFFDocumentBookmarks() or
   LoadTrapezeBookmarks(). The return value is the number of bookmarks/folders
   in bookmarks, or getTrapezeBookmarkCountFailed on failure. On failure, use
   GetLastError() to get the cause of the error. */
extern TrapezeDWORD TrapezeAPI GetTrapezeBookmarkCount(TrapezeBookmarks
                                                       bookmarks);

/* GetTrapezeBookmarkData
   Converts bookmarks into file storage format. bookmarks must be created
   using CreateTrapezeBookmarks(), GetTrapezeTIFFDocumentBookmarks() or
   LoadTrapezeBookmarks(). size is set to the size of the bookmark data. The
   return value is a handle containing the data on success, or NULL on
   failure. On failure, use GetLastError() to get the cause of the error. */
extern TrapezeHGLOBAL TrapezeAPI GetTrapezeBookmarkData(TrapezeBookmarks
                                                        bookmarks,
                                                        TrapezeLPDWORD size);

/* GetTrapezeBookmarkPageNo
   Gets the page number for a bookmark. bookmarks is a value returned by
   CreateTrapezeBookmarks(), GetTrapezeTIFFDocumentBookmarks() or
   LoadTrapezeBookmarks(). bookmarkNo specifies the bookmark to retrieve the
   page number for, starting at zero. The return value is
   getTrapezeBookmarkPageNoFailed on failure, or trapezeBookmarkFolder or the
   one based page number on success. On failure, use GetLastError() to get the
   cause of the error. */
extern TrapezeDWORD TrapezeAPI GetTrapezeBookmarkPageNo(TrapezeBookmarks
                                                        bookmarks,
                                                        TrapezeDWORD
                                                        bookmarkNo);

/* GetTrapezeBookmarkParent
   Gets the parent of a bookmark. bookmarks is a value returned by
   CreateTrapezeBookmarks(), GetTrapezeTIFFDocumentBookmarks() or
   LoadTrapezeBookmarks(). bookmarkNo specifies the bookmark to retrieve the
   parent for, starting at zero. The return value is
   getTrapezeBookmarkParentFailed on failure, or noTrapezeBookmarkParent or
   the zero based bookmark folder number on success. On failure, use
   GetLastError() to get the cause of the error. */
extern TrapezeDWORD TrapezeAPI GetTrapezeBookmarkParent(TrapezeBookmarks
                                                        bookmarks,
                                                        TrapezeDWORD
                                                        bookmarkNo);

/* GetTrapezeBookmarkText
   Gets the name or description of a bookmark. bookmarks is a value returned
   by CreateTrapezeBookmarks(), GetTrapezeTIFFDocumentBookmarks() or
   LoadTrapezeBookmarks(). bookmarkNo specifies the bookmark to retrieve the
   text for, starting at zero. If description is FALSE, the text returned is
   the bookmark name, otherwise the bookmark description is returned. The text
   is returned in text if text is not NULL. textSize specifies the size of
   text, in characters, if text is not NULL. The return value is the length of
   the text (without the NULL terminator) on success, or
   getTrapezeBookmarkTextFailed on failure. On failure, use GetLastError() to
   get the cause of the error. */
extern TrapezeDWORD TrapezeAPI GetTrapezeBookmarkText(TrapezeBookmarks
                                                      bookmarks,
                                                      TrapezeDWORD bookmarkNo,
                                                      TrapezeBOOL description,
                                                      TrapezeLPSTR text,
                                                      TrapezeDWORD textSize);

/* InsertTrapezeBookmark
   Inserts a bookmark. bookmarks must be a pointer to bookmarks created using
   CreateTrapezeBookmarks(), GetTrapezeTIFFDocumentBookmarks() or
   LoadTrapezeBookmarks(). parent is a parent folder previously returned by
   InsertTrapezeBookmark(), or noTrapezeBookmarkParent to insert the bookmark
   at the root. pageNo is the page number (one based) of the bookmark, or
   trapezeBookmarkFolder to insert a bookmark folder. name is the name of the
   bookmark. description is the name of the bookmark. The return value is the
   identifier of the bookmark on success, or insertTrapezeBookmarkFailed on
   failure. On failure, use GetLastError() to get the cause of the error. */
extern TrapezeDWORD TrapezeAPI InsertTrapezeBookmark(TrapezeBookmarksPtr
                                                     bookmarks,
                                                     TrapezeDWORD parent,
                                                     TrapezeDWORD pageNo,
                                                     TrapezeLPCSTR name,
                                                     TrapezeLPCSTR
                                                     description);

/* LoadTrapezeBookmarks
   Loads bookmarks from a file for use in other bookmark functions. fileName
   specifies the file to read the bookmarks from, a TIFF or a file created
   using SaveTrapezeBookmarksFile(). The return value is not NULL on success
   or NULL on failure. On failure, use GetLastError() to get the cause of the
   error. */
extern TrapezeBookmarks TrapezeAPI LoadTrapezeBookmarks(TrapezeLPCSTR
                                                        fileName);

/* SaveTrapezeBookmarksFile
   Saves bookmarks to a file. bookmarks must be a pointer to bookmarks created
   using CreateTrapezeBookmarks(), GetTrapezeTIFFDocumentBookmarks() or
   LoadTrapezeBookmarks(). fileName specifies the file to write the bookmarks
   to, for subsequent use with LoadTrapezeBookmarks(). The return value is
   TRUE on success or FALSE on failure. On failure, use GetLastError() to get
   the cause of the error. */
extern TrapezeBOOL TrapezeAPI SaveTrapezeBookmarksFile(TrapezeBookmarks
                                                       bookmarks,
                                                       TrapezeLPCSTR
                                                       fileName);

/* DIB Functions
   ============= */

/* ChangeTrapezeDIBColor
   Changes the color format of a DIB, returning a new DIB with the new color
   format. dibData is the data for the DIB pixels. dib contains information
   about the DIB. colorType specifies the new color format; it must be one
   of the *TrapezeColorType values other than otherTrapezeColorType,
   errorTrapezeColorType or ycbcr24TrapezeColorType, or the value
   changeTrapezeDIBColorBlackAndWhiteDither2x2. The return value is a handle
   to the new DIB, or NULL on failure. On failure, use GetLastError() to get
   the cause of the failure. */
extern TrapezeHGLOBAL TrapezeAPI ChangeTrapezeDIBColor(const TrapezeBYTE*
                                                       dibData,
                                                       const
                                                       TrapezeBITMAPINFO* dib,
                                                       TrapezeDWORD
                                                       colorType);

/* ChangeTrapezeDIBColorProgress
   Changes the color format of a DIB, returning a new DIB with the new color
   format, periodically calling a progress function. dibData is the data for
   the DIB pixels. dib contains information about the DIB. colorType specifies
   the new color format; it must be one of the *TrapezeColorType values other
   than otherTrapezeColorType, errorTrapezeColorType or
   ycbcr24TrapezeColorType, or the value
   changeTrapezeDIBColorBlackAndWhiteDither2x2. If progressProc is not NULL,
   it is called periodically with lParam passed to it; if it returns FALSE the
   function fails with the error ERROR_CANCELLED. The return value is a handle
   to the new DIB, or NULL on failure. On failure, use GetLastError() to get
   the cause of the failure. */
extern TrapezeHGLOBAL TrapezeAPI
ChangeTrapezeDIBColorProgress(const TrapezeBYTE* dibData,
                              const TrapezeBITMAPINFO* dib,
                              TrapezeDWORD colorType,
                              TrapezeProgressProc progressProc,
                              TrapezeLPVOID lParam);

/* ColorOptimizeTrapezeDIB
   Reduces the number of colors in a DIB to the minimum possible without
   removing any color information. dibData is the data for the DIB pixels. dib
   contains information about the DIB. The return value is a handle to a new
   color optimized packed DIB if the colors can be optimized, or NULL on
   failure or if the colors can't be optimized. The error code is only set
   when an error occurs, so to find out if an error occurred you need to call
   SetLastError(ERROR_SUCCESS) before calling this function. */
extern TrapezeHGLOBAL TrapezeAPI ColorOptimizeTrapezeDIB(const TrapezeBYTE*
                                                         dibData,
                                                         const
                                                         TrapezeBITMAPINFO*
                                                         dib);

/* ColorOptimizeTrapezeDIBProgress
   Reduces the number of colors in a DIB to the minimum possible without
   removing any color information, periodically calling a progress function.
   dibData is the data for the DIB pixels. dib contains information about the
   DIB. If progressProc is not NULL, it is called periodically with lParam
   passed to it; if it returns FALSE the function fails with the error
   ERROR_CANCELLED. The return value is a handle to a new color optimized
   packed DIB if the colors can be optimized, or NULL on failure or if the
   colors can't be optimized. The error code is only set when an error occurs,
   so to find out if an error occurred you need to call
   SetLastError(ERROR_SUCCESS) before calling this function. */
extern TrapezeHGLOBAL TrapezeAPI
ColorOptimizeTrapezeDIBProgress(const TrapezeBYTE* dibData,
                                const TrapezeBITMAPINFO* dib,
                                TrapezeProgressProc progressProc,
                                TrapezeLPVOID lParam);

/* CopyTrapezeDIBPixels
   Copies a DIB into another DIB at a specified location. srcDIBData is the
   data for the source DIB pixels. srcDIB contains information about the
   source DIB. destDIBData is the data for the destination pixels, which are
   changed by this function. destDIB contains information about the
   destination DIB. x and y specify the location within destDIB that srcDIB is
   copied to. The return value is TRUE on success or FALSE on failure. On
   failure, use GetLastError() to get the cause of the error.
   The destination DIB must have the same or more colors than the source DIB,
   otherwise this function will fail with ERROR_INVALID_PARAMETER.
   Only pixels lying inside the destination DIB are copied, so the specified
   location is allowed to copy some or all of the source DIB off the edges of
   the destination DIB. */
extern TrapezeBOOL TrapezeAPI CopyTrapezeDIBPixels(const TrapezeBYTE*
                                                   srcDIBData,
                                                   const TrapezeBITMAPINFO*
                                                   srcDIB,
                                                   TrapezeBYTE* destDIBData,
                                                   const TrapezeBITMAPINFO*
                                                   destDIB, TrapezeLONG x,
                                                   TrapezeLONG y);

/* CopyTrapezeDIBToDIB
   Copies a section of a DIB into a new DIB. rect specifies the source
   rectangle to be copied, before any rotation. If rect is NULL the entire DIB
   is copied. dibData is the data for the DIB pixels. dib contains information
   about the DIB. rotation specifies the rotation of the copied section
   relative to the original DIB. rotation can be one of 0, 90, 180, 270, in
   degrees clockwise. invert specifies whether the colors of the DIB are
   inverted. On success the return value is a handle to the new packed DIB, or
   NULL on failure. On failure, use GetLastError() to get the cause of the
   failure. */
extern TrapezeHGLOBAL TrapezeAPI CopyTrapezeDIBToDIB(TrapezeLPCRECT rect,
                                                     const TrapezeBYTE*
                                                     dibData,
                                                     const TrapezeBITMAPINFO*
                                                     dib,
                                                     TrapezeWORD rotation,
                                                     TrapezeBOOL invert);

/* CopyTrapezeDIBToDIBProgress
   Copies a section of a DIB into a new DIB, periodically calling a progress
   function. rect specifies the source rectangle to be copied, before any
   rotation. If rect is NULL the entire DIB is copied. dibData is the data for
   the DIB pixels. dib contains information about the DIB. rotation specifies
   the rotation of the copied section relative to the original DIB. rotation
   can be one of 0, 90, 180, 270, in degrees clockwise. invert specifies
   whether the colors of the DIB are inverted. If progressProc is not NULL, it
   is called periodically with lParam passed to it; if it returns FALSE the
   function fails with the error ERROR_CANCELLED. On success the return value
   is a handle to the new packed DIB, or NULL on failure. On failure, use
   GetLastError() to get the cause of the failure. */
extern TrapezeHGLOBAL TrapezeAPI
CopyTrapezeDIBToDIBProgress(TrapezeLPCRECT rect, const TrapezeBYTE* dibData,
                            const TrapezeBITMAPINFO* dib,
                            TrapezeWORD rotation, TrapezeBOOL invert,
                            TrapezeProgressProc progressProc,
                            TrapezeLPVOID lParam);

#ifdef _WIN32
/* CreateTrapezeDIBPalette
   Creates a palette for displaying a DIB. dibData is the data for the DIB
   pixels. dib contains information about the DIB. dc specifies the device
   context the palette is for. If dc is NULL the screen device context is
   used. If optimize is TRUE an optimized palette will be created containing
   no more than 236 colors (256 minus Windows' 20 standard colors). The return
   value is the palette, or NULL on failure. On failure, use GetLastError() to
   get the cause of the failure. */
extern HPALETTE TrapezeAPI CreateTrapezeDIBPalette(const BYTE* dibData,
                                                   const BITMAPINFO* dib,
                                                   HDC dc, BOOL optimize);
#endif /*_WIN32*/

/* CreateTrapezeMaskDIB
   Creates a black and white mask DIB from a source DIB, the same size as the
   source DIB. dibData is the data for the source DIB pixels. dib contains
   information about the source DIB. maskColor is the color used to make the
   mask; any pixel in the source DIB that is this color is set to black in the
   mask DIB; any pixel in the source DIB that is not this color is set to
   white in the mask DIB. The return value is the mask DIB, or NULL on
   failure. On failure, use GetLastError() to get the cause of the failure.
   The returned value must be freed using GlobalFree() when it is no longer
   required. */
extern TrapezeHGLOBAL TrapezeAPI CreateTrapezeMaskDIB(const TrapezeBYTE*
                                                      dibData,
                                                      const TrapezeBITMAPINFO*
                                                      dib,
                                                      TrapezeCOLORREF
                                                      maskColor);

/* DespeckleTrapezeDIB
   Despeckles a monochrome DIB, removing blobs of size speckSize or smaller.
   dibData is the data for the DIB pixels. dib contains information about the
   DIB. If flags contains:
   - despeckleTrapezeDIBBlack, black speckles are removed
   - despeckleTrapezeDIBWhite, white speckles are removed
   - despeckleTrapezeDIBInPlace, dibData is modified, otherwise a new DIB is
       returned
   If progressProc is not NULL, it is called periodically with lParam passed
   to it; if it returns FALSE the function fails with the error
   ERROR_CANCELLED.
   The return value depends on flags:
   If despeckleTrapezeDIBInPlace is not set in flags, the return value is the
   despeckled DIB, or NULL on failure. This DIB must be destroyed using
   GlobalFree() when it is no longer required.
   If despeckleTrapezeDIBInPlace is set in flags, the return value is non-NULL
   on success, or NULL on failure.
   On failure, use GetLastError() to get the cause of the error. */
extern TrapezeHGLOBAL TrapezeAPI DespeckleTrapezeDIB(TrapezeBYTE* dibData,
                                                     const TrapezeBITMAPINFO*
                                                     dib,
                                                     TrapezeBYTE speckSize,
                                                     TrapezeBYTE flags,
                                                     TrapezeProgressProc
                                                     progressProc,
                                                     TrapezeLPVOID lParam);

#ifdef _WIN32
/* DrawTrapezeDIBMask
   Draws a mask into a DIB, setting black pixels from the mask to color and
   not drawing white pixels from the mask. dibData is the data for the DIB
   pixels. dib contains information about the DIB. x and y specify the
   location to draw the mask; the mask cannot overlap any edges of the DIB.
   color specifies the color to draw black pixels from the mask. bitmap
   specifies the mask; it must be a monochrome bitmap. The return value is
   TRUE on success, or NULL on failure. On failure, use GetLastError() to get
   the cause of the error. */
extern TrapezeBOOL TrapezeAPI DrawTrapezeDIBMask(TrapezeBYTE* dibData,
                                                 TrapezeBITMAPINFO* dib,
                                                 TrapezeDWORD x,
                                                 TrapezeDWORD y,
                                                 TrapezeCOLORREF color,
                                                 HBITMAP bitmap);
#endif /*_WIN32*/

/* FillTrapezeDIBRect
   Draws a rectangle into a DIB, using the specified color. dibData is the
   data for the DIB pixels. dib contains information about the DIB. rect is
   the rectangle to draw into; if rect is NULL the entire DIB is filled. mode
   is the drawing mode to use; it is one of *TrapezeDIBMode. The return value
   is TRUE on success, or FALSE on failure. On failure, use GetLastError() to
   get the cause of the error. */
extern TrapezeBOOL TrapezeAPI FillTrapezeDIBRect(TrapezeBYTE* dibData,
                                                 TrapezeBITMAPINFO* dib,
                                                 TrapezeLPCRECT rect,
                                                 TrapezeCOLORREF color,
                                                 TrapezeBYTE mode);

/* GetTrapezeDIBColorType
   Gets the color format of a DIB. dibData is the data for the DIB pixels. dib
   contains information about the DIB. The return value is one of the
   *TrapezeColorType values, or errorTrapezeColorType on failure. On failure,
   use GetLastError() to get the cause of the failure. */
extern TrapezeDWORD TrapezeAPI GetTrapezeDIBColorType(const TrapezeBYTE*
                                                      dibData,
                                                      const TrapezeBITMAPINFO*
                                                      dib);

/* GetTrapezeDIBData
   Returns a pointer to the pixel data in a packed DIB. dib specifies the
   packed DIB. The return value is the DIB data on success, or NULL on
   failure. On failure, use GetLastError() to get the cause of the failure.
   The returned pointer is a pointer to data within dib so remains valid as
   long as dib remains valid. */
extern TrapezeLPVOID TrapezeAPI GetTrapezeDIBData(const TrapezeBITMAPINFO*
                                                  dib);

/* GetTrapezeDIBSize
   Returns the size of a packed DIB. dib specifies the packed DIB. The
   return value is the size of the DIB on success, or zero on failure. On
   failure, use GetLastError() to get the cause of the failure. */
extern TrapezeSIZE_T TrapezeAPI GetTrapezeDIBSize(const TrapezeBITMAPINFO*
                                                  dib);

/* OCRTrapezeDIB
   OCRs a section of a DIB. rect specifies the source rectangle to be OCRed.
   If rect is NULL the entire DIB is OCRed. format specifies the output format
   of the data; it must be one of ocrTrapeze*. If progressProc is not NULL, it
   is called periodically with lParam passed to it; if it returns FALSE the
   function fails with the error ERROR_CANCELLED. size is set to the size of
   the returned data, in bytes. The return value is the OCR data in the format
   requested. If the return value is NULL, either no text was found or an
   error occured. The last error value is not updated on success, so to
   determine success or failure for a NULL return value, call
   SetLastError(ERROR_SUCCESS) before calling OCRTrapezeDIB(), then call
   GetLastError() to get the cause of the error. If GetLastError() returns
   ERROR_SUCCESS then no text was found, otherwise an error occurred. The
   returned value must be freed using GlobalFree() when it is no longer
   required. */
extern TrapezeHGLOBAL TrapezeAPI OCRTrapezeDIB(TrapezeLPCRECT rect,
                                               const TrapezeBYTE* dibData,
                                               const TrapezeBITMAPINFO* dib,
                                               TrapezeBYTE format,
                                               TrapezeProgressProc
                                               progressProc,
                                               TrapezeLPVOID lParam,
                                               TrapezeLPDWORD size);

/* ReadTrapezeDIB
   Reads a DIB from an image file. fileName specifies the file to read the
   image from. pageNo specifies the page number (one based) of the document to
   read the image from; for a TIFF the full size image for the page will be
   retrieved. The return value is a handle to the packed DIB, or NULL on
   failure. On failure, use GetLastError() to get the cause of the error.
   The function will fail with the error ERROR_INVALID_PARAMETER if pageNo is
   zero or larger than the number of pages in the file. */
extern TrapezeHGLOBAL TrapezeAPI ReadTrapezeDIB(TrapezeLPCSTR fileName,
                                                TrapezeDWORD pageNo);

/* ReadTrapezeDIBEx
   Reads a DIB from an image file, according to information in readInfo. The
   return value is a handle to the packed DIB, or NULL on failure. On failure,
   use GetLastError() to get the cause of the error. */
extern TrapezeHGLOBAL TrapezeAPI ReadTrapezeDIBEx(ReadTrapezeDIBExCPtr
                                                  readInfo);

/* ReadTrapezeDIBOffset
   Reads a DIB from an offset in an image file. fileName specifies the file to
   read the image from. pageNo specifies the page number (one based) of the
   document to read the image from; for a TIFF the full size image for the
   page will be retrieved. offset specifies the offset within the file to
   start reading the image from. The return value is a handle to the packed
   DIB, or NULL on failure. On failure, use GetLastError() to get the cause of
   the error. */
extern TrapezeHGLOBAL TrapezeAPI ReadTrapezeDIBOffset(TrapezeLPCSTR fileName,
                                                      TrapezeDWORD pageNo,
                                                      TrapezeSIZE_T offset);

/* ReadTrapezeDIBOffsetProgress
   Reads a DIB from an offset in an image file, periodically calling a
   progress function. fileName specifies the file to read the image from.
   pageNo specifies the page number (one based) of the document to read the
   image from; for a TIFF the full size image for the page will be retrieved.
   offset specifies the offset within the file to start reading the image
   from. If progressProc is not NULL, it is called periodically with lParam
   passed to it; if it returns FALSE the function fails with the error
   ERROR_CANCELLED. The return value is a handle to the packed DIB, or NULL on
   failure. On failure, use GetLastError() to get the cause of the error. */
extern TrapezeHGLOBAL TrapezeAPI
ReadTrapezeDIBOffsetProgress(TrapezeLPCSTR fileName, TrapezeDWORD pageNo,
                             TrapezeSIZE_T offset,
                             TrapezeProgressProc progressProc,
                             TrapezeLPVOID lParam);

/* ReadTrapezeDIBProgress
   Reads a DIB from an image file, periodically calling a progress function.
   fileName specifies the file to read the image from. pageNo specifies the
   page number (one based) of the document to read the image from; for a TIFF
   the full size image for the page will be retrieved. If progressProc is not
   NULL, it is called periodically with lParam passed to it; if it returns
   FALSE the function fails with the error ERROR_CANCELLED. The return value
   is a handle to the packed DIB, or NULL on failure. On failure, use
   GetLastError() to get the cause of the error. */
extern TrapezeHGLOBAL TrapezeAPI
ReadTrapezeDIBProgress(TrapezeLPCSTR fileName, TrapezeDWORD pageNo,
                       TrapezeProgressProc progressProc,
                       TrapezeLPVOID lParam);

/* ResizeTrapezeDIB
   Resizes a DIB, averaging to get the reduced pixels when reducing. width and
   height specify the width and height, respectively, of the new DIB. dibData
   and dib specify the DIB to be resized. The return value is a handle to the
   resized packed DIB, or NULL on failure. On failure, use GetLastError() to
   get the cause of the error. */
extern TrapezeHGLOBAL TrapezeAPI ResizeTrapezeDIB(TrapezeDWORD width,
                                                  TrapezeDWORD height,
                                                  const TrapezeBYTE* dibData,
                                                  const TrapezeBITMAPINFO*
                                                  dib);

/* ResizeTrapezeDIBEx
   Resizes a DIB, according to the information in resizeInfo. The return value
   is a handle to the resized packed DIB, or NULL on failure. On failure, use
   GetLastError() to get the cause of the error. */
extern TrapezeHGLOBAL TrapezeAPI ResizeTrapezeDIBEx(ResizeTrapezeDIBExCPtr
                                                    resizeInfo);

/* ResizeTrapezeDIBProgress
   Resizes a DIB, averaging to get the reduced pixels when reducing,
   periodically calling a progress function. width and height specify the
   width and height, respectively, of the new DIB. dibData and dib specify the
   DIB to be resized. If progressProc is not NULL, it is called periodically
   with lParam passed to it; if it returns FALSE the function fails with the
   error ERROR_CANCELLED. The return value is a handle to the resized packed
   DIB, or NULL on failure. On failure, use GetLastError() to get the cause of
   the error. */
extern TrapezeHGLOBAL TrapezeAPI
ResizeTrapezeDIBProgress(TrapezeDWORD width, TrapezeDWORD height,
                         const TrapezeBYTE* dibData,
                         const TrapezeBITMAPINFO* dib,
                         TrapezeProgressProc progressProc,
                         TrapezeLPVOID lParam);

/* ReverseTrapezeDIBColors
   Reverses the order of colors in a palette based DIB. dibData and dib
   specify the DIB to be recolored. The return value is TRUE on success, or
   FALSE on failure. On failure, use GetLastError() to get the cause of the
   error.
   The DIB will still look the same when displayed; only internal color
   ordering is changed.
   This function currently only works on two color DIBs. */
extern TrapezeBOOL TrapezeAPI ReverseTrapezeDIBColors(TrapezeLPBYTE dibData,
                                                      TrapezeLPBITMAPINFO
                                                      dib);

/* SaveTrapezeBMPDIB
   Saves a DIB to a BMP. fileName specifies the file to write the BMP to.
   dibData and dib specify the DIB to be saved. The return value is TRUE on
   success, or NULL on failure. On failure, use GetLastError() to get the
   cause of the error. */
extern TrapezeBOOL TrapezeAPI SaveTrapezeBMPDIB(TrapezeLPCSTR fileName,
                                                const TrapezeBYTE* dibData,
                                                const TrapezeBITMAPINFO* dib);

/* SaveTrapezeDIB
   Saves a DIB to a file. fileName specifies the file to write the DIB to.
   dibData and dib specify the DIB to be saved. docType specifies the format
   to save the DIB in; it must be bmpTrapezeDocument, jpegTrapezeDocument or
   tiffTrapezeDocument.
   options specifies additional information based on docType, or NULL to use
   defaults. When docType is:
   bmpTrapezeDocument, options is NULL or a TrapezeBMPOptionsPtr.
   jpegTrapezeDocument, options is NULL or a TrapezeJPEGOptionsPtr.
   tiffTrapezeDocument, options is NULL or a TrapezeTIFFOptionsPtr.
   The return value is TRUE on success, or NULL on failure. On failure, use
   GetLastError() to get the cause of the error. */
extern TrapezeBOOL TrapezeAPI SaveTrapezeDIB(TrapezeLPCSTR fileName,
                                             const TrapezeBYTE* dibData,
                                             const TrapezeBITMAPINFO* dib,
                                             TrapezeWORD docType,
                                             TrapezeLPCVOID options);

#ifdef _WIN32
/* StretchTrapezeDIB
   An enhanced version of the Windows API function StretchDIBits() that
   provides rotation and enhanced reduction. dc specifies the device context
   to draw into. destRect specifies the rectangle within dc to draw the DIB
   into. srcRect specifies the pixels within the DIB to be drawn, before any
   rotation. If srcRect is NULL the entire DIB is drawn. dibData and dib
   specify the DIB to be drawn. colorUse specifies DIB_PAL_COLORS or
   DIB_RGB_COLORS, as for StretchDIBits(). rasterOp specifies how the source
   pixels, the device context's current brush and the destination pixels are
   to be combined, as for StretchDIBits(). rotation can be one of 0, 90, 180,
   270, in degrees clockwise. If the DIB is reduced in size during drawing :
   - If enhance is FALSE, pixels are removed.
   - If enhance is TRUE, color information from all pixels is used to produce
     an enhanced reduced image. For large color images this enhancement can be
     slow.
   The return value is TRUE on success, or FALSE on failure. On failure, use
   GetLastError() to get the cause of the error. */
extern BOOL WINAPI StretchTrapezeDIB(HDC dc, LPCRECT destRect,
                                     LPCRECT srcRect, const BYTE* dibData,
                                     const BITMAPINFO* dib, UINT colorUse,
                                     DWORD rasterOp, WORD rotation,
                                     BOOL enhance);
#endif /*_WIN32*/

/* DICOM Functions
   =============== */

/* CloseTrapezeDICOM
   Closes a DICOM file when it is no longer required. dicom specifies the
   DICOM file to close. dicom must not be used after this function is called.
   The return value is TRUE on success or FALSE on failure. On failure, use
   GetLastError() to get the cause of the failure. */
extern TrapezeBOOL TrapezeAPI CloseTrapezeDICOM(TrapezeDICOM dicom);

/* GetTrapezeDICOMData
   Gets the data set from a DICOM file. dicom specifies the DICOM file to get
   the data set from. flags specifies the format of the returned data set:
   - getTrapezeDICOMDataRaw, the data set is returned as a DICOM data set, as
     defined in the DICOM specification, with any pixel data removed
   - getTrapezeDICOMDataText, the data set is returned as a NULL terminated
     string for display, with lines terminated by LF
   If progressProc is not NULL, it is called periodically with lParam passed
   to it; if it returns FALSE the function fails with the error
   ERROR_CANCELLED. size is set to the size of the returned data, in bytes.
   pixelDataLength is set to the size of the removed pixel data, in bytes.
   undefinedTrapezeDICOMLength indicates undefined length pixel data.
   noTrapezeDICOMPixelData indicates no pixel data was removed. pixelDataVR is
   set to the pixel data Value Representation, in intel byte order. The return
   value is a handle containing the data on success, or NULL on failure. On
   failure, use GetLastError() to get the cause of the error. The returned
   value must be freed using GlobalFree() when it is no longer required. */
extern TrapezeHGLOBAL TrapezeAPI GetTrapezeDICOMData(TrapezeDICOM dicom,
                                                     TrapezeDWORD flags,
                                                     TrapezeProgressProc
                                                     progressProc,
                                                     TrapezeLPVOID lParam,
                                                     TrapezeLPSIZE_T size,
                                                     TrapezeLPSIZE_T
                                                     pixelDataLength,
                                                     TrapezeLPWORD
                                                     pixelDataVR);

/* GetTrapezeDICOMInfo
   Returns information about a DICOM file. dicom specifies the DICOM file
   to retrieve information from. The information is returned in dicomInfo.
   dicomInfo->size must be set to the size of dicomInfo before calling this
   function. dicomInfo->flags must be set before calling this function. If
   progressProc is not NULL, it is called periodically with lParam passed to
   it; if it returns FALSE the function fails with the error ERROR_CANCELLED.
   The return value is TRUE on success or FALSE on failure. On failure, use
   GetLastError() to get the cause of the failure. */
extern TrapezeBOOL TrapezeAPI GetTrapezeDICOMInfo(TrapezeDICOM dicom,
                                                  TrapezeDICOMInfoPtr
                                                  dicomInfo,
                                                  TrapezeProgressProc
                                                  progressProc,
                                                  TrapezeLPVOID lParam);

/* OpenTrapezeDICOM
   Opens a DICOM file for reading. fileName specifies the file containing the
   DICOM file. The return value is non-NULL on success or NULL on failure. On
   failure, use GetLastError() to get the cause of the failure. */
extern TrapezeDICOM TrapezeAPI OpenTrapezeDICOM(TrapezeLPCSTR fileName);

/* ReadTrapezeDICOMDIB
   Reads a DIB from a DICOM file. dicom specifies the DICOM file to read the
   DIB from. frameNo specifies the one-based frame number to read the image
   from. If progressProc is not NULL, it is called periodically with lParam
   passed to it; if it returns FALSE the function fails with the error
   ERROR_CANCELLED. The return value is a GlobalAlloc() handle on success, or
   NULL on failure. On failure, use GetLastError() to get the cause of the
   failure. The returned handle must be freed using GlobalFree() when it is no
   longer required. */
extern TrapezeHGLOBAL TrapezeAPI ReadTrapezeDICOMDIB(TrapezeDICOM dicom,
                                                     TrapezeDWORD frameNo,
                                                     TrapezeProgressProc
                                                     progressProc,
                                                     TrapezeLPVOID lParam);

/* Global Memory Functions
   ======================= */

#ifdef _WIN32
#define TrapezeGMEM_FIXED          GMEM_FIXED
#define TrapezeGMEM_MOVEABLE       GMEM_MOVEABLE
#define TrapezeGPTR                GPTR
#define TrapezeGHND                GHND
#define TrapezeGMEM_DDESHARE       GMEM_DDESHARE
#define TrapezeGMEM_SHARE          GMEM_SHARE
#define TrapezeGMEM_DISCARDABLE    GMEM_DISCARDABLE
#define TrapezeGMEM_ZEROINIT       GMEM_ZEROINIT
#define TrapezeGMEM_DISCARDED      GMEM_DISCARDED
#define TrapezeGMEM_LOCKCOUNT      GMEM_LOCKCOUNT
#define TrapezeGMEM_INVALID_HANDLE GMEM_INVALID_HANDLE
#define TrapezeGMEM_MODIFY         GMEM_MODIFY
#define TrapezeGlobalAlloc         GlobalAlloc
#define TrapezeGlobalDiscard       GlobalDiscard
#define TrapezeGlobalFlags         GlobalFlags
#define TrapezeGlobalFree          GlobalFree
#define TrapezeGlobalHandle        GlobalHandle
#define TrapezeGlobalLock          GlobalLock
#define TrapezeGlobalReAlloc       GlobalReAlloc
#define TrapezeGlobalSize          GlobalSize
#define TrapezeGlobalUnlock        GlobalUnlock
#else /*_WIN32*/
#define TrapezeGMEM_FIXED          0x0000
#define TrapezeGMEM_MOVEABLE       0x0002
#define TrapezeGMEM_ZEROINIT       0x0040
#define TrapezeGMEM_MODIFY         0x0080
#define TrapezeGMEM_DISCARDABLE    0x0100
#define TrapezeGMEM_SHARE          0x2000
#define TrapezeGMEM_DDESHARE       0x2000
#define TrapezeGMEM_DISCARDED      0x4000
#define TrapezeGMEM_INVALID_HANDLE 0x8000
#define TrapezeGPTR                (TrapezeGMEM_FIXED | TrapezeGMEM_ZEROINIT)
#define TrapezeGHND                (TrapezeGMEM_MOVEABLE | \
                                    TrapezeGMEM_ZEROINIT)
#define TrapezeGMEM_LOCKCOUNT      0x00FF
extern TrapezeHGLOBAL TrapezeAPI TrapezeGlobalAlloc(TrapezeUINT uFlags,
                                                    TrapezeSIZE_T dwBytes);
extern TrapezeHGLOBAL TrapezeAPI TrapezeGlobalDiscard(TrapezeHGLOBAL hglbMem);
extern TrapezeUINT TrapezeAPI TrapezeGlobalFlags(TrapezeHGLOBAL hMem);
extern TrapezeHGLOBAL TrapezeAPI TrapezeGlobalFree(TrapezeHGLOBAL hMem);
extern TrapezeHGLOBAL TrapezeAPI TrapezeGlobalHandle(TrapezeLPCVOID pMem);
extern TrapezeLPVOID TrapezeAPI TrapezeGlobalLock(TrapezeHGLOBAL hMem);
extern TrapezeHGLOBAL TrapezeAPI TrapezeGlobalReAlloc(TrapezeHGLOBAL hMem,
                                                      TrapezeSIZE_T dwBytes,
                                                      TrapezeUINT uFlags);
extern TrapezeSIZE_T TrapezeAPI TrapezeGlobalSize(TrapezeHGLOBAL hMem);
extern TrapezeBOOL TrapezeAPI TrapezeGlobalUnlock(TrapezeHGLOBAL hMem);
#ifndef NOTRAPEZEWINDOWSNAMES
#define GMEM_FIXED          TrapezeGMEM_FIXED
#define GMEM_MOVEABLE       TrapezeGMEM_MOVEABLE
#define GPTR                TrapezeGPTR
#define GHND                TrapezeGHND
#define GMEM_DDESHARE       TrapezeGMEM_DDESHARE
#define GMEM_SHARE          TrapezeGMEM_SHARE
#define GMEM_DISCARDABLE    TrapezeGMEM_DISCARDABLE
#define GMEM_ZEROINIT       TrapezeGMEM_ZEROINIT
#define GMEM_DISCARDED      TrapezeGMEM_DISCARDED
#define GMEM_LOCKCOUNT      TrapezeGMEM_LOCKCOUNT
#define GMEM_INVALID_HANDLE TrapezeGMEM_INVALID_HANDLE
#define GMEM_MODIFY         TrapezeGMEM_MODIFY
#define GlobalAlloc         TrapezeGlobalAlloc
#define GlobalDiscard       TrapezeGlobalDiscard
#define GlobalFlags         TrapezeGlobalFlags
#define GlobalFree          TrapezeGlobalFree
#define GlobalHandle        TrapezeGlobalHandle
#define GlobalLock          TrapezeGlobalLock
#define GlobalReAlloc       TrapezeGlobalReAlloc
#define GlobalSize          TrapezeGlobalSize
#define GlobalUnlock        TrapezeGlobalUnlock
#endif /*! NOTRAPEZEWINDOWSNAMES*/
#endif /*_WIN32*/

/* JPEG Functions
   ============== */

/* CloseTrapezeJPEG
   Closes a JPEG when it is no longer required. jpeg specifies the JPEG to
   close; it must be a value returned by OpenTrapezeJPEG(). jpeg must not be
   used after this function is called. The return value is TRUE on success or
   FALSE on failure. On failure, use GetLastError() to get the cause of the
   failure. */
extern TrapezeBOOL TrapezeAPI CloseTrapezeJPEG(TrapezeJPEG jpeg);

/* CopyTrapezeJPEGProgress
   Copies an open JPEG, periodically calling a progress function. jpeg
   specifies a JPEG opened with OpenTrapezeJPEG(). If progressProc is not
   NULL, it is called periodically with lParam passed to it; if it returns
   FALSE the function fails with the error ERROR_CANCELLED. If fileName is
   not NULL, it specifies the file to write a copy of JPEG to; any existing
   file will be replaced. If fileName is NULL, the function returns a
   GlobalAlloc() handle containing the JPEG. If size is not NULL, on success
   it is set to the size of the copied JPEG. The return value is non-NULL on
   success, or NULL on failure. On failure, use GetLastError() to get the
   cause of the error. If fileName is NULL, the returned handle must be freed
   using GlobalFree() when it is no longer required. */
extern TrapezeHGLOBAL TrapezeAPI
CopyTrapezeJPEGProgress(TrapezeJPEG jpeg, TrapezeProgressProc progressProc,
                        TrapezeLPVOID lParam, TrapezeLPCSTR fileName,
                        TrapezeLPSIZE_T size);

/* GetTrapezeJPEGInfoProgress
   Gets information about a JPEG, periodically calling a progress function.
   jpeg specifies a JPEG opened with OpenTrapezeJPEG(). If progressProc is not
   NULL, it is called periodically with lParam passed to it; if it returns
   FALSE the function fails with the error ERROR_CANCELLED. info specifies what
   information is to be retrieved and receives the information. info->size and
   info->flags must be set before calling this function. The return value is
   TRUE on success or FALSE on failure. On failure, use GetLastError() to get
   the cause of the failure. */
extern TrapezeBOOL TrapezeAPI
GetTrapezeJPEGInfoProgress(TrapezeJPEG jpeg, TrapezeProgressProc progressProc,
                           TrapezeLPVOID lParam, TrapezeJPEGInfoPtr info);

/* GetTrapezeJPEGMCUData
   Gets MCU data from a JPEG, periodically calling a progress function. jpeg
   specifies a JPEG opened with OpenTrapezeJPEG(). If mcuDataInfo->rect is not
   NULL, mcuDataInfo->rect specifies the rectangle to retrieve MCU data for;
   its width and height must be multiples of the MCU width and height for the
   JPEG, respectively, otherwise the function will fail with the error
   ERROR_INVALID_PARAMETER. If mcuDataInfo->rect is NULL, the data returned is
   the compression information needed for reading MCU data. If
   mcuDataInfo->progressProc is not NULL, it is called periodically with
   mcuDataInfo->lParam passed to it; if it returns FALSE the function fails
   with the error ERROR_CANCELLED. The size of the returned data, in bytes, is
   returned in size on success. The return value is a GlobalAlloc() handle
   containing the data on success, or NULL on failure. On failure, use
   GetLastError() to get the cause of the error. The returned handle must be
   freed using GlobalFree() when it is no longer required. If the JPEG is not
   a format that can be used with this function, the function will fail with
   the error unsupportedImageTypeTrapezeError. This function gets any
   supported type JPEG MCU data. */
extern TrapezeHGLOBAL TrapezeAPI
GetTrapezeJPEGMCUData(TrapezeJPEG jpeg, GetTrapezeJPEGMCUDataCPtr mcuDataInfo,
                      TrapezeLPSIZE_T size);

/* GetTrapezeJPEGMCUDataProgress
   Gets MCU data from a JPEG, periodically calling a progress function. jpeg
   specifies a JPEG opened with OpenTrapezeJPEG(). If rect is not NULL, rect
   specifies the rectangle to retrieve MCU data for; its width and height must
   be multiples of the MCU width and height for the JPEG, respectively,
   otherwise the function will fail with the error ERROR_INVALID_PARAMETER. If
   rect is NULL, the data returned is the compression information needed for
   reading MCU data. If progressProc is not NULL, it is called periodically
   with lParam passed to it; if it returns FALSE the function fails with the
   error ERROR_CANCELLED. The size of the returned data, in bytes, is returned
   in size on success. The return value is a GlobalAlloc() handle containing
   the data on success, or NULL on failure. On failure, use GetLastError() to
   get the cause of the error. The returned handle must be freed using
   GlobalFree() when it is no longer required. If the JPEG is not a format
   that can be used with this function, the function will fail with the error
   unsupportedImageTypeTrapezeError. This function gets type 1 JPEG MCU
   data. */
extern TrapezeHGLOBAL TrapezeAPI
GetTrapezeJPEGMCUDataProgress(TrapezeJPEG jpeg, TrapezeLPCRECT rect,
                              TrapezeProgressProc progressProc,
                              TrapezeLPVOID lParam, TrapezeLPSIZE_T size);

/* OpenTrapezeJPEG
   Opens an existing JPEG file for reading. fileName specifies the file to
   open the JPEG from. The return value is non-NULL on success or NULL on
   failure. On failure, use GetLastError() to get the cause of the failure.
   The returned JPEG must be closed using CloseTrapezeJPEG() when it is no
   longer required. */
extern TrapezeJPEG TrapezeAPI OpenTrapezeJPEG(TrapezeLPCSTR fileName);

/* ReadTrapezeJPEGDIBProgress
   Reads a DIB from an open JPEG, periodically calling a progress function.
   jpeg specifies the open JPEG to read the DIB from. If progressProc is not
   NULL, it is called periodically with lParam passed to it; if it returns
   FALSE the function fails with the error ERROR_CANCELLED. The return value
   is a GlobalAlloc() handle on success, or NULL on failure. On failure, use
   GetLastError() to get the cause of the failure. The returned handle must be
   freed using GlobalFree() when it is no longer required. */
extern TrapezeHGLOBAL TrapezeAPI
ReadTrapezeJPEGDIBProgress(TrapezeJPEG jpeg, TrapezeProgressProc progressProc,
                           TrapezeLPVOID lParam);

/* Miscellaneous Functions
   ======================= */

/* CheckTrapezeLanguage
   Checks if a language DLL can be used with Trapeze. If verbose is TRUE, a
   message is displayed indicating whether the language can be used, and if
   not, why not (message box on Windows, stdout on Unix). If verbose is FALSE,
   this function runs silently. US English
   ( MAKELANGID(LANG_ENGLISH, SUBLANG_ENGLISH_US) ) can always be used. The
   return value is TRUE on success or FALSE on failure. On failure, use
   GetLastError() to get the cause of the failure. The function will fail with
   error ERROR_FILE_NOT_FOUND if the requested language is not available. The
   function will fail with ERROR_SUCCESS if the requested language is a
   non-matching version or contains bad resources. */
extern TrapezeBOOL TrapezeAPI CheckTrapezeLanguage(TrapezeBOOL verbose,
                                                   TrapezeLANGID language);

/* EnableTrapezeDebug
   Enables or disables debugging messages. flags specifies the debugging to
   enable or disable; it is a combination of *TrapezeDebug values. If enable
   is TRUE, the specified messages are enabled, otherwise the specified
   messages are disabled. All messages are enabled by default. The return
   value is TRUE on success, or FALSE on failure. On failure, use
   GetLastError() to get the cause of the error.
   This function has no effect in release builds of the DLL. */
extern TrapezeBOOL TrapezeAPI EnableTrapezeDebug(TrapezeDWORD flags,
                                                 TrapezeBOOL enable);

/* FindTrapezeJobSeparator
   Finds a job separator in a DIB. The return value is one of the
   trapezeJobSeparator* values, or trapezeJobSeparatorError on failure. On
   failure, use GetLastError() to get the cause of the failure. */
extern TrapezeBYTE TrapezeAPI FindTrapezeJobSeparator(const TrapezeBYTE*
                                                      dibData,
                                                      const TrapezeBITMAPINFO*
                                                      dib);

/* FindTrapezeJobSeparatorBarcodes
   Finds barcodes on a job separator. dibData and dib specify the DIB
   containing the job separator. findInfo specifies barcode searching
   information, or NULL to use defaults. findInfo->rect is ignored; the entire
   DIB is always searched. findInfo->pageNo is ignored. findInfo->barcodes is
   ignored and not changed. The text for the barcodes is returned in text,
   which is set to NULL if no barcodes are found or an error occurs. The value
   returned in text must be freed using GlobalFree() when it is no longer
   required. If level is NULL the barcode text is not multi-level, otherwise
   the bookmark level of the barcode text is returned in level. The return
   value is TRUE on success, or FALSE on failure. On failure, use
   GetLastError() to get the cause of the error. */
extern TrapezeBOOL TrapezeAPI
FindTrapezeJobSeparatorBarcodes(const TrapezeBYTE* dibData,
                                const TrapezeBITMAPINFO* dib,
                                TrapezeFindBarcodesPtr findInfo,
                                TrapezeHGLOBAL* text, TrapezeLPDWORD level);

#ifdef _WIN32
#define GetLastTrapezeError GetLastError
#else /*_WIN32*/
extern TrapezeDWORD GetLastTrapezeError(void);
#ifndef NOTRAPEZEWINDOWSNAMES
#define GetLastError GetLastTrapezeError
#endif /*! NOTRAPEZEWINDOWSNAMES*/
#endif /*_WIN32*/

/* GetLastTrapezeErrorCode
   Returns the error code for the last error returned by Trapeze. This
   function is similar to GetLastError(), but will only return the last
   Trapeze error, so will work with languages that do not work correctly with
   GetLastError().
   Note: This function is not thread-safe; it returns the last available error
         code from any Trapeze function or message in any thread. */
extern TrapezeDWORD TrapezeAPI GetLastTrapezeErrorCode(void);

/* GetLastTrapezeErrorMessage
   Returns the error message for the last error returned by Trapeze. The
   last error message is returned in str. size specifies the size of str, in
   characters. The return value is TRUE on success, FALSE on failure. On
   failure, use GetLastError() to get the cause of the failure. This function
   can return a more detailed error message than that obtained using
   GetTrapezeErrorMessage(GetLastError(), str, size) (e.g. a message for
   ocrTrapezeError will contain the error message returned from the OCR
   engine). The last error message is only changed when an error occurs, so
   this function should only be called after a Trapeze function or message has
   failed.
   Note: This function is not thread-safe; it returns the last available error
         message from any Trapeze function or message in any thread. */
extern TrapezeBOOL TrapezeAPI GetLastTrapezeErrorMessage(TrapezeLPSTR str,
                                                         TrapezeWORD size);

/* TrapezeDWORD TrapezeAPI GetTrapezeDocumentTypeT(TrapezeLPCTSTR fileName) */
#ifdef UNICODE
#define GetTrapezeDocumentTypeT(fileName) GetTrapezeDocumentTypeW(fileName)
#else
#define GetTrapezeDocumentTypeT(fileName) GetTrapezeDocumentType(fileName)
#endif /*UNICODE*/

/* GetTrapezeDocumentType
   Returns the type of document contained in a file, based on the contents of
   the file. fileName specifies the file to determine the type of. The return
   value is one of the *TrapezeDocument values, or errorTrapezeDocument on
   failure. On failure, use GetLastError() to get the cause of the failure. If
   the return value is unknownTrapezeDocument then Trapeze cannot determine
   the document type and most likely cannot open the document. As little as
   possible of the file content is used to determine the document type, so it
   is possible to call this function with a partially downloaded file;
   typically the first 200 bytes is enough. */
extern TrapezeDWORD TrapezeAPI GetTrapezeDocumentType(TrapezeLPCSTR fileName);

/* GetTrapezeDocumentTypeW
   Returns the type of document contained in a file, based on the contents of
   the file (Unicode version). See GetTrapezeDocumentType() for details. */
extern TrapezeDWORD TrapezeAPI GetTrapezeDocumentTypeW(TrapezeLPCWSTR fileName);

/* GetTrapezeDocumentTypeCaps
   Gets the capabilities for a document type. docType specifies the document
   type to request capabilities for; it must be one of *TrapezeDocument, and
   cannot be unknownTrapezeDocument or errorTrapezeDocument. The return value
   is a combination of trapezeDocTypeCaps* values, or trapezeDocTypeCapsFailed
   on failure. On failure, use GetLastError() to get the cause of the
   failure. */
extern TrapezeDWORD TrapezeAPI GetTrapezeDocumentTypeCaps(TrapezeDWORD
                                                          docType);

/* GetTrapezeDocumentTypeFromExtension
   Returns the type of document contained in a file, based on the file
   extension. fileName specifies the file to determine the type of; it can be
   a full path (e.g. "c:\temp\test.tif"), a partial path (e.g. "test.tif"), or
   just an extension (e.g. ".tif"); it must contain a '.' character. The
   return value is one of the *TrapezeDocument values, or errorTrapezeDocument
   on failure. On failure, use GetLastError() to get the cause of the failure.
   If the return value is unknownTrapezeDocument then Trapeze cannot determine
   the document type and most likely cannot open the document. */
extern TrapezeDWORD TrapezeAPI GetTrapezeDocumentTypeFromExtension(
                                                                 TrapezeLPCSTR
                                                                   fileName);

/* TrapezeBOOL TrapezeAPI GetTrapezeErrorMessageT(TrapezeDWORD error,
                                                  TrapezeLPTSTR str,
                                                  TrapezeWORD size) */
#ifdef UNICODE
#define GetTrapezeErrorMessageT(error, str, size) \
  GetTrapezeErrorMessageW(error, str, size)
#else
#define GetTrapezeErrorMessageT(error, str, size) \
  GetTrapezeErrorMessage(error, str, size)
#endif /*UNICODE*/

/* GetTrapezeErrorMessage
   Returns the error message for any error returned by a Trapeze function or
   message. The message for error is returned in str. size specifies the size
   of str, in characters. The return value is TRUE on success, FALSE on
   failure. On failure, use GetLastError() to get the cause of the
   failure. */
extern TrapezeBOOL TrapezeAPI GetTrapezeErrorMessage(TrapezeDWORD error,
                                                     TrapezeLPSTR str,
                                                     TrapezeWORD size);

/* GetTrapezeErrorMessageW
   Returns the error message for any error returned by a Trapeze function or
   message (Unicode version). See GetTrapezeErrorMessage() for details. */
extern TrapezeBOOL TrapezeAPI GetTrapezeErrorMessageW(TrapezeDWORD error,
                                                      TrapezeLPWSTR str,
                                                      TrapezeWORD size);

/* GetTrapezeExifInfo
   Gets information from Exif data returned by ReadTrapezeExifData(),
   according to the information in info. The return value is TRUE on success,
   or FALSE on failure. On failure, use GetLastError() to get the cause of the
   error. */
extern TrapezeBOOL TrapezeAPI GetTrapezeExifInfo(GetTrapezeExifInfoPtr info);

/* GetTrapezeExtensionFromID
   Gets the file extension from a non-file document identifier specified in
   id, such as TRIM, ODMA, WebDAV etc. The extension is returned in extension,
   beginning with a '.' character. extension is set to an empty string if the
   extension can't be determined or an error occurs. size specifies the size
   of extension, in characters. The return value is the number of characters
   required for the extension, including the NULL terminator, or zero on
   error. On error, use GetLastError() to get the cause of the failure. */
extern TrapezeDWORD TrapezeAPI GetTrapezeExtensionFromID(TrapezeLPCSTR id,
                                                         TrapezeLPSTR
                                                         extension,
                                                         TrapezeDWORD size);

/* GetTrapezeImageSize
   Returns the size (width and height) of an image in a file, in pixels.
   fileName specifies the file containing the image. pageNo specifies the
   one-based page number of the image to retrieve the size of; for single page
   formats (such as BMP) this must be one. The width and height of the image,
   in pixels, are returned in width and height, respectively. The return value
   is TRUE on success, or FALSE on failure. On failure, use GetLastError() to
   get the cause of the failure. */
extern TrapezeBOOL TrapezeAPI GetTrapezeImageSize(TrapezeLPCSTR fileName,
                                                  TrapezeDWORD pageNo,
                                                  TrapezeLPDWORD width,
                                                  TrapezeLPDWORD height);

/* GetTrapezeLicense
   Returns information about a license in license. license->size must be
   initialized before calling this function. The return value is TRUE on
   success, or FALSE on failure. On failure, use GetLastError() to get the
   cause of the failure.
   If customer, location and key are all not NULL in licenseInfo, information
   about that license is returned, otherwise information about the current
   license is returned. If customer, location and key specify an invalid
   license, the function fails with licenseTrapezeError. */
extern TrapezeBOOL TrapezeAPI GetTrapezeLicense(TrapezeLicensePtr
                                                licenseInfo);

#ifdef _WIN32
/* GetTrapezeNoLocalOpenFileName
   This function behaves the same as the Windows API call GetOpenFileName()
   with the following exceptions:
   - The Open dialog box will not let the user access any local drives or let
     the user choose a file name that is on a local drive.
   - Use GetLastError() rather than CommDlgExtendedError() to get the cause of
     the failure when the function returns FALSE. GetLastError() will return
     one of the values listed in GetOpenFileName(), or zero if the dialog was
     cancelled by the user. GetLastError() will return CDERR_INITIALIZATION if
     anything in the OPENFILENAME structure is invalid.
   - openInfo->Flags must not include OFN_ALLOWMULTISELECT.
   - openInfo->Flags must include OFN_EXPLORER if the dialog is customized.
   - openInfo->lpstrFilter may use tab characters instead of NULL terminators
     for all except the last NULL terminator.
   - openInfo->lpstrCustomFilter may use a tab character instead of both NULL
     terminators (no NULL terminator is necessary after the second tab); if it
     does the returned value will also use tab characters instead of both NULL
     terminators, followed immediately by a NULL terminator.
   - openInfo->lpstrFile may use a tab character instead of the NULL
     terminator when this function is called. The returned value will always
     use a NULL terminator. */
extern BOOL WINAPI GetTrapezeNoLocalOpenFileName(LPOPENFILENAMEA openInfo);
#endif /*_WIN32*/

#ifdef _WIN32
/* GetTrapezeNoLocalSaveFileName
   This function behaves the same as the Windows API call GetSaveFileName()
   with the following exceptions:
   - The Save dialog box will not let the user access any local drives or let
     the user choose a file name that is on a local drive.
   - Use GetLastError() rather than CommDlgExtendedError() to get the cause of
     the failure when the function returns FALSE. GetLastError() will return
     one of the values listed in GetSaveFileName(), or zero if the dialog was
     cancelled by the user. GetLastError() will return CDERR_INITIALIZATION if
     anything in the OPENFILENAME structure is invalid.
   - saveInfo->Flags must not include OFN_ALLOWMULTISELECT.
   - saveInfo->Flags must include OFN_EXPLORER if the dialog is customized.
   - saveInfo->lpstrFilter may use tab characters instead of NULL terminators
     for all except the last NULL terminator.
   - saveInfo->lpstrCustomFilter may use a tab character instead of both NULL
     terminators (no NULL terminator is necessary after the second tab); if it
     does the returned value will also use tab characters instead of both NULL
     terminators, followed immediately by a NULL terminator.
   - saveInfo->lpstrFile may use a tab character instead of the NULL
     terminator when this function is called. The returned value will always
     use a NULL terminator. */
extern BOOL WINAPI GetTrapezeNoLocalSaveFileName(LPOPENFILENAMEA saveInfo);
#endif /*_WIN32*/

#ifdef _WIN32
/* GetTrapezeToolbarBitmap
   Returns information about a bitmap containing Trapeze toolbar button icons,
   for use in building a custom toolbar. bitmap specifies the bitmap to
   retrieve - it must be one of the trapeze*Bitmap constants from above. The
   number of button icons in the bitmap is returned in buttonCount. The
   instance containing the bitmap is returned in instance. The resource
   identifier of the bitmap is returned in bitmapID. The instance and id can
   be passed to LoadBitmap() or LoadResource() etc to load the bitmap. The
   return value is TRUE on success, or FALSE on failure. On failure, use
   GetLastError() to get the cause of the failure. */
extern BOOL WINAPI GetTrapezeToolbarBitmap(WORD bitmap, LPWORD buttonCount,
                                           HINSTANCE* instance,
                                           UINT* bitmapID);
#endif /*_WIN32*/

/* TrapezeWORD TrapezeAPI GetTrapezeVersionT(TrapezeLPTSTR version,
                                             TrapezeWORD versionSize) */
#ifdef UNICODE
#define GetTrapezeVersionT(version, versionSize) \
  GetTrapezeVersionW(version, versionSize)
#else
#define GetTrapezeVersionT(version, versionSize) \
  GetTrapezeVersion(version, versionSize)
#endif /*UNICODE*/

/* GetTrapezeVersion
   Gets Trapeze version information. The version string is returned in version
   if version is not NULL. versionSize specifies the size of version, in
   characters. The return value a version number on success or zero on
   failure. On success, the return value is major in the highest four bits,
   minor in the next four bits, bugfix in the next four bits, and
   alpha/beta/release count in the lowest four bits. e.g. Trapeze 2.1 Beta 3
   returns 0x2103. A comparison can be used to check if one version of Trapeze
   is later than another. On failure, use GetLastError() to get the cause of
   the error. */
extern TrapezeWORD TrapezeAPI GetTrapezeVersion(TrapezeLPSTR version,
                                                TrapezeWORD versionSize);

/* GetTrapezeVersionW
   Gets Trapeze version information (Unicode version). See GetTrapezeVersion()
   for details.*/
extern TrapezeWORD TrapezeAPI GetTrapezeVersionW(TrapezeLPWSTR version,
                                                 TrapezeWORD versionSize);

#ifdef _WIN32
#define InitTrapeze()
#else
/* InitTrapeze
   Initializes Trapeze for use on Unix. This (or InitTrapezeEx()) must be
   called before any other function. All other functions will return
   TrapezeCO_E_NOTINITIALIZED until this function is called. On Windows, this
   function is automatically called when the first Trapeze API call is
   made. */
extern void TrapezeAPI InitTrapeze(void);
#endif /*_WIN32*/

/* InitTrapezeEx
   Initializes Trapeze for use, when non-default features are required. This
   must be called before any other function. flags is a combination of
   initTrapeze* values. The return value is TRUE on success, FALSE on failure.
   On failure, use GetLastError() to get the cause of the failure. */
extern BOOL TrapezeAPI InitTrapezeEx(DWORD flags);

#ifdef _WIN32
/* IsTrapezeODMAAvailable
   Checks if ODMA is available for use from Trapeze. The return value is FALSE
   on error, or if ODMA is not available. The last error value is not updated
   on success, so to determine the reason for failure, call
   SetLastError(ERROR_SUCCESS) before calling IsTrapezeODMAAvailable(), then
   call GetLastError() to get the cause of the error. If GetLastError()
   returns ERROR_SUCCESS then ODMA is not available, otherwise an error
   occurred. */
extern TrapezeBOOL TrapezeAPI IsTrapezeODMAAvailable(void);
#endif /*_WIN32*/

#ifdef _WIN32
/* IsTrapezeTRIMAvailable
   Checks if TRIM is available for use from Trapeze. If topDrawer is FALSE,
   the return value is TRUE if TRIM is available for use from Trapeze. If
   topDrawer is TRUE, the return value is TRUE if TopDrawer is available for
   use from Trapeze. The return value is FALSE on error, or if TRIM/TopDrawer
   is not available. The last error value is not updated on success, so to
   determine the reason for failure, call SetLastError(ERROR_SUCCESS) before
   calling IsTrapezeTRIMAvailable(), then call GetLastError() to get the
   cause of the error. If GetLastError() returns ERROR_SUCCESS then
   TRIM/TopDrawer is not available, otherwise an error occurred. */
extern TrapezeBOOL TrapezeAPI IsTrapezeTRIMAvailable(TrapezeBOOL topDrawer);
#endif /*_WIN32*/

#ifdef _WIN32
/* LRESULT OpenTrapezeDocumentT(HWND window, LPCTSTR fileName,
                                BOOL readOnly) */
#ifdef UNICODE
#define OpenTrapezeDocumentT(window, fileName, readOnly) \
  OpenTrapezeDocumentW(window, fileName, readOnly)
#else
#define OpenTrapezeDocumentT(window, fileName, readOnly) \
  OpenTrapezeDocument(window, fileName, readOnly)
#endif /*UNICODE*/

/* OpenTrapezeDocument
   Opens a document in a Trapeze plugin window. window specifies the window to
   open the document in. fileName specifies the path of the file to be opened,
   which must already exist. If fileName is NULL, an open dialog box will be
   displayed by the plugin window. If readOnly is TRUE, the document will be
   opened read-only, otherwise the document will be opened read-write if
   possible or read-only if not. The return value is trapezeMsgSuccess on
   success, or an error code on failure. */
/* LRESULT OpenTrapezeDocument(HWND window, LPCSTR fileName, BOOL readOnly) */
#define OpenTrapezeDocument(window, fileName, readOnly) \
  SendMessage((window), openTrapezeDocumentMsg, (WPARAM) (readOnly), \
              (LPARAM) (fileName))

/* OpenTrapezeDocumentW
   Opens a document in a Trapeze plugin window (Unicode version). See
   OpenTrapezeDocument() for details. */
/* LRESULT OpenTrapezeDocumentW(HWND window, LPCWSTR fileName,
                                BOOL readOnly) */
#define OpenTrapezeDocumentW(window, fileName, readOnly) \
  SendMessage((window), openTrapezeDocumentWMsg, (WPARAM) (readOnly), \
              (LPARAM) (fileName))

#endif /*_WIN32*/

#ifdef _WIN32
/* PrintTrapezeDocument
   Prints some or all pages from a file to the default printer, without
   showing a Print dialog. parent specifies a parent window for any dialogs
   that need to be displayed; it may be NULL. Dialogs will only be displayed
   if errors occur. fileName specifies the file to print. pageRangeCount
   specifies the number of page ranges specified by pageRanges; if
   pageRangeCount is zero pageRanges is ignored and the entire document is
   printed. If pageRangeCount is not zero pageRanges must contain
   pageRangeCount entries, each entry specifying a range of pages to be
   printed. The return value is TRUE on success, or FALSE on failure. On
   failure, use GetLastError() to get the cause of the failure.
   The document is printed to the default printer, with its default settings,
   using default print options (e.g. fit to page, one image per page). */
extern BOOL WINAPI PrintTrapezeDocument(HWND parent, LPCSTR fileName,
                                        DWORD pageRangeCount,
                                        PrintTrapezeDocumentPageRangeCPtr
                                        pageRanges);
#endif /*_WIN32*/

#ifdef _WIN32
/* PrintTrapezeDocumentEx
   Prints some or all pages from a file to the default printer, optionally
   showing a Print dialog. printInfo->size must be set to the size of the
   printInfo structure. printInfo->parent specifies a parent window for any
   dialogs that need to be displayed; it may be NULL. If the print dialog is
   not shown, dialogs will only be displayed if errors or warnings occur.
   printInfo->fileName specifies the file to print. printInfo->pageRangeCount
   specifies the number of page ranges specified by printInfo->pageRanges; if
   printInfo->pageRangeCount is zero printInfo->pageRanges is ignored and the
   entire document is printed. printInfo->pageRangeCount must be zero if the
   print dialog is shown. If printInfo->pageRangeCount is not zero
   printInfo->pageRanges must contain printInfo->pageRangeCount entries, each
   entry specifying a range of pages to be printed. printInfo->printSizeLimit
   sets a print size limit as for the printSizeLimitTrapezeProperty property.
   printInfo->flags specifies how printing behaves, and can be used to show a
   print dialog before printing. The return value is TRUE on success, or FALSE
   on failure. On failure, use GetLastError() to get the cause of the failure.
   The document is printed to the default printer, with its default settings,
   using default print options (e.g. fit to page, one image per page). */
extern BOOL WINAPI PrintTrapezeDocumentEx(PrintTrapezeDocumentExPtr
                                          printInfo);
#endif /*_WIN32*/

/* ReadTrapezeExifData
   Reads Exif data from an image file. fileName specifies the file to read the
   data from. pageNo specifies the page number (one based) of the document to
   read the data from; for a TIFF the data for the full size image for the
   page will be retrieved. The return value is a handle to the Exif data, or
   NULL on failure or if there is no Exif data. The last error value is not
   updated on success, so to determine success or failure for a NULL return
   value, call SetLastError(ERROR_SUCCESS) before calling
   ReadTrapezeExifData(), then call GetLastError() to get the cause of the
   error. If GetLastError() returns ERROR_SUCCESS then no Exif data was found,
   otherwise an error occurred. The returned value must be freed using
   GlobalFree() when it is no longer required.
   The function will fail with the error ERROR_INVALID_PARAMETER if pageNo is
   zero or larger than the number of pages in the file. */
extern TrapezeHGLOBAL TrapezeAPI ReadTrapezeExifData(TrapezeLPCSTR fileName,
                                                     TrapezeDWORD pageNo,
                                                     TrapezeLPSIZE_T
                                                     dataSize);

#ifdef _WIN32
/* RegisterTrapezeEditDropTarget
   Registers the specified edit control for accepting dragged OCR text from
   Trapeze. The return value is TRUE on success, or FALSE on failure. On
   failure, use GetLastError() to get the cause of the error. RevokeDragDrop()
   must be called for the window before it is destroyed, when the drop target
   is no longer required.
   This function calls RegisterDragDrop() on the edit control, so the edit
   control must not be already registered as a drop target, otherwise the
   function will fail with DRAGDROP_E_ALREADYREGISTERED. Also, OleInitialize()
   must have been called from the thread calling this function, not
   CoInitialize() or CoInitializeEx(). */
extern BOOL WINAPI RegisterTrapezeEditDropTarget(HWND edit);
#endif /*_WIN32*/

/* SetLastTrapezeError
   Sets the last error code (and message) for Trapeze. This function is
   similar to SetLastError(), but only sets the last Trapeze error.
   Note: This function is not thread-safe; it sets the last available error
         code and message for any Trapeze function or message in any
         thread. */
extern void TrapezeAPI SetLastTrapezeError(DWORD error);

/* SetTrapezeDebugLogFile
   Sets the file to log debugging messages to. The default is none. The return
   value is TRUE on success, or FALSE on failure. On failure, use
   GetLastError() to get the cause of the error.
   Note: Debugging information will only be written to this file if Trapeze is
         a debug version and the file can be created. */
extern TrapezeBOOL TrapezeAPI SetTrapezeDebugLogFile(TrapezeLPCSTR fileName);

/* SetTrapezeDebugLogProc
   Sets the function to call with debugging messages. The default is none.
   flags is a combination of *TrapezeDebugLogProc values. debugLogProc
   specifies the function to call for each debug message, or NULL to set back
   to none. lParam is passed to debugLogProc each time it is called. */
extern TrapezeBOOL TrapezeAPI SetTrapezeDebugLogProc(TrapezeDWORD flags,
                                                     TrapezeDebugLogProc
                                                     debugLogProc,
                                                     TrapezeLPVOID lParam);

/* SetTrapezeExemptionCodes
   Sets the exemption codes used by any Trapeze windows created following this
   call. If file is not NULL, it specifies the file to read the exemption
   codes from, and custom exemption codes are disabled. Currently supported
   file types are an ini file or an XML file. If fileName is NULL the
   exemption codes are set to the default, which is the Trapeze ini file or
   the registry.  The return value is TRUE on success, or FALSE on failure. On
   failure, use GetLastError() to get the cause of the error. */
extern TrapezeBOOL TrapezeAPI SetTrapezeExemptionCodes(TrapezeLPCSTR fileName);

/* TrapezeBOOL TrapezeAPI SetTrapezeIniFileT(TrapezeLPCTSTR fileName) */
#ifdef UNICODE
#define SetTrapezeIniFileT(fileName) SetTrapezeIniFileW(fileName)
#else
#define SetTrapezeIniFileT(fileName) SetTrapezeIniFile(fileName)
#endif /*UNICODE*/

/* SetTrapezeIniFile
   Sets the ini file used for custom toolbars, preference defaults, DICOM tags
   and UIDs, etc. The default is the same as the DLL file name with an .ini
   extension, or trapeze3.ini on Unix. The return value is TRUE on success, or
   FALSE on failure. On failure, use GetLastError() to get the cause of the
   error.
   Note: If the path begins with .\ it will be changed to begin with the
         directory containing the Trapeze DLL (on Windows only). */
extern TrapezeBOOL TrapezeAPI SetTrapezeIniFile(TrapezeLPCSTR fileName);

/* SetTrapezeIniFileW
   Sets the ini file used for custom toolbars, preference defaults, DICOM tags
   and UIDs, etc (Unicode version). See SetTrapezeIniFile() for details. */
extern TrapezeBOOL TrapezeAPI SetTrapezeIniFileW(TrapezeLPCWSTR fileName);

/* SetTrapezeLanguage
   Sets the language used by Trapeze, if available. Setting the language to
   zero (the default) causes Trapeze to use the current user's language, or
   US English if the current user's language is not available. US English
   ( MAKELANGID(LANG_ENGLISH, SUBLANG_ENGLISH_US) ) is always available.
   The return value is TRUE on success or FALSE on failure. On failure, use
   GetLastError() to get the cause of the failure. The function will fail with
   error ERROR_FILE_NOT_FOUND if the requested language is not available. The
   language is global, so should be set before any windows are created;
   windows will not reload menus etc if the language is changed after they are
   created. Plugin windows will use the language from the user's preferences,
   unless this function is called before creating a Plugin window. If this
   function is called first, the Plugin window will use the language
   specified, and will remove the language setting from preferences. */
extern TrapezeBOOL TrapezeAPI SetTrapezeLanguage(TrapezeLANGID language);

/* TrapezeBOOL TrapezeAPI SetTrapezeLicenseT(TrapezeLPCTSTR customer,
                                             TrapezeLPCTSTR location,
                                             TrapezeLPCTSTR key) */
#ifdef UNICODE
#define SetTrapezeLicenseT(customer, location, key) \
  SetTrapezeLicenseW(customer, location, key)
#else
#define SetTrapezeLicenseT(customer, location, key) \
  SetTrapezeLicense(customer, location, key)
#endif /*UNICODE*/

/* SetTrapezeLicense
   Licenses the DLL for use by the callee. This function must be called before
   any other DLL function. customer, location and key specify the three parts
   of the key. The return value is TRUE on success, or FALSE on failure. On
   failure, use GetLastError() to get the cause of the failure. The license
   can only be set once; subsequent calls will fail with
   ERROR_ALREADY_EXISTS, except when customer, location and key are all NULL.
   If customer, location and key are all NULL, the license is cleared, as long
   as there are no windows or objects open from the DLL, otherwise the call
   will fail with ERROR_INVALID_PARAMETER. Once the license has been cleared,
   it can be set again with a subsequent call to SetTrapezeLicense(). */
extern TrapezeBOOL TrapezeAPI SetTrapezeLicense(TrapezeLPCSTR customer,
                                                TrapezeLPCSTR location,
                                                TrapezeLPCSTR key);

/* SetTrapezeLicenseW
   Licenses the DLL for use by the callee (Unicode version). See
   SetTrapezeLicense() for details. */
extern TrapezeBOOL TrapezeAPI SetTrapezeLicenseW(TrapezeLPCWSTR customer,
                                                 TrapezeLPCWSTR location,
                                                 TrapezeLPCWSTR key);

/* SetTrapezeOCREngine
   Sets the OCR engine to be used. ocrEngine specifies the OCR engine; it must
   be one of *TrapezeOCREngine. The return value is TRUE on success or FALSE
   on failure. On failure, use GetLastError() to get the cause of the failure.
   The default is defaultTrapezeOCREngine, which uses any available OCR
   engine.
   Note: This function does not check that the specified OCR engine is
         available or useable; it simply sets the OCR engine to be used for
         subsequent OCRing. */
extern TrapezeBOOL TrapezeAPI SetTrapezeOCREngine(TrapezeBYTE ocrEngine);

/* SetTrapezePointer
   Sets pointer to value, useful for languages such as PowerBuilder that don't
   support pointers in structures. */
extern void TrapezeAPI SetTrapezePointer(TrapezeLPVOID* pointer,
                                         TrapezeLPVOID value);

/* TrapezeBOOL TrapezeAPI SetTrapezeProductNameT(TrapezeLPCTSTR
                                                 productName) */
#ifdef UNICODE
#define SetTrapezeProductNameT(productName) \
  SetTrapezeProductNameW(productName)
#else
#define SetTrapezeProductNameT(productName) \
  SetTrapezeProductName(productName)
#endif /*UNICODE*/

/* SetTrapezeProductName
   Sets the product name for Trapeze to use, if allowed by the license.
   productName specifies the product name to use, or NULL to use the default.
   The return value is TRUE on success or FALSE on failure. On failure, use
   GetLastError() to get the cause of the failure. */
extern TrapezeBOOL TrapezeAPI SetTrapezeProductName(TrapezeLPCSTR
                                                    productName);

/* SetTrapezeProductNameW
   Sets the product name for Trapeze to use, if allowed by the license
   (Unicode version). See SetTrapezeProductName() for details. */
extern TrapezeBOOL TrapezeAPI SetTrapezeProductNameW(TrapezeLPCWSTR
                                                     productName);

/* SetTrapezeTempPath
   Sets the location used by Trapeze for temporary files. If path is NULL or
   an empty string the location is reset to the default, which is the location
   used by Windows for temporary files. The return value is TRUE on success,
   or FALSE on failure. On failure, use GetLastError() to get the cause of the
   failure.
   Note: This function will verify that the directory specified by path
     exists, and that files can be created in it. */
extern TrapezeBOOL TrapezeAPI SetTrapezeTempPath(TrapezeLPCSTR path);

#ifdef _WIN32
/* SubclassTrapezeColorButton
   Subclasses the specified button control to contain a box showing a color.
   The return value is TRUE on success, or FALSE on failure. On failure, use
   GetLastError() to get the cause of the error. The subclassing is
   automatically removed when the button is destroyed.
   Use getColorTrapezeColorButtonMsg to get the current color, and
   setColorTrapezeColorButtonMsg to change the current color. */
extern BOOL WINAPI SubclassTrapezeColorButton(HWND button);
#endif /*_WIN32*/

/* TerminateTrapezeDLL
   Frees all resources in use by the DLL. This will happen anyway during DLL
   unload on Windows, but in some cases termination during DLL unload causes
   problems and calling this function before unload can solve them. This must
   be called on Unix. */
extern void TrapezeAPI TerminateTrapezeDLL(void);

/* TrapezeDoubleToURational
   Converts a positive floating point number into a rational number
   (numerator / denominator). d specifies the number to be converted. The
   numerator and demoninator are returned in numerator and denominator,
   respectively. The return value is TRUE on success, FALSE on failure. On
   failure, use GetLastError() to get the cause of the failure. */
extern TrapezeBOOL TrapezeAPI TrapezeDoubleToURational(double d,
                                                       TrapezeLPDWORD
                                                       numerator,
                                                       TrapezeLPDWORD
                                                       denominator);

#ifdef _WIN32
extern BOOL WINAPI TrapezePageSetup(HWND parent,
                                    TrapezePageSetupPtr pageSetupInfo);
#endif /*_WIN32*/

#ifdef _WIN32
/* TrapezeStampAdmin
   Displays a stamp administration dialog, according to the information in
   info. The return value is TRUE on success, FALSE on failure. On
   failure, use GetLastError() to get the cause of the failure. */
extern BOOL WINAPI TrapezeStampAdmin(TrapezeStampAdminCPtr info);
#endif /*_WIN32*/

#ifdef _WIN32
/* TrapezeSystemInfo
   Displays a system information dialog. parent specifies the dialog parent.
   The return value is TRUE on success, FALSE on failure. On failure, use
   GetLastError() to get the cause of the failure. */
extern BOOL WINAPI TrapezeSystemInfo(HWND parent);
#endif /*_WIN32*/

#ifdef _WIN32
/* TrapezeViewCertificate
   Displays a certificate dialog box. parent specifies the dialog box parent.
   cert specifies the certificate to display. The return value is TRUE on
   success, FALSE on failure. On failure, use GetLastError() to get the cause
   of the failure. */
extern BOOL WINAPI TrapezeViewCertificate(HWND parent, PCCERT_CONTEXT cert);
#endif /*_WIN32*/

/* OCR Text Functions
   ================== */

/* DestroyTrapezeOCRText
   Destroys OCR text returned by GetTrapezeTIFFOCRText(). The return value is
   TRUE on success, or FALSE on failure. On failure, use GetLastError() to get
   the cause of the error. ocrText must not be used in any other Trapeze calls
   after this function has been called. */
extern TrapezeBOOL TrapezeAPI DestroyTrapezeOCRText(TrapezeOCRText ocrText);

/* GetTrapezeOCRTextCount
   Returns the number of OCR text blocks. ocrText and ocrTextSize must be
   values returned by GetTrapezeTIFFOCRText(). The return value is
   getTrapezeOCRTextCountFailed if an error occurred. On failure, use
   GetLastError() to get the cause of the error. */
extern TrapezeDWORD TrapezeAPI GetTrapezeOCRTextCount(TrapezeOCRText ocrText,
                                                      TrapezeDWORD
                                                      ocrTextSize);

/* GetTrapezeOCRTextRects
   Gets the locations for characters in an OCR text block. ocrText and
   ocrTextSize must be values returned by GetTrapezeTIFFOCRText(). ocrTextNo
   specifies the zero based number of the text block to retrieve the locations
   from. ocrTextNo must be less than the value returned by
   GetTrapezeOCRTextCount(), otherwise the function will fail with error
   ERROR_FILE_NOT_FOUND. charNo specifies the zero based character number
   within the block to get the first rectangle for, and count specifies how
   many characters (starting at charNo) to get rectangles for.
   charNo + count - 1 must be <= the count returned by
   GetTrapezeOCRTextText(), otherwise the function will fail with error
   ERROR_INVALID_PARAMETER. The rectangles are returned if rects is not NULL;
   rects must be large enough to hold the number of rectangles being returned.
   If rects is NULL only the count is returned. The return value is the number
   of rectangles, or getTrapezeOCRTextRectsFailed on failure. On failure, use
   GetLastError() to get the cause of the error. */
extern TrapezeDWORD TrapezeAPI GetTrapezeOCRTextRects(TrapezeOCRText ocrText,
                                                      TrapezeDWORD
                                                      ocrTextSize,
                                                      TrapezeDWORD ocrTextNo,
                                                      TrapezeDWORD charNo,
                                                      TrapezeDWORD count,
                                                      TrapezeLPRECT rects);

/* GetTrapezeOCRTextText
   Gets the text of an OCR text block. ocrText and ocrTextSize must be values
   returned by GetTrapezeTIFFOCRText(). ocrTextNo specifies the zero based
   number of the text block to retrieve the text from. ocrTextNo must be less
   than the value returned by GetTrapezeOCRTextCount(), otherwise the function
   will fail with error ERROR_FILE_NOT_FOUND. The text is returned in text if
   text is not NULL; if text is NULL the text is not returned. If text is not
   NULL textSize specifies the size of text, in characters. The return value
   is the length of the text, in characters, not including the NULL
   terminator, or getTrapezeOCRTextTextFailed on failure. On failure, use
   GetLastError() to get the cause of the error. */
extern TrapezeDWORD TrapezeAPI GetTrapezeOCRTextText(TrapezeOCRText ocrText,
                                                     TrapezeDWORD ocrTextSize,
                                                     TrapezeDWORD ocrTextNo,
                                                     TrapezeLPSTR text,
                                                     TrapezeDWORD textSize);

/* OTTP Functions
   ============== */

#ifdef _WIN32
/* CloseTrapezeOTTPConnection
   Closes an OTTP connection when it is no longer required. ottp specifies the
   connection to close. timeout specifies the timeout, in seconds, for any
   data that needs to be sent during closing. ottp must not be used after this
   function is called. The return value is TRUE on success or FALSE on
   failure. On failure, use GetLastError() to get the cause of the failure. */
extern BOOL WINAPI CloseTrapezeOTTPConnection(TrapezeOTTP ottp, WORD timeout);
#endif /*_WIN32*/

#ifdef _WIN32
/* CreateTrapezeOTTPConnection
   Creates an OTTP connection to url. The return value is the OTTP connection,
   or NULL on failure. On failure, use GetLastError() to get the cause of the
   failure. */
extern TrapezeOTTP WINAPI CreateTrapezeOTTPConnection(LPCSTR url);
#endif /*_WIN32*/

#ifdef _WIN32
/* GetTrapezeOTTPFullImage
   Gets the full image for an OTTP page. ottp specifies the OTTP connection to
   get the page from; it must have been opened successfully with
   OpenTrapezeOTTPConnection() before calling this function. pageNo specifies
   the one based page number of the page to get. timeout specifies the
   timeout, in seconds, before failure due to lack of response from the
   server. If fileName is not NULL, the path to the file containing the image
   for the page is returned in fileName; this file will be deleted when the
   OTTP connection is closed. size specifies the size of fileName, in
   characters. The return value is TRUE on success, or FALSE on failure. On
   failure, use GetLastError() to get the cause of the failure. */
extern BOOL WINAPI GetTrapezeOTTPFullImage(TrapezeOTTP ottp, DWORD pageNo,
                                           WORD timeout, LPSTR fileName,
                                           DWORD size);
#endif /*_WIN32*/

#ifdef _WIN32
/* OpenTrapezeOTTPConnection
   Opens an OTTP connection. ottp specifies the OTTP connection to open,
   created with CreateTrapezeOTTPConnection() and not yet successfully opened
   using this function. If readOnly is TRUE, the document is opened read-only,
   otherwise the document is opened for writing and will fail if access is
   denied. timeout specifies the timeout, in seconds, before failure due to
   lack of response from the server. parent specifies the parent window for
   any authentication or encryption dialogs. If encrypt is FALSE, encryption
   is only used if required by the server. If encrypt is TRUE, encryption is
   used if the server supports it. The return value is the number of pages in
   the document, or openTrapezeOTTPConnectionFailed on failure. On failure,
   use GetLastError() to get the cause of the failure. */
extern DWORD WINAPI OpenTrapezeOTTPConnection(TrapezeOTTP ottp, BOOL readOnly,
                                              WORD timeout, HWND parent,
                                              BOOL encrypt);
#endif /*_WIN32*/

/* Pasteboard Funtions
   =================== */

#ifdef _WIN32
extern HWND WINAPI CreateTrapezePasteboard(DWORD exStyle, LPCSTR caption,
                                           DWORD style, int x, int y,
                                           int width, int height, HWND parent,
                                           UINT id, HWND statusBar);
#endif /*_WIN32*/

/* PDF Functions
   ============= */

/* TrapezeBOOL TrapezeAPI AppendTrapezePDFPageT(TrapezePDF pdf,
                                                TrapezeLPCTSTR fileName,
                                                TrapezeDWORD pageNo) */
#ifdef UNICODE
#define AppendTrapezePDFPageT(pdf, fileName, pageNo) \
  AppendTrapezePDFPageW(pdf, fileName, pageNo)
#else
#define AppendTrapezePDFPageT(pdf, fileName, pageNo) \
  AppendTrapezePDFPage(pdf, fileName, pageNo)
#endif /*UNICODE*/

/* AppendTrapezePDFPage
   Appends a page to a PDF created using CreateTrapezePDF(). fileName
   specifies the file to read the page from. pageNo specifies the page number
   (one based) of the document to read the page from. The return value is TRUE
   on success, or NULL on failure. On failure, use GetLastError() to get the
   cause of the error.
   SaveTrapezePDF() must be called before closing pdf to complete the
   changes. */
extern TrapezeBOOL TrapezeAPI AppendTrapezePDFPage(TrapezePDF pdf,
                                                   TrapezeLPCSTR fileName,
                                                   TrapezeDWORD pageNo);

/* AppendTrapezePDFPageW
   Appends a page to a PDF created using CreateTrapezePDF() (Unicode version).
   See AppendTrapezePDFPage() for details. */
extern TrapezeBOOL TrapezeAPI AppendTrapezePDFPageW(TrapezePDF pdf,
                                                    TrapezeLPCWSTR fileName,
                                                    TrapezeDWORD pageNo);

/* CloseTrapezePDF
   Closes a PDF when it is no longer required. pdf specifies the PDF to close.
   pdf must not be used after this function is called. The return value is
   TRUE on success or FALSE on failure. On failure, use GetLastError() to get
   the cause of the failure.
   Any changes since pdf was last saved will be lost. */
extern TrapezeBOOL TrapezeAPI CloseTrapezePDF(TrapezePDF pdf);

/* CompactTrapezePDF
   Compacts a PDF according to the information in compactInfo. The return
   value is TRUE on success or FALSE on failure. On failure, use
   GetLastError() to get the cause of the failure. */
extern TrapezeBOOL TrapezeAPI CompactTrapezePDF(CompactTrapezePDFCPtr
                                                compactInfo);
/* ConvertTrapezePDFToTIFF
   Converts a PDF in srcFileName to a TIFF in destFileName. pageNo specifies
   the one based page number to convert, or convertTrapezePDFToTIFFAllPages to
   convert all pages in the PDF. The return value is TRUE on success or FALSE
   on failure. On failure, use GetLastError() to get the cause of the failure.
   Use SetTrapezePDFOptions() to control various conversion options. */
extern TrapezeBOOL TrapezeAPI ConvertTrapezePDFToTIFF(TrapezeLPCSTR
                                                      srcFileName,
                                                      TrapezeDWORD pageNo,
                                                      TrapezeLPCSTR
                                                      destFileName);

/* ConvertTrapezePDFToTIFFEx
   Converts a PDF to a TIFF according to the information in convertInfo. The
   return value is TRUE on success or FALSE on failure. On failure, use
   GetLastError() to get the cause of the failure. Use SetTrapezePDFOptions()
   to control default conversion options. Conversion options specified in
   convertInfo override those set with SetTrapezePDFOptions(). */
extern TrapezeBOOL TrapezeAPI ConvertTrapezePDFToTIFFEx(
                                                 ConvertTrapezePDFToTIFFExCPtr
                                                        convertInfo);

/* TrapezePDF TrapezeAPI CreateTrapezePDFT(TrapezeLPCTSTR fileName,
                                           TrapezeLPCTSTR srcFileName) */
#ifdef UNICODE
#define CreateTrapezePDFT(fileName, srcFileName) \
  CreateTrapezePDFW(fileName, srcFileName)
#else
#define CreateTrapezePDFT(fileName, srcFileName) \
  CreateTrapezePDF(fileName, srcFileName)
#endif /*UNICODE*/

/* CreateTrapezePDF
   Creates a PDF for appending pages to. fileName specifies the file for the
   new PDF; any existing file will be replaced. srcFileName specifies NULL to
   create an empty PDF, or a file containing a PDF to create a copy of an
   existing PDF. The return value is non-NULL on success or NULL on failure.
   On failure, use GetLastError() to get the cause of the failure. */
extern TrapezePDF TrapezeAPI CreateTrapezePDF(TrapezeLPCSTR fileName,
                                              TrapezeLPCSTR srcFileName);

/* CreateTrapezePDFW
   Creates a PDF for appending pages to (Unicode version). See
   CreateTrapezePDF() for details. */
extern TrapezePDF TrapezeAPI CreateTrapezePDFW(TrapezeLPCWSTR fileName,
                                               TrapezeLPCWSTR srcFileName);

extern TrapezeHGLOBAL TrapezeAPI GetTrapezePDFDocumentData(TrapezePDF pdf,
                                                           TrapezeLPSIZE_T
                                                           size);

/* GetTrapezePDFPageData
   Gets the unsent data for a PDF page. pdf specifies the open PDF to get the
   page data from, which must be opened using OpenTrapezePDF(). pageNo
   specifies the one-based page number to get the data for. flags is one of:
   - trapezePDFPageAll, to get all unsent data for the page, the PDF must have
     been opened with objectListTrapezePDFOption set
   - trapezePDFPageThumbnail, to get all unsent data for the thumbnail, the
     PDF must have been opened with objectListTrapezePDFOption set
   - trapezePDFPageNoAnnotations, to get all unsent data for the page except
     annotation data, the PDF must have been opened with
     objectListTrapezePDFOption set
   - trapezePDFPageDIB, to get the page as a DIB, using conversion options set
     using SetTrapezePDFOptions(). If trapezePDFPageJPEG is also set in flags
     and the page is a full page JPEG, the returned data will be a JPEG stream
     rather than a DIB; the data must be examined to determine if it is a DIB
     or JPEG. If trapezePDFPageResolution is also set in flags, size is a
     TrapezePDFPageDataResolutionPtr rather than a TrapezeLPSIZE_T, which is
     filled.
   - trapezePDFPageThumbnailDIB, to get the thumbnail as a DIB
   - trapezePDFPageOCRText, to get the OCR text for the page. The last error
     value is not updated on success, so to determine success or failure for a
     NULL return value, call SetLastError(ERROR_SUCCESS) before calling
     GetTrapezePDFPageData(), then call GetLastError() to get the cause of the
     error
   size is set to the size of the data. The return value is a GlobalAlloc()
   handle containing the data on success, or NULL on failure. On failure, use
   GetLastError() to get the cause of the error. The returned handle must be
   freed using GlobalFree() when it is no longer required. */
extern TrapezeHGLOBAL TrapezeAPI GetTrapezePDFPageData(TrapezePDF pdf,
                                                       TrapezeDWORD pageNo,
                                                       TrapezeDWORD flags,
                                                       TrapezeLPSIZE_T size);

/* GetTrapezePDFPageCount
   Returns the number of pages in a PDF document, or
   getTrapezePDFPageCountFailed on failure. pdf specifies the PDF to retrieve
   the page count from. On failure, use GetLastError() to get the cause of the
   failure. */
extern TrapezeDWORD TrapezeAPI GetTrapezePDFPageCount(TrapezePDF pdf);

/* OpenTrapezePDF
   Opens a PDF. fileName specifies the file containing the PDF. access
   specifies the required file access:
   - trapezeReadAccess specifies read-only access
   - trapezeWriteAccess or (trapezeReadAccess | trapezeWriteAccess) specifies
     read/write access
   The return value is non-NULL on success or NULL on failure. On failure, use
   GetLastError() to get the cause of the failure. */
extern TrapezePDF TrapezeAPI OpenTrapezePDF(TrapezeLPCSTR fileName,
                                            TrapezeDWORD access);

/* SaveTrapezePDF
   Saves any changes made to pdf. The return value is TRUE on success or FALSE
   on failure. On failure, use GetLastError() to get the cause of the
   error. This function will fail with ERROR_ACCESS_DENIED if pdf was opened
   with read-only access. */
extern TrapezeBOOL TrapezeAPI SaveTrapezePDF(TrapezePDF pdf);

/* SetTrapezePDFOptions
   Sets options for reading, converting and writing PDFs. pdfOptions must be
   filled appropriately before calling this function. The return value is
   TRUE on success, or FALSE on failure. On failure, use GetLastError() to get
   the cause of the error. */
extern TrapezeBOOL TrapezeAPI SetTrapezePDFOptions(TrapezePDFOptionsCPtr
                                                   pdfOptions);

/* SetTrapezePDFPageAnnotations
   Sets the annotations in a PDF page, replacing any existing annotations for
   that page. pdf specifies the PDF to change. pageNo specifies the one-based
   page number to replace the annotations in. annotations specifies the new
   annotations for the page, or NULL to remove any annotations.
   annotationsSize specifies the size of annotations, in bytes, if annotations
   is not NULL. The return value is TRUE on success, or FALSE on failure. On
   failure, use GetLastError() to get the cause of the error. The added PDF
   data is marked as unsent. */
extern TrapezeBOOL TrapezeAPI SetTrapezePDFPageAnnotations(TrapezePDF pdf,
                                                           TrapezeDWORD
                                                           pageNo,
                                                           TrapezeAnnotations
                                                           annotations,
                                                           TrapezeDWORD
                                                           annotationsSize);

/* SetTrapezePDFPageSent
   Sets data for a PDF page as sent. pdf specifies the open PDF to change the
   data status for, which must be opened using OpenTrapezePDF() with
   objectListTrapezePDFOption set. pageNo specifies the one-based page number
   to change the data status for. flags is one of:
   - trapezePDFPageAll, to mark all data for the page as sent
   - trapezePDFPageThumbnail, to mark all data for the thumbnail as sent
   - trapezePDFPageNoAnnotations, to mark all data for the page except
     annotation data as sent
   The return value is TRUE on success or FALSE on failure. On failure, use
   GetLastError() to get the cause of the error. */
extern TrapezeBOOL TrapezeAPI SetTrapezePDFPageSent(TrapezePDF pdf,
                                                    TrapezeDWORD pageNo,
                                                    TrapezeDWORD flags);

#ifdef _WIN32
/* Scanning Functions
   ================== */

/* CloseTrapezeScanner
   Closes an open scanner. If scanner is NULL this function has no effect. The
   return value is TRUE on success, FALSE on failure. On failure, use
   GetLastError() to get the cause of the failure. */
extern BOOL WINAPI CloseTrapezeScanner(TrapezeScanner scanner);

/* DestroyTrapezeScanInfo
   Destroys a scanInfo structure when it is no longer required, freeing any
   resources being used. scanInfo must have been initialized as for
   TrapezeScan() or passed to TrapezeScan() or SaveRestoreTrapezeScan() before
   calling DestroyTrapezeScanInfo(). The return value is TRUE on success,
   FALSE on failure. On failure, use GetLastError() to get the cause of the
   failure. */
extern BOOL WINAPI DestroyTrapezeScanInfo(TrapezeScanPtr scanInfo);

/* TrapezeScan
   Gets pages from a scanner. parent is the parent window for dialog boxes.
   title is the title of the scan dialog box. maxPageCount specifies the
   maximum number of pages that will be scanned. tempFileName specifies the
   file the pages will be written to if tempFileNameSize is zero. If
   tempFileNameSize is non-zero the pages will be written to a temporary file,
   and the name of that file will be returned in tempFileName. flags specifies
   how the scan is to be done:
   - noTrapezeScan indicates that no pages will be scanned (used for just
     displaying the scan dialog box).
   - noTrapezeScanDialog indicates that scanning will be performed without
     displaying the scan dialog box.
   - noTrapezeScanQueryFirstPage indicates that the first page will be scanned
     without first asking the user if there is a page to scan (e.g. for a
     flatbed scanner).
   - trapezeScanAutoDone automatically selects "Done" every time the "Continue
     Scan" dialog would be shown.
   - trapezeScanForceJobSep forces job separation by forcing "Single sided or
     double sided, all sides in order" and removing "None" from job separation
     options. Can cause a license error if no job separation is available.
     Returns an ERROR_INVALID_PARAMETER if told to scan without a dialog with
     non-job separation options.
   - noTrapezeScanDuplex disables duplex scanning. Disables scanning
     altogether when scanning with a dialog on scanners that only support
     duplex. Returns an ERROR_INVALID_PARAMETER error if told to scan without
     a dialog box with duplex selected.
   - noTrapezeScanFunctionSheets disables function sheets. Returns an
     ERROR_INVALID_PARAMETER if told to scan without a dialog with function
     sheets enabled.
   - noTrapezeScanMS61 disables MS61 scanning. This flag is only effective for
     the first call to TrapezeScan() in an application, after that it has no
     effect.
   - noTrapezeScanTWAIN disables TWAIN scanning. This flag is only effective
     for the first call to TrapezeScan() in an application, after that it has
     no effect.
   - trapezeScanCallback enables callback scanning. scanInfo->callbackProc is
     called for every page scanned or job separator detected.
     trapezeScanCallbackCanAdjustResolution specifies whether the callback can
     handle trapezeScanCallbackAdjustResolution
   - noTrapezeScanCancel removes the cancel button during scanning, whenever
     some pages have been scanned.
   - trapezeScanDefaultSoftwareJobSep causes job separation to default to
     software detected job separation instead of none, if possible.
   - noTrapezeScanBarcodes causes barcode options to be removed from job
     separation. Returns an ERROR_INVALID_PARAMETER error if told to scan
     without a dialog box with barcodes selected.
   scanInfo contains scan settings set by the user during the scan. It must be
   initialized to all zeros or using SaveRestoreTrapezeScan() before calling
   this function for the first time, with the exception of:
   - scanInfo->size, which must be initialized to the size of the scanInfo
     structure
   - scanInfo->jobFolderName and scanInfo->jobBookmarkName, which are used by
     this function and are read-only; the user cannot change them during the
     scan.
   - scanInfo->callbackProc and scanInfo->lParam, which are used by this
     function when the trapezeScanCallback flag is set, and are read-only.
   scanner is a pointer to the currently open scanner, which must be
   initialized to NULL before calling this function for the first time. Once
   the scanner is no longer required it must be passed to
   CloseTrapezeScanner() to close it. The return value indicates the number of
   pages scanned. If the return value is zero an error may have occurred; use
   GetLastError() to get the error code. The error code is only set when an
   error occurs, so to find out if an error occurred you need to call
   SetLastError(ERROR_SUCCESS) before calling this function. */
extern DWORD WINAPI TrapezeScan(HWND parent, LPCSTR title, DWORD maxPageCount,
                                LPSTR tempFileName, WORD tempFileNameSize,
                                DWORD flags, TrapezeScanPtr scanInfo,
                                TrapezeScannerPtr scanner);

/* SaveRestoreTrapezeScan
   Saves or restores scanner settings to or from the registry. If save is TRUE
   scanInfo is saved, otherwise it is restored. key specifies the key in the
   registry to save to or restore from. subKey specifies a sub-key under key.
   If subKey is NULL the information is saved or restored to or from key.
   scanInfo needs to be initialized before restoring as for TrapezeScan().
   scanInfo->jobFolderName and scanInfo->jobBookmarkName are ignored by this
   function. This function can be called safely with a non-existent key for
   both saving and restoring; if it is for restoring scanInfo will be
   initialized with defaults. When restoring, key can be NULL to get defaults
   (subKey will be ignored). The return value is TRUE on success, FALSE on
   failure. On failure, use GetLastError() to get the cause of the failure. If
   an error occurs during restoring scanInfo will remain unchanged.
   If key is saveRestoreTrapezeScanFileKey, scanner settings are saved to or
   restored from a file; subKey is the path to the file. */
extern BOOL WINAPI SaveRestoreTrapezeScan(BOOL save, HKEY key, LPCSTR subKey,
                                          TrapezeScanPtr scanInfo);

#endif /*_WIN32*/

#ifdef _WIN32
/* Status Bar Functions
   ==================== */

extern HWND WINAPI CreateTrapezeStatusBar(DWORD style, HWND parent, UINT id);

#endif /*_WIN32*/

/* TIFF Functions
   ============== */

/* AppendTrapezeThumbnail
   Appends a thumbnail to a TIFF. width and height specify the size of the
   added thumbnail, which is generated from image in the first IFD of the
   TIFF - the generated thumbnail will have the same aspect ratio as the
   image (i.e. won't be stretched out of shape), so may not be generated at
   exactly the specified size. compression specifies the compression for the
   thumbnail; currently it must be noTrapezeCompression or
   packBitsTrapezeCompression. fileName specifies the file containg the TIFF
   to append the thumbnail to. It is assumed to be a single page TIFF with no
   page numbers or thumbnails; if it isn't, you should probably use other
   functions (such as AppendTrapezeOpenTIFFEx()) to add the thumbnail
   correctly. The return value is TRUE on success, or FALSE on failure. On
   failure, use GetLastError() to get the cause of the failure. */
extern TrapezeBOOL TrapezeAPI AppendTrapezeThumbnail(TrapezeWORD width,
                                                     TrapezeWORD height,
                                                     TrapezeWORD compression,
                                                     TrapezeLPCSTR fileName);

/* AppendTrapezeOpenTIFF
   Appends all images in a TIFF from a file to an open TIFF. tiff specifies
   the open TIFF to append to. fileName specifies a file containing the TIFF
   to be appended. The return value is TRUE on success, or FALSE on failure.
   On failure, use GetLastError() to get the cause of the failure. */
extern TrapezeBOOL TrapezeAPI AppendTrapezeOpenTIFF(TrapezeTIFF tiff,
                                                    TrapezeLPCSTR fileName);

/* AppendTrapezeOpenTIFFEx
   Appends an image to an open TIFF, from a TIFF file, an open TIFF or a DIB.
   tiff specifies the open TIFF to append to. tiff must have been opened for
   writing. appendInfo specifies how the append is done:
   - size must be set to the size of the append structure.
   - srcFileName specifies the path to the file containing a TIFF to append
     from, if the append is from a file, or NULL if the append is not from a
     file.
   - srcTIFF specifies an open TIFF to append from, if the append is from an
     open TIFF, or NULL if the append is not from an open TIFF.
   - srcDIB specifies the DIB to append from, if the append is from a DIB, or
     NULL if the append is not from a DIB.
   Exactly one of srcFileName, srcTIFF and srcDIB must be non-NULL, and
   exactly two of them must be NULL.
   - srcIFDNo specifies the one-based IFD within the TIFF to append from if
     the append is from a file or open TIFF, or is ignored if the append is
     from a DIB.
   - flags specifies which attributes of the TIFF are to be changed during the
     append - it is a combination of appendTrapezeOpenTIFFEx* values. A value
     of zero means that the image will be appended with all attributes
     unchanged (where possible).
     - appendTrapezeOpenTIFFExAnnotations specifies that annotations are to be
       changed. If annotations is NULL the image will be written with no
       annotations, otherwise annotations and annotationsSize specify
       annotations retrieved with GetTrapezeTiffAnnotations(), to be written
       to the image.
     - appendTrapezeOpenTIFFExBookmarks specifies that bookmarks are to be
       changed. If bookmarks is NULL the image will be written with no
       bookmarks, otherwise bookmarks and bookmarksSize specify bookmarks
       returned by GetTrapezeBookmarkData(), to be written to the image.
       bookmarks can only be non-NULL for the first image appended to tiff,
       and must be NULL for every other image appended to TIFF if this flag is
       set.
     - appendTrapezeOpenTIFFExNewSubfileType specifies that the TIFF
       NewSubfileType is to be changed. newSubfileType specifies the new
       NewSubfileType - it must be a combination of *TrapezeNewSubfileType
       values.
     - appendTrapezeOpenTIFFExPageNumber specifies that the page number is to
       be changed. pageNo and pageCount specify the new page number. pageNo is
       zero based (i.e. page one is zero). A page count of zero indicates that
       the number of pages is unknown. If pageNo >= pageCount the image will
       be written with no page number.
     - appendTrapezeOpenTIFFExScale specifies that the scale is to be changed.
       If scale is NULL the image will be written with no scale, otherwise
       scale specifies the new scale.
     - appendTrapezeOpenTIFFExResolution specifies that the resolution is to
       be changed. resolution specifies the new resolution. An image cannot be
       written without resolution. If an image without resolution is appended
       and the resolution is not changed the resolution will be written as
       unknown with square pixels.
     - appendTrapezeOpenTIFFExCompression specifies that the compression of
       the image is to be changed. compression must be one of the
       *TrapezeCompression values.
     - appendTrapezeOpenTIFFExDateTime specifies that the data/time of the
       image is to be changed. dateTime specifies the new date/time. If
       dateTime is NULL or dateTime->year is zero the image will be written
       with no date/time.
   The return value is TRUE on success, or FALSE on failure. On failure, use
   GetLastError() to get the cause of the failure. */
extern TrapezeBOOL TrapezeAPI AppendTrapezeOpenTIFFEx(TrapezeTIFF tiff,
                                                   AppendTrapezeOpenTIFFExCPtr
                                                      appendInfo);

/* AppendTrapezeTIFF
   Appends all images in a TIFF file to another TIFF file. srcFileName
   specifies the file containing the TIFF to be appended. destFileName
   specifies the file containing the TIFF to be appended to. The return value
   is TRUE on success, or FALSE on failure. On failure, use GetLastError() to
   get the cause of the failure. */
extern TrapezeBOOL TrapezeAPI AppendTrapezeTIFF(TrapezeLPCSTR srcFileName,
                                                TrapezeLPCSTR destFileName);

/* CloseMemoryTrapezeTIFF
   Closes an in-memory TIFF that was created by calling OpenTrapezeTIFF() with
   fileName set to NULL. tiff specifies the in-memory TIFF to close. A handle
   containing the TIFF data is returned in memory. The size of the data in the
   handle is returned in memorySize. The return value is TRUE on success, or
   FALSE on failure. On failure, use GetLastError() to get the cause of the
   failure.
   tiff must not be used after calling this function. The data in returned in
   memory must be freed using GlobalFree() when it is no longer required. */
extern TrapezeBOOL TrapezeAPI CloseMemoryTrapezeTIFF(TrapezeTIFF tiff,
                                                     TrapezeHGLOBAL* memory,
                                                     TrapezeLPSIZE_T
                                                     memorySize);

/* CloseTrapezeTIFF
   Closes a TIFF when it is no longer required. tiff specifies the TIFF to
   close. tiff must not be used after this function is called. The return
   value is TRUE on success or FALSE on failure. On failure, use
   GetLastError() to get the cause of the failure.
   If tiff is an in-memory TIFF its data will be lost. */
extern TrapezeBOOL TrapezeAPI CloseTrapezeTIFF(TrapezeTIFF tiff);

/* ConvertTrapezeTIFFResolutionScale
   Converts a resolution or scale (in dots per unit) to different units.
   resScale specifies the resolution or scale (in dots per unit) to be
   converted. units specifies the units to be converted to; it must be
   one of *TrapezeTIFFResolutionScaleUnit. The return value is TRUE on success
   or FALSE on failure. On failure, use GetLastError() to get the cause of the
   failure.
   Note: This function converts dots per unit. To convert lengths, do the
         conversion backwards by reversing the source and destination units.
         i.e. set resScale->unit to the destination units and set unit to the
         source units. */
extern TrapezeBOOL TrapezeAPI
ConvertTrapezeTIFFResolutionScale(TrapezeTIFFResolutionScalePtr resScale,
                                  TrapezeWORD unit);

/* ConvertTrapezeTIFFToPDF
   Converts a TIFF to a PDF. srcFileName specifies the file containing the
   TIFF to be converted. destFileName specifies the file to write the PDF to;
   any existing file will be overwritten. The return value is TRUE on success,
   or FALSE on failure. On failure, use GetLastError() to get the cause of the
   failure. */
extern TrapezeBOOL TrapezeAPI ConvertTrapezeTIFFToPDF(TrapezeLPCSTR
                                                      srcFileName,
                                                      TrapezeLPCSTR
                                                      destFileName);

/* ConvertTrapezeTIFFToPDFEx
   Converts a TIFF to a PDF according to the information in convertInfo. The
   return value is TRUE on success, or FALSE on failure. On failure, use
   GetLastError() to get the cause of the failure. */
extern TrapezeBOOL TrapezeAPI ConvertTrapezeTIFFToPDFEx(
                                                 ConvertTrapezeTIFFToPDFExCPtr
                                                        convertInfo);

extern TrapezeBOOL TrapezeAPI CreateMultilayerTIFF(TrapezeLPCSTR
                                                   backgroundFileName,
                                                   TrapezeWORD layerCount,
                                                   TrapezeLayerPtr layers,
                                                   TrapezeLPCSTR
                                                   outputFileName);

/* GetTrapezeTIFFAnnotations
   Retrieves annotations from a TIFF. Annotation types that can be retrieved
   are: Onstream Trapeze, Watermark, Kodak/Wang Imaging. tiff specifies the
   TIFF to retrieve the annotations from. ifdNo specifies the one based IFD
   number within the TIFF to retrieve the annotations from. The annotations
   are returned in annotations and their size is returned in annotationsSize.
   The return value is TRUE on success or FALSE on failure. On success,
   annotations is set to NULL if there are no annotations. On failure, use
   GetLastError() to get the cause of the failure. The returned annotations
   must be freed using DestroyTrapezeAnnotations() when they are no longer
   required. */
extern TrapezeBOOL TrapezeAPI GetTrapezeTIFFAnnotations(TrapezeTIFF tiff,
                                                        TrapezeDWORD ifdNo,
                                                        TrapezeAnnotationsPtr
                                                        annotations,
                                                        TrapezeLPDWORD
                                                        annotationsSize);

/* GetTrapezeTIFFColorType
   Gets the color format of an image in an open TIFF. tiff specifies the TIFF
   containing the image. ifdNo specifies the one-based IFD number of the
   image. The return value is one of the *TrapezeColorType values, or
   errorTrapezeColorType on failure. On failure, use GetLastError() to get the
   cause of the failure. */
extern TrapezeDWORD TrapezeAPI GetTrapezeTIFFColorType(TrapezeTIFF tiff,
                                                       TrapezeDWORD ifdNo);

/* GetTrapezeTIFFCompression
   Gets the compression of an image in an open TIFF. tiff specifies the TIFF
   containing the image. ifdNo specifies the one-based IFD number of the
   image. The compression is returned in compression - it must be one of the
   *TrapezeCompression values. The return value is TRUE on success, or FALSE
   on failure. On failure, use GetLastError() to get the cause of the
   failure. */
extern TrapezeBOOL TrapezeAPI GetTrapezeTIFFCompression(TrapezeTIFF tiff,
                                                        TrapezeDWORD ifdNo,
                                                        TrapezeLPWORD
                                                        compression);

/* GetTrapezeTIFFDateTime
   Gets the date/time from an image in an open TIFF. tiff specifies the TIFF
   containing the image. ifdNo specifies the one-based IFD number of the
   image. If the date/time in the image is valid, it is returned in dateTime
   and invalidDateTime is set to NULL. If the date/time in the image is
   invalid, all fields in dateTime are set to zero and invalidDateTime is set
   to a handle containing the invalid date/time string (LPCSTR) from the
   image. This handle must be freed using GlobalFree() when it is no longer
   required. If the image doesn't have a date/time, the function will fail
   with the error ERROR_NOT_FOUND. The return value is TRUE on success, or
   FALSE on failure. On failure, use GetLastError() to get the cause of the
   failure. invalidDateTime will be set to NULL if the function fails. */
extern TrapezeBOOL TrapezeAPI GetTrapezeTIFFDateTime(TrapezeTIFF tiff,
                                                     TrapezeDWORD ifdNo,
                                                     TrapezeTIFFDateTimePtr
                                                     dateTime,
                                                     TrapezeHGLOBAL*
                                                     invalidDateTime);

/* GetTrapezeTIFFIFDSize
   Gets the approximate size of all data used by an image in an open TIFF.
   tiff specifies the TIFF containing the image. ifdNo specifies the one-based
   IFD number of the image. The return value is the approximate size of the
   image data on success, or getTrapezeTIFFIFDSizeFailed on failure. On
   failure, use GetLastError() to get the cause of the failure. */
extern TrapezeSIZE_T TrapezeAPI GetTrapezeTIFFIFDSize(TrapezeTIFF tiff,
                                                      TrapezeDWORD ifdNo);

/* GetTrapezeTIFFOCRText
   Retrieves OCR text from a TIFF. tiff specifies the TIFF to retrieve the OCR
   text from. ifdNo specifies the one based IFD number within the TIFF to
   retrieve the OCR text from. The OCR text is returned in ocrText and its
   size is returned in ocrTextSize. The return value is TRUE on success or
   FALSE on failure. On success, ocrText is set to NULL if there is no OCR
   text. On failure, use GetLastError() to get the cause of the failure. The
   returned OCR text must be freed using DestroyTrapezeOCRText() when it is
   no longer required. */
extern TrapezeBOOL TrapezeAPI GetTrapezeTIFFOCRText(TrapezeTIFF tiff,
                                                    TrapezeDWORD ifdNo,
                                                    TrapezeOCRTextPtr ocrText,
                                                    TrapezeLPDWORD
                                                    ocrTextSize);

/* GetTrapezeTIFFOCRTextEx
   Retrieves OCR text from a TIFF, according to the information in
   ocrTextInfo. The return value is TRUE on success or FALSE on failure. On
   failure, use GetLastError() to get the cause of the failure. */
extern TrapezeBOOL TrapezeAPI GetTrapezeTIFFOCRTextEx(
                                                    GetTrapezeTIFFOCRTextExPtr
                                                      ocrTextInfo);

/* GetTrapezeTIFFPhotoInt
   Gets the photometric interpretation from an image in an open TIFF. tiff
   specifies the TIFF containing the image. ifdNo specifies the one-based IFD
   number of the image. The return value is one of the *TrapezeTIFFPhotoInt
   values; errorTrapezeTIFFPhotoInt indicates failure. On failure, use
   GetLastError() to get the cause of the failure. */
extern TrapezeDWORD TrapezeAPI GetTrapezeTIFFPhotoInt(TrapezeTIFF tiff,
                                                      TrapezeDWORD ifdNo);

/* GetTrapezeTIFFResolution
   Gets the resolution from an image in an open TIFF. tiff specifies the TIFF
   containing the image. ifdNo specifies the one-based IFD number of the
   image. The resolution is returned in resolution. The return value is TRUE
   on success, or FALSE on failure. On failure, use GetLastError() to get the
   cause of the failure. If the image has no resolution an invalid resolution
   of x/0 will be returned. */
extern TrapezeBOOL TrapezeAPI GetTrapezeTIFFResolution(TrapezeTIFF tiff,
                                                       TrapezeDWORD ifdNo,
                                                 TrapezeTIFFResolutionScalePtr
                                                       resolution);

/* GetTrapezeTIFFScale
   Gets the scale from an image in an open TIFF. tiff specifies the TIFF
   containing the image. ifdNo specifies the one-based IFD number of the
   image. The scale is returned in scale. The return value is TRUE on success,
   or FALSE on failure. On failure, use GetLastError() to get the cause of the
   failure. If the image has no scale an invalid scale of x/0 will be
   returned. */
extern TrapezeBOOL TrapezeAPI GetTrapezeTIFFScale(TrapezeTIFF tiff,
                                                  TrapezeDWORD ifdNo,
                                                 TrapezeTIFFResolutionScalePtr
                                                  scale);

/* GetTrapezeTIFFSize
   Gets the size (width and height) of an image in an open TIFF, in pixels.
   tiff specifies the TIFF containing the image. ifdNo specifies the one-based
   IFD number of the image. The width and height of the image, in pixels, are
   returned in width and height, respectively. The return value is TRUE on
   success, or FALSE on failure. On failure, use GetLastError() to get the
   cause of the failure. */
extern TrapezeBOOL TrapezeAPI GetTrapezeTIFFSize(TrapezeTIFF tiff,
                                                 TrapezeDWORD ifdNo,
                                                 TrapezeLPDWORD width,
                                                 TrapezeLPDWORD height);

extern TrapezeBOOL TrapezeAPI IsTrapezeTIFFTiled(TrapezeTIFF tiff,
                                                 TrapezeDWORD ifdNo,
                                                 TrapezeLPBOOL tiled);

/* MultiplyTrapezeTIFFResolutionScale
   Muliplies both values (horizontal and vertical) of a resolution or scale by
   the value numerator/denominator (numerator divided by denominator).
   resScale specifies the resolution or scale to be multiplied. numerator and
   denominator specify the multiplier. The return value is TRUE on success, or
   FALSE on failure. On failure, use GetLastError() to get the cause of the
   failure. resScale->unit is ignored (and not checked). */
extern TrapezeBOOL TrapezeAPI
MultiplyTrapezeTIFFResolutionScale(TrapezeTIFFResolutionScalePtr resScale,
                                   TrapezeDWORD numerator,
                                   TrapezeDWORD denominator);

/* OCRTrapezeTIFF
   OCRs a TIFF, periodically calling a progress function. fileName specifies
   the TIFF to OCR. If replace is TRUE, any existing OCR text in the TIFF is
   replaced, otherwise only pages without OCR text are OCRed. If progressProc
   is not NULL, it is called periodically with lParam passed to it; if it
   returns FALSE the function fails with the error ERROR_CANCELLED. The return
   value is TRUE on success or FALSE on failure. On failure, use
   GetLastError() to get the cause of the error. */
extern TrapezeBOOL TrapezeAPI OCRTrapezeTIFF(TrapezeLPCSTR fileName,
                                             TrapezeBOOL replace,
                                             TrapezeProgressProc progressProc,
                                             TrapezeLPVOID lParam);

/* OCRTrapezeTIFFEx
   OCRs a TIFF, when non-default features are required. ocrInfo->size must be
   set to the size of the ocrInfo structure. ocrInfo->fileName specifies the
   TIFF to OCR. If ocrInfo->replace is TRUE, any existing OCR text in the TIFF
   is replaced, otherwise only pages without OCR text are OCRed. If
   ocrInfo->progressProc is not NULL, it is called periodically with
   ocrInfo->lParam passed to it; if it returns FALSE the function fails with
   the error ERROR_CANCELLED. If ocrInfo->errorProc is NULL the function fails
   if a page cannot be OCRed, otherwise ocrInfo->errorProc is called with
   ocrInfo->lParam passed to it; if ocrInfo->errorProc returns TRUE the error
   is ignored, otherwise OCRTrapezeTIFFEx() wil fail. The return value is TRUE
   on success or FALSE on failure. On failure, use GetLastError() to get the
   cause of the error. */
extern TrapezeBOOL TrapezeAPI OCRTrapezeTIFFEx(OCRTrapezeTIFFExPtr ocrInfo);

/* OpenTrapezeRapidRedactFile
   The same as OpenTrapezeTIFF(), but opens a RapidRedact file instead. The
   return value can be used anywhere the return value from OpenTrapezeTIFF()
   can be used. */
extern TrapezeTIFF TrapezeAPI OpenTrapezeRapidRedactFile(TrapezeLPCSTR
                                                         fileName,
                                                         TrapezeDWORD access,
                                                         TrapezeBOOL create);

/* TrapezeTIFF TrapezeAPI OpenTrapezeTIFFT(TrapezeLPCTSTR fileName,
                                           TrapezeDWORD access,
                                           TrapezeBOOL create) */
#ifdef UNICODE
#define OpenTrapezeTIFFT(fileName, access, create) \
  OpenTrapezeTIFFW(fileName, access, create)
#else
#define OpenTrapezeTIFFT(fileName, access, create) \
  OpenTrapezeTIFF(fileName, access, create)
#endif /*UNICODE*/

/* OpenTrapezeTIFF
   Opens a TIFF. fileName specifies the file containing the TIFF. If fileName
   is NULL a new in-memory TIFF will be opened. access specifies the required
   file access (read or read/write). If access is read/write and create is
   TRUE a new TIFF will be created in the file, otherwise the file must
   already contain a TIFF. The return value is non-NULL on success or NULL on
   failure. On failure, use GetLastError() to get the cause of the failure. */
extern TrapezeTIFF TrapezeAPI OpenTrapezeTIFF(TrapezeLPCSTR fileName,
                                              TrapezeDWORD access,
                                              TrapezeBOOL create);

/* OpenTrapezeTIFFW
   Opens a TIFF (Unicode version). See OpenTrapezeTIFF() for details. */
extern TrapezeTIFF TrapezeAPI OpenTrapezeTIFFW(TrapezeLPCWSTR fileName,
                                               TrapezeDWORD access,
                                               TrapezeBOOL create);

/* ReadTrapezeOpenTIFFASCIITag
   Reads an ASCII (string) value from a TIFF tag. tiff specifies the open TIFF
   to read the tag value from. ifdNo specifies the one-based IFD number to
   read the tag value from. tagNo specifies the tag number to read; the
   *TrapezeTIFFASCIITag values define standard ASCII tags. The tag value is
   returned in ascii, if ascii is not NULL. If ascii is not NULL, size
   specifies the size of ascii, in characters. The return value is the length
   of the ASCII text, in characters, not including the NULL terminator, or
   readTrapezeOpenTIFFASCIITagFailed on failure. On failure, use
   GetLastError() to get the cause of the error. On success, the returned
   length is the entire length of the tag value, which may be larger than what
   was actually returned in ascii. If the requested tag isn't found, the
   function fails with the error ERROR_FILE_NOT_FOUND. If the requested tag
   doesn't contain an ASCII value, the function fails with the error
   ERROR_INVALID_DATATYPE. */
extern TrapezeDWORD TrapezeAPI ReadTrapezeOpenTIFFASCIITag(TrapezeTIFF tiff,
                                                           TrapezeDWORD ifdNo,
                                                           TrapezeWORD tagNo,
                                                           TrapezeLPSTR ascii,
                                                           TrapezeDWORD size);

/* ReadTrapezeOpenTIFFDIB
   Reads a DIB from an open TIFF. tiff specifies the open TIFF to read the DIB
   from. ifdNo specifies the one-based IFD number to read the image from. The
   return value is a GlobalAlloc() handle on success, or NULL on failure. On
   failure, use GetLastError() to get the cause of the failure. The returned
   handle must be freed using GlobalFree() when it is no longer required. */
extern TrapezeHGLOBAL TrapezeAPI ReadTrapezeOpenTIFFDIB(TrapezeTIFF tiff,
                                                        TrapezeDWORD ifdNo);

/* ReadTrapezeOpenTIFFDIBEx
   Reads a DIB from an open TIFF, according to information in readInfo. The
   return value is a GlobalAlloc() handle on success, or NULL on failure. On
   failure, use GetLastError() to get the cause of the failure. The returned
   handle must be freed using GlobalFree() when it is no longer required. */
extern TrapezeHGLOBAL TrapezeAPI ReadTrapezeOpenTIFFDIBEx(
                                                  ReadTrapezeOpenTIFFDIBExCPtr
                                                          readInfo);

/* ReadTrapezeOpenTIFFDIBProgress
   Reads a DIB from an open TIFF, periodically calling a progress function.
   tiff specifies the open TIFF to read the DIB from. ifdNo specifies the
   one-based IFD number to read the image from. If progressProc is not NULL,
   it is called periodically with lParam passed to it; if it returns FALSE the
   function fails with the error ERROR_CANCELLED. The return value is a
   GlobalAlloc() handle on success, or NULL on failure. On failure, use
   GetLastError() to get the cause of the failure. The returned handle must be
   freed using GlobalFree() when it is no longer required. */
extern TrapezeHGLOBAL TrapezeAPI
ReadTrapezeOpenTIFFDIBProgress(TrapezeTIFF tiff, TrapezeDWORD ifdNo,
                               TrapezeProgressProc progressProc,
                               TrapezeLPVOID lParam);

/* ReadTrapezeTIFFDIB
   Reads a DIB from a TIFF. fileName specifies the file containing the TIFF.
   ifdNo specifies the one-based IFD number to read the image from. The return
   value is a GlobalAlloc() handle on success, or NULL on failure. On failure,
   use GetLastError() to get the cause of the failure. The returned handle
   must be freed using GlobalFree() when it is no longer required. */
extern TrapezeHGLOBAL TrapezeAPI ReadTrapezeTIFFDIB(TrapezeLPCSTR fileName,
                                                    TrapezeDWORD ifdNo);

/* SaveTrapezeBookmarks
   Saves bookmarks into a TIFF. bookmarks is a value returned by
   CreateTrapezeBookmarks(), GetTrapezeTIFFDocumentBookmarks() or
   LoadTrapezeBookmarks(). fileName specifies the TIFF to write the bookmarks
   into. The return value is TRUE on success or FALSE on failure. On failure,
   use GetLastError() to get the cause of the error. */
extern TrapezeBOOL TrapezeAPI SaveTrapezeBookmarks(TrapezeBookmarks bookmarks,
                                                   TrapezeLPCSTR fileName);

/* SaveTrapezeTIFFDIB
   Saves dib to a single page TIFF in fileName, using the specified
   compression. compression is one of *TrapezeCompression. The return value is
   TRUE on success or FALSE on failure. On failure, use GetLastError() to get
   the cause of the error. */
extern TrapezeBOOL TrapezeAPI SaveTrapezeTIFFDIB(TrapezeLPCSTR fileName,
                                                 TrapezeHGLOBAL dib,
                                                 TrapezeWORD compression);

/* SetMemoryTrapezeTIFFSize
   Sets the buffer size and/or grow size of an in-memory TIFF that was created
   by calling OpenTrapezeTIFF() with fileName set to NULL. tiff specifies the
   in-memory TIFF to set sizes for. size specifies the new buffer size for
   tiff, in bytes. The size of the buffer will not reduce, so the size will
   remain unchanged if size is less than the current buffer size. This means
   that setting size to zero will have no effect. This doesn't affect the size
   of tiff, just the size of the buffer allocated for tiff to grow. grow
   specifies the amount that the buffer is grown by when it needs to grow, in
   bytes. If grow is:
   - zero, the buffer will only ever grow as large as it needs to
   - setMemoryTrapezeTIFFAutoGrow, the grow size will automatically adjust as
     tiff gets larger
   - setMemoryTrapezeTIFFNoChange, this function will not change the grow size
   - any other value, the buffer will grow by at least grow bytes whenever it
     needs to get bigger
   The default grow size is zero. The return value is the current size of
   tiff, in bytes, or setMemoryTrapezeTIFFSizeFailed on failure. On failure,
   use GetLastError() to get the cause of the failure.
   To get the current size of an in-memory TIFF, call
   SetMemoryTrapezeTIFFSize(tiff, 0, setMemoryTrapezeTIFFNoChange)
   */
extern TrapezeSIZE_T TrapezeAPI SetMemoryTrapezeTIFFSize(TrapezeTIFF tiff,
                                                         TrapezeSIZE_T size,
                                                         TrapezeSIZE_T grow);

/* SetTrapezeTIFFResolutionScale
   Sets a resolution or scale value. resScale specifies the resolution or
   scale to be set. xNumerator and xDenominator specify the numerator and
   denominator for the horizontal resolution or scale. yNumerator and
   yDenominator specify the numerator and denominator for the vertical
   resolution or scale. unit specifies the units; it must be one of
   *TrapezeTIFFResolutionScaleUnit. The return value is TRUE on success or
   FALSE on failure. On failure, use GetLastError() to get the cause of the
   failure. */
extern TrapezeBOOL TrapezeAPI
SetTrapezeTIFFResolutionScale(TrapezeTIFFResolutionScalePtr resScale,
                              TrapezeDWORD xNumerator,
                              TrapezeDWORD xDenominator,
                              TrapezeDWORD yNumerator,
                              TrapezeDWORD yDenominator,
                              TrapezeWORD unit);

/* TIFF Document Functions
   ======================= */

/* CloseTrapezeTIFFDocument
   Closes a TIFF document when it is no longer required. tiffDoc specifies the
   TIFF document to close; it must be a value returned by
   OpenTrapezeTIFFDocument() or OpenTrapezeTIFFDocumentTIFF(). tiffDoc and any
   TIFFs returned by GetTrapezeTIFFDocumentTIFF() must not be used after this
   function is called. The return value is TRUE on success or FALSE on
   failure. On failure, use GetLastError() to get the cause of the failure. */
extern TrapezeBOOL TrapezeAPI CloseTrapezeTIFFDocument(TrapezeTIFFDocument
                                                       tiffDoc);

/* CopyTrapezeTIFFDocumentPage
   Copies all images for a page from a TIFF document into an open TIFF.
   tiffDoc specifies the TIFF document to copy from. pageNo specifies the page
   (one based) within the TIFF document to be copied. If changePageNo is TRUE
   the page number of the images will be changed during the copy, otherwise it
   will be left as is. If changePageNo is TRUE:
   newPageNo specifies the new one based page number, and newPageCount
   specifies the new page count for the open TIFF. Setting newPageCount to
   zero indicates that the page count is unknown, otherwise if
   newPageNo > newPageCount the page number will be removed during the copy.
   The page count should be the same for every page.
   tiff specifies the open TIFF the page is to be appended to. The return
   value is TRUE on success or FALSE on failure. On failure, use
   GetLastError() to get the cause of the error. */
extern TrapezeBOOL TrapezeAPI CopyTrapezeTIFFDocumentPage(TrapezeTIFFDocument
                                                          tiffDoc,
                                                          TrapezeDWORD pageNo,
                                                          TrapezeBOOL
                                                          changePageNo,
                                                          TrapezeDWORD
                                                          newPageNo,
                                                          TrapezeDWORD
                                                          newPageCount,
                                                          TrapezeTIFF tiff);

/* GetTrapezeTIFFDocumentBookmarks
   Gets bookmarks from a TIFF document. tiffDoc specifies the TIFF document to
   retrieve the bookmarks from. The return value is NULL if there are no
   bookmarks or the function failed, otherwise it is a non-NULL value for the
   bookmarks, which must be destroyed using DestroyTrapezeBookmarks() when it
   is no longer required. If the return value is NULL, use GetLastError() to
   get the cause of the error. If GetLastError() returns ERROR_SUCCESS then
   tiffDoc has no bookmarks. */
extern TrapezeBookmarks TrapezeAPI
GetTrapezeTIFFDocumentBookmarks(TrapezeTIFFDocument tiffDoc);

/* GetTrapezeTIFFDocumentIFDCount
   Gets the number of IFDs in a TIFF document. tiffDoc specifies the TIFF
   document to retrieve the number of IFDs from. The return value is
   getTrapezeTIFFDocumentIFDCountFailed if an error occurred. On failure, use
   GetLastError() to get the cause of the error. */
extern TrapezeDWORD TrapezeAPI GetTrapezeTIFFDocumentIFDCount(
                                                           TrapezeTIFFDocument
                                                              tiffDoc);

/* GetTrapezeTIFFDocumentIFDInfo
   Gets information about an IFD in a TIFF document. tiffDoc specifies the
   TIFF document to retrieve the information from. ifdNo specifies the one
   based IFD number within the TIFF to retrieve the information for. The IFD
   information is returned in ifdInfo. ifdInfo->size must be set to the size
   of ifdInfo before calling this function. The return value is TRUE on
   success or FALSE on failure. On failure, use GetLastError() to get the
   cause of the failure. */
extern TrapezeBOOL TrapezeAPI GetTrapezeTIFFDocumentIFDInfo(
                                                           TrapezeTIFFDocument
                                                           tiffDoc,
                                                           TrapezeDWORD ifdNo,
                                                 TrapezeTIFFDocumentIFDInfoPtr
                                                 ifdInfo);

extern TrapezeBOOL TrapezeAPI GetTrapezeTIFFDocumentIFDTIFF(
                                                           TrapezeTIFFDocument
                                                            tiffDoc,
                                                            TrapezeDWORD
                                                            ifdNo,
                                                            TrapezeHGLOBAL*
                                                            tiff,
                                                            TrapezeLPSIZE_T
                                                            tiffSize);

/* GetTrapezeTIFFDocumentPageCount
   Returns the number of pages in a TIFF document, or
   getTrapezeTIFFDocumentPageCountFailed on failure. tiffDoc specifies the
   TIFF document to retrieve the page count from. On failure, use
   GetLastError() to get the cause of the failure. */
extern TrapezeDWORD TrapezeAPI GetTrapezeTIFFDocumentPageCount(
                                                           TrapezeTIFFDocument
                                                               tiffDoc);

/* GetTrapezeTIFFDocumentPageNumbersOK
   Returns TRUE if the page numbers in a TIFF document are OK, or FALSE on
   error or if the page numbering in the TIFF is incorrect. The last error
   value is not updated on success, so to determine success or failure for a
   FALSE return value, call SetLastError(ERROR_SUCCESS) before calling
   GetTrapezeTIFFDocumentPageNumbersOK(), then call GetLastError() to get the
   cause of the error. If GetLastError() returns ERROR_SUCCESS then the page
   numbers are incorrect, otherwise an error occurred. */
extern TrapezeBOOL TrapezeAPI GetTrapezeTIFFDocumentPageNumbersOK(
                                                           TrapezeTIFFDocument
                                                                  tiffDoc);

/* GetTrapezeTIFFDocumentTIFF
   Returns the TIFF associated with a TIFF document for use in TIFF functions
   (such as GetTrapezeTIFFResolution(), for example). tiffDoc specifies the
   TIFF document to retrieve the TIFF from. The return value is a TIFF on
   success, or NULL on failure. On failure, use GetLastError() to get the
   cause of the failure. The returned TIFF is valid until the TIFF document is
   closed with CloseTrapezeTIFFDocument(). It must not be freed using
   CloseTrapezeTIFF(). */
extern TrapezeTIFF TrapezeAPI GetTrapezeTIFFDocumentTIFF(TrapezeTIFFDocument
                                                         tiffDoc);

/* OpenTrapezeTIFFDocument
   Opens a TIFF document from a file. fileName specifies the file to open the
   TIFF from. The return value is non-NULL on success or NULL on failure. On
   failure, use GetLastError() to get the cause of the failure. */
extern TrapezeTIFFDocument TrapezeAPI OpenTrapezeTIFFDocument(TrapezeLPCSTR
                                                              fileName);

/* OpenTrapezeTIFFDocumentEx
   Opens a TIFF document, according to information in openInfo. The return
   value is non-NULL on success or NULL on failure. On failure, use
   GetLastError() to get the cause of the failure. */
extern TrapezeTIFFDocument TrapezeAPI OpenTrapezeTIFFDocumentEx(
                                                 OpenTrapezeTIFFDocumentExCPtr
                                                                openInfo);

/* OpenTrapezeTIFFDocumentTIFF
   Opens a TIFF document from an open TIFF. tiff is the open TIFF to open a
   TIFF document from. If closeTIFF is TRUE, tiff will be closed when the
   returned TIFF document is closed. The return value is the TIFF document
   on success, or NULL on failure. On failure, use GetLastError() to get the
   cause of the failure. */
extern TrapezeTIFFDocument TrapezeAPI OpenTrapezeTIFFDocumentTIFF(TrapezeTIFF
                                                                  tiff,
                                                                  TrapezeBOOL
                                                                  closeTIFF);

#ifdef _WIN32
/* Trapeze File Functions
   ====================== */

extern TrapezeBOOL TrapezeAPI AppendTrapezeOpenFileTIFF(TrapezeFile file,
                                                        TrapezeLPCSTR
                                                        fileName);

extern TrapezeBOOL TrapezeAPI AppendTrapezeOpenFileTIFFIFD(TrapezeFile file,
                                                           TrapezeLPCSTR
                                                           fileName,
                                                           TrapezeDWORD ifdNo,
                                                           TrapezeLPCSTR
                                                           reference,
                                                           TrapezeDWORD
                                                           refIFDNo,
                                                           TrapezeDWORD
                                                           pageNo);

extern TrapezeBOOL TrapezeAPI AppendTrapezeOpenFileTIFFIFDEx(TrapezeFile file,
                                                             TrapezeDWORD
                                                             imageWidth,
                                                             TrapezeDWORD
                                                             imageHeight,
                                                             TrapezeDWORD
                                                             newSubfileType,
                                                             TrapezeDWORD
                                                             photoInt,
                                                             TrapezeDWORD
                                                             ifdNo,
                                                             TrapezeLPCSTR
                                                             reference,
                                                             TrapezeDWORD
                                                             pageNo);

extern TrapezeBOOL TrapezeAPI CloseMemoryTrapezeFile(TrapezeFile file,
                                                     TrapezeHGLOBAL* memory,
                                                     TrapezeLPSIZE_T
                                                     memorySize);

extern TrapezeBOOL TrapezeAPI CloseTrapezeFile(TrapezeFile file);

extern TrapezeFile TrapezeAPI CreateTrapezeFile(TrapezeLPCSTR fileName,
                                                TrapezeBOOL overwrite);

extern TrapezeBOOL TrapezeAPI SaveTrapezeFileBookmarks(TrapezeFile file,
                                                       TrapezeBookmarks
                                                       bookmarks);
#endif /*_WIN32*/

#ifdef _WIN32
/* Trapeze Window Functions
   ======================== */

/* CreateTrapezePluginWindow
   Creates a Trapeze plugin window. style is the window style, which must
   contain WS_CHILD. x, y, width and height specify the position of the window
   within its parent. id specifies the window ID. The return value is the
   handle of the window on success, or NULL on failure. On failure, use
   GetLastError() to get the cause of the failure. */
/* HWND CreateTrapezePluginWindow(DWORD style, int x, int y, int width,
                                  int height, HWND parent, UINT id) */
#define CreateTrapezePluginWindow(style, x, y, width, height, parent, id) \
  CreateTrapezeWindow((style) | pluginTrapezeWindowStyle, (x), (y), (width), \
                      (height), (parent), (id))

/* CreateTrapezeWindow
   Creates a Trapeze window. style is the window style, which must contain
   WS_CHILD. x, y, width and height specify the position of the window
   within its parent. id specifies the window ID. The return value is the
   handle of the window on success, or NULL on failure. On failure, use
   GetLastError() to get the cause of the failure. */
extern HWND WINAPI CreateTrapezeWindow(DWORD style, int x, int y, int width,
                                       int height, HWND parent, UINT id);

/* HWND WINAPI CreateTrapezeWindowExT(DWORD exStyle, LPCTSTR caption,
                                      DWORD style, int x, int y, int width,
                                      int height, HWND parent, UINT id,
                                      HWND statusBar) */
#ifdef UNICODE
#define CreateTrapezeWindowExT(exStyle, caption, style, x, y, width, height, \
                               parent, id, statusBar) \
  CreateTrapezeWindowExW(exStyle, caption, style, x, y, width, height, \
                         parent, id, statusBar)
#else
#define CreateTrapezeWindowExT(exStyle, caption, style, x, y, width, height, \
                               parent, id, statusBar) \
  CreateTrapezeWindowEx(exStyle, caption, style, x, y, width, height, \
                        parent, id, statusBar)
#endif /*UNICODE*/

/* CreateTrapezeWindowEx
   Creates a Trapeze window. exStyle is the extended window style. caption is
   the window caption, or NULL for the default caption. style is the window
   style, which must contain WS_CHILD. x, y, width and height specify the
   position of the window within its parent. id specifies the window ID.
   statusBar is a Trapeze Status Bar, or NULL for the new Trapeze window to
   create and manage its own internal status bar. The return value is the
   handle of the window on success, or NULL on failure. On failure, use
   GetLastError() to get the cause of the failure. */
extern HWND WINAPI CreateTrapezeWindowEx(DWORD exStyle, LPCSTR caption,
                                         DWORD style, int x, int y, int width,
                                         int height, HWND parent, UINT id,
                                         HWND statusBar);

/* CreateTrapezeWindowExW
   Creates a Trapeze window (Unicode version). See CreateTrapezeWindowEx() for
   details. This function automatically adds unicodeTrapezeWindowStyle to
   style. */
extern HWND WINAPI CreateTrapezeWindowExW(DWORD exStyle, LPCWSTR caption,
                                          DWORD style, int x, int y,
                                          int width, int height, HWND parent,
                                          UINT id, HWND statusBar);

/* DestroyTrapezeWindow
   Destroys a Trapeze window. This is the only way to destroy a Trapeze window
   created with the newThreadTrapezeWindowStyle style; DestroyWindow() can't
   be used. The return value is TRUE on success, or FALSE on failure. On
   failure, use GetLastError() to get the cause of the failure. */
extern BOOL WINAPI DestroyTrapezeWindow(HWND window);

/* BOOL WINAPI SetTrapezeWindowPrefsRootT(LPCTSTR prefsRoot) */
#ifdef UNICODE
#define SetTrapezeWindowPrefsRootT(prefsRoot) \
  SetTrapezeWindowPrefsRootW(prefsRoot)
#else
#define SetTrapezeWindowPrefsRootT(prefsRoot) \
  SetTrapezeWindowPrefsRoot(prefsRoot)
#endif /*UNICODE*/

/* SetTrapezeWindowPrefsRoot
   Sets the registry key under HKEY_CURRENT_USER used to store preferences for
   a plugin window. If prefsRoot is NULL or an empty string the registry key
   is reset to the default. The return value is TRUE on success, or FALSE on
   failure. On failure, use GetLastError() to get the cause of the failure.
   Note: A Trapeze window will use the value that was set when it was created.
     Calling this function will not affect any existing Trapeze windows. */
extern BOOL WINAPI SetTrapezeWindowPrefsRoot(LPCSTR prefsRoot);

/* SetTrapezeWindowPrefsRootW
   Sets the registry key under HKEY_CURRENT_USER used to store preferences for
   a plugin window (Unicode version). See SetTrapezeWindowPrefsRoot() for
   details. */
extern BOOL WINAPI SetTrapezeWindowPrefsRootW(LPCWSTR prefsRoot);

#endif /*_WIN32*/

#endif /*TRAPEZEDYNAMIC*/

#ifdef _WIN32
#if ! defined(_WIN32_WINNT) || (_WIN32_WINNT < 0x0400) || \
    ! defined(__WINCRYPT_H__)
#undef PCCERT_CONTEXT
#endif /*! defined(_WIN32_WINNT) || (_WIN32_WINNT < 0x0400) ||
         ! defined(__WINCRYPT_H__)*/
#endif /*_WIN32*/

/* Restore structure alignment */
#ifdef _MSC_VER
#pragma pack(pop)
#elif __GNUC__
#ifdef __APPLE__
/* GCC on Mac OS X */
#pragma pack()
#else
/* GCC on anything except Mac OS X */
#pragma pack(pop)
#endif /*__APPLE__*/
#elif __IBMC__
/* IBM Compiler on OS/390 */
#pragma pack(reset)
#else
#error Unknown compiler
#endif

#ifdef __cplusplus
} /*extern "C"*/
#endif /*__cplusplus*/

#endif /*! _TRAPEZE_H_*/

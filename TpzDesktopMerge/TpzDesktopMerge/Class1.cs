﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TpzDesktopMerge
{
    class Class1
    {

/* PDF Functions
   ============= */


/* TrapezeBOOL TrapezeAPI AppendTrapezePDFPageT(TrapezePDF pdf,
                                                TrapezeLPCTSTR fileName,
                                                TrapezeDWORD pageNo) */
#ifdef UNICODE
#define AppendTrapezePDFPageT(pdf, fileName, pageNo) \
  AppendTrapezePDFPageW(pdf, fileName, pageNo)
#else
#define AppendTrapezePDFPageT(pdf, fileName, pageNo) \
  AppendTrapezePDFPage(pdf, fileName, pageNo)
#endif /*UNICODE*/

/* AppendTrapezePDFPage
   Appends a page to a PDF created using CreateTrapezePDF(). fileName
   specifies the file to read the page from. pageNo specifies the page number
   (one based) of the document to read the page from. The return value is TRUE
   on success, or NULL on failure. On failure, use GetLastError() to get the
   cause of the error.
   SaveTrapezePDF() must be called before closing pdf to complete the
   changes. */
extern TrapezeBOOL TrapezeAPI AppendTrapezePDFPage(TrapezePDF pdf,
                                                   TrapezeLPCSTR fileName,
                                                   TrapezeDWORD pageNo);

/* AppendTrapezePDFPageW
   Appends a page to a PDF created using CreateTrapezePDF() (Unicode version).
   See AppendTrapezePDFPage() for details. */
extern TrapezeBOOL TrapezeAPI AppendTrapezePDFPageW(TrapezePDF pdf,
                                                    TrapezeLPCWSTR fileName,
                                                    TrapezeDWORD pageNo);

/* CloseTrapezePDF
   Closes a PDF when it is no longer required. pdf specifies the PDF to close.
   pdf must not be used after this function is called. The return value is
   TRUE on success or FALSE on failure. On failure, use GetLastError() to get
   the cause of the failure.
   Any changes since pdf was last saved will be lost. */
extern TrapezeBOOL TrapezeAPI CloseTrapezePDF(TrapezePDF pdf);

/* CompactTrapezePDF
   Compacts a PDF according to the information in compactInfo. The return
   value is TRUE on success or FALSE on failure. On failure, use
   GetLastError() to get the cause of the failure. */
extern TrapezeBOOL TrapezeAPI CompactTrapezePDF(CompactTrapezePDFCPtr
                                                compactInfo);
/* ConvertTrapezePDFToTIFF
   Converts a PDF in srcFileName to a TIFF in destFileName. pageNo specifies
   the one based page number to convert, or convertTrapezePDFToTIFFAllPages to
   convert all pages in the PDF. The return value is TRUE on success or FALSE
   on failure. On failure, use GetLastError() to get the cause of the failure.
   Use SetTrapezePDFOptions() to control various conversion options. */
extern TrapezeBOOL TrapezeAPI ConvertTrapezePDFToTIFF(TrapezeLPCSTR
                                                      srcFileName,
                                                      TrapezeDWORD pageNo,
                                                      TrapezeLPCSTR
                                                      destFileName);

/* ConvertTrapezePDFToTIFFEx
   Converts a PDF to a TIFF according to the information in convertInfo. The
   return value is TRUE on success or FALSE on failure. On failure, use
   GetLastError() to get the cause of the failure. Use SetTrapezePDFOptions()
   to control default conversion options. Conversion options specified in
   convertInfo override those set with SetTrapezePDFOptions(). */
extern TrapezeBOOL TrapezeAPI ConvertTrapezePDFToTIFFEx(
                                                 ConvertTrapezePDFToTIFFExCPtr
                                                        convertInfo);

/* TrapezePDF TrapezeAPI CreateTrapezePDFT(TrapezeLPCTSTR fileName,
                                           TrapezeLPCTSTR srcFileName) */
#ifdef UNICODE
#define CreateTrapezePDFT(fileName, srcFileName) \
  CreateTrapezePDFW(fileName, srcFileName)
#else
#define CreateTrapezePDFT(fileName, srcFileName) \
  CreateTrapezePDF(fileName, srcFileName)
#endif /*UNICODE*/

/* CreateTrapezePDF
   Creates a PDF for appending pages to. fileName specifies the file for the
   new PDF; any existing file will be replaced. srcFileName specifies NULL to
   create an empty PDF, or a file containing a PDF to create a copy of an
   existing PDF. The return value is non-NULL on success or NULL on failure.
   On failure, use GetLastError() to get the cause of the failure. */
extern TrapezePDF TrapezeAPI CreateTrapezePDF(TrapezeLPCSTR fileName,
                                              TrapezeLPCSTR srcFileName);

/* CreateTrapezePDFW
   Creates a PDF for appending pages to (Unicode version). See
   CreateTrapezePDF() for details. */
extern TrapezePDF TrapezeAPI CreateTrapezePDFW(TrapezeLPCWSTR fileName,
                                               TrapezeLPCWSTR srcFileName);

extern TrapezeHGLOBAL TrapezeAPI GetTrapezePDFDocumentData(TrapezePDF pdf,
                                                           TrapezeLPSIZE_T
                                                           size);

/* GetTrapezePDFPageData
   Gets the unsent data for a PDF page. pdf specifies the open PDF to get the
   page data from, which must be opened using OpenTrapezePDF(). pageNo
   specifies the one-based page number to get the data for. flags is one of:
   - trapezePDFPageAll, to get all unsent data for the page, the PDF must have
     been opened with objectListTrapezePDFOption set
   - trapezePDFPageThumbnail, to get all unsent data for the thumbnail, the
     PDF must have been opened with objectListTrapezePDFOption set
   - trapezePDFPageNoAnnotations, to get all unsent data for the page except
     annotation data, the PDF must have been opened with
     objectListTrapezePDFOption set
   - trapezePDFPageDIB, to get the page as a DIB, using conversion options set
     using SetTrapezePDFOptions(). If trapezePDFPageJPEG is also set in flags
     and the page is a full page JPEG, the returned data will be a JPEG stream
     rather than a DIB; the data must be examined to determine if it is a DIB
     or JPEG. If trapezePDFPageResolution is also set in flags, size is a
     TrapezePDFPageDataResolutionPtr rather than a TrapezeLPSIZE_T, which is
     filled.
   - trapezePDFPageThumbnailDIB, to get the thumbnail as a DIB
   - trapezePDFPageOCRText, to get the OCR text for the page. The last error
     value is not updated on success, so to determine success or failure for a
     NULL return value, call SetLastError(ERROR_SUCCESS) before calling
     GetTrapezePDFPageData(), then call GetLastError() to get the cause of the
     error
   size is set to the size of the data. The return value is a GlobalAlloc()
   handle containing the data on success, or NULL on failure. On failure, use
   GetLastError() to get the cause of the error. The returned handle must be
   freed using GlobalFree() when it is no longer required. */
extern TrapezeHGLOBAL TrapezeAPI GetTrapezePDFPageData(TrapezePDF pdf,
                                                       TrapezeDWORD pageNo,
                                                       TrapezeDWORD flags,
                                                       TrapezeLPSIZE_T size);

/* GetTrapezePDFPageCount
   Returns the number of pages in a PDF document, or
   getTrapezePDFPageCountFailed on failure. pdf specifies the PDF to retrieve
   the page count from. On failure, use GetLastError() to get the cause of the
   failure. */
extern TrapezeDWORD TrapezeAPI GetTrapezePDFPageCount(TrapezePDF pdf);

/* OpenTrapezePDF
   Opens a PDF. fileName specifies the file containing the PDF. access
   specifies the required file access:
   - trapezeReadAccess specifies read-only access
   - trapezeWriteAccess or (trapezeReadAccess | trapezeWriteAccess) specifies
     read/write access
   The return value is non-NULL on success or NULL on failure. On failure, use
   GetLastError() to get the cause of the failure. */
extern TrapezePDF TrapezeAPI OpenTrapezePDF(TrapezeLPCSTR fileName,
                                            TrapezeDWORD access);

/* SaveTrapezePDF
   Saves any changes made to pdf. The return value is TRUE on success or FALSE
   on failure. On failure, use GetLastError() to get the cause of the
   error. This function will fail with ERROR_ACCESS_DENIED if pdf was opened
   with read-only access. */
extern TrapezeBOOL TrapezeAPI SaveTrapezePDF(TrapezePDF pdf);

/* SetTrapezePDFOptions
   Sets options for reading, converting and writing PDFs. pdfOptions must be
   filled appropriately before calling this function. The return value is
   TRUE on success, or FALSE on failure. On failure, use GetLastError() to get
   the cause of the error. */
extern TrapezeBOOL TrapezeAPI SetTrapezePDFOptions(TrapezePDFOptionsCPtr
                                                   pdfOptions);

/* SetTrapezePDFPageAnnotations
   Sets the annotations in a PDF page, replacing any existing annotations for
   that page. pdf specifies the PDF to change. pageNo specifies the one-based
   page number to replace the annotations in. annotations specifies the new
   annotations for the page, or NULL to remove any annotations.
   annotationsSize specifies the size of annotations, in bytes, if annotations
   is not NULL. The return value is TRUE on success, or FALSE on failure. On
   failure, use GetLastError() to get the cause of the error. The added PDF
   data is marked as unsent. */
extern TrapezeBOOL TrapezeAPI SetTrapezePDFPageAnnotations(TrapezePDF pdf,
                                                           TrapezeDWORD
                                                           pageNo,
                                                           TrapezeAnnotations
                                                           annotations,
                                                           TrapezeDWORD
                                                           annotationsSize);

/* SetTrapezePDFPageSent
   Sets data for a PDF page as sent. pdf specifies the open PDF to change the
   data status for, which must be opened using OpenTrapezePDF() with
   objectListTrapezePDFOption set. pageNo specifies the one-based page number
   to change the data status for. flags is one of:
   - trapezePDFPageAll, to mark all data for the page as sent
   - trapezePDFPageThumbnail, to mark all data for the thumbnail as sent
   - trapezePDFPageNoAnnotations, to mark all data for the page except
     annotation data as sent
   The return value is TRUE on success or FALSE on failure. On failure, use
   GetLastError() to get the cause of the error. */
extern TrapezeBOOL TrapezeAPI SetTrapezePDFPageSent(TrapezePDF pdf,
                                                    TrapezeDWORD pageNo,
                                                    TrapezeDWORD flags);

    }
}

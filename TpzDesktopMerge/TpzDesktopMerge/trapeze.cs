/* Onstream Trapeze C# Class
 *
 * Copyright 1997-2012 Onstream Systems Ltd
 */

using System;
using System.Collections.Generic;
//using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Runtime.InteropServices;

class Trapeze {

  /* Properties
   * 
   * uint lastError
   * OpenTIFFDocumentArtModeEnum openTIFFDocumentArtMode
   * bool raiseTrapezeErrors
   * short version
   * string versionString
   * 
   * Methods
   * 
   * bool AnnotationPrefsBlob(Form parent, UInt16 command, ref IntPtr blob,
   *                          ref UInt32 blobSize)
   * bool AppendOpenTIFFEx(IntPtr tiff,
   *                       ref AppendTrapezeOpenTIFFExRec appendInfo)
   * IntPtr BurnInAnnotations(IntPtr annotations, uint annotationsSize,
   *                          IntPtr dibData, IntPtr dib, uint flags)
   * IntPtr BurnInAnnotations(IntPtr annotations, ref uint annotationsSize,
                              IntPtr dibData, IntPtr dib, uint flags)
   * IntPtr BurnInAnnotationsEx(ref BurnInTrapezeAnnotationsExRec burnInInfo)
   * bool CloseTIFF(IntPtr tiff)
   * bool CloseTIFFDocument(IntPtr tiffDoc)
   * bool CloseTrapezeWindowDocument(IntPtr window, bool update)
   * bool ConvertPDFToTIFFEx(ref ConvertTrapezePDFToTIFFExRec convertInfo)
   * bool ConvertTIFFToPDF(String srcFileName, String destFileName)
   * bool ConvertTIFFToPDFEx(ref ConvertTrapezeTIFFToPDFExRec convertInfo)
   * bool CopyTIFFDocumentPage(IntPtr tiffDoc, uint pageNo, bool changePageNo,
   *                           uint newPageNo, uint newPageCount, IntPtr tiff)
   * IntPtr CreatePluginWindow(uint style, int x, int y, int width,
   *                           int height, IntPtr parent, uint id)
   * IntPtr CreateTrapezeWindow(uint style, int x, int y, int width,
   *                            int height, IntPtr parent, uint id)
   * bool DestroyAnnotations(IntPtr annotations)
   * void DestroyBookmarkData(IntPtr bookmarkData)
   * bool DestroyBookmarks(IntPtr bookmarks)
   * void DestroyDIB(IntPtr dib)
   * bool DisableTrapezeWindowTablet(IntPtr window)
   * object GetAnnotationProperty(IntPtr annotations, uint annotationsSize,
   *                              uint annotationNo, String property)
   * Rect GetAnnotationRect(IntPtr annotations, uint annotationsSize,
   *                        uint annotationNo)
   * uint GetAnnotationsCount(IntPtr annotations, uint annotationsSize)
   * IntPtr GetBookmarkData(IntPtr bookmarks, ref uint size)
   * uint GetDIBColorType(IntPtr dibData, IntPtr dib)
   * uint GetDocumentType(String fileName)
   * IntPtr GetTIFFAnnotations(IntPtr tiff, uint ifdNo,
   *                           ref uint annotationsSize)
   * bool GetTIFFCompression(IntPtr tiff, uint ifdNo, ref ushort compression)
   * IntPtr GetTIFFDocumentBookmarks(IntPtr tiffDoc)
   * uint GetTIFFDocumentIFDCount(IntPtr tiffDoc)
   * bool GetTIFFDocumentIFDInfo(IntPtr tiffDoc, uint ifdNo,
   *                             ref TrapezeTIFFDocumentIFDInfoRec ifdInfo)
   * uint GetTIFFDocumentPageCount(IntPtr tiffDoc)
   * IntPtr GetTIFFDocumentTIFF(IntPtr tiffDoc)
   * uint GetTrapezeWindowOCRTextCount(IntPtr window)
   * Rect[] GetTrapezeWindowOCRTextRects(IntPtr window, uint ocrTextNo,
   *                                     uint charNo, uint count)
   * String GetTrapezeWindowOCRTextText(IntPtr window, uint ocrTextNo)
   * bool GetTrapezeWindowProperty(IntPtr window, uint property, ref bool value)
   * bool GetTrapezeWindowProperty(IntPtr window, uint property, ref uint value)
   * bool IsTrapezeWindowNotify(IntPtr window, Message m)
   * bool IsTrapezeWindowDocumentOpen(IntPtr window)
   * IntPtr LockDIB(IntPtr dib, ref IntPtr dibData)
   * void MoveTrapezeWindow(IntPtr window, int x, int y, int width,
   *                        int height)
   * bool NewTrapezeWindowTIFF(IntPtr window, String title)
   * bool NotifyTrapezeWindowCommand(IntPtr window, uint command)
   * bool OCRTIFF(String fileName, bool replace)
   * IntPtr OpenTIFF(String fileName, uint access, bool create)
   * IntPtr OpenTIFFDocument(String fileName)
   * bool OpenTrapezeWindowDocument(IntPtr window, String fileName,
   *                                bool readOnly)
   * void PrintTrapezeWindow(IntPtr window)
   * IntPtr ReadDIB(String fileName, uint pageNo)
   * IntPtr ReadOpenTIFFDIB(IntPtr tiff, uint ifdNo)
   * bool RemoveTrapezeWindowCommand(IntPtr window, uint command)
   * bool SaveTIFFDIB(String fileName, IntPtr dib, ushort compression);
   * bool SetAnnotationProperty(ref IntPtr annotations,
   *                            ref uint annotationsSize, uint annotationNo,
   *                            String property, byte value)
   * bool SetAnnotationProperty(ref IntPtr annotations,
   *                            ref uint annotationsSize, uint annotationNo,
   *                            String property, String value)
   * bool SetAnnotationProperty(ref IntPtr annotations,
   *                            ref uint annotationsSize, uint annotationNo,
   *                            String property, uint value)
   * bool SetDebugLogFile(String fileName)
   * bool SetLicense(String customer, String location, String key)
   * bool SetTrapezeWindowPrefValue(IntPtr window, String pref, bool value)
   * bool SetTrapezeWindowProperty(IntPtr window, uint property, bool value)
   * bool SetTrapezeWindowProperty(IntPtr window, uint property, ushort value)
   * bool SetTrapezeWindowProperty(IntPtr window, uint property, uint value)
   * bool TrapezeWindowCommand(IntPtr window, ushort command, bool force)
   * bool TrapezeWindowImageToScreenRect(IntPtr window, ref Rect rect)
   * bool TrapezeWindowSaveAs(IntPtr window, String fileName)
   * bool TrapezeWindowTRIMViewer(IntPtr window)
   * void UnlockDIB(IntPtr dib)
   * 
   * Classes
   * 
   * ConvertPDFToTIFFClass
   * ConvertTIFFToPDFClass
   */

  /* Constants */

  public const ushort trapezeCopyCommand = 1;
  public const ushort trapezeZoomSelectionCommand = 9;
  public const ushort trapezeSaveCommand = 13;
  public const ushort trapezeHelpTopicsCommand = 14;
  public const ushort trapezeAboutCommand = 53;
  public const ushort trapezeOCRDocumentCommand = 66;
  public const ushort trapezeRectRedactionToolCommand = 82;

  public const uint trapezeReadAccess = 0x80000000;
  public const uint trapezeWriteAccess = 0x40000000;

  public const uint appendTrapezeOpenTIFFExAnnotations = 0x1;
  public const uint appendTrapezeOpenTIFFExBookmarks = 0x2;
  public const uint appendTrapezeOpenTIFFExNewSubfileType = 0x4;
  public const uint appendTrapezeOpenTIFFExPageNumber = 0x8;
  public const uint appendTrapezeOpenTIFFExScale = 0x10;
  public const uint appendTrapezeOpenTIFFExResolution = 0x20;
  public const uint appendTrapezeOpenTIFFExCompression = 0x40;
  public const uint appendTrapezeOpenTIFFExDateTime = 0x80;
  public const uint appendTrapezeOpenTIFFExCompressOptions = 0x100;
  public const uint appendTrapezeOpenTIFFExProgress = 0x200;
  public const uint appendTrapezeOpenTIFFExParamError = 0x400;
  public const uint appendTrapezeOpenTIFFExJPEG6 = 0x800;
  public const uint appendTrapezeOpenTIFFExOCRText = 0x1000;
  public const uint appendTrapezeOpenTIFFExDocName = 0x2000;
  public const uint appendTrapezeOpenTIFFExImageDescription = 0x4000;
  public const uint appendTrapezeOpenTIFFExMake = 0x8000;
  public const uint appendTrapezeOpenTIFFExModel = 0x10000;
  public const uint appendTrapezeOpenTIFFExSoftware = 0x20000;
  public const uint appendTrapezeOpenTIFFExArtist = 0x40000;
  public const uint appendTrapezeOpenTIFFExHostComputer = 0x80000;
  public const uint appendTrapezeOpenTIFFExCopyright = 0x100000;
  public const uint appendTrapezeOpenTIFFExRemoveOrientation = 0x200000;
  public const uint appendTrapezeOpenTIFFExUnicode = 0x400000;
  public const uint appendTrapezeOpenTIFFExOrientation = 0x1000000;

  /* Document types */
  public const uint bmpTrapezeDocument = 0; /* Windows or OS/2 bitmap */
  public const uint gifTrapezeDocument = 1; /* GIF */
  public const uint jpegTrapezeDocument = 2; /* JPEG */
  public const uint ottpTrapezeDocument = 3; /* OTTP URL */
  public const uint pcxTrapezeDocument = 4; /* PCX */
  public const uint pngTrapezeDocument = 5; /* PNG */
  public const uint tiffTrapezeDocument = 6; /* TIFF */
  public const uint trapezeFileTrapezeDocument = 7; /* Trapeze File */
  public const uint unknownTrapezeDocument = 8; /* Unknown */
  public const uint pdfTrapezeDocument = 9; /* PDF */
  public const uint dicomTrapezeDocument = 10; /* DICOM */
  public const uint iconTrapezeDocument = 11; /* Icon */
  public const uint cursorTrapezeDocument = 12; /* Cursor */
  public const uint calsTrapezeDocument = 13; /* CALS */
  public const uint pnmTrapezeDocument = 14; /* PNM (PBM/PGM/PPM) */
  public const uint rapidRedactTrapezeDocument = 15; /* RapidRedact file */
  public const uint jbig2TrapezeDocument = 16; /* JBIG2 */
  public const uint rapidRedactPDFTrapezeDocument = 17; /* RapidRedact PDF */
  public const uint dwgTrapezeDocument = 18; /* DWG */
  public const uint dxfTrapezeDocument = 19; /* DXF */
  public const uint wdpTrapezeDocument = 20; /* Windows Media Photo */
  public const uint hdpTrapezeDocument = 20; /* HD Photo, previously known as
                                                Windows Media Photo */
  public const uint tgaTrapezeDocument = 21; /* Truevision TGA */
  public const uint mdiTrapezeDocument = 22; /* Microsoft Document Imaging */
  public const uint dwfTrapezeDocument = 23; /* DWF */
  public const uint wsqTrapezeDocument = 24; /* WSQ */
  public const uint iHeadTrapezeDocument = 25; /* IHead */
  public const uint dgnTrapezeDocument = 26; /* DGN */
  public const uint errorTrapezeDocument = 0xffffffff; /* An error occurred */
                                                             
  public const uint increaseColorsTrapezeBurnIn = 1; // Increase colours if necessary
  public const uint inPlaceTrapezeBurnIn = 2; // Burn in in-place if possible
  public const uint redactionsTrapezeBurnIn = 4; // Burn in redactions only
  public const uint showAllTrapezeBurnIn = 8; // Burn in any annotations hidden by group

  public const uint blackAndWhiteTrapezeColorType = 0; // Black and white
  public const uint palette2TrapezeColorType = 1; // 2 colors
  public const uint gray16TrapezeColorType = 2; // 16 grays
  public const uint palette16TrapezeColorType = 3; // 16 colors
  public const uint gray256TrapezeColorType = 4; // 256 grays
  public const uint palette256TrapezeColorType = 5; // 256 colors
  public const uint rgb24TrapezeColorType = 6; // 24 bit RGB color
  public const uint ycbcr24TrapezeColorType = 7; // 24 bit YCbCr color
  public const uint otherTrapezeColorType = 8; /* None of the other defined values -
                                                  any unknown values should be
                                                  treated as equivalent to this to
                                                  allow for future additions to this
                                                  list */
  public const byte bw2x2DitherTrapezePDFToTIFFColorType = 254;
  public const uint errorTrapezeColorType = 0xffffffff; // An error occurred

  public const uint getTrapezeAnnotationsCountFailed = 0xffffffff;
  public const uint getTrapezeTIFFDocumentIFDCountFailed = 0xffffffff;
  public const uint getTrapezeTIFFDocumentPageCountFailed = 0xffffffff;

  public const ushort noTrapezeCompression = 0; // None
  public const ushort packBitsTrapezeCompression = 1; // Macintosh RLE
  public const ushort g4TrapezeCompression = 2; // CCITT Group 4 (T.6)
  public const ushort unknownTrapezeCompression = 3; // Unknown (none of the others here)
  public const ushort g3TrapezeCompression = 4; // CCITT Group 3 (T.4)
  public const ushort lzwTrapezeCompression = 5; // LZW
  public const ushort jpeg6TrapezeCompression = 6; // JPEG 6
  public const ushort jpeg7TrapezeCompression = 7; // JPEG 7
  public const ushort ccitt1DWTrapezeCompression = 8; // CCITT modified Huffman RLE with word alignment
  public const ushort thunderscanTrapezeCompression = 9; // Thunderscan RLE
  public const ushort ccitt1DTrapezeCompression = 10; // CCITT modified Huffman RLE

  public const ushort openTrapezeTIFFDocumentExArtUnicode = 1;
  public const ushort openTrapezeTIFFDocumentExArtFirst = 2;
  public const ushort openTrapezeTIFFDocumentExArtMerge = 4;

  public const ushort pdfaTrapezeTIFFToPDFOption = 1;
  public const ushort ocrTextTrapezeTIFFToPDFOption = 2;
  public const ushort xmpPropertiesTrapezeTIFFToPDFOption = 4;
  public const ushort unicodeTrapezeTIFFToPDFOption = 8;
  public const ushort options2TrapezeTIFFToPDFOption = 0x8000;

  public const ushort resolutionTrapezePDFOption = 0x0001;
  public const ushort colorTypeTrapezePDFOption = 0x0002;
  public const ushort tiffOCRTextToPDFTrapezePDFOption = 0x0004;
  public const ushort colorOptimizeTrapezePDFOption = 0x0008;
  public const ushort rgb24CompressionTrapezePDFOption = 0x0010;
  public const ushort gray256CompressionTrapezePDFOption = 0x0020;
  public const ushort blackWhiteCompressionTrapezePDFOption = 0x0040;
  public const ushort pdfTextToTIFFTrapezePDFOption = 0x0080;
  public const ushort openPasswordTrapezePDFOption = 0x0100;
  public const ushort ditherTrapezePDFOption = 0x0200;
  public const ushort rapidRedactPDFTrapezePDFOption = 0x0400;
  public const ushort noRapidRedactPDFPasswordTrapezePDFOption = 0x0800;
  public const ushort objectListTrapezePDFOption = 0x1000;
  public const ushort safeModeTrapezePDFOption = 0x2000;
  public const ushort tiffToPDFATrapezePDFOption = 0x4000;
  public const ushort options2TrapezePDFOption = 0x8000;

  public const uint pluginTrapezeWindowStyle = 0x0001;
  public const uint noAnnotateTrapezeWindowStyle = 0x0008;
  public const uint noRedactTrapezeWindowStyle = 0x0010;
  public const uint noPageModifyTrapezeWindowStyle = 0x0020;

  public const byte trapezeImagePane = 1;

  public const uint palette2CompressionTrapezePDFOption2 = 0x00000001;
  public const uint gray16CompressionTrapezePDFOption2 = 0x00000002;
  public const uint palette16CompressionTrapezePDFOption2 = 0x00000004;
  public const uint palette256CompressionTrapezePDFOption2 = 0x00000008;
  public const uint compressionOptionsTrapezePDFOption2 = 0x00000010;
  public const uint tiffBookmarksToPDFTrapezePDFOption2 = 0x00000020;
  public const uint tiffThumbnailsToPDFTrapezePDFOption2 = 0x00000040;
  public const uint pdfBookmarksToTIFFTrapezePDFOption2 = 0x00000080;
  public const uint pdfTextToTIFFPageRangeTrapezePDFOption2 = 0x00000100;
  public const uint pdfTextToTIFFPageListTrapezePDFOption2 = 0x00000200;
  public const uint pdfTextToTIFFPhysicalLayoutModeTrapezePDFOption2 = 0x00000400;
  public const uint unicodeTrapezePDFOption2 = 0x00000800;
  public const uint keepCompressionTrapezePDFOption2 = 0x00001000;
  public const uint noOrientationTrapezePDFOption2 = 0x00002000;
  public const uint forceColorTypeTrapezePDFOption2 = 0x00004000;

  public const byte cmTrapezeUnit = 0;
  public const byte inchTrapezeUnit = 1;

  /* The 0th row represents the visual top of the image, and the 0th column
     represents the visual left-hand side */
  public const ushort topLeftTrapezeOrientation = 1;
  /* The 0th row represents the visual top of the image, and the 0th column
     represents the visual right-hand side */
  public const ushort topRightTrapezeOrientation = 2;
  /* The 0th row represents the visual bottom of the image, and the 0th column
     represents the visual right-hand side */
  public const ushort bottomRightTrapezeOrientation = 3;
  /* The 0th row represents the visual bottom of the image, and the 0th column
     represents the visual left-hand side */
  public const ushort bottomLeftTrapezeOrientation = 4;
  /* The 0th row represents the visual left-hand side of the image, and the 0th
     column represents the visual top */
  public const ushort leftTopTrapezeOrientation = 5;
  /* The 0th row represents the visual right-hand side of the image, and the 0th
     column represents the visual top */
  public const ushort rightTopTrapezeOrientation = 6;
  /* The 0th row represents the visual right-hand side of the image, and the 0th
     column represents the visual bottom */
  public const ushort rightBottomTrapezeOrientation = 7;
  /* The 0th row represents the visual left-hand side of the image, and the 0th
     column represents the visual bottom */
  public const ushort leftBottomTrapezeOrientation = 8;

  public const uint readTrapezeOpenTIFFDIBExIgnoreOrientation = 1;

  public const int sizeTrapezeNotify = 5;
  public const int pageNoTrapezeNotify = 6;
  public const int zoomTrapezeNotify = 7;
  public const int scrollTrapezeNotify = 49;

  public const ushort WM_NOTIFY = 0x004E;

  public IntPtr burnInTrapezeAnnotationsInPlace = (IntPtr) (-1);

  public IntPtr nullAnnotations = (IntPtr) 0;
  public IntPtr nullBookmarks = (IntPtr) 0;
  public IntPtr nullHGLOBAL = (IntPtr) 0;
  public IntPtr nullProgressProc = (IntPtr) 0;
  public IntPtr nullPtr = (IntPtr) 0;
  public IntPtr nullTIFF = (IntPtr) 0;
  public IntPtr nullTIFFDoc = (IntPtr) 0;

  /* Trapeze window properties */
  public const uint fitTrapezeProperty = 0;
  public const uint zoomTrapezeProperty = 1;
  public const uint viewBookmarksTrapezeProperty = 12;
  public const uint imageToolTrapezeProperty = 30;
  public const uint pageCountTrapezeProperty = 34;
  public const uint rotationTrapezeProperty = 39;
  public const uint viewAnnotationsTrapezeProperty = 53;
  public const uint tabletSettingsTrapezeProperty = 56;
  public const uint imageWarningTrapezeProperty = 58;
  public const uint hideInternetPrefsTrapezeProperty = 66;
  public const uint mouseWheelZoomTrapezeProperty = 91;
  public const uint enhanceColorTrapezeProperty = 92;
  public const uint enhanceMonochromeTrapezeProperty = 93;
  public const uint printStretchTrapezeProperty = 95;
  public const uint showAccelTrapezeProperty = 110;

  /* Image tools */

  public const ushort noTrapezeImageTool = 0;
  public const ushort zoomInTrapezeImageTool = 1;
  public const ushort zoomOutTrapezeImageTool = 2;
  public const ushort dragTrapezeImageTool = 3;
  public const ushort selectTrapezeImageTool = 4;
  public const ushort selectZoomTrapezeImageTool = 5;
  public const ushort selectAnnotationTrapezeImageTool = 6;
  public const ushort arrowAnnotationTrapezeImageTool = 7;
  public const ushort filledRectAnnotationTrapezeImageTool = 8;
  public const ushort highlighterAnnotationTrapezeImageTool = 9;
  public const ushort hollowRectAnnotationTrapezeImageTool = 10;
  public const ushort lineAnnotationTrapezeImageTool = 11;
  public const ushort pictureAnnotationTrapezeImageTool = 12;
  public const ushort polygonAnnotationTrapezeImageTool = 13;
  public const ushort polylineAnnotationTrapezeImageTool = 14;
  public const ushort rectAnnotationTrapezeImageTool = 15;
  public const ushort scribbleAnnotationTrapezeImageTool = 16;
  public const ushort stickyNoteAnnotationTrapezeImageTool = 17;
  public const ushort straightLineAnnotationTrapezeImageTool = 18;
  public const ushort textAnnotationTrapezeImageTool = 19;
  public const ushort magnifyTrapezeImageTool = 20;
  public const ushort otherTrapezeImageTool = 21;
  public const ushort measureTrapezeImageTool = 22;
  public const ushort calibrateTrapezeImageTool = 23;
  public const ushort ocrTextTrapezeImageTool = 24;
  public const ushort stampAnnotationTrapezeImageTool = 25;
  public const ushort rectRedactionTrapezeImageTool = 26;
  public const ushort ellipseAnnotationTrapezeImageTool = 27;
  public const ushort filledEllipseAnnotationTrapezeImageTool = 28;
  public const ushort hollowEllipseAnnotationTrapezeImageTool = 29;
  public const ushort ellipseRedactionTrapezeImageTool = 30;
  public const ushort circleMeasureTrapezeImageTool = 31;
  public const ushort scribbleRedactionTrapezeImageTool = 32;
  public const ushort lightTableTrapezeImageTool = 33;
  public const ushort deskewTrapezeImageTool = 34;
  public const ushort lineMeasurementAnnotationTrapezeImageTool = 35;
  public const ushort recessionPlaneAnnotationTrapezeImageTool = 36;
  public const ushort hConeOfVisionAnnotationTrapezeImageTool = 37;

  /* Fit values */

  public const ushort fitTrapezeHeight = 0; /* Fit to height */
  public const ushort fitTrapezeNone = 1; /* No fitting */
  public const ushort fitTrapezeWidth = 2; /* Fit to width */
  public const ushort fitTrapezeWindow = 3; /* Fit to window */
  public const ushort fitTrapezeWindowNoEnlarge = 4; /* Fit to window if larger
                                                        than window, 100% otherwise */

  /* Enumerations */

  public enum OpenTIFFDocumentArtModeEnum {
    openTIFFDocumentArtLast, /* .art file annotations are only read if no
                              * internal annotations are present */
    openTIFFDocumentArtFirst, /* Internal annotations are only read if no .art
                               * file is present */
    openTIFFDocumentArtMerge /* Internal annotations are merged with .art file
                              * annotations */
  };


  /* Structures */

  public const ushort appendTrapezeOpenTIFFExRecSize1 = 64;
  public const ushort appendTrapezeOpenTIFFExRecSize2 = 68;
  public const ushort appendTrapezeOpenTIFFExRecSize3 = 72;
  public const ushort appendTrapezeOpenTIFFExRecSize4 = 84;
  public const ushort appendTrapezeOpenTIFFExRecSize5 = 92;
  public const ushort appendTrapezeOpenTIFFExRecSize6 = 124;
  public const ushort appendTrapezeOpenTIFFExRecSize = 128;

  [StructLayout(LayoutKind.Sequential)]
  public struct AppendTrapezeOpenTIFFExRec {
    public ushort size;
    public String srcFileName;
    public IntPtr srcTIFF;
    public uint srcIFDNo;
    public uint flags;
    public IntPtr annotations;
    public uint annotationsSize;
    public IntPtr scale;
    public IntPtr bookmarks;
    public uint bookmarksSize;
    public uint pageNo;
    public uint pageCount;
    public uint newSubfileType;
    public IntPtr resolution;
    public ushort compression;
    public IntPtr srcDIB; /* appendTrapezeOpenTIFFExRecSize1 to here */
    public IntPtr dateTime; /* appendTrapezeOpenTIFFExRecSize2 to here */
    public IntPtr compressOptions; /* appendTrapezeOpenTIFFExRecSize3 to here */
    public IntPtr progressProc;
    public IntPtr lParam;
    public uint paramError; /* appendTrapezeOpenTIFFExRecSize4 to here */
    public IntPtr ocrText;
    public uint ocrTextSize; /* appendTrapezeOpenTIFFExRecSize5 to here */
    public String docName;
    public String imageDescription;
    public String make;
    public String model;
    public String software;
    public String artist;
    public String hostComputer;
    public String copyright; /* appendTrapezeOpenTIFFExRecSize6 to here */
    public ushort orientation;
  };

  public const ushort convertTrapezeTIFFToPDFExRecSize = 28;

  [StructLayout(LayoutKind.Sequential)]
  public struct ConvertTrapezeTIFFToPDFExRec {
    public ushort size;
    public ushort options;
    public ushort optionFlags;
    public String srcFileName;
    public String destFileName;
    public String xmpProperties;
    public uint options2;
    public uint optionFlags2;
  };

  public const ushort convertTrapezePDFToTIFFExRecSize = 60;

  [StructLayout(LayoutKind.Sequential, CharSet=CharSet.Ansi)]
  public struct ConvertTrapezePDFToTIFFExRec {
    public ushort size;
    public ushort options;
    public ushort optionFlags;
    public byte colorType;
    public byte resolutionUnits;
    public uint resolution;
    public byte blackWhiteCompression;
    public byte gray256Compression;
    public byte rgb24Compression;
    public String openPassword;
    public String srcFileName;
    public uint pageNo;
    public String destFileName;
    public IntPtr progressProc;
    public IntPtr lParam;
    public uint dither;
    public uint options2;
    public byte palette2Compression;
    public byte gray16Compression;
    public byte palette16Compression;
    public byte palette256Compression;
    public IntPtr compressionOptions;
    public uint optionFlags2;
  };

  [StructLayout(LayoutKind.Sequential)] 
  public struct NMHDR {
    public IntPtr hwndFrom;
    public int idFrom;
    public int code;
  };

  [StructLayout(LayoutKind.Sequential)]
  public struct Rect {
    public int left;
    public int top;
    public int right;
    public int bottom;
  };

  [StructLayout(LayoutKind.Explicit, Size=16)]
  private struct VariantRec {
    [FieldOffset(0)] public ushort vt;
    [FieldOffset(2)] public ushort wReserved1;
    [FieldOffset(4)] public ushort wReserved2;
    [FieldOffset(6)] public ushort wReserved3;
    [FieldOffset(8)] public byte bVal;
    [FieldOffset(8)] public int lVal;
    [FieldOffset(8)] public IntPtr bstrVal;
    [FieldOffset(8)] public short boolVal;
  };

  [StructLayout(LayoutKind.Sequential)]
  public struct GetTrapezeOCRTextRectRec {
    public uint charNo;
    public uint count;
    public uint ocrTextNo;
    public uint rectNo;
    public Rect rect;
  };

  [StructLayout(LayoutKind.Sequential)]
  public struct GetTrapezeOCRTextRectsRec {
    public uint charNo;
    public uint count;
    public uint ocrTextNo;
    public IntPtr rects;
  };

  [StructLayout(LayoutKind.Sequential)]
  public struct GetTrapezeOCRTextTextRec {
    public uint ocrTextNo;
    public uint textSize;
    public String text;
  };

  public const ushort getTrapezeTabletSettingsPropertyRecSize = 12;

  [StructLayout(LayoutKind.Sequential)]
  public struct GetTrapezeTabletSettingsPropertyRec {
    public ushort size;
    public ushort flags;
    public byte eraserCursor;
    public byte penCursor;
    public uint disable;
  };

  public const ushort openTrapezeTIFFDocumentExRecSize = 8;

  [StructLayout(LayoutKind.Sequential)]
  public struct OpenTrapezeTIFFDocumentExRec {
    public ushort size;
    public ushort flags;
    public String fileName;
  };

  public const uint trapezeTIFFDocumentIFDInfoRecSize1 = 24;
  public const uint trapezeTIFFDocumentIFDInfoRecSize2 = 28;
  public const uint trapezeTIFFDocumentIFDInfoRecSize = 32;

  [StructLayout(LayoutKind.Sequential)]
  public struct TrapezeTIFFDocumentIFDInfoRec {
    public uint size;
    public uint pageNo;
    public uint width;
    public uint height;
    public uint newSubfileType;
    public uint photoInt; /* trapezeTIFFDocumentIFDInfoRecSize1 to here */
    public uint orientationSwap; /* trapezeTIFFDocumentIFDInfoRecSize2 to here */
    public uint orientation;
  };

  [StructLayout(LayoutKind.Sequential)]
  public struct TrapezeScrollNotifyRec {
    public NMHDR nmhdr;
    public byte pane;
  };

  [StructLayout(LayoutKind.Sequential)]
  public struct TrapezePageNoNotifyRec {
    public NMHDR nmhdr;
    public uint oldPageNo;
    public uint newPageNo;
    public uint error;
  };

  public const ushort readTrapezeOpenTIFFDIBExRecSize = 24;
  
  [StructLayout(LayoutKind.Sequential)]
  public struct ReadTrapezeOpenTIFFDIBExRec {
    public ushort size;
    public IntPtr tiff;
    public uint ifdNo;
    public IntPtr progressProc;
    public IntPtr lParam;
    public uint flags;
  };

  public const ushort burnInTrapezeAnnotationsExRecSize = 24;

  [StructLayout(LayoutKind.Sequential)]
  public struct BurnInTrapezeAnnotationsExRec {
    public ushort size;
    public ushort rotation;
    public IntPtr annotations;
    public uint annotationsSize;
    public IntPtr dibData;
    public IntPtr dib;
    public uint flags;
  };

  /* Properties */

  public uint lastError {
    get {
      return(lastTrapezeError);
    } /* property lastError.get */
  } /* property lastError */

  public OpenTIFFDocumentArtModeEnum openTIFFDocumentArtMode =
    OpenTIFFDocumentArtModeEnum.openTIFFDocumentArtLast;

  public bool raiseTrapezeErrors = true;

  public ushort version {
    get {
      ushort version;

      version = GetTrapezeVersion(null, 0);
      if (version == 0)
        RaiseError("GetTrapezeVersion");
      return(version);
    } /* property version.get */
  } /* property version */
  public string versionString {
    get {
      ushort version;
      StringBuilder versionStr = new StringBuilder(256);

      version = GetTrapezeVersion(versionStr, (ushort) versionStr.Capacity);
      if (version == 0)
        RaiseError("GetTrapezeVersion");
      return(versionStr.ToString());
    } /* property versionString.get */
  } /* property versionString*/

  /* Methods */

  public bool AnnotationPrefsBlob(Form parent, UInt16 command,
                                  ref IntPtr blob, ref UInt32 blobSize)
  {

    bool ok;
    Form form;
    IntPtr trapezeWindow;
    TrapezeAnnotationPrefsBlobRec p;

    form = new Form();
    form.CreateControl();
    trapezeWindow = CreateTrpzWindow(WS_CHILD, 0, 0, 0, 0, form.Handle, 0);
    if (trapezeWindow == nullWindow) {
      RaiseError("CreateTrapezeWindow");
      return(false);
    }
    p = new TrapezeAnnotationPrefsBlobRec();
    p.blob = blob;
    p.blobSize = blobSize;
    if (parent == null)
      p.parent = nullWindow;
    else
      p.parent = parent.Handle;
    ok = (SendMessageAnnotationPrefsBlob(trapezeWindow,
                                         annotationPrefsBlobTrapezeMsg,
                                         (IntPtr)
                                         trapezeRectRedactionToolCommand,
                                         ref p) == trapezeMsgSuccess);
    form.Dispose();
    if (! ok) {
      RaiseError("SendMessage(annotationPrefsBlobTrapezeMsg)");
      return(false);
    }
    blob = p.blob;
    blobSize = p.blobSize;
    return(true);
  } /* method AnnotationPrefsBlob */

  public bool AppendOpenTIFFEx(IntPtr tiff,
                               ref AppendTrapezeOpenTIFFExRec appendInfo)
  {
    bool ok;

    ok = (AppendTrapezeOpenTIFFEx(tiff, ref appendInfo) != 0);
    if (! ok)
      RaiseError("AppendTrapezeOpenTIFFEx");
    return(ok);
  } /* method AppendOpenTIFFEx */

  /* BurnInAnnotations
     Burns annotations into a DIB. annotations and annotationsSize must be
     values returned by GetTIFFAnnotations() or InsertAnnotation(). dibData is
     the data for the DIB pixels. dib contains information about the DIB.
     flags is a combination of:
     - increaseColorsTrapezeBurnIn, if necessary, the number of colors in the
       DIB is increased so that there is no annotation color loss during the
       burn in. If this flag is not set, the nearest available colors from the
       image are used.
     - inPlaceTrapezeBurnIn, to, if possible, burn the annotations into the
       passed in DIB rather than returning a new DIB.
     - redactionsTrapezeBurnIn, to only burn in redactions from annotations.
       After the burn in, the burned in redactions are removed from
       annotations. When this flag is set, pass annotationsSize by reference;
       it is modified to reflect the new size of annotations. If
       annotationSize is set to zero, all annotations were redactions and
       annotations should be freed using DestroyAnnotations() immediately.
     - showAllTrapezeBurnIn, to burn in any annotations hidden by group.
       The return value is a handle to a new DIB,
       burnInTrapezeAnnotationsInPlace, or nullHGLOBAL on failure. On failure,
       lastError contains the cause of the failure.
       burnInTrapezeAnnotationsInPlace is returned if inPlaceTrapezeBurnIn was
       set and the annotations were burned into the passed in DIB; in that
       case dibData will have been modified. If a new DIB is returned, it must
       be freed using DestroyDIB() when it is no longer required. */

  public IntPtr BurnInAnnotations(IntPtr annotations, uint annotationsSize,
                                  IntPtr dibData, IntPtr dib, uint flags)
  {
    IntPtr dib2;

    if ((flags & redactionsTrapezeBurnIn) != 0)
      throw new System.Exception("Use ref annotationsSize when redactionsTrapezeBurnIn is set");
    dib2 = BurnInTrapezeAnnotations(annotations, annotationsSize, dibData,
                                    dib, flags);
    if (dib2 == nullHGLOBAL)
      RaiseError("BurnInTrapezeAnnotations");
    return(dib2);
  } /* method BurnInAnnotations */

  public IntPtr BurnInAnnotations(IntPtr annotations, ref uint annotationsSize,
                                  IntPtr dibData, IntPtr dib, uint flags)
  {
    IntPtr dib2;

    if ((flags & redactionsTrapezeBurnIn) == 0)
      throw new System.Exception("Don't use ref annotationsSize when redactionsTrapezeBurnIn is not set");
    dib2 = BurnInTrapezeAnnotations2(annotations, ref annotationsSize, dibData,
                                     dib, flags);
    if (dib2 == nullHGLOBAL)
      RaiseError("BurnInTrapezeAnnotations");
    return(dib2);
  } /* method BurnInAnnotations */

  public IntPtr BurnInAnnotationsEx(ref BurnInTrapezeAnnotationsExRec burnInInfo)
  {
    IntPtr dib2;

    dib2 = BurnInTrapezeAnnotationsEx(ref burnInInfo);
    if (dib2 == nullHGLOBAL)
      RaiseError("BurnInTrapezeAnnotationsEx");
    return(dib2);
  } /* method BurnInAnnotationsEx */

  public bool CloseTIFF(IntPtr tiff)
  {
    bool ok;

    ok = (CloseTrapezeTIFF(tiff) != 0);
    if (! ok)
      RaiseError("CloseTrapezeTIFF");
    return(ok);
  } /* method CloseTIFF */

  public bool CloseTIFFDocument(IntPtr tiffDoc)
  {
    bool ok;

    ok = (CloseTrapezeTIFFDocument(tiffDoc) != 0);
    if (! ok)
      RaiseError("CloseTrapezeTIFFDocument");
    return(ok);
  } /* method CloseTIFFDocument */

  public bool CloseTrapezeWindowDocument(IntPtr window, bool update)
  {
    IntPtr result;

    result = SendMessageLL(window, closeTrapezeDocumentMsg,
                           (IntPtr) (update ? 1 : 0), (IntPtr) 0);
    if (result != trapezeMsgSuccess)
      RaiseError("SendMessage closeTrapezeDocumentMsg", (uint) result);
    return(result == trapezeMsgSuccess);
  } /* method CloseTrapezeWindowDocument */

  public bool ConvertPDFToTIFFEx(ref ConvertTrapezePDFToTIFFExRec
                                 convertInfo)
  {
    bool ok;

    ok = (ConvertTrapezePDFToTIFFEx(ref convertInfo) != 0);
    if (! ok)
      RaiseError("ConvertTrapezePDFToTIFFEx");
    return(ok);
  } /* method ConvertPDFToTIFFEx */

  public bool ConvertTIFFToPDF(String srcFileName, String destFileName)
  {
    bool ok;

    ok = (ConvertTrapezeTIFFToPDF(srcFileName, destFileName) != 0);
    if (! ok)
      RaiseError("ConvertTrapezeTIFFToPDF");
    return(ok);
  } /* method ConvertTIFFToPDF */

  public bool ConvertTIFFToPDFEx(ref ConvertTrapezeTIFFToPDFExRec
                                 convertInfo)
  {
    bool ok;

    ok = (ConvertTrapezeTIFFToPDFEx(ref convertInfo) != 0);
    if (! ok)
      RaiseError("ConvertTrapezeTIFFToPDFEx");
    return(ok);
  } /* method ConvertTIFFToPDFEx */

  public bool CopyTIFFDocumentPage(IntPtr tiffDoc, uint pageNo, bool changePageNo,
                                   uint newPageNo, uint newPageCount, IntPtr tiff)
  {
    bool ok;

    ok = (CopyTrapezeTIFFDocumentPage(tiffDoc, pageNo, (uint) (changePageNo ? 1 : 0),
                                      newPageNo, newPageCount, tiff) != 0);
    if (! ok)
      RaiseError("CopyTrapezeTIFFDocumentPage");
    return(ok);
  } /*CopyTIFFDocumentPage*/

  public IntPtr CreatePluginWindow(uint style, int x, int y, int width,
                                   int height, IntPtr parent, uint id)
  {
    IntPtr window;

    window = CreateTrpzWindow(style | pluginTrapezeWindowStyle, x, y,
                              width, height, parent, id);
    if (window == null)
        RaiseError("CreateTrapezeWindow");
    return(window);
  } /*CreatePluginWindow*/

  public IntPtr CreateTrapezeWindow(uint style, int x, int y, int width,
                                    int height, IntPtr parent, uint id)
  {
    IntPtr window;

    window = CreateTrpzWindow(style, x, y, width, height, parent, id);
    if (window == null)
        RaiseError("CreateTrapezeWindow");
    return(window);
  } /*CreateTrapezeWindow*/

  public bool DestroyAnnotations(IntPtr annotations)
  {
    bool ok;

    ok = (DestroyTrapezeAnnotations(annotations) != 0);
    if (! ok)
      RaiseError("DestroyTrapezeAnnotations");
    return(ok);
  } /* method DestroyAnnotations */

  public void DestroyBookmarkData(IntPtr bookmarkData)
  {
    GlobalFree(bookmarkData);
  } /* method DestroyBookmarkData */

  public bool DestroyBookmarks(IntPtr bookmarks)
  {
    bool ok;

    ok = (DestroyTrapezeBookmarks(bookmarks) != 0);
    if (! ok)
      RaiseError("DestroyTrapezeBookmarks");
    return(ok);
  } /* method DestroyBookmarks */

  public void DestroyDIB(IntPtr dib)
  {
    GlobalFree(dib);
  } /* method DestroyDIB */

  public bool DisableTrapezeWindowTablet(IntPtr window)
  {
    GetTrapezeTabletSettingsPropertyRec t;
    IntPtr result;

    t.size = getTrapezeTabletSettingsPropertyRecSize;
    t.flags = foundTrapezeTabletSetting;
    t.eraserCursor = noTrapezeTabletCursor;
    t.penCursor = noTrapezeTabletCursor;
    t.disable = 1;
    result = SendMessageTabletSettings(window, setTrapezePropertyMsg,
                                       (IntPtr) tabletSettingsTrapezeProperty,
                                       ref t);
    if (result != trapezeMsgSuccess)
      RaiseError("SendMessage setTrapezePropertyMsg", (uint) result);
    return(result == trapezeMsgSuccess);
  } /* method DisableTrapezeWindowTablet */

  public object GetAnnotationProperty(IntPtr annotations, uint annotationsSize,
                                      uint annotationNo, String property)
  {
    uint result;
    VariantRec v = new VariantRec();
    object obj = null;

    v.vt = 0; /* VT_EMPTY */
    result = GetTrapezeAnnotationProperty(annotations, annotationsSize,
                                          annotationNo, property, ref v);
    if (result == 0)
      RaiseError("GetTrapezeAnnotationProperty");
    else {
      switch (v.vt) {
      case 3 : /* VT_I4 */
        obj = v.lVal;
        break;
      case 11 : /* VT_BOOL */
        obj = (v.boolVal != 0) ? true : false;
        break;
      case 17 : /* VT_UI1 */
        obj = v.bVal;
        break;
      } /*switch*/
    } /*else*/
    VariantClear(ref v);
    return(obj);
  } /* method GetAnnotationProperty */

  public uint GetAnnotationsCount(IntPtr annotations, uint annotationsSize)
  {
    uint count;

    count = GetTrapezeAnnotationsCount(annotations, annotationsSize);
    if (count == getTrapezeAnnotationsCountFailed)
      RaiseError("GetTrapezeAnnotationsCount");
    return(count);
  } /* method GetAnnotationsCount */

  public Rect GetAnnotationRect(IntPtr annotations, uint annotationsSize,
                                uint annotationNo)
  {
    Rect r;

    r.left = (int) GetAnnotationProperty(annotations, annotationsSize, annotationNo, "X");
    r.top = (int) GetAnnotationProperty(annotations, annotationsSize, annotationNo, "Y");
    r.right = r.left + (int) GetAnnotationProperty(annotations, annotationsSize, annotationNo, "Width");
    r.bottom = r.top + (int) GetAnnotationProperty(annotations, annotationsSize, annotationNo, "Height");
    return(r);
  } /* method GetAnnotationRect */

  public IntPtr GetBookmarkData(IntPtr bookmarks, ref uint size)
  {
    IntPtr bookmarkData;

    bookmarkData = GetTrapezeBookmarkData(bookmarks, ref size);
    if (bookmarkData == nullHGLOBAL)
      RaiseError("GetTrapezeBookmarkData");
    return(bookmarkData);
  } /* method GetBookmarkData */

  public uint GetDIBColorType(IntPtr dibData, IntPtr dib)
  {
    uint colorType;

    colorType = GetTrapezeDIBColorType(dibData, dib);
    if (colorType == errorTrapezeColorType)
      RaiseError("GetTrapezeDIBColorType");
    return(colorType);
  } /* method GetDIBColorType */

  public uint GetDocumentType(String fileName)
  {
    uint docType;

    docType = GetTrapezeDocumentType(fileName);
    if (docType == errorTrapezeDocument)
      RaiseError("GetTrapezeDocumentType");
    return(docType);
  } /* method GetDocumentType */

  public IntPtr GetTIFFAnnotations(IntPtr tiff, uint ifdNo,
                                   ref uint annotationsSize)
  {
    IntPtr annotations;
    uint result;

    annotations = nullAnnotations;
    result = GetTrapezeTIFFAnnotations(tiff, ifdNo, ref annotations,
                                       ref annotationsSize);
    if (result != 0)
      SetLastError(0);
    else {
      RaiseError("GetTrapezeTIFFAnnotations");
      lastTrapezeError = GetLastTrapezeErrorCode();
      annotations = nullAnnotations;
    }
    return(annotations);
  } /* method GetTIFFAnnotations */

  public bool GetTIFFCompression(IntPtr tiff, uint ifdNo,
                                 ref ushort compression)
  {
    bool ok;

    ok = (GetTrapezeTIFFCompression(tiff, ifdNo, ref compression) != 0);
    if (! ok)
      RaiseError("GetTrapezeTIFFCompression");
    return(ok);
  } /* method GetTIFFCompression */

  public IntPtr GetTIFFDocumentBookmarks(IntPtr tiffDoc)
  {
    IntPtr bookmarks;

    SetLastError(0);
    bookmarks = GetTrapezeTIFFDocumentBookmarks(tiffDoc);
    if ((bookmarks == nullBookmarks) && (GetLastTrapezeErrorCode() != 0))
      RaiseError("GetTIFFDocumentBookmarks");
    return(bookmarks);
  } /* method GetTIFFDocumentBookmarks */

  public uint GetTIFFDocumentIFDCount(IntPtr tiffDoc)
  {
    uint ifdCount;

    ifdCount = GetTrapezeTIFFDocumentIFDCount(tiffDoc);
    if (ifdCount == getTrapezeTIFFDocumentIFDCountFailed)
      RaiseError("GetTrapezeTIFFDocumentIFDCount");
    return(ifdCount);
  } /* method GetTIFFDocumentIFDCount */

  public bool GetTIFFDocumentIFDInfo(IntPtr tiffDoc, uint ifdNo,
                                     ref TrapezeTIFFDocumentIFDInfoRec
                                     ifdInfo)
  {
    bool ok;

    ok = (GetTrapezeTIFFDocumentIFDInfo(tiffDoc, ifdNo, ref ifdInfo) != 0);
    if (! ok)
      RaiseError("GetTrapezeTIFFDocumentIFDInfo");
    return(ok);
  } /* method GetTIFFDocumentIFDInfo */

  public uint GetTIFFDocumentPageCount(IntPtr tiffDoc)
  {
    uint pageCount;

    pageCount = GetTrapezeTIFFDocumentPageCount(tiffDoc);
    if (pageCount == getTrapezeTIFFDocumentPageCountFailed)
      RaiseError("GetTrapezeTIFFDocumentPageCount");
    return(pageCount);
  } /* method GetTIFFDocumentPageCount */

  public IntPtr GetTIFFDocumentTIFF(IntPtr tiffDoc)
  {
    IntPtr tiff;

    tiff = GetTrapezeTIFFDocumentTIFF(tiffDoc);
    if (tiff == nullTIFF)
      RaiseError("GetTIFFDocumentBookmarks");
    return(tiff);
  } /* method GetTIFFDocumentTIFF */

  public uint GetTrapezeWindowOCRTextCount(IntPtr window)
  {
    IntPtr result;
    uint count;

    count = 0xffffffff;
    result = SendMessageLPUINT(window, getOCRTextCountTrapezeMsg, (IntPtr) 0,
                               ref count);
    if (result != trapezeMsgSuccess) {
      RaiseError("SendMessage getOCRTextCountTrapezeMsg", (uint) result);
      count = 0xffffffff;
    } /*if*/
    return(count);
  } /* method GetTrapezeWindowOCRTextCount */

  public Rect[] GetTrapezeWindowOCRTextRects(IntPtr window, uint ocrTextNo,
                                             uint charNo, uint count)
  {
    GetTrapezeOCRTextRectRec rectInfo;
    GetTrapezeOCRTextRectsRec rectsInfo;
    IntPtr result;
    Rect[] rects;
    uint rectNo;

    /* Get number of rectangles */
    rectsInfo.ocrTextNo = ocrTextNo;
    rectsInfo.charNo = charNo;
    rectsInfo.count = count;
    rectsInfo.rects = (IntPtr) 0;
    result = SendMessageOCRTextRects(window, getOCRTextRectsTrapezeMsg,
                                     (IntPtr) 0, ref rectsInfo);
    if (result != trapezeMsgSuccess) {
      RaiseError("SendMessage getOCRTextRectsTrapezeMsg", (uint) result);
      return(new Rect[0]);
    } /*if*/
    rects = new Rect[rectsInfo.count];
    for (rectNo = 0; rectNo < rectsInfo.count; rectNo ++) {
      rectInfo.ocrTextNo = ocrTextNo;
      rectInfo.charNo = charNo;
      rectInfo.count = count;
      rectInfo.rectNo = rectNo;
      rectInfo.rect.left = 0;
      rectInfo.rect.top = 0;
      rectInfo.rect.right = 0;
      rectInfo.rect.bottom = 0;
      result = SendMessageOCRTextRect(window, getOCRTextRectTrapezeMsg,
                                      (IntPtr) 0, ref rectInfo);
      if (result != trapezeMsgSuccess) {
        RaiseError("SendMessage getOCRTextRectTrapezeMsg", (uint) result);
        return(new Rect[0]);
      } /*if*/
      rects[rectNo] = rectInfo.rect;
    } /*for*/
    return(rects);
  } /* method GetTrapezeWindowOCRTextRects */

  public String GetTrapezeWindowOCRTextText(IntPtr window, uint ocrTextNo)
  {
    GetTrapezeOCRTextTextRec textInfo;
    IntPtr result;

    textInfo.ocrTextNo = ocrTextNo;
    textInfo.textSize = 0;
    textInfo.text = null;
    result = SendMessageOCRTextText(window, getOCRTextTextTrapezeMsg,
                                    (IntPtr) 0, ref textInfo);
    if (result != trapezeMsgSuccess) {
      RaiseError("SendMessage getOCRTextTextTrapezeMsg", (uint) result);
      return("");
    } /*if*/
    if (textInfo.textSize == 0)
      /* Empty string */
      return("");
    textInfo.text = new String(' ', (int) textInfo.textSize);
    textInfo.textSize ++; /* Doesn't include null terminator */
    result = SendMessageOCRTextText(window, getOCRTextTextTrapezeMsg,
                                    (IntPtr) 0, ref textInfo);
    if (result != trapezeMsgSuccess) {
      RaiseError("SendMessage getOCRTextTextTrapezeMsg", (uint) result);
      return("");
    } /*if*/
    return(textInfo.text);
  } /* method GetTrapezeWindowOCRTextText */

  public bool GetTrapezeWindowProperty(IntPtr window, uint property, ref bool value)
  {
    IntPtr result;
    uint u = 0;

    result = SendMessageLPUINT(window, getTrapezePropertyMsg, (IntPtr) property, ref u);
    if (result != trapezeMsgSuccess)
      RaiseError("SendMessage getTrapezePropertyMsg", (uint) result);
    else
      value = (u != 0);
    return(result == trapezeMsgSuccess);
  } /* method GetTrapezeWindowProperty */

  public bool GetTrapezeWindowProperty(IntPtr window, uint property, ref uint value)
  {
    IntPtr result;

    result = SendMessageLPUINT(window, getTrapezePropertyMsg, (IntPtr) property, ref value);
    if (result != trapezeMsgSuccess)
      RaiseError("SendMessage getTrapezePropertyMsg", (uint) result);
    return(result == trapezeMsgSuccess);
  } /* method GetTrapezeWindowProperty */

  public bool IsTrapezeWindowNotify(IntPtr window, Message m)
  {
    NMHDR nmhdr;

    if (m.Msg != WM_NOTIFY)
      return(false);
    nmhdr = (NMHDR) m.GetLParam(typeof(NMHDR));
    return(nmhdr.hwndFrom == window);
  } /* method IsTrapezeWindowNotify */

  public bool IsTrapezeWindowDocumentOpen(IntPtr window)
  {
    IntPtr result;
    uint pageCount;

    pageCount = 0;
    result = SendMessageLPUINT(window, getTrapezePropertyMsg,
                               (IntPtr) pageCountTrapezeProperty,
                               ref pageCount);
    if (result == trapezeMsgSuccess)
      return(true);
    if (result == (IntPtr) noDocTrapezeError)
      return(false);
    RaiseError("GetProperty pageCountTrapezeProperty", (uint) result);
    return(false);
  } /* method IsTrapezeWindowDocumentOpen */

  public IntPtr LockDIB(IntPtr dib, ref IntPtr dibData)
  {
    IntPtr lockedDIB;

    lockedDIB = GlobalLock(dib);
    dibData = GetTrapezeDIBData(lockedDIB);
    if (dibData == nullPtr)
      RaiseError("GetTrapezeDIBData");
    return(lockedDIB);
  } /* method LockDIB */

  public void MoveTrapezeWindow(IntPtr window, int x, int y, int width,
                                int height)
  {
    MoveWindow(window, x, y, width, height, 1);
  } /* method MoveTrapezeWindow */

  public bool NotifyTrapezeWindowCommand(IntPtr window, uint command)
  {
    IntPtr result;

    result = SendMessageLL(window, notifyTrapezeCommandMsg, (IntPtr) command,
                           (IntPtr) 0);
    if (result != trapezeMsgSuccess)
      RaiseError("SendMessage notifyTrapezeCommandMsg", (uint) result);
    return(result == trapezeMsgSuccess);
  } /* method NotifyTrapezeWindowCommand */

  public bool NewTrapezeWindowTIFF(IntPtr window, String title)
  {
    IntPtr result;

    result = SendMessageLS(window, newTIFFTrapezeMsg, (IntPtr) 0, title);
    if (result != trapezeMsgSuccess)
      RaiseError("SendMessage newTIFFTrapezeMsg", (uint) result);
    return(result == trapezeMsgSuccess);
  } /* method NewTrapezeWindowTIFF */

  public bool OCRTIFF(String fileName, bool replace)
  {
    bool ok;

    ok = (OCRTrapezeTIFF(fileName, (uint) (replace ? 1 : 0), nullProgressProc, (IntPtr) 0) != 0);
    if (! ok)
      RaiseError("OCRTrapezeTIFF");
    return(ok);
  } /* method OCRTIFF */

  public IntPtr OpenTIFF(String fileName, uint access, bool create)
  {
    IntPtr tiff;

    tiff = OpenTrapezeTIFF(fileName, access, (uint) (create ?  1 : 0));
    if (tiff == nullTIFF)
      RaiseError("OpenTrapezeTIFF");
    return(tiff);
  } /* method OpenTIFF */

  /* OpenTIFFDocument
     Set openTIFFDocumentArtMode to one of the OpenTIFFDocumentArtModeEnum
     values to change the .art file mode before calling OpenTIFFDocument(). */

  public IntPtr OpenTIFFDocument(String fileName)
  {
    IntPtr tiffDoc;
    OpenTrapezeTIFFDocumentExRec openInfo;

    if (openTIFFDocumentArtMode ==
        OpenTIFFDocumentArtModeEnum.openTIFFDocumentArtLast) {
      tiffDoc = OpenTrapezeTIFFDocument(fileName);
      if (tiffDoc == nullTIFFDoc)
        RaiseError("OpenTrapezeTIFFDocument");
    } /*if*/
    else {
      openInfo.size = openTrapezeTIFFDocumentExRecSize;
      openInfo.flags = 0;
      if (openTIFFDocumentArtMode ==
          OpenTIFFDocumentArtModeEnum.openTIFFDocumentArtFirst)
        openInfo.flags |= openTrapezeTIFFDocumentExArtFirst;
      else if (openTIFFDocumentArtMode ==
               OpenTIFFDocumentArtModeEnum.openTIFFDocumentArtMerge)
        openInfo.flags |= openTrapezeTIFFDocumentExArtMerge;
      openInfo.fileName = fileName;
      tiffDoc = OpenTrapezeTIFFDocumentEx(ref openInfo);
      if (tiffDoc == nullTIFFDoc)
        RaiseError("OpenTrapezeTIFFDocumentEx");
    } /*else*/
    return(tiffDoc);
  } /* method OpenTIFFDocument */

  public bool OpenTrapezeWindowDocument(IntPtr window, String fileName,
                                        bool readOnly)
  {
    IntPtr result;
    
    if (fileName == "")
      result = SendMessageLL(window, openTrapezeDocumentMsg, (IntPtr) 0, (IntPtr) 0);
    else
      result = SendMessageLS(window, openTrapezeDocumentMsg,
                             (IntPtr) (readOnly ? 1 : 0), fileName);
    if (result != trapezeMsgSuccess)
      RaiseError("SendMessage openTrapezeDocumentMsg", (uint) result);
    return(result == trapezeMsgSuccess);
  } /* method OpenTrapezeWindowDocument */

  public void PrintTrapezeWindow(IntPtr window)
  {
    SendMessageLL(window, printDialogTrapezeMsg, (IntPtr) 0, (IntPtr) 0);
  } /* method PrintTrapezeWindow */

  public IntPtr ReadDIB(String fileName, uint pageNo)
  {
    IntPtr dib;

    dib = ReadTrapezeDIB(fileName, pageNo);
    if (dib == nullHGLOBAL)
      RaiseError("ReadTrapezeDIB");
    return(dib);
  } /* method ReadDIB */

  public IntPtr ReadOpenTIFFDIB(IntPtr tiff, uint ifdNo)
  {
    IntPtr dib;

    dib = ReadTrapezeOpenTIFFDIB(tiff, ifdNo);
    if (dib == nullHGLOBAL)
      RaiseError("ReadTrapezeOpenTIFFDIB");
    return(dib);
  } /* method ReadOpenTIFFDIB */

  public IntPtr ReadOpenTIFFDIBEx(ref ReadTrapezeOpenTIFFDIBExRec readInfo)
  {
    IntPtr dib;

    dib = ReadTrapezeOpenTIFFDIBEx(ref readInfo);
    if (dib == nullHGLOBAL)
      RaiseError("ReadTrapezeOpenTIFFDIBEx");
    return(dib);
  } /* method ReadOpenTIFFDIBEx */

  public bool RemoveTrapezeWindowCommand(IntPtr window, uint command)
  {
    IntPtr result;

    result = SendMessageLL(window, removeTrapezeCommandMsg, (IntPtr) command,
                           (IntPtr) 0);
    if (result != trapezeMsgSuccess)
      RaiseError("SendMessage removeTrapezeCommandMsg", (uint) result);
    return(result == trapezeMsgSuccess);
  } /* method RemoveTrapezeWindowCommand */

  public bool SaveTIFFDIB(String fileName, IntPtr dib, ushort compression)
  {
    bool ok;

    ok = (SaveTrapezeTIFFDIB(fileName, dib, compression) != 0);
    if (! ok)
      RaiseError("SaveTrapezeTIFFDIB");
    return(ok);
  } /* method SaveTIFFDIB */

  public bool SetAnnotationProperty(ref IntPtr annotations,
                                    ref uint annotationsSize,
                                    uint annotationNo, String property,
                                    byte value)
  {
    VariantRec v = new VariantRec();

    v.vt = 17; /* VT_UI1 */
    v.bVal = value;
    return(SetAnnotationProperty(ref annotations, ref annotationsSize,
                                 annotationNo, property, v));
  } /* method SetAnnotationProperty */

  public bool SetAnnotationProperty(ref IntPtr annotations,
                                    ref uint annotationsSize,
                                    uint annotationNo, String property,
                                    String value)
  {
    bool ok;
    VariantRec v = new VariantRec();

    v.vt = 8; /* VT_BSTR */
    v.bstrVal = SysAllocString(value);
    ok = SetAnnotationProperty(ref annotations, ref annotationsSize,
                               annotationNo, property, v);
    SysFreeString(v.bstrVal);
    return(ok);
  } /* method SetAnnotationProperty */

  public bool SetAnnotationProperty(ref IntPtr annotations,
                                    ref uint annotationsSize,
                                    uint annotationNo, String property,
                                    uint value)
  {
    VariantRec v = new VariantRec();

    v.vt = 3; /* VT_I4 */
    v.lVal = (int) value;
    return(SetAnnotationProperty(ref annotations, ref annotationsSize,
                                 annotationNo, property, v));
  } /* method SetAnnotationProperty */

  private bool SetAnnotationProperty(ref IntPtr annotations,
                                     ref uint annotationsSize,
                                     uint annotationNo, String property,
                                     VariantRec value)
  {
    bool ok;

    ok = (SetTrapezeAnnotationProperty(ref annotations,
                                       ref annotationsSize, annotationNo,
                                       property, ref value) != 0);
    if (! ok)
      RaiseError("SetTrapezeAnnotationProperty");
    return(ok);
  } /* method SetAnnotationProperty */

  public bool SetDebugLogFile(String fileName)
  {
    bool ok;
    
    ok = (SetTrapezeDebugLogFile(fileName) != 0);
    if (! ok)
      RaiseError("SetTrapezeDebugLogFile");
    return(ok);
  } /* method SetDebugLogFile */

  public bool SetLicense(String customer, String location, String key)
  {
    bool ok;
    
    ok = (SetTrapezeLicense(customer, location, key) != 0);
    if (! ok)
      RaiseError("SetTrapezeLicense");
    return(ok);
  } /* method SetLicense */

  public bool SetTrapezeWindowPrefValue(IntPtr window, String pref, bool value)
  {
    IntPtr result;
    VariantRec v = new VariantRec();

    v.vt = 11; /* VT_BOOL */
    v.boolVal = (short) (value ? -1 : 0);
    result = SendMessageSVariant(window, setTrapezePrefValueMsg, pref, ref v);
    if (result != trapezeMsgSuccess)
      RaiseError("SendMessage setTrapezePrefValueMsg", (uint) result);
    return(result == trapezeMsgSuccess);
  } /* method SetTrapezeWindowPrefValue */

  public bool SetTrapezeWindowProperty(IntPtr window, uint property, bool value)
  {
    IntPtr result;

    result = SendMessageLL(window, setTrapezePropertyMsg, (IntPtr) property,
                           (IntPtr) (value ? 1 : 0));
    if (result != trapezeMsgSuccess)
      RaiseError("SendMessage setTrapezePropertyMsg", (uint) result);
    return(result == trapezeMsgSuccess);
  } /* method SetTrapezeWindowProperty */

  public bool SetTrapezeWindowProperty(IntPtr window, uint property, ushort value)
  {
    IntPtr result;

    result = SendMessageLL(window, setTrapezePropertyMsg, (IntPtr) property,
                           (IntPtr) value);
    if (result != trapezeMsgSuccess)
      RaiseError("SendMessage setTrapezePropertyMsg", (uint) result);
    return(result == trapezeMsgSuccess);
  } /* method SetTrapezeWindowProperty */

  public bool SetTrapezeWindowProperty(IntPtr window, uint property, uint value)
  {
    IntPtr result;

    result = SendMessageLL(window, setTrapezePropertyMsg, (IntPtr) property,
                           (IntPtr) value);
    if (result != trapezeMsgSuccess)
      RaiseError("SendMessage setTrapezePropertyMsg", (uint) result);
    return(result == trapezeMsgSuccess);
  } /* method SetTrapezeWindowProperty */

  public bool TrapezeWindowCommand(IntPtr window, ushort command, bool force)
  {
    IntPtr result;
  
    result = SendMessageLL(window, trapezeCommandMsg, (IntPtr) command,
                           (IntPtr) (force ? 1 : 0));
    if (result != trapezeMsgSuccess)
      RaiseError("SendMessage trapezeCommandMsg", (uint) result);
    return(result == trapezeMsgSuccess);
  } /* method TrapezeWindowCommand */

  public bool TrapezeWindowImageToScreenRect(IntPtr window, ref Rect rect)
  {
    IntPtr result;
  
    result = SendMessageRect(window, imageToScreenRectTrapezeMsg, (IntPtr) 0,
                             ref rect);
    if (result != trapezeMsgSuccess)
      RaiseError("SendMessage imageToScreenRectTrapezeMsg", (uint) result);
    return(result == trapezeMsgSuccess);
  } /* method TrapezeWindowImageToScreenRect */

  public bool TrapezeWindowSaveAs(IntPtr window, String fileName)
  {
    IntPtr result;
  
    result = SendMessageLS(window, saveAsTrapezeMsg, (IntPtr) 0, fileName);
    if (result != trapezeMsgSuccess)
      RaiseError("SendMessage saveAsTrapezeMsg", (uint) result);
    return(result == trapezeMsgSuccess);
  } /* method TrapezeWindowSaveAs */
  
  public bool TrapezeWindowTRIMViewer(IntPtr window)
  {
    IntPtr result;
  
    result = SendMessageLL(window, trimViewerTrapezeMsg, (IntPtr) 0, (IntPtr) 0);
    if (result != trapezeMsgSuccess)
      RaiseError("SendMessage trimViewerTrapezeMsg", (uint) result);
    return(result == trapezeMsgSuccess);
  } /* method TrapezeWindowTRIMViewer */

  public void UnlockDIB(IntPtr dib)
  {
    GlobalUnlock(dib);
  } /* method LockDIB */

  /* Classes */

  public class ConvertPDFToTIFFClass
  {
    /* Properties
     *
     * byte blackWhiteCompression
     * bool colorOptimize
     * byte colorType
     * bool convertText
     * double dpi
     * bool forceColorType
     * byte gray16Compression
     * byte gray256Compression
     * byte palette2Compression
     * byte palette16Compression
     * byte palette256Compression
     * byte rgb24Compression
     *
     * Methods
     *
     * bool Convert(String srcFileName, String destFileName)
     */

    /* Properties */

    public byte blackWhiteCompression {
      set {
        switch (value) {
        case (byte) noTrapezeCompression :
        case (byte) packBitsTrapezeCompression :
        case (byte) lzwTrapezeCompression :
        case (byte) g4TrapezeCompression :
        case (byte) g3TrapezeCompression :
          break;
        default :
          throw new System.Exception("Invalid compression value");
        }
        convertInfo.options |= blackWhiteCompressionTrapezePDFOption;
        convertInfo.blackWhiteCompression = value;
      } /* property blackWhiteCompression.set */
    } /* property blackWhiteCompression */

    public bool colorOptimize {
      set {
        convertInfo.options |= colorOptimizeTrapezePDFOption;
        if (value)
          convertInfo.optionFlags |= colorOptimizeTrapezePDFOption;
        else
          convertInfo.optionFlags = (ushort) (convertInfo.optionFlags & ~colorOptimizeTrapezePDFOption);
      } /* property colorOptimize.set */
    } /* property colorOptimize */

    public byte colorType {
      set {
        convertInfo.options |= colorTypeTrapezePDFOption;
        convertInfo.colorType = value;
      } /* property colorType.set */
    } /* property colorType */

    public bool convertText {
      set {
        convertInfo.options |= pdfTextToTIFFTrapezePDFOption;
        if (value)
          convertInfo.optionFlags |= pdfTextToTIFFTrapezePDFOption;
        else
          convertInfo.optionFlags = (ushort) (convertInfo.optionFlags & ~pdfTextToTIFFTrapezePDFOption);
      } /* property convertText.set */
    } /* property convertText */

    public bool forceColorType {
      set {
        convertInfo.options |= options2TrapezePDFOption;
        convertInfo.options2 |= forceColorTypeTrapezePDFOption2;
        convertInfo.optionFlags2 |= forceColorTypeTrapezePDFOption2;
      } /* property forceColorType.set */
    } /* property forceColorType */

    public double dpi {
      set {
        convertInfo.options |= resolutionTrapezePDFOption;
        convertInfo.resolution = (uint) (value * 1000);
        convertInfo.resolutionUnits = inchTrapezeUnit;
      } /* property bookmarksToPDF.set */
    } /* property dpi */

    public byte gray16Compression {
      set {
        switch (value) {
        case (byte) noTrapezeCompression :
        case (byte) packBitsTrapezeCompression :
        case (byte) lzwTrapezeCompression :
          break;
        default :
          throw new System.Exception("Invalid compression value");
        }
        convertInfo.options |= options2TrapezePDFOption;
        convertInfo.options2 |= gray16CompressionTrapezePDFOption2;
        convertInfo.gray16Compression = value;
      } /* property gray16Compression.set */
    } /* property gray16Compression */

    public byte gray256Compression {
      set {
        switch (value) {
        case (byte) noTrapezeCompression :
        case (byte) packBitsTrapezeCompression :
        case (byte) lzwTrapezeCompression :
        case (byte) jpeg6TrapezeCompression :
        case (byte) jpeg7TrapezeCompression :
          break;
        default :
          throw new System.Exception("Invalid compression value");
        }
        convertInfo.options |= gray256CompressionTrapezePDFOption;
        convertInfo.gray256Compression = value;
      } /* property gray256Compression.set */
    } /* property gray256Compression */

    public byte palette2Compression {
      set {
        switch (value) {
        case (byte) noTrapezeCompression :
        case (byte) packBitsTrapezeCompression :
        case (byte) lzwTrapezeCompression :
        case (byte) g3TrapezeCompression :
        case (byte) g4TrapezeCompression :
          break;
        default :
          throw new System.Exception("Invalid compression value");
        }
        convertInfo.options |= options2TrapezePDFOption;
        convertInfo.options2 |= palette2CompressionTrapezePDFOption2;
        convertInfo.palette2Compression = value;
      } /* property palette2Compression.set */
    } /* property palette2Compression */

    public byte palette16Compression {
      set {
        switch (value) {
        case (byte) noTrapezeCompression :
        case (byte) packBitsTrapezeCompression :
        case (byte) lzwTrapezeCompression :
          break;
        default :
          throw new System.Exception("Invalid compression value");
        }
        convertInfo.options |= options2TrapezePDFOption;
        convertInfo.options2 |= palette16CompressionTrapezePDFOption2;
        convertInfo.palette16Compression = value;
      } /* property palette16Compression.set */
    } /* property palette16Compression */

    public byte palette256Compression {
      set {
        switch (value) {
        case (byte) noTrapezeCompression :
        case (byte) packBitsTrapezeCompression :
        case (byte) lzwTrapezeCompression :
          break;
        default :
          throw new System.Exception("Invalid compression value");
        }
        convertInfo.options |= options2TrapezePDFOption;
        convertInfo.options2 |= palette256CompressionTrapezePDFOption2;
        convertInfo.palette256Compression = value;
      } /* property palette256Compression.set */
    } /* property palette256Compression */

    public byte rgb24Compression {
      set {
        switch (value) {
        case (byte) noTrapezeCompression :
        case (byte) packBitsTrapezeCompression :
        case (byte) lzwTrapezeCompression :
        case (byte) jpeg6TrapezeCompression :
        case (byte) jpeg7TrapezeCompression :
          break;
        default :
          throw new System.Exception("Invalid compression value");
        }
        convertInfo.options |= rgb24CompressionTrapezePDFOption;
        convertInfo.rgb24Compression = value;
      } /* property rgb24Compression.set */
    } /* property rgb24Compression */

    /* Methods */

    public ConvertPDFToTIFFClass(Trapeze trapeze) {
      convertInfo = new ConvertTrapezePDFToTIFFExRec();
      convertInfo.size = convertTrapezePDFToTIFFExRecSize;
      convertInfo.options = 0;
      convertInfo.optionFlags = 0;
      convertInfo.options2 = 0;
      convertInfo.optionFlags2 = 0;
      trpz = trapeze;
    } /* constructor */

    public bool Convert(String srcFileName, String destFileName) {
      convertInfo.srcFileName = srcFileName;
      convertInfo.destFileName = destFileName;
      return(trpz.ConvertPDFToTIFFEx(ref convertInfo));
    } /* method Convert */

    /* Private properties */

    private ConvertTrapezePDFToTIFFExRec convertInfo;
    private Trapeze trpz;

  }; /* class ConvertPDFToTIFFClass */

  public class ConvertTIFFToPDFClass
  {
    /* Properties
     * 
     * bool bookmarksToPDF
     * bool noOrientation
     * bool ocrText
     * bool pdfa
     * bool thumbnailsToPDF
     * 
     * Methods
     * 
     * bool Convert(String srcFileName, String destFileName)
     */

    /* Properties */

    public bool bookmarksToPDF {
      set {
        convertInfo.options |= options2TrapezeTIFFToPDFOption;
        convertInfo.options2 |= tiffBookmarksToPDFTrapezePDFOption2;
        if (value)
          convertInfo.optionFlags2 |= tiffBookmarksToPDFTrapezePDFOption2;
        else
          convertInfo.optionFlags2 &= ~tiffBookmarksToPDFTrapezePDFOption2;
      } /* property bookmarksToPDF.set */
    } /* property bookmarksToPDF */

    public bool noOrientation {
      set {
        convertInfo.options |= options2TrapezeTIFFToPDFOption;
        convertInfo.options2 |= noOrientationTrapezePDFOption2;
        if (value)
          convertInfo.optionFlags2 |= noOrientationTrapezePDFOption2;
        else
          convertInfo.optionFlags2 &= ~noOrientationTrapezePDFOption2;
      } /* property noOrientation.set */
    } /* property noOrientation */

    public bool pdfa {
      set {
        convertInfo.options |= pdfaTrapezeTIFFToPDFOption;
        if (value)
          convertInfo.optionFlags |= pdfaTrapezeTIFFToPDFOption;
        else
          convertInfo.optionFlags &= (pdfaTrapezeTIFFToPDFOption ^ 0xffff);
      } /* property pdfa.set */
    } /* property pdfa */

    public bool ocrText {
      set {
        convertInfo.options |= ocrTextTrapezeTIFFToPDFOption;
        if (value)
          convertInfo.optionFlags |= ocrTextTrapezeTIFFToPDFOption;
        else
          convertInfo.optionFlags &= (ocrTextTrapezeTIFFToPDFOption ^ 0xffff);
      } /* property ocrText.set */
    } /* property ocrText */

    public bool thumbnailsToPDF {
      set {
        convertInfo.options |= options2TrapezeTIFFToPDFOption;
        convertInfo.options2 |= tiffThumbnailsToPDFTrapezePDFOption2;
        if (value)
          convertInfo.optionFlags2 |= tiffThumbnailsToPDFTrapezePDFOption2;
        else
          convertInfo.optionFlags2 &= ~tiffThumbnailsToPDFTrapezePDFOption2;
      } /* property thumbnailsToPDF.set */
    } /* property thumbnailsToPDF */

    /* Methods */

    public ConvertTIFFToPDFClass(Trapeze trapeze) {
      convertInfo = new ConvertTrapezeTIFFToPDFExRec();
      convertInfo.size = convertTrapezeTIFFToPDFExRecSize;
      convertInfo.options = 0;
      convertInfo.optionFlags = 0;
      convertInfo.xmpProperties = "";
      convertInfo.options2 = 0;
      convertInfo.optionFlags2 = 0;
      trpz = trapeze;
    } /* constructor */

    public bool Convert(String srcFileName, String destFileName) {
      convertInfo.srcFileName = srcFileName;
      convertInfo.destFileName = destFileName;
      return(trpz.ConvertTIFFToPDFEx(ref convertInfo));
    } /* method Convert */

    /* Private properties */

    private ConvertTrapezeTIFFToPDFExRec convertInfo;
    private Trapeze trpz;

  }; /* class ConvertTIFFToPDFClass */

  /* Private constants */

  private const uint WS_CHILD = 0x40000000;
  /* Error codes */
  private const uint noDocTrapezeError = 0x2000011;
  /* Messages */
  private const uint WM_USER = 0x0400;
  private const uint openTrapezeDocumentMsg = WM_USER;
  private const uint setTrapezePropertyMsg = WM_USER + 1;
  private const uint getTrapezePropertyMsg = WM_USER + 2;
  private const uint closeTrapezeDocumentMsg = WM_USER + 3;
  private const uint printDialogTrapezeMsg = WM_USER + 13;
  private const uint trapezeCommandMsg = WM_USER + 14;
  private const uint removeTrapezeCommandMsg = WM_USER + 35;
  private const uint saveAsTrapezeMsg = WM_USER + 36;
  private const uint newTIFFTrapezeMsg = WM_USER + 37;
  private const uint notifyTrapezeCommandMsg = WM_USER + 52;
  private const uint trimViewerTrapezeMsg = WM_USER + 57;
  private const uint getOCRTextCountTrapezeMsg = WM_USER + 74;
  private const uint getOCRTextTextTrapezeMsg = WM_USER + 75;
  private const uint getOCRTextRectsTrapezeMsg = WM_USER + 76;
  private const uint annotationPrefsBlobTrapezeMsg = WM_USER + 149;
  private const uint setTrapezePrefValueMsg = WM_USER + 159;
  private const uint getOCRTextRectTrapezeMsg = WM_USER + 163;
  private const uint imageToScreenRectTrapezeMsg = WM_USER + 164;
  private IntPtr trapezeMsgSuccess = (IntPtr) (-1);
  private IntPtr nullWindow = (IntPtr) 0;

  private const ushort foundTrapezeTabletSetting = 1;
  private const byte noTrapezeTabletCursor = 255;

  /* Private structures */

  [StructLayout(LayoutKind.Sequential)]
  private struct TrapezeAnnotationPrefsBlobRec {
    public UInt32 blobSize;
    public IntPtr blob;
    public IntPtr parent;
  };

  /* Private properties */

  private uint lastTrapezeError;

  /* Private methods */

  private void RaiseError(String location) {
    if (raiseTrapezeErrors)
      RaiseError(location, GetLastTrapezeErrorCode());
  } /* method RaiseError */

  private void RaiseError(String location, uint errorCode)
  {
    if (raiseTrapezeErrors)
    {
      StringBuilder str = new StringBuilder(256);

      lastTrapezeError = errorCode;
      GetLastTrapezeErrorMessage(str, (ushort)str.Capacity);
      throw new System.Exception(location + ": " + System.String.Format("{0}", lastTrapezeError) + " " + str.ToString());
    }
  } /* method RaiseError */

  private void SetLastError(uint error)
  {
    lastTrapezeError = error;
    SetLastTrapezeError(error);
  } /* method SetLastError*/

  /* Dll Imports */

  [DllImport("trapeze3.dll")]
  static extern uint AppendTrapezeOpenTIFFEx(IntPtr tiff,
                                             ref AppendTrapezeOpenTIFFExRec
                                             appendInfo);

  [DllImport("trapeze3.dll")]
  static extern IntPtr BurnInTrapezeAnnotations(IntPtr annotations,
                                                uint annotationsSize,
                                                IntPtr dibData, IntPtr dib,
                                                uint flags);

  [DllImport("trapeze3.dll", EntryPoint = "BurnInTrapezeAnnotations")]
  static extern IntPtr BurnInTrapezeAnnotations2(IntPtr annotations,
                                                 ref uint annotationsSize,
                                                 IntPtr dibData, IntPtr dib,
                                                 uint flags);

  [DllImport("trapeze3.dll")]
  static extern IntPtr BurnInTrapezeAnnotationsEx(ref
                                                 BurnInTrapezeAnnotationsExRec
                                                  burnInInfo);

  [DllImport("trapeze3.dll")]
  static extern uint CloseTrapezeTIFF(IntPtr tiff);

  [DllImport("trapeze3.dll")]
  static extern uint CloseTrapezeTIFFDocument(IntPtr tiffDoc);

  [DllImport("trapeze3.dll")]
  static extern uint ConvertTrapezePDFToTIFFEx(ref
                                               ConvertTrapezePDFToTIFFExRec
                                               convertInfo);

  [DllImport("trapeze3.dll")]
  static extern uint ConvertTrapezeTIFFToPDF(String srcFileName,
                                             String destFileName);

  [DllImport("trapeze3.dll")]
  static extern uint ConvertTrapezeTIFFToPDFEx(ref
                                               ConvertTrapezeTIFFToPDFExRec
                                               convertInfo);

  [DllImport("trapeze3.dll")]
  static extern uint CopyTrapezeTIFFDocumentPage(IntPtr tiffDoc, uint pageNo,
                                                 uint changePageNo, uint newPageNo,
                                                 uint newPageCount, IntPtr tiff);

  [DllImport("trapeze3.dll", EntryPoint = "CreateTrapezeWindow")]
  static extern IntPtr CreateTrpzWindow(UInt32 style, int x, int y,
                                        int width, int height,
                                        IntPtr parent, uint id);

  [DllImport("trapeze3.dll")]
  static extern uint DestroyTrapezeAnnotations(IntPtr annotations);

  [DllImport("trapeze3.dll")]
  static extern uint DestroyTrapezeBookmarks(IntPtr bookmarks);

  [DllImport("trapeze3.dll")]
  static extern uint GetLastTrapezeErrorCode();

  [DllImport("trapeze3.dll")]
  static extern uint GetLastTrapezeErrorMessage(StringBuilder str,
                                                ushort size);

  [DllImport("trapeze3.dll")]
  static extern uint GetTrapezeAnnotationProperty(IntPtr annotations,
                                                  uint annotationsSize,
                                                  uint annotationNo,
                                                  String property,
                                                  ref VariantRec value);

  [DllImport("trapeze3.dll")]
  static extern uint GetTrapezeAnnotationsCount(IntPtr annotations,
                                                uint annotationsSize);

  [DllImport("trapeze3.dll")]
  static extern IntPtr GetTrapezeBookmarkData(IntPtr bookmarks,
                                              ref uint size);

  [DllImport("trapeze3.dll")]
  static extern IntPtr GetTrapezeDIBData(IntPtr dib);

  [DllImport("trapeze3.dll")]
  static extern uint GetTrapezeDIBColorType(IntPtr dibData, IntPtr dib);

  [DllImport("trapeze3.dll")]
  static extern uint GetTrapezeDocumentType(String fileName);

  [DllImport("trapeze3.dll")]
  static extern uint GetTrapezeTIFFAnnotations(IntPtr tiff, uint ifdNo,
                                               ref IntPtr annotations,
                                               ref uint annotationsSize);

  [DllImport("trapeze3.dll")]
  static extern uint GetTrapezeTIFFCompression(IntPtr tiff, uint ifdNo,
                                               ref ushort compression);

  [DllImport("trapeze3.dll")]
  static extern IntPtr GetTrapezeTIFFDocumentBookmarks(IntPtr tiffDoc);

  [DllImport("trapeze3.dll")]
  static extern uint GetTrapezeTIFFDocumentIFDCount(IntPtr tiffDoc);

  [DllImport("trapeze3.dll")]
  static extern uint GetTrapezeTIFFDocumentIFDInfo(IntPtr tiffDoc, uint ifdNo,
                                                   ref
                                                 TrapezeTIFFDocumentIFDInfoRec
                                                   ifdInfo);

  [DllImport("trapeze3.dll")]
  static extern uint GetTrapezeTIFFDocumentPageCount(IntPtr tiffDoc);

  [DllImport("trapeze3.dll")]
  static extern IntPtr GetTrapezeTIFFDocumentTIFF(IntPtr tiffDoc);

  [DllImport("trapeze3.dll")]
  static extern ushort GetTrapezeVersion(StringBuilder version,
                                         ushort versionSize);

  [DllImport("kernel32.dll")]
  static extern IntPtr GlobalFree(IntPtr hMem);

  [DllImport("kernel32.dll")]
  static extern IntPtr GlobalLock(IntPtr hMem);

  [DllImport("kernel32.dll")]
  static extern uint GlobalUnlock(IntPtr hMem);

  [DllImport("user32.dll")]
  static extern uint MoveWindow(IntPtr window, int x, int y, int width,
                                int height, long repaint);

  [DllImport("trapeze3.dll")]
  static extern IntPtr OpenTrapezeTIFFDocument(String fileName);

  [DllImport("trapeze3.dll")]
  static extern IntPtr OpenTrapezeTIFFDocumentEx(ref
                                                 OpenTrapezeTIFFDocumentExRec
                                                 openInfo);

  [DllImport("trapeze3.dll")]
  static extern uint OCRTrapezeTIFF(String fileName, uint replace,
                                    IntPtr progressProc, IntPtr lParam);

  [DllImport("trapeze3.dll")]
  static extern IntPtr OpenTrapezeTIFF(String fileName, uint access,
                                       uint create);

  [DllImport("trapeze3.dll")]
  static extern IntPtr ReadTrapezeDIB(String fileName, uint pageNo);

  [DllImport("trapeze3.dll")]
  static extern IntPtr ReadTrapezeOpenTIFFDIB(IntPtr tiff, uint ifdNo);

  [DllImport("trapeze3.dll")]
  static extern IntPtr ReadTrapezeOpenTIFFDIBEx(ref
                                                ReadTrapezeOpenTIFFDIBExRec
                                                readInfo);

  [DllImport("trapeze3.dll")]
  static extern uint SaveTrapezeTIFFDIB(String fileName, IntPtr dib,
                                        ushort compression);

  [DllImport("trapeze3.dll")]
  static extern void SetLastTrapezeError(uint error);

  [DllImport("trapeze3.dll")]
  static extern uint SetTrapezeDebugLogFile(String fileName);

  [DllImport("trapeze3.dll")]
  static extern uint SetTrapezeLicense(String customer, String location,
                                       String key);

  [DllImport("user32.dll", EntryPoint = "SendMessageA")]
  static extern IntPtr SendMessageAnnotationPrefsBlob(IntPtr hWnd, uint Msg,
                                                      IntPtr wParam,
                                    ref TrapezeAnnotationPrefsBlobRec lParam);

  [DllImport("user32.dll", EntryPoint = "SendMessageA")]
  static extern IntPtr SendMessageLL(IntPtr hWnd, uint Msg,
                                     IntPtr wParam, IntPtr lParam);

  [DllImport("user32.dll", EntryPoint = "SendMessageA")]
  static extern IntPtr SendMessageLPUINT(IntPtr hWnd, uint Msg,
                                         IntPtr wParam, ref uint lParam);

  [DllImport("user32.dll", EntryPoint = "SendMessageA")]
  static extern IntPtr SendMessageLS(IntPtr hWnd, uint Msg,
                                     IntPtr wParam, String lParam);

  [DllImport("user32.dll", EntryPoint = "SendMessageA")]
  static extern IntPtr SendMessageOCRTextRect(IntPtr hWnd, uint Msg,
                                              IntPtr wParam,
                                         ref GetTrapezeOCRTextRectRec lParam);

  [DllImport("user32.dll", EntryPoint = "SendMessageA")]
  static extern IntPtr SendMessageOCRTextRects(IntPtr hWnd, uint Msg,
                                               IntPtr wParam,
                                        ref GetTrapezeOCRTextRectsRec lParam);

  [DllImport("user32.dll", EntryPoint = "SendMessageA")]
  static extern IntPtr SendMessageOCRTextText(IntPtr hWnd, uint Msg,
                                              IntPtr wParam,
                                         ref GetTrapezeOCRTextTextRec lParam);

  [DllImport("user32.dll", EntryPoint = "SendMessageA")]
  static extern IntPtr SendMessageRect(IntPtr hWnd, uint Msg, IntPtr wParam,
                                       ref Rect lParam);

  [DllImport("user32.dll", EntryPoint = "SendMessageA")]
  static extern IntPtr SendMessageSVariant(IntPtr hWnd, uint Msg,
                                           String wParam,
                                           ref VariantRec lParam);

  [DllImport("user32.dll", EntryPoint = "SendMessageA")]
  static extern IntPtr SendMessageTabletSettings(IntPtr hWnd, uint Msg,
                                                 IntPtr wParam,
                                                 ref GetTrapezeTabletSettingsPropertyRec lParam);

  [DllImport("trapeze3.dll")]
  static extern uint SetTrapezeAnnotationProperty(ref IntPtr annotations,
                                                  ref uint annotationsSize,
                                                  uint annotationNo,
                                                  String property,
                                                  ref VariantRec value);

  [DllImport("oleaut32.dll", CharSet=CharSet.Unicode)]
  static extern IntPtr SysAllocString(String str);

  [DllImport("oleaut32.dll")]
  static extern void SysFreeString(IntPtr bstr);

  [DllImport("oleaut32.dll")]
  static extern int VariantClear(ref VariantRec v);

} /* class Trapeze */

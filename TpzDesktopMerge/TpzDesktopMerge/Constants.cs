﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TpzDesktopMerge
{
    class Constants
    {
        //Error Codes
        public const int GENERAL_ERROR_CODE = 1;
        public const int DESTINATION_EXISTS = 2;
        public const int SOURCE_DOESNT_EXIST = 3;
        public const int LICENSE_INVALID = 4;
        public const int LICENSE_NOT_FOUND = 5;

        //Reading and Writing
        public const uint GENERIC_READ = 0x80000000;
        public const uint GENERIC_WRITE = 0x40000000;

        //Error Messages

        //Registry Keys
        public const string X86_TRAPEZE_LICENSE_KEY = @"Software\Onstream\Trapeze\3.0\License";
        public const string X64_TRAPEZE_LICENSE_KEY = @"Software\Wow6432Node\Onstream\Trapeze\3.0\License";

        //Trapeze3.dll location
        public const string TRAPEZE3_DDL_LOC = @"Trapeze3.dll";

        //Trapeze3.dll License Info
        public const string TRAPEZE3_CUSTOMER = @"Onstream Systems";
        public const string TRAPEZE3_LOCATION = @"Trapeze Document Utility";
        public const string TRAPEZE3_KEY = @"EAB79VSNQ9JL2THCX2WRNEDCMC";

    }
}

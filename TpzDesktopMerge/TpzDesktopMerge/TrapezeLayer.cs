﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.InteropServices;
using Microsoft.Win32;


namespace TpzDesktopMerge
{
    public class TrapezeLayer
    {
        //Constants

        
        //Global Variables
        private string dstFile = System.Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
        private uint TrapezePDF;
        private Structs.TrapezeLicenseInfo tpzLicInfo;
        

        //Constructor
        public TrapezeLayer()
        {
            #region Testing
            //bool test = SetTrapezeLicense("Redman Solutions", "Not For Resale", "7PE3Y95M8AJHPEQZQ8VJG6SD8G");
            //string New = "ate"     
            //"Onstream Systems", "Trapeze Document Utility", "EAB79VSNQ9JL2THCX2WRNEDCMC"
#endregion  
            //Initialise legacy data constructs
            tpzLicInfo = new Structs.TrapezeLicenseInfo();
            tpzLicInfo.expiryDate = new Structs.expiryDate();

            
            //Find and Set license for use with trapeze3.dll
            FindAndSetLicense();
        }


        public uint CreateNewPDF()
        {
            return TrapezePDF = CreateTrapezePDF(dstFile, null);
        }
        public uint CreateNewPDF(string dstFile)
        {
            this.dstFile = dstFile;
            return CreateNewPDF();
        } 

        //Open PDF File
        /*public uint OpenPDFFile(string filePath)
        {
            return OpenTrapezePDF(filePath, GENERIC_READ);
        }*/

        //Append PDF File (with Overload)
        public bool AppendPDFFile(string filePath)
        {
            uint appendFile = OpenTrapezePDF(filePath, Constants.GENERIC_READ);
            uint pageCount = GetTrapezePDFPageCount(appendFile);
            CloseTrapezePDF(appendFile);

            for (uint i = 1; i <= pageCount; i++)
            {
                if (!AppendTrapezePDFPage(TrapezePDF,filePath,i))
                {
                    return false;
                }
            }
            return true;


        } 
        public bool AppendPDFFile(string filePath, uint TrapezePDFID)
        {
            TrapezePDF = TrapezePDFID;
            return AppendPDFFile(filePath);
        }

        //Close PDF File (with Overloads)
        public bool ClosePDFFile(bool save)
        {
            if (save)
            {
                if (!SaveTrapezePDF(TrapezePDF))
                {
                    return false;
                }   
            }
            return CloseTrapezePDF(TrapezePDF);
        }
        public bool ClosePDFFile(bool save, uint TrapezePDFID)
        {
            TrapezePDF = TrapezePDFID;
            return ClosePDFFile(save);
        }

        //Find License and Set in registry
        private bool FindAndSetLicense()
        {

            string keyName;
            string arch = System.Environment.GetEnvironmentVariable("PROCESSOR_ARCHITECTURE", EnvironmentVariableTarget.Machine);
            string arch6432 = System.Environment.GetEnvironmentVariable("PROCESSOR_ARCHITEW6432", EnvironmentVariableTarget.Machine);

            if (arch == "AMD64" || arch6432 == "AMD64")
            {
                keyName = Constants.X64_TRAPEZE_LICENSE_KEY;
            }
            else
            {
                keyName = Constants.X86_TRAPEZE_LICENSE_KEY;
            }

            RegistryKey HKLMreg = Registry.LocalMachine.OpenSubKey(keyName, RegistryKeyPermissionCheck.ReadSubTree);
            RegistryKey HKCUreg = Registry.CurrentUser.OpenSubKey(keyName, RegistryKeyPermissionCheck.ReadSubTree);
           

            if (HKLMreg == null && HKCUreg == null)
            {
                throw new Exception(Constants.LICENSE_NOT_FOUND.ToString());
            }
            else if (HKLMreg != null)
            {
                tpzLicInfo.customer = (string)HKLMreg.GetValue("Customer");
                tpzLicInfo.location = (string)HKLMreg.GetValue("Location");
                tpzLicInfo.key = (string)HKLMreg.GetValue("Key");
                tpzLicInfo.size = (ushort)System.Runtime.InteropServices.Marshal.SizeOf(tpzLicInfo);
                tpzLicInfo.size = (ushort)System.Runtime.InteropServices.Marshal.SizeOf(tpzLicInfo);

                if (GetTrapezeLicense(ref tpzLicInfo))
                {
                    //License Found. Set .DLL license
                    SetTrapezeLicense(Constants.TRAPEZE3_CUSTOMER, Constants.TRAPEZE3_LOCATION, Constants.TRAPEZE3_KEY);
                } else
                {
                    throw new Exception(Constants.LICENSE_INVALID.ToString());
                }
                return true;
            } else if (HKCUreg != null)
            {
                tpzLicInfo.customer = (string)HKCUreg.GetValue("Customer");
                tpzLicInfo.location = (string)HKCUreg.GetValue("Location");
                tpzLicInfo.key = (string)HKCUreg.GetValue("Key");
                tpzLicInfo.size = (ushort)System.Runtime.InteropServices.Marshal.SizeOf(tpzLicInfo);
                tpzLicInfo.size = (ushort)System.Runtime.InteropServices.Marshal.SizeOf(tpzLicInfo);

                if (GetTrapezeLicense(ref tpzLicInfo))
                {
                    //License Found. Set .DLL license
                    SetTrapezeLicense(Constants.TRAPEZE3_CUSTOMER, Constants.TRAPEZE3_LOCATION, Constants.TRAPEZE3_KEY);
                }
                else
                {
                    throw new Exception(Constants.LICENSE_INVALID.ToString());
                }
                return true;
            }
            return false;
  
        }
       
        // DLL Imports
        #region DLL Imports

        //Set License
        [DllImport(Constants.TRAPEZE3_DDL_LOC)]
        public static extern bool SetTrapezeLicense(string customer, string location, string key);

        //Get License
        [DllImport(Constants.TRAPEZE3_DDL_LOC)]
        public static extern bool GetTrapezeLicense(ref Structs.TrapezeLicenseInfo tpzlicinfo);

        /* GetLastTrapezeErrorCode
           Returns the error code for the last error returned by Trapeze. This
           function is similar to GetLastError(), but will only return the last
           Trapeze error, so will work with languages that do not work correctly with
           GetLastError().
           Note: This function is not thread-safe; it returns the last available error
           code from any Trapeze function or message in any thread. */
        [DllImport(Constants.TRAPEZE3_DDL_LOC)]
        public static extern uint GetLastTrapezeErrorCode();

        /* GetLastTrapezeErrorMessage
           Returns the error message for the last error returned by Trapeze. The
           last error message is returned in str. size specifies the size of str, in
           characters. The return value is TRUE on success, FALSE on failure. On
           failure, use GetLastError() to get the cause of the failure. This function
           can return a more detailed error message than that obtained using
           GetTrapezeErrorMessage(GetLastError(), str, size) (e.g. a message for
           ocrTrapezeError will contain the error message returned from the OCR
           engine). The last error message is only changed when an error occurs, so
           this function should only be called after a Trapeze function or message has
           failed.
           Note: This function is not thread-safe; it returns the last available error
                 message from any Trapeze function or message in any thread. */
        [DllImport(Constants.TRAPEZE3_DDL_LOC)]
        public static extern bool GetLastTrapezeErrorMessage(string str, ushort size);

         /*CreateTrapezePDF
        Creates a PDF for appending pages to. fileName specifies the file for the
        new PDF; any existing file will be replaced. srcFileName specifies NULL to
        create an empty PDF, or a file containing a PDF to create a copy of an
        existing PDF. The return value is non-NULL on success or NULL on failure.
        On failure, use GetLastError() to get the cause of the failure.*/
        [DllImport(Constants.TRAPEZE3_DDL_LOC)]
        static extern uint CreateTrapezePDF(string fileName, string srcFileName);

        /* OpenTrapezePDF
        Opens a PDF. fileName specifies the file containing the PDF. access
        specifies the required file access:
        - trapezeReadAccess specifies read-only access
        - trapezeWriteAccess or (trapezeReadAccess | trapezeWriteAccess) specifies
            read/write access
        The return value is non-NULL on success or NULL on failure. On failure, use
        GetLastError() to get the cause of the failure. */
        //extern TrapezePDF TrapezeAPI OpenTrapezePDF(TrapezeLPCSTR fileName, TrapezeDWORD access);
        [DllImport(Constants.TRAPEZE3_DDL_LOC)]
        static extern uint OpenTrapezePDF(string fileName, uint access);

        /* AppendTrapezePDFPage
        Appends a page to a PDF created using CreateTrapezePDF(). fileName
        specifies the file to read the page from. pageNo specifies the page number
        (one based) of the document to read the page from. The return value is TRUE
        on success, or NULL on failure. On failure, use GetLastError() to get the
        cause of the error.
        SaveTrapezePDF() must be called before closing pdf to complete the
        changes. */
        [DllImport(Constants.TRAPEZE3_DDL_LOC)]
        static extern bool AppendTrapezePDFPage(uint pdf, string fileName, uint pageNo);
       
        /* SaveTrapezePDF
        Saves any changes made to pdf. The return value is TRUE on success or FALSE
        on failure. On failure, use GetLastError() to get the cause of the
        error. This function will fail with ERROR_ACCESS_DENIED if pdf was opened
        with read-only access. */
        //extern TrapezeBOOL TrapezeAPI SaveTrapezePDF(TrapezePDF pdf);
        [DllImport(Constants.TRAPEZE3_DDL_LOC)]
        static extern bool SaveTrapezePDF(uint pdf);

        /* CloseTrapezePDF
        Closes a PDF when it is no longer required. pdf specifies the PDF to close.
        pdf must not be used after this function is called. The return value is
        TRUE on success or FALSE on failure. On failure, use GetLastError() to get
        the cause of the failure.
        Any changes since pdf was last saved will be lost. */
        //extern TrapezeBOOL TrapezeAPI CloseTrapezePDF(TrapezePDF pdf);
        [DllImport(Constants.TRAPEZE3_DDL_LOC)]
        static extern bool CloseTrapezePDF(uint pdf);

        /* GetTrapezeVersion
         * 
         */
       [DllImport(Constants.TRAPEZE3_DDL_LOC)]
        static extern ushort GetTrapezeVersion(StringBuilder version, ushort versionSize);
        
        /* GetTrapezePDFPageCount
        Returns the number of pages in a PDF document, or
        getTrapezePDFPageCountFailed on failure. pdf specifies the PDF to retrieve
        the page count from. On failure, use GetLastError() to get the cause of the
        failure. */
        [DllImport(Constants.TRAPEZE3_DDL_LOC)]
        static extern uint GetTrapezePDFPageCount(uint pdf);

        /* SaveTrapezePDF
        Saves any changes made to pdf. The return value is TRUE on success or FALSE
        on failure. On failure, use GetLastError() to get the cause of the
        error. This function will fail with ERROR_ACCESS_DENIED if pdf was opened
        with read-only access. 
        extern TrapezeBOOL TrapezeAPI SaveTrapezePDF(TrapezePDF pdf);
        */
        //[DllImport("trapeze3.dll")]
        //static extern uint SaveTrapezePDF(TrapezePDF pdf);

        #endregion  
    }
}

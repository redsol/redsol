﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Diagnostics;

namespace TpzDesktopMerge
{
    class Program
    {

        //Trapeze3.dll does not support multithreading - must explicitly define single thread.
        [STAThread]
        static void Main(string[] args)
        {
            /* Command Line Parameters
            * args[0] Destination Path
            * args[1-*] Source Files
            * */
            string dstPath = "";
            List<String> srcPath = new List<string>();

            // Trap Ctrl+C to cancel process
            Console.TreatControlCAsInput = false;
            Console.CancelKeyPress += new ConsoleCancelEventHandler(CancelPressed);

            #region Testing            
            //--FOR TESTING PURPOSES ONLY
            //Check for debug commandline Argument and LaunchDebugger if found
            if (args[0] == "-debug" && Debugger.IsAttached == false)
            {
                Debugger.Launch();
               for(int i = 0; i < args.Length - 1; i++)
               {
                   args[i] = args[i + 1];
               }
               args[args.Length - 1] = "";
            }
            #endregion  

            //Check number of arguments
            if (args.Length < 3)
            {
                Console.WriteLine("Usage: %s DestinationPath SourcePath1 SourcePath2 SourcePath3 ... /n");
                System.Environment.Exit(Constants.GENERAL_ERROR_CODE);
            }

            dstPath = args[0];
            for (int i = 1; i < args.Length; i++)
            {
                if (args[i] != "")
                {
                    srcPath.Add(args[i]);
                }
               
            }
            

            //Check Paths are valid
            if(File.Exists(dstPath))
            {
                Console.WriteLine("Destination File Exists. Cannot Continue");
                System.Environment.Exit(Constants.DESTINATION_EXISTS);
            }

            foreach (string path in srcPath)
            {
                if(!File.Exists(path))
                {
                    Console.WriteLine("Source File " + path + " does not exist. Cannot Continue");
                    System.Environment.Exit(Constants.SOURCE_DOESNT_EXIST);
                }
            }

            try
            {
                // Create the new Trapeze Layer Class
                TrapezeLayer tpzPDF = new TrapezeLayer();

                // Create New Blank PDF
                tpzPDF.CreateNewPDF(dstPath);

                //Append Source Files to new PDF
                foreach (string path in srcPath)
                {
                    tpzPDF.AppendPDFFile(path);
                }
                //Close and Save
                tpzPDF.ClosePDFFile(true);
            }
            catch (Exception ex)
            {
                System.Environment.Exit(Convert.ToInt32(ex.Message));
            }
        }
        
        protected static void CancelPressed(object sender, ConsoleCancelEventArgs args)
        {
            args.Cancel = true;
        }
    }
}

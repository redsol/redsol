﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using System.Xml;
using System.IO;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using IWord = Microsoft.Office.Interop.Word;
using Office = Microsoft.Office.Core;
using Word = Microsoft.Office.Tools.Word;


namespace CDMSmith
{
    public partial class ThisAddIn
    {
        #region Declarations
        // Global Internal Data Structures
        public List<CDMStaff> staffList = new List<CDMStaff>();
        public List<CDMOffice> officeList = new List<CDMOffice>();
        private List<String> documentVersion = new List<String>();
        private List<CDMConfig> configuration = new List<CDMConfig>();

        // Add-In XML path locations
        private string installerPath;
        public string defaultTemplatesPath;


        // Global Variables
        //private string configXMLLocation = System.Environment.SpecialFolder["Program Files"];

        // Constants - Window Formatting
        // Display Constants
        private const int GRID_OBJECT_MARGIN = 5;
        private const int PROPERTIES_WINDOW_WIDTH = 600;
        private const int MAX_CONTROL_WIDTH = 350;
        private const int FOOTER_MARGIN = 10;
        private const int LGE_TEXTBOX_HEIGHT = 60;
        private const int BUTTON_WIDTH = 150;
        private const int ROW_BODY_MIN_HEIGHT = 35;
        private const int ROW_BODY_MAX_HEIGHT = 60;
        private const int BUTTON_MIN_HEIGHT = 60;

        // Name Constants
        private const string CUSTOM_XML_PART_NAME = "CDMSmith";
        private const string DISPLAYNAME_ATTRIBUTE_NAME = "DisplayName";
        private const string TYPE_ATTRIBUTE_NAME = "Type";
        private const string SOURCE_ATTRIBUTE_NAME = "Source";

        // Section Names
        private const string EXEC_SUMMARY_NAME = "ExecSummary";
        private const string BODY_NAME = "Body";
        private const string TABLES_NAME = "Tables";
        private const string PLATES_NAME = "Plates";
        private const string FIGURES_NAME = "Figures";
        private const string GLOSSARY_NAME = "Glossary";
        private const string APPENDIX_NAME = "Appendix";
        private const string DISCLAIMER_NAME = "Disclaimer";

        // Style Names
        private const string TABLE_STYLE_NAME = "LFT Table Style";
        private const string CAPTION_STYLE_NAME = "LFT Table Title";

        // Paths and File Names       
        private const string IMG_LOGO_PATH = @"pack://application:,,,/CDMSmith;component/Resources/logo.jpg";
        private const string XPATH_XML_ROOT_NODE = "/ns:CDMSmithProperties/*";
        public string FAX_FILE_NAME = "CDMSmith-Fax.dotx";
        public string LETTER_FILE_NAME = "CDMSmith-Letter.dotx";
        public string REPORT_FILE_NAME = "CDMSmith-Report.dotx";
        private const string CDMSTAFF_XML_NAME = "CDMStaff.xml";
        private const string CDMOFFICE_XML_NAME = "CDMOffice.xml";
        private const string CONFIG_XML_NAME = "Config.xml";

        #endregion


        private void ThisAddIn_Startup(object sender, System.EventArgs e)
        {
            // Set Global Variables
            defaultTemplatesPath = Globals.ThisAddIn.Application.Options.DefaultFilePath[IWord.WdDefaultFilePath.wdUserTemplatesPath];
            string programDataPath = System.Environment.GetFolderPath(System.Environment.SpecialFolder.CommonApplicationData);
            installerPath = programDataPath + @"\Redman Solutions\CDMSmith AddIn\";
            documentVersion.Add("Final");
            documentVersion.Add("Draft");

            // Load configuration from config.xml
            // Configuration options
            // -- File Path of CDMStaff.xml and CDMOffice.xml
            // -- Load Style Resource Dictionary
            // Look for Config.XML
            // if not found, notify user and say loading internal data from internal defaults

            try
            {
                // Looking for Config.xml ...

                if (File.Exists(installerPath + CONFIG_XML_NAME))
                {
                    Load_From_XML(installerPath + CONFIG_XML_NAME);

                } else if (File.Exists(defaultTemplatesPath + CONFIG_XML_NAME))
                {
                    Load_From_XML(defaultTemplatesPath + CONFIG_XML_NAME);

                }
                else
                {
                    // Not Found - load preconfigured defaults into the Data Structure.
                    MessageBox.Show("Up to date Staff and Office information could not be found. Information listed in the Add In may be out of date.", "Error", MessageBoxButton.OK, MessageBoxImage.Asterisk);

                    CDMConfig configurationItem = new CDMConfig();
                    configurationItem.itemName = "CDMStaffFilePath";
                    configurationItem.itemValue = Unpack_Embedded_Resource(CDMSTAFF_XML_NAME);
                    configuration.Add(configurationItem);
                    configurationItem = new CDMConfig();
                    configurationItem.itemName = "CDMOfficeFilePath";
                    configurationItem.itemValue = Unpack_Embedded_Resource(CDMOFFICE_XML_NAME);
                    configuration.Add(configurationItem);
                        }


                // Load CDMStaff.xml and CDMOffice.xml
                CDMConfig staffFilePath = configuration.Find(x => x.itemName == "CDMStaffFilePath");
                CDMConfig officeFilePath = configuration.Find(x => x.itemName == "CDMOfficeFilePath");

                if (File.Exists(staffFilePath.itemValue))
                {
                    Load_From_XML(staffFilePath.itemValue);
                }
                else
                {
                    throw new Exception("The XML File containing the CDM Staff list cannot be located. " + staffFilePath.itemValue);
                }

                if (File.Exists(officeFilePath.itemValue))
                {
                    Load_From_XML(officeFilePath.itemValue);
                }
                else
                {
                    throw new Exception("The XML File containing the CDM Office list cannot be located. " + officeFilePath.itemValue);
                }



            } 
            catch (Exception ex)
            {
                String messageInfo = @"There was an error loading the CDM Smith Add-In." + Environment.NewLine + Environment.NewLine;
                MessageBox.Show(messageInfo + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Asterisk);
            }
            
        }

        private void ThisAddIn_Shutdown(object sender, System.EventArgs e)
        {
        }

        private void Load_From_XML(String filename)
        {
            if (!File.Exists(filename)) throw new Exception("XML file does not exist at the location " + filename);

            XmlReader reader = XmlReader.Create(filename);
            

            if (reader == null) throw new Exception("XML Reader object failed to create from the file " + filename);

            reader.MoveToContent();

            switch (reader.Name)
            {
                case "CDMStaff":
                    
                    while (reader.ReadToFollowing("staff"))
                    {
                        CDMStaff staffItem = new CDMStaff();
                        staffItem.id = reader.GetAttribute("id");
                        reader.ReadToDescendant("name");
                        staffItem.name = reader.ReadElementContentAsString().Trim();
                        reader.ReadToNextSibling("title");
                        staffItem.title = reader.ReadElementContentAsString().Trim();
                        reader.ReadToNextSibling("office");
                        staffItem.office = reader.ReadElementContentAsString().Trim();
                        reader.ReadToNextSibling("phone");
                        staffItem.phone = reader.ReadElementContentAsString().Trim();
                        reader.ReadToNextSibling("username");
                        staffItem.username = reader.ReadElementContentAsString().Trim();
                        staffList.Add(staffItem);
                        
                        staffItem = null;
                    }
                    break;
                case "CDMOffice":
                    
                    while (reader.ReadToFollowing("office"))
                    {
                        CDMOffice officeItem = new CDMOffice();
                        officeItem.id = reader.GetAttribute("id");
                        reader.ReadToDescendant("name");
                        officeItem.name = reader.ReadElementContentAsString().Trim();
                        reader.ReadToNextSibling("street");
                        officeItem.street = reader.ReadElementContentAsString().Trim();
                        reader.ReadToNextSibling("suburb");
                        officeItem.suburb = reader.ReadElementContentAsString().Trim();
                        reader.ReadToNextSibling("state");
                        officeItem.state = reader.ReadElementContentAsString().Trim();
                        reader.ReadToNextSibling("postcode");
                        officeItem.postcode = reader.ReadElementContentAsString().Trim();
                        reader.ReadToNextSibling("phone");
                        officeItem.phone = reader.ReadElementContentAsString().Trim();
                        reader.ReadToNextSibling("fax");
                        officeItem.fax = reader.ReadElementContentAsString().Trim();
                        officeList.Add(officeItem);
                        officeItem = null;
                    }
                    break;
                case "CDMConfig":
                    
                    while (reader.ReadToFollowing("ConfigItem"))
                    {
                        CDMConfig configurationItem = new CDMConfig();

                        configurationItem.itemName = reader.GetAttribute("Name");
                        if (!reader.IsEmptyElement)
                        {
                            configurationItem.itemValue = reader.ReadElementContentAsString().Trim();
                        } else
                        {
                            configurationItem.itemValue = "";
                        }

                        configuration.Add(configurationItem);
                    }
                    break;     
            }
        
        }

        public string Unpack_Embedded_Resource(string name)
        {
            if (name == null) throw new Exception("Embedded Resource Name is Null");

            string filePath = Path.GetTempPath() + name;
            System.Reflection.Assembly asm = System.Reflection.Assembly.GetExecutingAssembly();
            Stream input = asm.GetManifestResourceStream("CDMSmith.Resources." + name);

            if (input == null) throw new Exception("Embedded Resource " + name + " was unable to be found");

            try
            {
                using (Stream output = File.Create(filePath))
                {
                    input.CopyTo(output);
                }

            } catch (Exception ex)
            {
                // File is locked by another process - maybe - anyways lets assume it is and send back the filepath
                return filePath;
            }

            return filePath;
        }

        protected override Microsoft.Office.Core.IRibbonExtensibility CreateRibbonExtensibilityObject()
        {
            return new CDMRibbon();
        }

        /// <summary>
        /// This function allows the user to edit the custom properties of the document linked to the custom controls.
        /// It looks for the customXML attached to the document and dynamically creates a form to edit
        /// the document properties.
        /// </summary>
        /// 
        public void Edit_Properties(string mode)
        {
            
            // Identify the Active Document from which to modify the document properties
            IWord.Document activeDoc = Globals.ThisAddIn.Application.ActiveDocument;

            try
            {
                // Select the CDMSmithCustomXMLPart object within our ActiveDocument
                Office.CustomXMLParts docPropertiesXML = activeDoc.CustomXMLParts.SelectByNamespace(CUSTOM_XML_PART_NAME);

                
                // Check to see that CustomXMLPart object was found
                if (docPropertiesXML.Count != 0)
                {
                    
                    //CDM Smith properties have been found. Lets dynamically create our task panel user control
                    DocPropertiesForm documentPropertiesForm = new DocPropertiesForm();
                    
                    // Lets create the Grid
                    Grid grid = new Grid();
                    grid.Width = PROPERTIES_WINDOW_WIDTH;
                    ColumnDefinition left = new ColumnDefinition();
                    left.Width = new GridLength(3, GridUnitType.Star);

                    ColumnDefinition right = new ColumnDefinition();
                    right.Width = new GridLength(7, GridUnitType.Star);
                    grid.ColumnDefinitions.Add(left);
                    grid.ColumnDefinitions.Add(right);

                    NameScope.SetNameScope(grid, new NameScope());
                    // Add in our header row
                    RowDefinition header = new RowDefinition();
                    header.Height = new GridLength(60);

                    grid.RowDefinitions.Add(header);

                    Image imgLogo = new Image();
                    imgLogo.Margin = new System.Windows.Thickness(GRID_OBJECT_MARGIN);
                    imgLogo.Name = "imgLogo";
                    imgLogo.SetValue(Grid.ColumnProperty, 0);
                    imgLogo.SetValue(Grid.RowProperty, 0);
                    imgLogo.Source = new BitmapImage(new Uri(IMG_LOGO_PATH));
                    grid.RegisterName(imgLogo.Name, imgLogo);
                    grid.Children.Add(imgLogo);

                    if (docPropertiesXML[1].NamespaceManager.LookupNamespace("CDMSmith") == null)
                    {
                        docPropertiesXML[1].NamespaceManager.AddNamespace("ns", "CDMSmith");
                    }

                    // Select all children nodes underneath CDMSmith Properties
                    Office.CustomXMLNodes xmlNodes = docPropertiesXML[1].SelectNodes(XPATH_XML_ROOT_NODE);
                    if (xmlNodes.Count == 0) throw new Exception("Unable to select XML Nodes from embedded XML document.");

                    foreach (Office.CustomXMLNode xmlNode in xmlNodes)
                   {
                       // Get Attributes of current node if any
                       Office.CustomXMLNodes attributes = xmlNode.Attributes;
                       string displayName = "";
                       string displayType = "";
                       string displaySource = "";
                       if (attributes.Count != 0)
                       {
                            // if the display name attribute is listed - present it.
                           foreach (Office.CustomXMLNode attribute in attributes)
                           {
                               if (attribute.BaseName == DISPLAYNAME_ATTRIBUTE_NAME)
                               {
                                   displayName = attribute.Text;
                               }
                               if (attribute.BaseName == TYPE_ATTRIBUTE_NAME)
                               {
                                   displayType = attribute.Text;
                               }
                               if (attribute.BaseName == SOURCE_ATTRIBUTE_NAME)
                               {
                                   displaySource = attribute.Text;
                               }
                           }
                       }

                       // Create new Row on our Form Grid
                       RowDefinition body = new RowDefinition();
                       body.MinHeight = ROW_BODY_MIN_HEIGHT;
                       body.MaxHeight = ROW_BODY_MAX_HEIGHT;

                       grid.RowDefinitions.Add(body);
                       
                       
                       // Create the Label and add it to the form
                       Label newLabel = new Label();
                       newLabel.Name = "lbl" + xmlNode.BaseName;
                       newLabel.Content = displayName + ":";
                       newLabel.Style = (Style)documentPropertiesForm.Resources["LabelStyle"];
                       newLabel.Margin = new System.Windows.Thickness(GRID_OBJECT_MARGIN);
                       newLabel.VerticalAlignment = System.Windows.VerticalAlignment.Center;
                       newLabel.HorizontalAlignment = System.Windows.HorizontalAlignment.Right;
                       newLabel.SetValue(Grid.ColumnProperty, 0);
                       newLabel.SetValue(Grid.RowProperty, grid.RowDefinitions.Count - 1);
                       grid.RegisterName(newLabel.Name, newLabel);
                       grid.Children.Add(newLabel);

                       
                       switch (displayType)
                       {
                            case "Combo":
                                // Create new Combo Box and format it
                                
                                ComboBox newCmbbox = new ComboBox();
                                newCmbbox.Name = xmlNode.BaseName;
                                newCmbbox.Margin = new System.Windows.Thickness(GRID_OBJECT_MARGIN);
                                newCmbbox.VerticalAlignment = System.Windows.VerticalAlignment.Center;
                                newCmbbox.MaxWidth = MAX_CONTROL_WIDTH;
                                newCmbbox.SetValue(Grid.ColumnProperty, 1);
                                newCmbbox.SetValue(Grid.RowProperty, grid.RowDefinitions.Count - 1);

                                if (displaySource == "CDMStaff")
                                {
                                    foreach (CDMStaff staff in staffList)
                                    {
                                        newCmbbox.Items.Add(staff.name);
                                        if (mode == "New" && Environment.UserName == staff.username && xmlNode.BaseName == "Author")
                                        {
                                            newCmbbox.SelectedItem = staff.name;
                                            newCmbbox.IsEnabled = false;
                                        }
                                    }
                                } else if (displaySource == "CDMOffice")
                                {
                                    foreach (CDMOffice office in officeList)
                                    {
                                        newCmbbox.Items.Add(office.name);
                                    }
                                } else if (displaySource == "DocVersion")
                                {
                                    foreach (String versionItem in documentVersion)
                                    {
                                        newCmbbox.Items.Add(versionItem);
                                    }
                                }

                                if (newCmbbox.SelectedIndex == -1)
                                {
                                    newCmbbox.SelectedItem = xmlNode.Text;
                                }

                                
                                
                                grid.RegisterName(newCmbbox.Name, newCmbbox);
                                grid.Children.Add(newCmbbox);
                                break;
                
                           case "Date":
                                DatePicker selectDate = new DatePicker();
                                selectDate.Name = xmlNode.BaseName;
                                selectDate.Margin = new System.Windows.Thickness(GRID_OBJECT_MARGIN);
                                selectDate.MaxWidth = MAX_CONTROL_WIDTH;
                                selectDate.VerticalAlignment = System.Windows.VerticalAlignment.Center;
                                selectDate.SelectedDateFormat = DatePickerFormat.Long;
                                selectDate.SetValue(Grid.ColumnProperty, 1);
                                selectDate.SetValue(Grid.RowProperty, grid.RowDefinitions.Count - 1);
                               
                                if (xmlNode.Text == " " || xmlNode.Text == null)
                                {
                                    selectDate.SelectedDate = DateTime.Now;
                                }
                                else
                                {
                                    selectDate.SelectedDate = Convert.ToDateTime(xmlNode.Text);
                                }
                               
                                grid.RegisterName(selectDate.Name, selectDate);
                                grid.Children.Add(selectDate);
                                break;

                           case "Text":
                                TextBox newTxtbox = new TextBox();
                                newTxtbox.Name = xmlNode.BaseName;
                                newTxtbox.Text = xmlNode.Text;
                                newTxtbox.Margin = new System.Windows.Thickness(GRID_OBJECT_MARGIN);
                                newTxtbox.MaxWidth = MAX_CONTROL_WIDTH;
                                newTxtbox.VerticalAlignment = System.Windows.VerticalAlignment.Center;
                                newTxtbox.SetValue(Grid.ColumnProperty, 1);
                                newTxtbox.SetValue(Grid.RowProperty, grid.RowDefinitions.Count - 1);
                                grid.RegisterName(newTxtbox.Name, newTxtbox);
                                grid.Children.Add(newTxtbox);
                                break;
                           case "LargeText":
                                TextBox newLgeTxtbox = new TextBox();
                                newLgeTxtbox.Name = xmlNode.BaseName;
                                newLgeTxtbox.Text = xmlNode.Text;
                                newLgeTxtbox.Margin = new System.Windows.Thickness(GRID_OBJECT_MARGIN);
                                newLgeTxtbox.MaxWidth = MAX_CONTROL_WIDTH;
                                newLgeTxtbox.VerticalAlignment = System.Windows.VerticalAlignment.Top;
                                newLgeTxtbox.Height = LGE_TEXTBOX_HEIGHT;
                                newLgeTxtbox.AcceptsReturn = true;
                                newLgeTxtbox.SetValue(Grid.ColumnProperty, 1);
                                newLgeTxtbox.SetValue(Grid.RowProperty, grid.RowDefinitions.Count - 1);
                                newLgeTxtbox.SetValue(Grid.RowSpanProperty, 2);
                                grid.RegisterName(newLgeTxtbox.Name, newLgeTxtbox);
                                grid.Children.Add(newLgeTxtbox);
                                RowDefinition largeText = new RowDefinition();
                                largeText.Height = new GridLength(LGE_TEXTBOX_HEIGHT);
                                grid.RowDefinitions.Add(largeText);
                                break;
                            case "CheckboxGroup":
                                StackPanel checkPanel = new StackPanel();
                                NameScope.SetNameScope(checkPanel, new NameScope());
                                checkPanel.Name = xmlNode.BaseName;
                                checkPanel.MaxWidth = MAX_CONTROL_WIDTH;
                                checkPanel.Orientation = Orientation.Vertical;
                                checkPanel.SetValue(Grid.RowSpanProperty, 3);
                                checkPanel.SetValue(Grid.ColumnProperty, 1);
                                checkPanel.SetValue(Grid.RowProperty, grid.RowDefinitions.Count - 1);
                           
                                
                                foreach (Office.CustomXMLNode childNode in xmlNode.ChildNodes)
                                {

                                    if (childNode.Attributes.Count != 0)
                                    {
                                        CheckBox chk = new CheckBox();
                                        chk.Name = childNode.BaseName;
                                        
                                        

                                        foreach (Office.CustomXMLNode attribute in childNode.Attributes)
                                        {
                                            if (attribute.BaseName == "Type" && attribute.Text != "Checkbox") break;
                                            if (attribute.BaseName == "DisplayName")
                                            {
                                                chk.Content = attribute.Text;
                                            }
                                        }

                                        if (childNode.Text == "True")
                                        {
                                            chk.IsChecked = true;

                                        }
                                        else if (childNode.Text == "False" || childNode.Text == "")
                                        {
                                            chk.IsChecked = false;

                                        }
                                        checkPanel.RegisterName(chk.Name, chk);
                                        checkPanel.Children.Add(chk);
                                        chk = null;
                                    }

                                }
                                grid.RegisterName(checkPanel.Name, checkPanel);
                                grid.RowDefinitions.Add(new RowDefinition());
                                grid.Children.Add(checkPanel);

                                RowDefinition bufferRow = new RowDefinition();
                                bufferRow.Height = new GridLength(LGE_TEXTBOX_HEIGHT);
                                grid.RowDefinitions.Add(bufferRow);
                                break;
                            case "CheckBox":
                                break;
                           default:
                               throw new Exception("The XML Element " + xmlNode.BaseName + " does not have a 'Type' attribute applied. There is a problem with the underlying XML properties of this document");
                               
                       }
         
                   }

                   // All elements from XML were added to the form - now lets Add Ok and Cancel buttons
                   // Create new Row on our Form Grid
                    RowDefinition footer = new RowDefinition();
                    footer.MinHeight = BUTTON_MIN_HEIGHT;
                    grid.RowDefinitions.Add(footer);

                    Grid footerGrid = new Grid();
                    footerGrid.ColumnDefinitions.Add(new ColumnDefinition());
                    footerGrid.ColumnDefinitions.Add(new ColumnDefinition());

                    footerGrid.RowDefinitions.Add(new RowDefinition());
                    footerGrid.SetValue(Grid.ColumnProperty, 0);
                    footerGrid.SetValue(Grid.RowProperty, grid.RowDefinitions.Count - 1);
                    footerGrid.SetValue(Grid.ColumnSpanProperty, 2);
                    footerGrid.Margin = new Thickness(FOOTER_MARGIN);
                    
                    Button okBtn = new Button();
                    okBtn.Name = "okBtn";
                    okBtn.Style = (Style) documentPropertiesForm.Resources["GradientButton"];
                    okBtn.Margin = new System.Windows.Thickness(GRID_OBJECT_MARGIN);             
                    okBtn.Content = "OK";
                    okBtn.Width = BUTTON_WIDTH;
                    okBtn.Click += (sender, e) => Edit_Properties_Ok_Click(documentPropertiesForm);
                    okBtn.SetValue(Grid.ColumnProperty, 0);
                    
                    //footerGrid.RegisterName(okBtn.Name, okBtn);
                    footerGrid.Children.Add(okBtn);

                    Button cancelBtn = new Button();
                    cancelBtn.Name = "cancelBtn";
                    cancelBtn.Style = (Style)documentPropertiesForm.Resources["GradientButton"];
                    cancelBtn.Width = BUTTON_WIDTH;
                    cancelBtn.Margin = new System.Windows.Thickness(GRID_OBJECT_MARGIN);
                    cancelBtn.Content = "Cancel";
                    cancelBtn.Click += (sender, e) => Cancel_Button_Click(documentPropertiesForm);
                    cancelBtn.SetValue(Grid.ColumnProperty, 1);
                    //footerGrid.RegisterName(cancelBtn.Name, cancelBtn);
                    footerGrid.Children.Add(cancelBtn);

                    grid.Children.Add(footerGrid);

                    //Assign the newly created grid to the XAML window and show
                    documentPropertiesForm.WindowStartupLocation = WindowStartupLocation.CenterScreen;
                    documentPropertiesForm.Content = grid;
                    if (mode == "Edit")
                    {
                        documentPropertiesForm.Title = "Edit Document Properties";
                    } else if (mode == "New")
                    {
                        documentPropertiesForm.Title = "Create New Document";
                    }
                    
                    documentPropertiesForm.Show();
                  
                    
                } else if (docPropertiesXML.Count == 0)
                {
                    throw new Exception("CDM Smith document properties were not found with this document. The document you have open was not created with the latest CDM Smith Australia templates.");
                }

            } catch (Exception ex)
            {
                String messageInfo = @"There was an error editing the document properties." + Environment.NewLine + Environment.NewLine;
                MessageBox.Show(messageInfo + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Asterisk);
            }

        }

        public void Edit_Properties_Ok_Click(DocPropertiesForm form)
        {
            // Place updated information from the form back into the CustomXMLPart
            
             // Identify the Active Document from which to modify the document properties
            IWord.Document activeDoc = Globals.ThisAddIn.Application.ActiveDocument;
            try
            {
                // Select the CDMSmithCustomXMLPart object within our ActiveDocument
                Office.CustomXMLParts docPropertiesXML = activeDoc.CustomXMLParts.SelectByNamespace(CUSTOM_XML_PART_NAME);

                // Check to see that CustomXMLPart object was found
                if (docPropertiesXML.Count != 0)
                {
                    // CustomXMLPart Found - lets begin updating it
                    Office.CustomXMLNodes xmlNodes = docPropertiesXML[1].SelectNodes(XPATH_XML_ROOT_NODE);
                    Grid grid = (Grid)form.Content;

                    // Validation - can be expanded for all control types.
                    // Check to ensure all form combo boxes have been selected
                   
                    foreach (object gridChild in grid.Children)
                    {
                        if (gridChild is ComboBox)
                        {
                            ComboBox cmb = (ComboBox)gridChild;
                            if (cmb.SelectedItem == null)
                            {
                                throw new Exception(cmb.Name + " has not been selected. Please ensure you have selected the " + cmb.Name);
                            }
                        } 
                    }

                    foreach (Office.CustomXMLNode xmlNode in xmlNodes)
                    {
                        // Find object on Form Grid that matches xml element
                        var formObject = grid.FindName(xmlNode.BaseName);
                        if (formObject != null)
                        {
                            // Corresponding Element found in XML - lets update it.
                            // Get Attributes of current node if any
                            Office.CustomXMLNodes attributes = xmlNode.Attributes;
                            string displayName = "";
                            string displayType = "";
                            string displaySource = "";
                            if (attributes.Count == 0) throw new Exception("No attributes found on the " + xmlNode.BaseName + " element. There is a problem with the underlying XML in this document");
                                       
                            // if the display name attribute is listed - present it.
                            foreach (Office.CustomXMLNode attribute in attributes)
                            {
                                if (attribute.BaseName == DISPLAYNAME_ATTRIBUTE_NAME)
                                {
                                    displayName = attribute.Text;
                                }
                                if (attribute.BaseName == TYPE_ATTRIBUTE_NAME)
                                {
                                    displayType = attribute.Text;
                                }
                                if (attribute.BaseName == SOURCE_ATTRIBUTE_NAME)
                                {
                                    displaySource = attribute.Text;
                                }
                            }

                            if (displayType == null) throw new Exception("DisplayType attribute not found on the " + xmlNode.BaseName + " element. There is a problem with the underlying XML in this document");

                            switch(displayType)
                            {
                                case "Text":
                                    if (formObject.GetType() != typeof(TextBox)) throw new Exception("Mismatch between form object type and type specified in the XML. There is a problem with the underlying XML in this document.");
                                    TextBox txt = (TextBox)formObject;
                                    xmlNode.Text = txt.Text.Trim();
                                    break;
                                case "LargeText":
                                    if (formObject.GetType() != typeof(TextBox)) throw new Exception("Mismatch between form object type and type specified in the XML. There is a problem with the underlying XML in this document.");
                                    TextBox lgeTxt = (TextBox)formObject;
                                    xmlNode.Text = lgeTxt.Text.Trim();
                                    break;
                                case "Combo":
                                    if (formObject.GetType() != typeof(ComboBox)) throw new Exception("Mismatch between form object type and type specified in the XML. There is a problem with the underlying XML in this document.");
                                    ComboBox cmb = (ComboBox)formObject;
                                    
                                    if (displaySource == "CDMStaff")
                                    {
                                        if (cmb.SelectedItem == null) throw new Exception(displayName + " has not been selected. Please ensure you have selected the " + displayName);
                                        CDMStaff staff = staffList.Find(x => x.name == cmb.SelectedItem.ToString());
                                        
                                        if (staff == null) throw new Exception("Staff Member not found in CDM Staff list. There is a problem with the underlying XML in this document.");
                                        xmlNode.Text = staff.name.ToString().Trim();
                                        foreach (Office.CustomXMLNode attribute in xmlNode.Attributes)
                                        {

                                            switch (attribute.BaseName)
                                            {
                                                case "ID":
                                                    attribute.Text = staff.id;
                                                    break;

                                                case "Title":
                                                    attribute.Text = staff.title;
                                                    break;

                                                case "Phone":
                                                    attribute.Text = staff.phone;
                                                    break;
                                            }
                                        }
                                    } else if (displaySource == "CDMOffice")
                                    {
                                        if (cmb.SelectedItem == null) throw new Exception("Office has not been selected. Please ensure you have an Office selected in the drop down list");
                                       
                                        CDMOffice office = officeList.Find(x => x.name == cmb.SelectedItem.ToString());
                                        if (office == null) throw new Exception("Office not found in CDM Office list. There is a problem with the underlying XML in this document.");
                                        xmlNode.Text = office.name.ToString().Trim();
                                        foreach (Office.CustomXMLNode attribute in xmlNode.Attributes)
                                        {

                                            switch (attribute.BaseName)
                                            {
                                                case "ID":
                                                    attribute.Text = office.id;
                                                    break;
                                                case "Street":
                                                    attribute.Text = office.street;
                                                    break;
                                                case "Suburb":
                                                    attribute.Text = office.suburb;
                                                    break;
                                                case "State":
                                                    attribute.Text = office.state;
                                                    break;
                                                case "PostCode":
                                                    attribute.Text = office.postcode;
                                                    break;
                                                case "Phone":
                                                    attribute.Text = office.phone;
                                                    break;
                                                case "Fax":
                                                    attribute.Text = office.fax;
                                                    break;
                                            }
                                        }
                                    } else if (displaySource == "DocVersion")
                                    {
                                        if (cmb.SelectedItem == null) throw new Exception("Version has not been selected. Please ensure you have Version selected in the drop down list");

                                        xmlNode.Text = cmb.SelectedItem.ToString();

                                    } else
                                    {
                                        throw new Exception("The Display Source of " + displaySource + " is invalid. There is a problem with the underlying XML of this document");
                                    }
                                    
                                    break;
                                case "Date":
                                    if (formObject.GetType() != typeof(DatePicker)) throw new Exception("Mismatch between form object type and type specified in the XML. There is a problem with the underlying XML in this document.");
                                    DatePicker date = (DatePicker)formObject;
                                    xmlNode.Text = date.Text;
                                    break;
                                case "CheckboxGroup":
                                    if (!(formObject is StackPanel)) throw new Exception("Mismatch between form object type and type specified in the XML. There is a problem with the underlying XML in this document.");
                                    StackPanel checkPanel = (StackPanel)formObject;
                                    foreach (object panelObject in checkPanel.Children)
                                    {
                                        if(panelObject is CheckBox)
                                        {
                                           
                                            CheckBox chk = new CheckBox();
                                            chk = (CheckBox)panelObject;
                                            foreach (Office.CustomXMLNode childNode in xmlNode.ChildNodes)
                                            {
                                                if (childNode.Attributes.Count != 0)
                                                {
                                                    if (chk.Name == childNode.BaseName)
                                                    {
                                                        if (chk.IsChecked == true)
                                                        {
                                                            Show_Section(childNode.BaseName.ToString());

                                                        } else if (chk.IsChecked == false)
                                                        {
                                                            Hide_Section(childNode.BaseName.ToString());
                                                        }

                                                        childNode.Text = chk.IsChecked.ToString();
                                                    
                                                        break;
                                                    }
                                                }
                                            }
                                        }
                                    }

                                    

                                    break;
                                default:
                                    throw new Exception("Unknown type specified for Element " + xmlNode.BaseName + ". There is a problem with the underlying XML of this document");
                            }
                           

                        }
                    }

                form.Close();
                }

            } catch (Exception ex)
            {
                String messageInfo = @"There was an error updating the document properties." + Environment.NewLine + Environment.NewLine;
                MessageBox.Show(messageInfo + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Asterisk);
            }


        }

        private void Show_Section(string sectionName)
        {
            IWord.Document activeDoc = Globals.ThisAddIn.Application.ActiveDocument;
            activeDoc.Bookmarks.ShowHidden = true;

            foreach (IWord.Bookmark bookmark in activeDoc.Bookmarks)
            {
                if (bookmark.Name == "_" + sectionName)
                {
                    bookmark.Range.Font.Hidden = 0;
                }
            }

            activeDoc.Bookmarks.ShowHidden = false;
        }
              
        private void Hide_Section(string sectionName)
        {
            IWord.Document activeDoc = Globals.ThisAddIn.Application.ActiveDocument;
            activeDoc.Bookmarks.ShowHidden = true;

            foreach (IWord.Bookmark bookmark in activeDoc.Bookmarks)
            {
                if (bookmark.Name == "_" + sectionName)
                {
                    bookmark.Range.Font.Hidden = 1;
                }
            }

            activeDoc.Bookmarks.ShowHidden = false;
        }

        private int Is_Hidden(string sectionName)
        {
            IWord.Document activeDoc = Globals.ThisAddIn.Application.ActiveDocument;

            foreach (IWord.Bookmark bookmark in activeDoc.Bookmarks)
            {
                if (bookmark.Name == "_" + sectionName)
                {
                    if (bookmark.Range.Font.Hidden == 1)
                    {
                        return 1;
                    } else if (bookmark.Range.Font.Hidden == 0)
                    {
                        return 0;
                    }
                }
            }
            return 2;
        }
        public void Cancel_Button_Click(DocPropertiesForm form)
        {
            form.Close();
            Globals.ThisAddIn.Application.ActiveDocument.Close(IWord.WdSaveOptions.wdDoNotSaveChanges);
        }
        
        public void Insert_Table()
        {
            IWord.Document activeDoc = Globals.ThisAddIn.Application.ActiveDocument;
            try
            {
                if (Application.Selection.Information[IWord.WdInformation.wdWithInTable] == true)
                {
                    throw new Exception("To insert a new Table, your cursor must not be within another table");
                }

                InsertTable newTable = new InsertTable();
                newTable.Show();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Asterisk);
            }
            
        }

        private void Build_Table(int rows, int columns, string caption)
        {

            IWord.Document activeDoc = Globals.ThisAddIn.Application.ActiveDocument;

            string captionTitleText = "Table ";
            string captionLabel = "Table";
            
            // Is the current cursor in the tables section bookmark?
    
            activeDoc.Bookmarks.ShowHidden = true;

            foreach (IWord.Bookmark bookmark in Application.Selection.Bookmarks)
            {
                if (bookmark.Name == "_" + TABLES_NAME)
                {
                    captionTitleText = "Table T";
                    captionLabel = "Table_Section";
                }
                
            }
            activeDoc.Bookmarks.ShowHidden = false;

            // Does table style exist?

            if (!Style_Exists(TABLE_STYLE_NAME))
            {
                throw new Exception("Cannot find the Table Style. Are you using a CDM Smith Australia document?");
            }
            if (!Style_Exists(CAPTION_STYLE_NAME))
            {
                throw new Exception("Cannot find the Caption Style. Are you using a CDM Smith Australia document?");

            }

            //Create the table
            this.Application.Selection.TypeParagraph();
            IWord.Table newTable = activeDoc.Tables.Add(this.Application.Selection.Range, rows, columns);


            // Set the style
            newTable.set_Style(TABLE_STYLE_NAME);

            // Add the Caption
            newTable.Select();
            Application.Selection.MoveUp(IWord.WdUnits.wdLine);
            Application.Selection.InsertCaption(captionLabel, null, null, IWord.WdCaptionPosition.wdCaptionPositionAbove , true);
            Application.Selection.MoveStart(IWord.WdUnits.wdCharacter);
            Application.Selection.MoveUp(IWord.WdUnits.wdLine);
            Application.Selection.set_Style(activeDoc.Styles[CAPTION_STYLE_NAME]);
            Application.Selection.HomeKey(IWord.WdUnits.wdLine);
            Application.Selection.TypeText(captionTitleText);
            Application.Selection.EndKey(IWord.WdUnits.wdLine);
            Application.Selection.TypeText(" " + caption);
            Application.Selection.MoveDown(IWord.WdUnits.wdLine);

        }

        private bool Style_Exists(string styleName)
        {
            IWord.Document activeDoc = Globals.ThisAddIn.Application.ActiveDocument;

            foreach (IWord.Style docStyle in activeDoc.Styles)
            {
                if (docStyle.NameLocal == styleName)
                {
                    return true;
                }
            }
            return false;
        }

        public void Insert_Table_Ok_Click(object sender)
        {
            InsertTable newTable = (InsertTable)sender;

            // Build Table
            try
            {
                Build_Table(Convert.ToInt32(newTable.txtNumRow.Text), Convert.ToInt32(newTable.txtNumCol.Text), newTable.txtCaption.Text);
                newTable.Close();

            } catch (Exception ex)
            {
                String messageInfo = @"There was an error creating the Table." + Environment.NewLine + Environment.NewLine;
                MessageBox.Show(messageInfo + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Asterisk);
            }

        }

        public void Insert_Table_Cancel_Click(object sender)
        {
            InsertTable newtable = (InsertTable)sender;
            newtable.Close();
        }
        
        public void Insert_Figure()
        {
            IWord.Document activeDoc = Globals.ThisAddIn.Application.ActiveDocument;
            try
            {
                if (Application.Selection.Information[IWord.WdInformation.wdWithInTable] == true)
                {
                    throw new Exception("To insert a new Figure, your cursor must not be within a table");
                }

                InsertFigure newFigure = new InsertFigure();
                newFigure.Show();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Asterisk);
            }
        }

        public void Insert_Figure_Ok_Click(object sender)
        {
            InsertFigure newFigure = (InsertFigure)sender;

            // Build Figure
            try
            {
                Build_Figure(newFigure.txtCaption.Text);
                newFigure.Close();

            }
            catch (Exception ex)
            {
                String messageInfo = @"There was an error creating the Table." + Environment.NewLine + Environment.NewLine;
                MessageBox.Show(messageInfo + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Asterisk);
            }
        }

        private void Build_Figure(string caption)
        {
            IWord.Document activeDoc = Globals.ThisAddIn.Application.ActiveDocument;

            string captionTitleText = "Figure ";
            string captionLabel = "Figure";

            // Is the current cursor in the tables section bookmark?
           
            activeDoc.Bookmarks.ShowHidden = true;

            foreach (IWord.Bookmark bookmark in Application.Selection.Bookmarks)
            {
                if (bookmark.Name == "_" + FIGURES_NAME)
                {
                    captionTitleText = "Figure F";
                    captionLabel = "Figure_Section";
                }

            }
            activeDoc.Bookmarks.ShowHidden = false;

            // Does caption style exist?
            if (!Style_Exists(CAPTION_STYLE_NAME))
            {
                throw new Exception("Cannot find the Caption Style. Are you using a CDM Smith Australia document?");

            }

            // Create Figure Caption
            this.Application.Selection.TypeParagraph();

            Application.Selection.MoveUp(IWord.WdUnits.wdLine);
            Application.Selection.InsertCaption(captionLabel, null, null, IWord.WdCaptionPosition.wdCaptionPositionAbove, true);
            Application.Selection.MoveStart(IWord.WdUnits.wdCharacter);
            Application.Selection.MoveUp(IWord.WdUnits.wdLine);
            Application.Selection.set_Style(activeDoc.Styles[CAPTION_STYLE_NAME]);
            Application.Selection.HomeKey(IWord.WdUnits.wdLine);
            Application.Selection.TypeText(captionTitleText);
            Application.Selection.EndKey(IWord.WdUnits.wdLine);
            Application.Selection.TypeText(" " + caption);
            Application.Selection.MoveDown(IWord.WdUnits.wdLine);

        }
        public void Insert_Figure_Cancel_Click(object sender)
        {

        }

        public void Insert_Plate()
        {

        }

        public void Insert_Plate_Ok_Click(object sender)
        {

        }

        public void Insert_Plate_Cancel_Click(object sender)
        {
        }

        public void Add_Irregular_Page(string size, string orientation)
        {

        }


        #region VSTO generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InternalStartup()
        {
            this.Startup += new System.EventHandler(ThisAddIn_Startup);
            this.Shutdown += new System.EventHandler(ThisAddIn_Shutdown);
        }
        
        #endregion
    }
}

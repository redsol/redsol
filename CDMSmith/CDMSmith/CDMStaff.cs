﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CDMSmith
{
    /// <summary>
    /// Class for storing Staff objects from XML file
    /// </summary>
    public class CDMStaff
    {
        public string id { get; set; }
        public string name { get; set; }
        public string title { get; set; }
        public string office { get; set; }
        public string phone { get; set; }
        public string username { get; set; }

    }
}

﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Windows;
using Office = Microsoft.Office.Core;
using Word = Microsoft.Office.Interop.Word;
// TODO:  Follow these steps to enable the Ribbon (XML) item:

// 1: Copy the following code block into the ThisAddin, ThisWorkbook, or ThisDocument class.

//  protected override Microsoft.Office.Core.IRibbonExtensibility CreateRibbonExtensibilityObject()
//  {
//      return new Ribbon1();
//  }

// 2. Create callback methods in the "Ribbon Callbacks" region of this class to handle user
//    actions, such as clicking a button. Note: if you have exported this Ribbon from the Ribbon designer,
//    move your code from the event handlers to the callback methods and modify the code to work with the
//    Ribbon extensibility (RibbonX) programming model.

// 3. Assign attributes to the control tags in the Ribbon XML file to identify the appropriate callback methods in your code.  

// For more information, see the Ribbon XML documentation in the Visual Studio Tools for Office Help.


namespace CDMSmith
{
    [ComVisible(true)]
    public class CDMRibbon : Office.IRibbonExtensibility
    {
        private Office.IRibbonUI ribbon;
         
        public CDMRibbon()
        {
        }

        #region IRibbonExtensibility Members

        public string GetCustomUI(string ribbonID)
        {
            return GetResourceText("CDMSmith.CDMRibbon.xml");
        }

        #endregion

        #region Ribbon Callbacks
        //Create callback methods here. For more information about adding callback methods, visit http://go.microsoft.com/fwlink/?LinkID=271226

        public void Ribbon_Load(Office.IRibbonUI ribbonUI)
        {
            this.ribbon = ribbonUI;
        }

        // Template Generators
        public void New_Fax(Office.IRibbonControl control)
        {

            Globals.ThisAddIn.Application.Documents.Add(Globals.ThisAddIn.Unpack_Embedded_Resource(Globals.ThisAddIn.FAX_FILE_NAME));
            Globals.ThisAddIn.Edit_Properties("New");
            
        }

        public void New_Letter(Office.IRibbonControl control)
        {
            Globals.ThisAddIn.Application.Documents.Add(Globals.ThisAddIn.Unpack_Embedded_Resource(Globals.ThisAddIn.LETTER_FILE_NAME));
            Globals.ThisAddIn.Edit_Properties("New");
        }

        public void New_Report(Office.IRibbonControl control)
        {
            Globals.ThisAddIn.Application.Documents.Add(Globals.ThisAddIn.Unpack_Embedded_Resource(Globals.ThisAddIn.REPORT_FILE_NAME));
            Globals.ThisAddIn.Edit_Properties("New");
        }

        public void Insert_Table(Office.IRibbonControl control)
        {
            Globals.ThisAddIn.Insert_Table();
        }
        public void Ribbon_Edit_Properties(Office.IRibbonControl control)
        {
            Globals.ThisAddIn.Edit_Properties("Edit");
        }
       
        /*public void Insert_Table(Office.IRibbonControl control)
        {
            Globals.ThisAddIn.Insert_Table();
        }
    
        public void Update_All_Fields(Office.IRibbonUI ribbonUI);
        public void Insert_Figure(Office.IRibbonUI ribbonUI);
        public void Insert_Table(Office.IRibbonUI ribbonUI);
        public void Insert_Plate(Office.IRibbonUI ribbonUI);
        public void Insert_Appendix(Office.IRibbonUI ribbonUI);


        // Styles
        public void Num_Heading1(Office.IRibbonUI ribbonUI);
        public void Num_Heading2(Office.IRibbonUI ribbonUI);
        public void Num_Heading3(Office.IRibbonUI ribbonUI);
        public void Num_Heading4(Office.IRibbonUI ribbonUI);
        public void Unnum_Heading1(Office.IRibbonUI ribbonUI);
        public void Unnum_Heading2(Office.IRibbonUI ribbonUI);
        public void Unnum_Heading3(Office.IRibbonUI ribbonUI);
        public void Unnum_Heading4(Office.IRibbonUI ribbonUI);
        public void Bullet1(Office.IRibbonUI ribbonUI);
        public void Bullet2(Office.IRibbonUI ribbonUI);
        public void Bullet3(Office.IRibbonUI ribbonUI);
        public void Normal(Office.IRibbonUI ribbonUI);
        public void Table_Footer(Office.IRibbonUI ribbonUI);
        public void Unformatted(Office.IRibbonUI ribbonUI);

        
        // Private Methods
        //private void Set_Language();
        //private void Table_Caption();
        //private void Check_Table_Position(); */
        #endregion

        #region Helpers

        private static string GetResourceText(string resourceName)
        {
            Assembly asm = Assembly.GetExecutingAssembly();
            string[] resourceNames = asm.GetManifestResourceNames();
            for (int i = 0; i < resourceNames.Length; ++i)
            {
                if (string.Compare(resourceName, resourceNames[i], StringComparison.OrdinalIgnoreCase) == 0)
                {
                    using (StreamReader resourceReader = new StreamReader(asm.GetManifestResourceStream(resourceNames[i])))
                    {
                        if (resourceReader != null)
                        {
                            return resourceReader.ReadToEnd();
                        }
                    }
                }
            }
            return null;
        }

        #endregion
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CDMSmith
{
    /// <summary>
    /// Interaction logic for InsertTable.xaml
    /// </summary>
    public partial class InsertPlate : Window
    {
        public InsertPlate()
        {
            InitializeComponent();
        }
        
        public void Insert_Plate_OK_Click(object sender, RoutedEventArgs e)
        {
            Globals.ThisAddIn.Insert_Plate_Ok_Click(this);
        }

        private void Insert_Plate_Cancel_Click(object sender, RoutedEventArgs e)
        {
            Globals.ThisAddIn.Insert_Plate_Cancel_Click(sender);
        }
       
    }
}

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CDMSmith
{
    /// <summary>
    /// Class for storing global Office objects from xml file
    /// </summary>
    public class CDMOffice
    {

        public string id { get; set; }
        public string name { get; set; }
        public string street { get; set; }
        public string suburb { get; set; }
        public string state { get; set; }
        public string postcode { get; set; }
        public string phone { get; set; }
        public string fax { get; set; }

    }
}

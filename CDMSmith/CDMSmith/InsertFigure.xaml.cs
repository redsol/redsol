﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CDMSmith
{
    /// <summary>
    /// Interaction logic for InsertTable.xaml
    /// </summary>
    public partial class InsertFigure : Window
    {
        public InsertFigure()
        {
            InitializeComponent();
        }
        
        public void Insert_Figure_OK_Click(object sender, RoutedEventArgs e)
        {
            Globals.ThisAddIn.Insert_Table_Ok_Click(this);
        }

        private void Insert_Figure_Cancel_Click(object sender, RoutedEventArgs e)
        {
            Globals.ThisAddIn.Insert_Table_Cancel_Click(sender);
        }
       
    }
}

﻿Global strGlobalTemplate As String

Sub EditDocumentProperties(ByVal control As IRibbonControl)
    If ActiveDocument.BuiltInDocumentProperties("Comments").Value = "CDMSmith" Then
        Application.Run macroname:="Document_Edit"
    Else
        MsgBox("The Edit Document Properties feature can only be used on documents created by the CDM Smith (Australia) Templates.", vbCritical)
    End If
End Sub

Sub UpdateFields(ByVal control As IRibbonControl)
    If ActiveDocument.BuiltInDocumentProperties("Comments").Value = "CDMSmith" Then
        Application.Run macroname:="UpdateAllFields"
    Else
        MsgBox("The Update All Fields feature can only be used on documents created by the CDM Smith (Australia) Templates.", vbCritical)
    End If
End Sub

Sub UpdateAllFields()

    ActiveDocument.Bookmarks("\StartOfDoc").Range.Select()
    ActiveDocument.Fields.Update()

    Application.ScreenUpdating = False

    Dim toc As TableOfContents
    For Each toc In ActiveDocument.TablesOfContents
        toc.Update()
    Next
    With ActiveDocument
        .PrintPreview()
        .ClosePrintPreview()
    End With

    Application.ScreenUpdating = True

End Sub

Sub InsertFigure(ByVal control As IRibbonControl)
    If ActiveDocument.BuiltInDocumentProperties("Comments").Value = "CDMSmith" Then
        If ActiveDocument.Bookmarks.Exists("Figures") Then
            frmPlate.lblSection.Visible = True
            frmPlate.optTable.Visible = True
            frmPlate.optBody.Visible = True
        End If

        'Adjust Plate Form to suit.
        With frmPlate
            .Caption = "Insert Figure"
            .lblCaption.Caption = "Figure Caption:"
            .txtCaption = ""
            .Show()
        End With

        frmPlate.optBody.Value = True

        If strGlobalTemplate = "" Then
            Call SetTemplateNames()
        End If
        Selection.TypeParagraph()

        'Check to see if within another table
        If Selection.Information(wdWithInTable) Then

            MsgBox("To insert a new Figure your cursor must not be within another table", vbCritical)
            End
        End If

        Call CheckStartingPosition()





        If frmPlate.optTable.Value = True Then
            ActiveDocument.AttachedTemplate.AutoTextEntries("Figure_Section").Insert( _
            where:=Selection.Range, RichText:=True)

            If ActiveDocument.Bookmarks.Exists("FigureCaption") Then
                ActiveDocument.Bookmarks("FigureCaption").Select()
                With ActiveDocument.Bookmarks("FigureCaption").Range
                    .Text = frmPlate.txtCaption.Value
                End With
            End If

        Else
            ActiveDocument.AttachedTemplate.AutoTextEntries("Figure").Insert( _
            where:=Selection.Range, RichText:=True)

            If ActiveDocument.Bookmarks.Exists("FigureCaption") Then
                ActiveDocument.Bookmarks("FigureCaption").Select()
                With ActiveDocument.Bookmarks("FigureCaption").Range
                    .Text = frmPlate.txtCaption.Value
                End With
            End If

        End If


        Selection.Paragraphs(1).Range.Select()
        Selection.Fields.Update()

        Selection.MoveLeft(unit:=wdCharacter, Count:=1)
        Selection.MoveDown(unit:=wdLine, Count:=1)

        With frmPlate
            .Caption = "Insert Plate"
            .lblCaption.Caption = "Plate Caption:"
            .txtCaption.Value = ""
        End With
        End
    Else
        MsgBox("The Insert Figure feature can only be used on documents created by the CDM Smith (Australia) Templates.", vbCritical)
    End If

End Sub

Sub InsertTable(ByVal control As IRibbonControl)
    If ActiveDocument.BuiltInDocumentProperties("Comments").Value = "CDMSmith" Then
        'On Error GoTo subErrorHandler
        If ActiveDocument.Bookmarks.Exists("Tables") Then
            frmTable.lblSection.Visible = True
            frmTable.optTable.Visible = True
            frmTable.optBody.Visible = True
        End If

        frmTable.optBody.Value = True

        Selection.TypeParagraph()

        'Check to see if within another table
        If Selection.Information(wdWithInTable) Then

            MsgBox("To insert new Table your cursor must not be within another table", vbCritical)
            End
        End If

        Call CheckStartingPosition()
        frmTable.Show()

        With Selection.Tables(1)
            .Range.Style = "LFT Body"
        End With

        Selection.Tables(1).Select()
        Selection.Cells.AutoFit()
        Selection.SelectRow()
        Selection.Tables(1).ApplyStyleHeadingRows = Not Selection.Tables(1).ApplyStyleHeadingRows
        Selection.Tables(1).Select()

        With Selection.Tables(1)
            With .Borders(wdBorderLeft)
                .LineStyle = wdLineStyleSingle
                .LineWidth = wdLineWidth050pt
                .Color = wdColorBlack
            End With
            With .Borders(wdBorderRight)
                .LineStyle = wdLineStyleSingle
                .LineWidth = wdLineWidth050pt
                .Color = wdColorBlack
            End With
            With .Borders(wdBorderTop)
                .LineStyle = wdLineStyleSingle
                .LineWidth = wdLineWidth050pt
                .Color = wdColorBlack
            End With
            With .Borders(wdBorderBottom)
                .LineStyle = wdLineStyleSingle
                .LineWidth = wdLineWidth050pt
                .Color = wdColorBlack
            End With
            With .Borders(wdBorderHorizontal)
                .LineStyle = wdLineStyleSingle
                .LineWidth = wdLineWidth050pt
                .Color = wdColorBlack
            End With
            With .Borders(wdBorderVertical)
                .LineStyle = wdLineStyleSingle
                .LineWidth = wdLineWidth050pt
                .Color = wdColorBlack
            End With
            .Borders(wdBorderDiagonalDown).LineStyle = wdLineStyleNone
            .Borders(wdBorderDiagonalUp).LineStyle = wdLineStyleNone
            .Borders.Shadow = False
        End With

        Selection.Style = ActiveDocument.Styles("LFT Table text")
        Selection.MoveLeft(unit:=wdCharacter, Count:=1)
        Selection.SelectRow()
        Selection.Style = ActiveDocument.Styles("LFT Table header 2")
        Selection.Rows.HeadingFormat = wdToggle
        With Selection.Cells
            With .Shading
                .Texture = wdTextureNone
                .ForegroundPatternColor = -553598977
                .BackgroundPatternColor = -553598977
            End With
            .VerticalAlignment = wdCellAlignVerticalCenter
            .HeightRule = wdRowHeightAtLeast
            .Height = 16.73
        End With

        With Selection.Rows(1).Range
            .ParagraphFormat.Alignment = wdAlignParagraphLeft

        End With
        '    Selection.Rows.HeadingFormat = wdToggle
        '    Selection.MoveLeft unit:=wdCharacter, Count:=1

        'Fix Table
        Selection.Tables(1).Select()
        'Selection.Cells.HeightRule = wdRowHeightAuto
        With Selection.Rows
            .Alignment = wdAlignRowLeft
            .AllowBreakAcrossPages = False
            .SetLeftIndent(LeftIndent:=CentimetersToPoints(0.24), RulerStyle:= _
                wdAdjustNone)
        End With
        Selection.HomeKey unit:=wdLine
        Selection.Cells.AutoFit()

        'Fit to margins
        Selection.Tables(1).Select()

        Selection.Cells.DistributeWidth()
        Selection.MoveLeft(unit:=wdCharacter, Count:=1)

        Call TableCaption()

        'Set location to invisible
        frmTable.lblSection.Visible = False
        frmTable.optTable.Visible = False
        frmTable.optBody.Visible = False
        frmTable.Hide()
        End
    Else
        MsgBox("The Insert Table feature can only be used on documents created by the CDM Smith (Australia) Templates.", vbCritical)
    End If
End Sub

Sub TableCaption()

    If strGlobalTemplate = "" Then
        Call SetTemplateNames()

    End If
    Selection.MoveUp(unit:=wdLine, Count:=1)

    'Ensure that the position selected will not corrupt document
    'Call CheckStartingPosition

    'Insert Table Caption Autotext with full Sequence
    Selection.Style = ActiveDocument.Styles("LFT Table Title")

    'If specified to be in the table section, then add appropriate autotext
    If frmTable.optTable.Value = True Then

        ActiveDocument.AttachedTemplate.AutoTextEntries("Table_Section").Insert( _
        where:=Selection.Range, RichText:=True)

        If ActiveDocument.Bookmarks.Exists("TableCaption") Then
            ActiveDocument.Bookmarks("TableCaption").Select()
            With ActiveDocument.Bookmarks("TableCaption").Range
                .Text = frmTable.txtCaption.Value
            End With
        End If


        Selection.MoveDown(unit:=wdLine, Count:=1)
        Selection.Delete()
        Selection.MoveUp(unit:=wdLine, Count:=1)
        Selection.Paragraphs(1).Range.Select()
        Selection.Fields.Update()

        Selection.MoveLeft(unit:=wdCharacter, Count:=1)
        Selection.MoveDown(unit:=wdLine, Count:=1)

    Else

        ActiveDocument.AttachedTemplate.AutoTextEntries("Table").Insert( _
        where:=Selection.Range, RichText:=True)

        If ActiveDocument.Bookmarks.Exists("TableCaption") Then
            ActiveDocument.Bookmarks("TableCaption").Select()
            With ActiveDocument.Bookmarks("TableCaption").Range
                .Text = frmTable.txtCaption.Value
            End With
        End If

        Selection.MoveDown(unit:=wdLine, Count:=1)
        Selection.Delete()
        Selection.MoveUp(unit:=wdLine, Count:=1)
        Selection.Paragraphs(1).Range.Select()
        Selection.Fields.Update()

        Selection.MoveLeft(unit:=wdCharacter, Count:=1)
        Selection.MoveDown(unit:=wdLine, Count:=1)

    End If
End Sub

Sub CheckStartingPosition()
startagain:
    If ActiveDocument.Range.Characters.Count = 1 Then
        Selection.TypeParagraph()
    End If

    If Selection.Information(wdWithInTable) Then
        Selection.Tables(1).Range.Select()
        Selection.MoveDown(unit:=wdLine, Count:=1)
        If Selection.Paragraphs(1).Range.Characters(1).Text = Chr(13) Then
            With Selection
                .TypeParagraph()
                .Style = "Normal"
            End With
        End If
        GoTo startagain

    End If


    If Selection.Paragraphs(1).Range.Characters.Count > 1 Then
        With Selection
            .Paragraphs(1).Range.Select()
            .MoveRight(unit:=wdCharacter, Count:=1)
            .MoveLeft(unit:=wdCharacter, Count:=1)

            If .Characters(1).Text = Chr(12) Then
                .MoveRight(unit:=wdCharacter, Count:=1)
                If Selection.Paragraphs(1).Range.Characters.Count <= 2 Then
                    .Style = "LFT Body"
                    Exit Sub
                Else
                    .TypeParagraph()
                    Exit Sub
                End If
            Else
                .TypeParagraph()

            End If
            .Style = "LFT Body"
            GoTo startagain

        End With
    End If


End Sub

Sub InsertPlate(ByVal control As IRibbonControl)
    If ActiveDocument.BuiltInDocumentProperties("Comments").Value = "CDMSmith" Then
        If ActiveDocument.Bookmarks.Exists("Plates") Then
            frmPlate.lblSection.Visible = True
            frmPlate.optTable.Visible = True
            frmPlate.optBody.Visible = True
        End If


        frmPlate.optBody.Value = True

        If strGlobalTemplate = "" Then
            Call SetTemplateNames()
        End If

        Call CheckStartingPosition()
        If Selection.Information(wdWithInTable) Then
            MsgBox("To insert new Plate your cursor must on the outside of a table", vbCritical)
            End
        End If

        ' Get Plate caption name from form
        frmPlate.Show()

        '' Check for landscape layout - To Finalise

        'If Selection.Sections(1).PageSetup.Orientation = wdOrientLandscape Then
        ' Insert Plate Autotext Table depending on layout
        '    Templates(strGlobalTemplate).AutoTextEntries("PlatesCaptionLscpe").Insert _
        'where:=Selection.Range, RichText:=True
        'Else

        If frmPlate.optTable.Value = True Then

            ActiveDocument.AttachedTemplate.AutoTextEntries("Plate_Section").Insert( _
        where:=Selection.Range, RichText:=True)

            If ActiveDocument.Bookmarks.Exists("PlateCaption") Then
                ActiveDocument.Bookmarks("PlateCaption").Select()
                With ActiveDocument.Bookmarks("PlateCaption").Range
                    .Text = frmPlate.txtCaption.Value
                End With
            End If

        Else

            ActiveDocument.AttachedTemplate.AutoTextEntries("Plate").Insert( _
        where:=Selection.Range, RichText:=True)

            If ActiveDocument.Bookmarks.Exists("PlateCaption") Then
                ActiveDocument.Bookmarks("PlateCaption").Select()
                With ActiveDocument.Bookmarks("PlateCaption").Range
                    .Text = frmPlate.txtCaption.Value
                End With
            End If

        End If
        Selection.MoveLeft(unit:=wdCharacter, Count:=1)
        Selection.MoveUp(unit:=wdLine, Count:=1)

        frmPlate.lblSection.Visible = False
        frmPlate.optTable.Visible = False
        frmPlate.optBody.Visible = False
        End
    Else
        MsgBox("The Insert Plate feature can only be used on documents created by the CDM Smith (Australia) Templates.", vbCritical)
    End If
End Sub

Sub InsertAppendix(ByVal control As IRibbonControl)
    If ActiveDocument.BuiltInDocumentProperties("Comments").Value = "CDMSmith" Then
        frmPlate.lblSection.Visible = False
        frmPlate.optTable.Visible = False
        frmPlate.optBody.Visible = False
        frmPlate.optBody.Value = True

        If strGlobalTemplate = "" Then
            Call SetTemplateNames()
        End If
        Selection.TypeParagraph()

        'Check to see if within another table
        If Selection.Information(wdWithInTable) Then

            MsgBox("To insert a new Appendix your cursor must not be within another table", vbCritical)
            End
        End If

        Call CheckStartingPosition()

        'Adjust Plate Form to suit.
        With frmPlate
            .Caption = "Insert Appendix"
            .lblCaption.Caption = "Appendix Heading: "
            .txtCaption = ""
            .Show()
        End With


        ActiveDocument.AttachedTemplate.AutoTextEntries("Appendix_Section").Insert( _
        where:=Selection.Range, RichText:=True)

        If ActiveDocument.Bookmarks.Exists("AppendixCaption") Then
            ActiveDocument.Bookmarks("AppendixCaption").Select()
            With ActiveDocument.Bookmarks("AppendixCaption").Range
                .Text = frmPlate.txtCaption.Value
            End With
        End If


        Selection.Paragraphs(1).Range.Select()
        Selection.Fields.Update()

        Selection.MoveLeft(unit:=wdCharacter, Count:=1)
        Selection.MoveDown(unit:=wdLine, Count:=1)

        frmPlate.lblSection.Visible = False
        frmPlate.optTable.Visible = False
        frmPlate.optBody.Visible = False
        With frmPlate
            .Caption = "Insert Plate"
            .lblCaption.Caption = "Plate Caption"
            .optTable.Caption = "Plate Section"
            .txtCaption.Value = ""
        End With
        End
    Else
        MsgBox("The Insert Appendix feature can only be used on documents created by the CDM Smith (Australia) Templates.", vbCritical)
    End If
End Sub

Sub SetLanguage()
    Dim dicLoop As Dictionary
    On Error Resume Next

    For Each dicLoop In CustomDictionaries
        dicLoop.LanguageSpecific = True
        dicLoop.LanguageID = wdEnglishAUS
    Next dicLoop

End Sub

Sub SetTemplateNames()
    strGlobalTemplate = Options.DefaultFilePath(Path:=wdStartupPath) & "\E3Global.dotm"
    strUserTemplatePath = Options.DefaultFilePath(Path:=wdUserTemplatesPath)

    Call SetLanguage()

End Sub

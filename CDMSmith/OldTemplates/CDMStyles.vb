﻿
Sub Apply_NumHeading1(ByVal control As IRibbonControl)
    If ActiveDocument.BuiltInDocumentProperties("Comments").Value = "CDMSmith" Then
        If ActiveDocument.BuiltInDocumentProperties("Category").Value = "Report" Then
            Selection.InsertBreak Type:=wdSectionBreakNextPage
        End If

        'If Selection is single, grab whole paragraph, else use existing selection
        If Selection.Range.Characters.Count = 1 Then
            Selection.Paragraphs(1).Range.Select()
            Selection.Style = ActiveDocument.Styles("Heading 1")
        Else
            Selection.Style = ActiveDocument.Styles("Heading 1")
        End If
    Else
        MsgBox("This style can only be applied on documents created by the CDM Smith (Australia) Templates.", vbCritical)

    End If
End Sub

Sub Apply_NumHeading2(ByVal control As IRibbonControl)
    If ActiveDocument.BuiltInDocumentProperties("Comments").Value = "CDMSmith" Then
        If Selection.Range.Characters.Count = 1 Then
            Selection.Paragraphs(1).Range.Select()
            Selection.Style = ActiveDocument.Styles("Heading 2")
        Else
            Selection.Style = ActiveDocument.Styles("Heading 2")
        End If
    Else
        MsgBox("This style can only be applied on documents created by the CDM Smith (Australia) Templates.", vbCritical)

    End If
End Sub

Sub Apply_NumHeading3(ByVal control As IRibbonControl)
    If ActiveDocument.BuiltInDocumentProperties("Comments").Value = "CDMSmith" Then
        If Selection.Range.Characters.Count = 1 Then
            Selection.Paragraphs(1).Range.Select()
            Selection.Style = ActiveDocument.Styles("Heading 3")
        Else
            Selection.Style = ActiveDocument.Styles("Heading 3")
        End If
    Else
        MsgBox("This style can only be applied on documents created by the CDM Smith (Australia) Templates.", vbCritical)

    End If
End Sub

Sub Apply_NumHeading4(ByVal control As IRibbonControl)
    If ActiveDocument.BuiltInDocumentProperties("Comments").Value = "CDMSmith" Then
        If Selection.Range.Characters.Count = 1 Then
            Selection.Paragraphs(1).Range.Select()
            Selection.Style = ActiveDocument.Styles("Heading 4")
        Else
            Selection.Style = ActiveDocument.Styles("Heading 4")
        End If
    Else
        MsgBox("This style can only be applied on documents created by the CDM Smith (Australia) Templates.", vbCritical)

    End If
End Sub

Sub Apply_UnNumHeading1(ByVal control As IRibbonControl)

    If ActiveDocument.BuiltInDocumentProperties("Comments").Value = "CDMSmith" Then
        If Selection.Range.Characters.Count = 1 Then
            Selection.Paragraphs(1).Range.Select()
            Selection.Style = ActiveDocument.Styles("UnNum Heading 1")
        Else
            Selection.Style = ActiveDocument.Styles("UnNum Heading 1")
        End If
    Else
        MsgBox("This style can only be applied on documents created by the CDM Smith (Australia) Templates.", vbCritical)

    End If
End Sub

Sub Apply_UnNumHeading2(ByVal control As IRibbonControl)
    If ActiveDocument.BuiltInDocumentProperties("Comments").Value = "CDMSmith" Then
        If Selection.Range.Characters.Count = 1 Then
            Selection.Paragraphs(1).Range.Select()
            Selection.Style = ActiveDocument.Styles("UnNum Heading 2")
        Else
            Selection.Style = ActiveDocument.Styles("UnNum Heading 2")
        End If
    Else
        MsgBox("This style can only be applied on documents created by the CDM Smith (Australia) Templates.", vbCritical)

    End If
End Sub

Sub Apply_UnNumHeading3(ByVal control As IRibbonControl)
    If ActiveDocument.BuiltInDocumentProperties("Comments").Value = "CDMSmith" Then
        If Selection.Range.Characters.Count = 1 Then
            Selection.Paragraphs(1).Range.Select()
            Selection.Style = ActiveDocument.Styles("UnNum Heading 3")
        Else
            Selection.Style = ActiveDocument.Styles("UnNum Heading 3")
        End If
    Else
        MsgBox("This style can only be applied on documents created by the CDM Smith (Australia) Templates.", vbCritical)

    End If
End Sub

Sub Apply_UnNumHeading4(ByVal control As IRibbonControl)
    If ActiveDocument.BuiltInDocumentProperties("Comments").Value = "CDMSmith" Then
        If Selection.Range.Characters.Count = 1 Then
            Selection.Paragraphs(1).Range.Select()
            Selection.Style = ActiveDocument.Styles("UnNum Heading 4")
        Else
            Selection.Style = ActiveDocument.Styles("UnNum Heading 4")
        End If
    Else
        MsgBox("This style can only be applied on documents created by the CDM Smith (Australia) Templates.", vbCritical)

    End If
End Sub

Sub Apply_BulletStyle1(ByVal control As IRibbonControl)
    If ActiveDocument.BuiltInDocumentProperties("Comments").Value = "CDMSmith" Then
        If Selection.Range.Characters.Count = 1 Then
            Selection.Paragraphs(1).Range.Select()
            Selection.Style = ActiveDocument.Styles("LFT Body")
            Selection.Style = ActiveDocument.Styles("LFT Bullet 1")
        Else
            Selection.Style = ActiveDocument.Styles("LFT Body")
            Selection.Style = ActiveDocument.Styles("LFT Bullet 1")
        End If
    Else
        MsgBox("This style can only be applied on documents created by the CDM Smith (Australia) Templates.", vbCritical)

    End If
End Sub

Sub Apply_BulletStyle2(ByVal control As IRibbonControl)
    If ActiveDocument.BuiltInDocumentProperties("Comments").Value = "CDMSmith" Then
        If Selection.Range.Characters.Count = 1 Then
            Selection.Paragraphs(1).Range.Select()
            Selection.Style = ActiveDocument.Styles("LFT Body")
            Selection.Style = ActiveDocument.Styles("LFT Bullet 1")
        Else
            Selection.Style = ActiveDocument.Styles("Normal")
            Selection.Style = ActiveDocument.Styles("LFT Bullet 1")
        End If
        Selection.Range.SetListLevel Level:=2
    Else
        MsgBox("This style can only be applied on documents created by the CDM Smith (Australia) Templates.", vbCritical)

    End If
End Sub

Sub Apply_BulletStyle3(ByVal control As IRibbonControl)
    If ActiveDocument.BuiltInDocumentProperties("Comments").Value = "CDMSmith" Then
        If Selection.Range.Characters.Count = 1 Then
            Selection.Paragraphs(1).Range.Select()
            Selection.Style = ActiveDocument.Styles("LFT Body")
            Selection.Style = ActiveDocument.Styles("LFT Bullet 1")
        Else
            Selection.Style = ActiveDocument.Styles("LFT Body")
            Selection.Style = ActiveDocument.Styles("LFT Bullet 1")
        End If
1:      Selection.Range.SetListLevel Level:=3
    Else
        MsgBox("This style can only be applied on documents created by the CDM Smith (Australia) Templates.", vbCritical)

    End If
End Sub

Sub Apply_NormalStyle(ByVal control As IRibbonControl)
    If ActiveDocument.BuiltInDocumentProperties("Comments").Value = "CDMSmith" Then
        If Selection.Range.Characters.Count = 1 Then
            Selection.Paragraphs(1).Range.Select()
            Selection.Style = ActiveDocument.Styles("LFT Body")
        Else
            Selection.Style = ActiveDocument.Styles("LFT Body")
        End If
    Else
        MsgBox("This style can only be applied on documents created by the CDM Smith (Australia) Templates.", vbCritical)

    End If
End Sub

Sub Apply_TableFooter(ByVal control As IRibbonControl)
    If ActiveDocument.BuiltInDocumentProperties("Comments").Value = "CDMSmith" Then
        If Selection.Range.Characters.Count = 1 Then
            Selection.Paragraphs(1).Range.Select()
            Selection.Style = ActiveDocument.Styles("LFT Footer Custom Text")
        Else
            Selection.Style = ActiveDocument.Styles("LFT Footer Custom Text")
        End If
    Else
        MsgBox("This style can only be applied on documents created by the CDM Smith (Australia) Templates.", vbCritical)

    End If
End Sub

Sub Paste_Unformatted(ByVal control As IRibbonControl)

    Selection.PasteSpecial(Link:=False, DataType:=wdPasteText)

End Sub


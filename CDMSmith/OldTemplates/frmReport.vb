﻿Private Sub Document_New()
    Load frmReport
    frmReport.Show()
    Application.DisplayDocumentInformationPanel = False

End Sub

Sub Document_Edit()
    Load frmReport
    frmReport.frmSections.Visible = False
    frmReport.cmdCancel.Visible = False
    frmReport.cmdOk.Top = 222
    frmReport.Height = 272.45
    frmReport.Show()


End Sub



Dim numberStaff, numberOffices As Integer
Dim strUserDetails(250, 3)
Dim strOfficeDetails(20, 7)
Dim strGlobalTemplate
Option Explicit


Private Sub txtDate_MouseUp(ByVal Button As Integer, ByVal Shift As Integer, ByVal X As Single, ByVal Y As Single)
    txtDate.SelStart = 0
    txtDate.SelLength = Len(txtDate.Text)
End Sub

Private Sub txtSubHdr_MouseUp(ByVal Button As Integer, ByVal Shift As Integer, ByVal X As Single, ByVal Y As Single)
    txtSubHdr.SelStart = 0
    txtSubHdr.SelLength = Len(txtSubHdr.Text)
End Sub

Private Sub txtRptName_MouseUp(ByVal Button As Integer, ByVal Shift As Integer, ByVal X As Single, ByVal Y As Single)
    txtRptName.SelStart = 0
    txtRptName.SelLength = Len(txtRptName.Text)
End Sub

Private Sub cmbProjMan_MouseUp(ByVal Button As Integer, ByVal Shift As Integer, ByVal X As Single, ByVal Y As Single)
    cmbProjMan.SelStart = 0
    cmbProjMan.SelLength = Len(cmbProjMan.Text)
End Sub

Private Sub cmbAuthor_MouseUp(ByVal Button As Integer, ByVal Shift As Integer, ByVal X As Single, ByVal Y As Single)
    cmbAuthor.SelStart = 0
    cmbAuthor.SelLength = Len(cmbAuthor.Text)
End Sub

Private Sub cmbOffice_MouseUp(ByVal Button As Integer, ByVal Shift As Integer, ByVal X As Single, ByVal Y As Single)
    cmbOffice.SelStart = 0
    cmbOffice.SelLength = Len(cmbOffice.Text)
End Sub

Private Sub cmbDocVersion_MouseUp(ByVal Button As Integer, ByVal Shift As Integer, ByVal X As Single, ByVal Y As Single)
    cmbDocVersion.SelStart = 0
    cmbDocVersion.SelLength = Len(cmbDocVersion.Text)
End Sub

Private Sub txtProjNum_MouseUp(ByVal Button As Integer, ByVal Shift As Integer, ByVal X As Single, ByVal Y As Single)
    txtProjNum.SelStart = 0
    txtProjNum.SelLength = Len(txtProjNum.Text)
End Sub

Private Sub cmdCancel_Click()
    On Error GoTo Err_CommandButtonCancel_Click
    Unload Me
    'If frmReport.Visible = True Then
    ActiveDocument.Close savechanges:=wdDoNotSaveChanges
    'End If
    End
Exit_CommandButtonCancel_Click:
    Exit Sub
Err_CommandButtonCancel_Click:
    Select Case Err

        Case Else
            MsgBox "Unanticipated error number:" & Err.Number & " :" & Err.Description
    End Select
End Sub

Private Sub cmdOk_Click()
    'Declarations
    Dim j, AuthorNum, OfficeNum, answer As Integer
    strGlobalTemplate = Options.DefaultFilePath(Path:=wdStartupPath) & "\E3Global.dotm"
    Application.ScreenUpdating = False

    'Input Checking
    If txtRptName = "" Then
        MsgBox("Project Name Cannot be Blank", vbExclamation)
        txtRptName.SetFocus()
        Exit Sub
    End If

    If txtSubHdr = "" Then
        MsgBox("Client Name Cannot be Blank", vbExclamation)
        txtSubHdr.SetFocus()
        Exit Sub
    End If

    If cmbProjMan = "" Then
        MsgBox("Project Manager Cannot be Blank", vbExclamation)
        cmbProjMan.SetFocus()
        Exit Sub
    End If

    If cmbAuthor = "" Then
        MsgBox("Primary Author Cannot be Blank", vbExclamation)
        cmbAuthor.SetFocus()
        Exit Sub
    End If

    If cmbOffice = "" Then
        MsgBox("Office Cannot be Blank", vbExclamation)
        cmbOffice.SetFocus()
        Exit Sub
    End If

    If cmbDocVersion = "" Then
        MsgBox("Doc Version cannot be Blank", vbExclamation)
        cmbDocVersion.SetFocus()
        Exit Sub
    End If

    'Set Author according to form submission
    j = 0
    While j < numberStaff

        If cmbAuthor = strUserDetails(j, 1) Then
            AuthorNum = j
            j = numberStaff
        Else
            j = j + 1
        End If
    End While

    'Set office accoriding to form submission
    j = 0
    While j < numberOffices

        If cmbOffice = strOfficeDetails(j, 1) Then
            OfficeNum = j
            j = numberOffices
        Else
            j = j + 1
        End If

    End While
    j = 1

    'hide form
    Me.Hide()


    With ActiveDocument
        'Remove locks on content controls
        'While j <= 18
        '.ContentControls.Item(j).LockContents = False
        'j = j + 1
        'Wend

        'Add Data to content controls
        '1 client-1
        '.ContentControls.Item(1).Range.Text = txtSubHdr.Value
        .SelectContentControlsByTag("client-1").Item(1).LockContents = False
        .SelectContentControlsByTag("client-1").Item(1).Range.Text = txtSubHdr.Value
        '.SelectContentControlsByTag("client-1").Item(1).LockContents = True


        '2 project-1
        '.ContentControls.Item(2).Range.Text = txtRptName.Value
        .SelectContentControlsByTag("project-1").Item(1).LockContents = False
        .SelectContentControlsByTag("project-1").Item(1).Range.Text = txtRptName.Value
        '.SelectContentControlsByTag("project-1").Item(1).LockContents = True

        '3 client-2
        '.ContentControls.Item(3).Range.Text = txtSubHdr.Value
        .SelectContentControlsByTag("client-2").Item(1).LockContents = False
        .SelectContentControlsByTag("client-2").Item(1).Range.Text = txtSubHdr.Value
        '.SelectContentControlsByTag("client-2").Item(1).LockContents = True

        '4 project-2
        '.ContentControls.Item(4).Range.Text = txtRptName.Value
        .SelectContentControlsByTag("project-2").Item(1).LockContents = False
        .SelectContentControlsByTag("project-2").Item(1).Range.Text = txtRptName.Value
        '.SelectContentControlsByTag("project-2").Item(1).LockContents = True

        '5 Date
        '.ContentControls.Item(5).Range.Text = txtDate.Text
        .SelectContentControlsByTag("date").Item(1).LockContents = False
        .SelectContentControlsByTag("date").Item(1).Range.Text = txtDate.Text

        '6 officeAddress
        '.ContentControls.Item(6).Range.Text = strOfficeDetails(OfficeNum, 2)
        .SelectContentControlsByTag("street").Item(1).LockContents = False
        .SelectContentControlsByTag("street").Item(1).Range.Text = strOfficeDetails(OfficeNum, 2)
        '.SelectContentControlsByTag("street").Item(1).LockContents = True

        '7 suburb
        '.ContentControls.Item(7).Range.Text = strOfficeDetails(OfficeNum, 3)
        .SelectContentControlsByTag("suburb").Item(1).LockContents = False
        .SelectContentControlsByTag("suburb").Item(1).Range.Text = strOfficeDetails(OfficeNum, 3)
        '.SelectContentControlsByTag("suburb").Item(1).LockContents = True

        '8 State
        '.ContentControls.Item(8).Range.Text = strOfficeDetails(OfficeNum, 4)
        .SelectContentControlsByTag("state").Item(1).LockContents = False
        .SelectContentControlsByTag("state").Item(1).Range.Text = strOfficeDetails(OfficeNum, 4)
        '.SelectContentControlsByTag("state").Item(1).LockContents = True

        '9 postCode
        '.ContentControls.Item(9).Range.Text = strOfficeDetails(OfficeNum, 5)
        .SelectContentControlsByTag("postcode").Item(1).LockContents = False
        .SelectContentControlsByTag("postcode").Item(1).Range.Text = strOfficeDetails(OfficeNum, 5)
        '.SelectContentControlsByTag("postCode").Item(1).LockContents = True

        '10 officePhone
        '.ContentControls.Item(10).Range.Text = strOfficeDetails(OfficeNum, 6)
        .SelectContentControlsByTag("telephone").Item(1).LockContents = False
        .SelectContentControlsByTag("telephone").Item(1).Range.Text = strOfficeDetails(OfficeNum, 6)
        '.SelectContentControlsByTag("officePhone").Item(1).LockContents = True

        '11 officeFax
        '.ContentControls.Item(11).Range.Text = strOfficeDetails(OfficeNum, 7)
        .SelectContentControlsByTag("fax").Item(1).LockContents = False
        .SelectContentControlsByTag("fax").Item(1).Range.Text = strOfficeDetails(OfficeNum, 7)
        '.SelectContentControlsByTag("officeFax").Item(1).LockContents = True

        '12 Author
        '.ContentControls.Item(12).Range.Text = strUserDetails(AuthorNum, 1)
        .SelectContentControlsByTag("author").Item(1).LockContents = False
        .SelectContentControlsByTag("author").Item(1).Range.Text = strUserDetails(AuthorNum, 1)
        '.SelectContentControlsByTag("author").Item(1).LockContents = True

        '13 projectManager
        '.ContentControls.Item(13).Range.Text = cmbProjMan.Value
        .SelectContentControlsByTag("projectManager").Item(1).LockContents = False
        .SelectContentControlsByTag("projectManager").Item(1).Range.Text = cmbProjMan.Value
        '.SelectContentControlsByTag("projectManager").Item(1).LockContents = True

        '14 client-3
        '.ContentControls.Item(14).Range.Text = txtRptName.Value
        '.SelectContentControlsByTag("client-3").Item(1).LockContents = False
        '.SelectContentControlsByTag("client-3").Item(1).Range.Text = txtSubHdr.Value
        '.SelectContentControlsByTag("client-3").Item(1).LockContents = True

        '14 client-3
        '.ContentControls.Item(14).Range.Text = txtRptName.Value
        .SelectContentControlsByTag("client-5").Item(1).LockContents = False
        .SelectContentControlsByTag("client-5").Item(1).Range.Text = txtSubHdr.Value
        '.SelectContentControlsByTag("client-3").Item(1).LockContents = True
        '15 project-3
        '.ContentControls.Item(15).Range.Text = txtSubHdr.Value
        .SelectContentControlsByTag("project-3").Item(1).LockContents = False
        .SelectContentControlsByTag("project-3").Item(1).Range.Text = txtRptName.Value
        '.SelectContentControlsByTag("project-3").Item(1).LockContents = True

        '16 docVersion
        '.ContentControls.Item(16).Range.Text = cmbDocVersion.Value
        .SelectContentControlsByTag("docVersion").Item(1).LockContents = False
        .SelectContentControlsByTag("docVersion").Item(1).Range.Text = cmbDocVersion.Value
        '.SelectContentControlsByTag("docVersion").Item(1).LockContents = True

        '17 project-4
        '.ContentControls.Item(15).Range.Text = txtSubHdr.Value
        .SelectContentControlsByTag("project-4").Item(1).LockContents = False
        .SelectContentControlsByTag("project-4").Item(1).Range.Text = txtRptName.Value
        '.SelectContentControlsByTag("project-3").Item(1).LockContents = True

        '18 proNum
        '.ContentControls.Item(18).Range.Text = txtProjNum.Value
        .SelectContentControlsByTag("proNum").Item(1).LockContents = False
        .SelectContentControlsByTag("proNum").Item(1).Range.Text = txtProjNum.Value
        '.SelectContentControlsByTag("proNum").Item(1).LockContents = True

        '19 client-4
        '.ContentControls.Item(19).Range.Text = txtSubHdr.Value
        With ActiveDocument.Sections(4).Headers.Item(wdHeaderFooterPrimary).Range
            ActiveDocument.SelectContentControlsByTag("client-4").Item(1).LockContents = False
            ActiveDocument.SelectContentControlsByTag("client-4").Item(1).Range.Text = txtSubHdr.Value
            ' ActiveDocument.SelectContentControlsByTag("client-4").Item(1).LockContents = True
        End With


        '20 project-5
        '.ContentControls.Item(19).Range.Text = txtSubHdr.Value
        With ActiveDocument.Sections(4).Headers.Item(wdHeaderFooterPrimary).Range
            ActiveDocument.SelectContentControlsByTag("project-5").Item(1).LockContents = False
            ActiveDocument.SelectContentControlsByTag("project-5").Item(1).Range.Text = txtRptName.Value
        End With

        '21 project-6
        '.ContentControls.Item(19).Range.Text = txtSubHdr.Value
        With ActiveDocument.Sections(4).Headers.Item(wdHeaderFooterPrimary).Range
            ActiveDocument.SelectContentControlsByTag("project-6").Item(1).LockContents = False
            ActiveDocument.SelectContentControlsByTag("project-6").Item(1).Range.Text = txtRptName.Value


        End With



    End With



    'Delete Exec Summary if not needed
    If chkExecSummary = False Then
        ActiveDocument.Bookmarks("ExecSummary").Range.Select()
        Selection.MoveRight(unit:=wdCharacter, Count:=1, Extend:=wdExtend)
        Selection.Delete()
    End If

    'Delete Tables Section and associate TOC if not needed
    If chkTables = False Then
        ActiveDocument.Bookmarks("Tables").Range.Select()
        Selection.MoveRight(unit:=wdCharacter, Count:=1, Extend:=wdExtend)
        Selection.Delete()
        ActiveDocument.Bookmarks("TablesTOC").Range.Select()
        Selection.Delete()
    End If

    'Delete Plates Section and Associated TOC if not needed
    If chkPlates = False Then
        ActiveDocument.Bookmarks("Plates").Range.Select()
        Selection.MoveRight(unit:=wdCharacter, Count:=1, Extend:=wdExtend)
        Selection.Delete()
        ActiveDocument.Bookmarks("PlatesTOC").Range.Select()
        Selection.Delete()
    End If

    'Delete Figures Section and Associated TOC if not needed
    If chkFigures = False Then
        ActiveDocument.Bookmarks("Figures").Range.Select()
        Selection.MoveRight(unit:=wdCharacter, Count:=1, Extend:=wdExtend)
        Selection.Delete()
        ActiveDocument.Bookmarks("FiguresTOC").Range.Select()
        Selection.Delete()
    End If

    'Delete Glossary Section if not required
    If chkGlossary = False Then
        ActiveDocument.Bookmarks("Glossary").Range.Select()
        Selection.MoveRight(unit:=wdCharacter, Count:=1, Extend:=wdExtend)
        Selection.Delete()
    End If

    'Delete Appendix Section and Associated TOC if not required.
    'If chkAppendicies = False Then
    'ActiveDocument.Bookmarks("Appendicies").Range.Select
    'Selection.MoveRight unit:=wdCharacter, Count:=1, Extend:=wdExtend
    'Selection.Delete
    'ActiveDocument.Bookmarks("AppendiciesTOC").Range.Select
    'Selection.Delete

    'Delete Standard Disclaimer Section if not required
    'If chkStdDisclaimer = False Then
    'ActiveDocument.Bookmarks("StdDisclaimer").Range.Select
    'Selection.MoveRight unit:=wdCharacter, Count:=1, Extend:=wdExtend
    'Selection.Delete
    'End If
    'End If

    'Unload the form
    Unload Me
    Application.Run macroname:="UpdateAllFields"

    'Error Handling
    ActiveWindow.View.Type = wdPrintView
    SendKeys("%(Y)", False)
    SendKeys("{ESC}", False)
    SendKeys("{ESC}", False)

Exit_CommandButtonOK_Click:
    Exit Sub

Err_CommandButtonOK_Click:
    Select Case Err
        Case Is = 5941 'Bookmark not in document
            Resume Next
        Case Else
            MsgBox "Unanticipated error number:" & Err.Number & " :" & Err.Description
    End Select
End Sub


Private Sub UserForm_Initialize()
    Dim strFilePath, strFileOpen As String
    Dim i, commaPosition As Integer
    strFilePath = Options.DefaultFilePath(Path:=wdUserTemplatesPath)

    If ActiveDocument.SelectContentControlsByTag("date").Item(1).Range.Text = " " Then
    txtDate.Text = Format(Date, "d mmmm yyyy")
    Else
        txtDate.Value = ActiveDocument.SelectContentControlsByTag("date").Item(1).Range.Text
    End If

    txtSubHdr.Value = ActiveDocument.SelectContentControlsByTag("client-1").Item(1).Range.Text
    txtRptName.Value = ActiveDocument.SelectContentControlsByTag("project-1").Item(1).Range.Text
    txtProjNum.Value = ActiveDocument.SelectContentControlsByTag("proNum").Item(1).Range.Text


    'Open csv file for reading in variables
    strFileOpen = strFilePath + "\E3Staff.csv"

Open strFileOpen For Input As #1

    i = 0

    While Not EOF(1)
    Line Input #1, strUserDetails(i, 0)

        commaPosition = InStr(strUserDetails(i, 0), ",")
        strUserDetails(i, 1) = Mid(strUserDetails(i, 0), 1, commaPosition - 1)
        strUserDetails(i, 0) = Mid(strUserDetails(i, 0), commaPosition + 1)

        commaPosition = InStr(strUserDetails(i, 0), ",")
        strUserDetails(i, 2) = Mid(strUserDetails(i, 0), 1, commaPosition - 1)
        strUserDetails(i, 0) = Mid(strUserDetails(i, 0), commaPosition + 1)

        commaPosition = InStr(strUserDetails(i, 0), ",")
        strUserDetails(i, 3) = Mid(strUserDetails(i, 0), 1, commaPosition - 1)
        strUserDetails(i, 0) = Mid(strUserDetails(i, 0), commaPosition + 1)


        i = i + 1

    End While

    numberStaff = i
Close #1
    strFileOpen = strFilePath + "\E3Office.csv"

Open strFileOpen For Input As #1

    'Reset Counter
    i = 0

    While Not EOF(1)
    Line Input #1, strOfficeDetails(i, 0)

        commaPosition = InStr(strOfficeDetails(i, 0), ",")
        strOfficeDetails(i, 1) = Mid(strOfficeDetails(i, 0), 1, commaPosition - 1)
        strOfficeDetails(i, 0) = Mid(strOfficeDetails(i, 0), commaPosition + 1)

        commaPosition = InStr(strOfficeDetails(i, 0), ",")
        strOfficeDetails(i, 2) = Mid(strOfficeDetails(i, 0), 1, commaPosition - 1)
        strOfficeDetails(i, 0) = Mid(strOfficeDetails(i, 0), commaPosition + 1)

        commaPosition = InStr(strOfficeDetails(i, 0), ",")
        strOfficeDetails(i, 3) = Mid(strOfficeDetails(i, 0), 1, commaPosition - 1)
        strOfficeDetails(i, 0) = Mid(strOfficeDetails(i, 0), commaPosition + 1)

        commaPosition = InStr(strOfficeDetails(i, 0), ",")
        strOfficeDetails(i, 4) = Mid(strOfficeDetails(i, 0), 1, commaPosition - 1)
        strOfficeDetails(i, 0) = Mid(strOfficeDetails(i, 0), commaPosition + 1)

        commaPosition = InStr(strOfficeDetails(i, 0), ",")
        strOfficeDetails(i, 5) = Mid(strOfficeDetails(i, 0), 1, commaPosition - 1)
        strOfficeDetails(i, 0) = Mid(strOfficeDetails(i, 0), commaPosition + 1)

        commaPosition = InStr(strOfficeDetails(i, 0), ",")
        strOfficeDetails(i, 6) = Mid(strOfficeDetails(i, 0), 1, commaPosition - 1)
        strOfficeDetails(i, 0) = Mid(strOfficeDetails(i, 0), commaPosition + 1)

        commaPosition = InStr(strOfficeDetails(i, 0), ",")
        strOfficeDetails(i, 7) = Mid(strOfficeDetails(i, 0), 1, commaPosition - 1)
        strOfficeDetails(i, 0) = Mid(strOfficeDetails(i, 0), commaPosition + 1)

        i = i + 1

    End While
    numberOffices = i
Close #1

    Me.cmbDocVersion.AddItem("Final")
    Me.cmbDocVersion.AddItem("Draft")
    Me.cmbDocVersion.ListIndex = 0
    Me.cmbDocVersion.Value = ActiveDocument.SelectContentControlsByTag("docVersion").Item(1).Range.Text

    Dim j As Integer
    j = 0
    While j < numberStaff

        Me.cmbAuthor.AddItem(strUserDetails(j, 1))
        Me.cmbAuthor.ListIndex = 0

        j = j + 1
    End While
    j = 0
    Me.cmbAuthor.Value = ActiveDocument.SelectContentControlsByTag("author").Item(1).Range.Text

    While j < numberOffices

        Me.cmbOffice.AddItem(strOfficeDetails(j, 1))
        Me.cmbOffice.ListIndex = 0
        j = j + 1
    End While
    'If ActiveDocument.SelectContentControlsByTag("postCode").Item(1).Range.Text = "4102" Then
    'Me.cmbOffice.Value = "Brisbane"
    'End If
    'If ActiveDocument.SelectContentControlsByTag("postCode").Item(1).Range.Text = "2060" Then
    'Me.cmbOffice.Value = "Sydney"
    'End If


    j = 0
    While j < numberStaff

        Me.cmbProjMan.AddItem(strUserDetails(j, 1))
        Me.cmbProjMan.ListIndex = 0
        j = j + 1
    End While
    Me.cmbProjMan.Value = ActiveDocument.SelectContentControlsByTag("projectManager").Item(1).Range.Text

    'Set All Section Checkboxes to be checked by default
    chkExecSummary.Value = True
    'chkAppendicies.Value = True
    chkPlates.Value = True
    chkGlossary.Value = True
    chkTables.Value = True
    chkFigures.Value = True
    'chkStdDisclaimer = True

    txtSubHdr.SetFocus()
    txtSubHdr.SelStart = 0
    txtSubHdr.SelLength = Len(txtSubHdr.Text)

End Sub


﻿Private Sub Document_New()

    Load frmLetter
    frmLetter.Show()

End Sub

Sub Document_Edit()
    Load frmLetter
    frmLetter.CancelButton.Visible = False
    frmLetter.Show()
End Sub

Dim numberStaff, numberOffices As Integer
Dim strUserDetails(250, 3)
Dim strOfficeDetails(20, 7)
Option Explicit

Private Sub CancelButton_Click()
    On Error GoTo Err_CommandButtonCancel_Click
    Unload Me
    'If frmReport.Visible = True Then
    ActiveDocument.Close savechanges:=wdDoNotSaveChanges
    'End If
    End
Exit_CommandButtonCancel_Click:
    Exit Sub
Err_CommandButtonCancel_Click:
    Select Case Err

        Case Else
            MsgBox "Unanticipated error number:" & Err.Number & " :" & Err.Description
    End Select
End Sub

Private Sub OkButton_Click()
    'Declarations
    Dim j, AuthorNum, OfficeNum As Integer


    Application.ScreenUpdating = False

    'Input Checking
    If cmbTitle = "" Then
        MsgBox("Title Cannot be Blank", vbExclamation)
        cmbTitle.SetFocus()
        Exit Sub
    End If

    If txtFirstname = "" Then
        MsgBox("Firstname Cannot be Blank", vbExclamation)
        txtFirstname.SetFocus()
        Exit Sub
    End If

    If txtSurname = "" Then
        MsgBox("Surname Number Cannot be Blank", vbExclamation)
        txtSurname.SetFocus()
        Exit Sub
    End If

    If txtPosition = "" Then
        MsgBox("Position Cannot be Blank", vbExclamation)
        txtPosition.SetFocus()
        Exit Sub
    End If

    If txtCompany = "" Then
        MsgBox("Company Cannot be Blank", vbExclamation)
        txtCompany.SetFocus()
        Exit Sub
    End If

    If txtAddress = "" Then
        MsgBox("Address Cannot be Blank", vbExclamation)
        txtAddress.SetFocus()
        Exit Sub
    End If

    If cmbSender = "" Then
        MsgBox("Sender Cannot be Blank", vbExclamation)
        cmbSender.SetFocus()
        Exit Sub
    End If

    If cmbOffice = "" Then
        MsgBox("Office Cannot be Blank", vbExclamation)
        cmbOffice.SetFocus()
        Exit Sub
    End If

    'Set Sender according toform submission
    j = 0
    While j < numberStaff

        If cmbSender = strUserDetails(j, 1) Then
            AuthorNum = j
            j = numberStaff
        Else
            j = j + 1
        End If
    End While
    'Set office accoriding to form submission
    j = 0
    While j < numberOffices

        If cmbOffice = strOfficeDetails(j, 1) Then
            OfficeNum = j
            j = numberOffices
        Else
            j = j + 1
        End If

    End While
    'hide form
    Me.Hide()

    With ActiveDocument


        'Add Data to new Template
        'officeAddress
        '.Bookmarks("OfficeAddress").Range.Text = strOfficeDetails(OfficeNum, 2)
        .SelectContentControlsByTag("officeAddress").Item(1).LockContents = False
        .SelectContentControlsByTag("officeAddress").Item(1).Range.Text = strOfficeDetails(OfficeNum, 2)
        '.SelectContentControlsByTag("officeAddress").Item(1).LockContents = True

        'officePhone
        '.Bookmarks("OfficePhone").Range.Text = strOfficeDetails(OfficeNum, 6)
        .SelectContentControlsByTag("officePhone").Item(1).LockContents = False
        .SelectContentControlsByTag("officePhone").Item(1).Range.Text = strOfficeDetails(OfficeNum, 6)
        '.SelectContentControlsByTag("officePhone").Item(1).LockContents = True

        'officeFax
        '.Bookmarks("OfficeFax").Range.Text = strOfficeDetails(OfficeNum, 7)
        .SelectContentControlsByTag("officeFax").Item(1).LockContents = False
        .SelectContentControlsByTag("officeFax").Item(1).Range.Text = strOfficeDetails(OfficeNum, 7)
        '.SelectContentControlsByTag("officeFax").Item(1).LockContents = True

        'suburb
        '.Bookmarks("Suburb").Range.Text = strOfficeDetails(OfficeNum, 3)
        .SelectContentControlsByTag("suburb").Item(1).LockContents = False
        .SelectContentControlsByTag("suburb").Item(1).Range.Text = strOfficeDetails(OfficeNum, 3)
        '.SelectContentControlsByTag("suburb").Item(1).LockContents = True

        'state
        '.Bookmarks("State").Range.Text = strOfficeDetails(OfficeNum, 4)
        .SelectContentControlsByTag("state").Item(1).LockContents = False
        .SelectContentControlsByTag("state").Item(1).Range.Text = strOfficeDetails(OfficeNum, 4)
        '.SelectContentControlsByTag("state").Item(1).LockContents = True

        'postCode
        '.Bookmarks("PostCode").Range.Text = strOfficeDetails(OfficeNum, 5)
        .SelectContentControlsByTag("postCode").Item(1).LockContents = False
        .SelectContentControlsByTag("postCode").Item(1).Range.Text = strOfficeDetails(OfficeNum, 5)
        '.SelectContentControlsByTag("postCode").Item(1).LockContents = True

        'title
        '.Bookmarks("Title").Range.Text = cmbTitle.Value
        .SelectContentControlsByTag("title").Item(1).LockContents = False
        .SelectContentControlsByTag("title").Item(1).Range.Text = cmbTitle.Value
        '.SelectContentControlsByTag("title").Item(1).LockContents = True

        'firstName
        '.Bookmarks("Firstname").Range.Text = txtFirstname.Value
        .SelectContentControlsByTag("firstName").Item(1).LockContents = False
        .SelectContentControlsByTag("firstName").Item(1).Range.Text = txtFirstname.Value
        '.SelectContentControlsByTag("firstName").Item(1).LockContents = True

        'surname
        '.Bookmarks("Surname").Range.Text = txtSurname.Value
        .SelectContentControlsByTag("surname").Item(1).LockContents = False
        .SelectContentControlsByTag("surname").Item(1).Range.Text = txtSurname.Value
        '.SelectContentControlsByTag("surname").Item(1).LockContents = True

        'position
        '.Bookmarks("Position").Range.Text = txtPosition.Value
        .SelectContentControlsByTag("position").Item(1).LockContents = False
        .SelectContentControlsByTag("position").Item(1).Range.Text = txtPosition.Value
        '.SelectContentControlsByTag("position").Item(1).LockContents = True

        'company
        '.Bookmarks("Company").Range.Text = txtCompany.Value
        .SelectContentControlsByTag("company").Item(1).LockContents = False
        .SelectContentControlsByTag("company").Item(1).Range.Text = txtCompany.Value
        '.SelectContentControlsByTag("company").Item(1).LockContents = True

        'companyAddress
        '.Bookmarks("CompanyAddress").Range.Text = txtAddress.Value
        .SelectContentControlsByTag("companyAddress").Item(1).LockContents = False
        .SelectContentControlsByTag("companyAddress").Item(1).Range.Text = txtAddress.Value
        '.SelectContentControlsByTag("companyAddress").Item(1).LockContents = True

        'date
        '.Bookmarks("Date").Range.Text = txtDate.Value
        .SelectContentControlsByTag("date").Item(1).LockContents = False
        .SelectContentControlsByTag("date").Item(1).Range.Text = txtDate.Value
        '.SelectContentControlsByTag("date").Item(1).LockContents = True

        'projNum
        '.Bookmarks("ProjNum").Range.Text = txtProjNum.Value
        .SelectContentControlsByTag("projectNum").Item(1).LockContents = False
        .SelectContentControlsByTag("projectNum").Item(1).Range.Text = txtProjNum.Value
        '.SelectContentControlsByTag("projNum").Item(1).LockContents = True

        'salutation
        '.Bookmarks("Salutation").Range.Text = txtSalutation.Value
        .SelectContentControlsByTag("salutation").Item(1).LockContents = False
        .SelectContentControlsByTag("salutation").Item(1).Range.Text = txtSalutation.Value
        '.SelectContentControlsByTag("salutation").Item(1).LockContents = True

        'subject
        '.Bookmarks("Subject").Range.Text = txtSubject.Value
        .SelectContentControlsByTag("subject").Item(1).LockContents = False
        .SelectContentControlsByTag("subject").Item(1).Range.Text = txtSubject.Value
        '.SelectContentControlsByTag("subject").Item(1).LockContents = True

        'author
        '.Bookmarks("Author").Range.Text = strUserDetails(AuthorNum, 1)
        .SelectContentControlsByTag("author").Item(1).LockContents = False
        .SelectContentControlsByTag("author").Item(1).Range.Text = strUserDetails(AuthorNum, 1)
        '.SelectContentControlsByTag("author").Item(1).LockContents = True

        'senderTitle
        '.Bookmarks("SenderTitle").Range.Text = strUserDetails(AuthorNum, 2)
        .SelectContentControlsByTag("senderTitle").Item(1).LockContents = False
        .SelectContentControlsByTag("senderTitle").Item(1).Range.Text = strUserDetails(AuthorNum, 2)
        '.SelectContentControlsByTag("senderTitle").Item(1).LockContents = True

        'enclosures
        '.Bookmarks("Enclosures").Range.Text = txtEnclosures.Value
        .SelectContentControlsByTag("enclosures").Item(1).LockContents = False
        .SelectContentControlsByTag("enclosures").Item(1).Range.Text = txtEnclosures.Value
        '.SelectContentControlsByTag("enclosures").Item(1).LockContents = True

        'cc
        '.Bookmarks("CC").Range.Text = txtCC.Value
        .SelectContentControlsByTag("cc").Item(1).LockContents = False
        .SelectContentControlsByTag("cc").Item(1).Range.Text = txtCC.Value
        '.SelectContentControlsByTag("cc").Item(1).LockContents = True

        'subjectHdr
        '.Bookmarks("SubjectHdr").Range.Text = txtSubject.Value
        .SelectContentControlsByTag("subjectHdr").Item(1).LockContents = False
        .SelectContentControlsByTag("subjectHdr").Item(1).Range.Text = txtSubject.Value
        '.SelectContentControlsByTag("subjectHdr").Item(1).LockContents = True

        'dateHdr
        '.Bookmarks("DateHdr").Range.Text = txtDate.Value
        .SelectContentControlsByTag("dateHdr").Item(1).LockContents = False
        .SelectContentControlsByTag("dateHdr").Item(1).Range.Text = txtDate.Value
        '.SelectContentControlsByTag("dateHdr").Item(1).LockContents = True

        'companyHdr
        '.Bookmarks("CompanyHdr").Range.Text = txtCompany.Value
        .SelectContentControlsByTag("companyHdr").Item(1).LockContents = False
        .SelectContentControlsByTag("companyHdr").Item(1).Range.Text = txtCompany.Value
        '.SelectContentControlsByTag("companyHdr").Item(1).LockContents = True

    End With
    Application.Run macroname:="UpdateAllFields"

    'Unload the form
    Unload Me
    'Focus the E3 Consult Ribbon
    SendKeys("%(Y)", False)
    SendKeys("{ESC}", False)
    SendKeys("{ESC}", False)

    'Error Handling

Exit_CommandButtonOK_Click:
    Exit Sub

Err_CommandButtonOK_Click:
    Select Case Err
        Case Is = 5941 'Bookmark not in document
            Resume Next
        Case Else
            MsgBox "Unanticipated error number:" & Err.Number & " :" & Err.Description
    End Select
End Sub

Private Sub optMadam_Click()
    Me.txtSalutation.Value = "Madam"
End Sub

Private Sub optName_Click()
    Me.txtSalutation.Value = txtFirstname.Value
End Sub

Private Sub optSir_Click()
    Me.txtSalutation.Value = "Sir"
End Sub

Private Sub optSirMadam_Click()
    Me.txtSalutation.Value = "Sir / Madam"
End Sub

Private Sub UserForm_Initialize()
    'Set Todays Date

    cmbTitle.SetFocus()
    Dim strFilePath, strFileOpen As String
    Dim i, commaPosition As Integer
    strFilePath = Options.DefaultFilePath(Path:=wdUserTemplatesPath)

    If ActiveDocument.SelectContentControlsByTag("date").Item(1).Range.Text = " " Then
    txtDate.Text = Format(Date, "d mmmm yyyy")
    Else
        txtDate.Value = ActiveDocument.SelectContentControlsByTag("date").Item(1).Range.Text
    End If

    cmbTitle.Value = ActiveDocument.SelectContentControlsByTag("title").Item(1).Range.Text
    txtFirstname.Value = ActiveDocument.SelectContentControlsByTag("firstName").Item(1).Range.Text
    txtSurname.Value = ActiveDocument.SelectContentControlsByTag("surname").Item(1).Range.Text
    txtPosition.Value = ActiveDocument.SelectContentControlsByTag("position").Item(1).Range.Text
    txtCompany.Value = ActiveDocument.SelectContentControlsByTag("company").Item(1).Range.Text
    txtAddress.Value = ActiveDocument.SelectContentControlsByTag("companyAddress").Item(1).Range.Text
    txtSubject.Value = ActiveDocument.SelectContentControlsByTag("subject").Item(1).Range.Text
    txtEnclosures.Value = ActiveDocument.SelectContentControlsByTag("enclosures").Item(1).Range.Text
    txtCC.Value = ActiveDocument.SelectContentControlsByTag("cc").Item(1).Range.Text
    txtSalutation.Value = ActiveDocument.SelectContentControlsByTag("salutation").Item(1).Range.Text
    'txtProjNum.Value = ActiveDocument.SelectContentControlsByTag("projNum").Item(1).Range.Text




    'Open csv file for reading in variables
    strFileOpen = strFilePath + "\E3Staff.csv"

Open strFileOpen For Input As #1

    i = 0


    While Not EOF(1)
    Line Input #1, strUserDetails(i, 0)

        commaPosition = InStr(strUserDetails(i, 0), ",")
        strUserDetails(i, 1) = Mid(strUserDetails(i, 0), 1, commaPosition - 1)
        strUserDetails(i, 0) = Mid(strUserDetails(i, 0), commaPosition + 1)

        commaPosition = InStr(strUserDetails(i, 0), ",")
        strUserDetails(i, 2) = Mid(strUserDetails(i, 0), 1, commaPosition - 1)
        strUserDetails(i, 0) = Mid(strUserDetails(i, 0), commaPosition + 1)

        commaPosition = InStr(strUserDetails(i, 0), ",")
        strUserDetails(i, 3) = Mid(strUserDetails(i, 0), 1, commaPosition - 1)
        strUserDetails(i, 0) = Mid(strUserDetails(i, 0), commaPosition + 1)


        i = i + 1

    End While

    numberStaff = i
Close #1
    strFileOpen = strFilePath + "\E3Office.csv"

Open strFileOpen For Input As #1
    'Declare Array

    'Reset Counter
    i = 0

    While Not EOF(1)
    Line Input #1, strOfficeDetails(i, 0)

        commaPosition = InStr(strOfficeDetails(i, 0), ",")
        strOfficeDetails(i, 1) = Mid(strOfficeDetails(i, 0), 1, commaPosition - 1)
        strOfficeDetails(i, 0) = Mid(strOfficeDetails(i, 0), commaPosition + 1)

        commaPosition = InStr(strOfficeDetails(i, 0), ",")
        strOfficeDetails(i, 2) = Mid(strOfficeDetails(i, 0), 1, commaPosition - 1)
        strOfficeDetails(i, 0) = Mid(strOfficeDetails(i, 0), commaPosition + 1)

        commaPosition = InStr(strOfficeDetails(i, 0), ",")
        strOfficeDetails(i, 3) = Mid(strOfficeDetails(i, 0), 1, commaPosition - 1)
        strOfficeDetails(i, 0) = Mid(strOfficeDetails(i, 0), commaPosition + 1)

        commaPosition = InStr(strOfficeDetails(i, 0), ",")
        strOfficeDetails(i, 4) = Mid(strOfficeDetails(i, 0), 1, commaPosition - 1)
        strOfficeDetails(i, 0) = Mid(strOfficeDetails(i, 0), commaPosition + 1)

        commaPosition = InStr(strOfficeDetails(i, 0), ",")
        strOfficeDetails(i, 5) = Mid(strOfficeDetails(i, 0), 1, commaPosition - 1)
        strOfficeDetails(i, 0) = Mid(strOfficeDetails(i, 0), commaPosition + 1)

        commaPosition = InStr(strOfficeDetails(i, 0), ",")
        strOfficeDetails(i, 6) = Mid(strOfficeDetails(i, 0), 1, commaPosition - 1)
        strOfficeDetails(i, 0) = Mid(strOfficeDetails(i, 0), commaPosition + 1)

        commaPosition = InStr(strOfficeDetails(i, 0), ",")
        strOfficeDetails(i, 7) = Mid(strOfficeDetails(i, 0), 1, commaPosition - 1)
        strOfficeDetails(i, 0) = Mid(strOfficeDetails(i, 0), commaPosition + 1)

        i = i + 1

    End While
    numberOffices = i
Close #1

    Dim j As Integer
    j = 0
    While j < numberStaff

        Me.cmbSender.AddItem(strUserDetails(j, 1))
        Me.cmbSender.ListIndex = 0
        j = j + 1
    End While
    Me.cmbSender.Value = ActiveDocument.SelectContentControlsByTag("author").Item(1).Range.Text
    j = 0
    While j < numberOffices

        Me.cmbOffice.AddItem(strOfficeDetails(j, 1))
        Me.cmbOffice.ListIndex = 0
        j = j + 1
    End While

    If ActiveDocument.SelectContentControlsByTag("postCode").Item(1).Range.Text = "4102" Then
        Me.cmbOffice.Value = "Brisbane"
    End If
    If ActiveDocument.SelectContentControlsByTag("postCode").Item(1).Range.Text = "2060" Then
        Me.cmbOffice.Value = "Sydney"
    End If

    Me.cmbTitle.AddItem("Mr")
    Me.cmbTitle.AddItem("Mrs")
    Me.cmbTitle.AddItem("Ms")
    Me.cmbTitle.AddItem("Miss")
    Me.cmbTitle.AddItem("Dr")

End Sub



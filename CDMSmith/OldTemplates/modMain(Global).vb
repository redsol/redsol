﻿Sub AutoExec()

    SendKeys("%(Y)", False)
    SendKeys("{ESC}", False)
    SendKeys("{ESC}", False)

End Sub

Sub GenerateCDMFax(ByVal control As IRibbonControl)
    Documents.Add Template:="CDMSmith-Fax.dotm"

    Selection.Find.ClearFormatting()

End Sub

Sub GenerateCDMLtr(ByVal control As IRibbonControl)
    Documents.Add Template:="CDMSmith-Letter.dotm"

    Selection.Find.ClearFormatting()

End Sub

Sub GenerateCDMRpt(ByVal control As IRibbonControl)
    Documents.Add Template:="CDMSmith-Report.dotm"

    Selection.Find.ClearFormatting()

End Sub

Sub GenerateCDMBlank(ByVal control As IRibbonControl)
    Documents.Add Template:="CDMSmith-Blank.dotm"

    Selection.Find.ClearFormatting()

End Sub


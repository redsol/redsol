﻿Private Sub Document_New()

    Load frmFax
    frmFax.Show()

End Sub

Sub Document_Edit()
    Load frmFax
    frmFax.CancelButton.Visible = False
    frmFax.Show()

End Sub


Dim numberStaff, numberOffices As Integer
Dim strUserDetails(250, 3)
Dim strOfficeDetails(20, 7)

Option Explicit
Private Sub txtDate_MouseUp(ByVal Button As Integer, ByVal Shift As Integer, ByVal X As Single, ByVal Y As Single)
    txtDate.SelStart = 0
    txtDate.SelLength = Len(txtDate.Text)
End Sub

Private Sub txtTo_MouseUp(ByVal Button As Integer, ByVal Shift As Integer, ByVal X As Single, ByVal Y As Single)
    txtTo.SelStart = 0
    txtTo.SelLength = Len(txtTo.Text)
End Sub

Private Sub txtAttn_MouseUp(ByVal Button As Integer, ByVal Shift As Integer, ByVal X As Single, ByVal Y As Single)
    txtAttn.SelStart = 0
    txtAttn.SelLength = Len(txtAttn.Text)
End Sub

Private Sub txtFaxNum_MouseUp(ByVal Button As Integer, ByVal Shift As Integer, ByVal X As Single, ByVal Y As Single)
    txtFaxNum.SelStart = 0
    txtFaxNum.SelLength = Len(txtFaxNum.Text)
End Sub

Private Sub txtProjNum_MouseUp(ByVal Button As Integer, ByVal Shift As Integer, ByVal X As Single, ByVal Y As Single)
    txtProjNum.SelStart = 0
    txtProjNum.SelLength = Len(txtProjNum.Text)
End Sub

Private Sub txtSalutation_MouseUp(ByVal Button As Integer, ByVal Shift As Integer, ByVal X As Single, ByVal Y As Single)
    txtSalutation.SelStart = 0
    txtSalutation.SelLength = Len(txtSalutation.Text)
End Sub

Private Sub txtcc_MouseUp(ByVal Button As Integer, ByVal Shift As Integer, ByVal X As Single, ByVal Y As Single)
    txtcc.SelStart = 0
    txtcc.SelLength = Len(txtcc.Text)
End Sub

Private Sub cmbAuthor_MouseUp(ByVal Button As Integer, ByVal Shift As Integer, ByVal X As Single, ByVal Y As Single)
    cmbAuthor.SelStart = 0
    cmbAuthor.SelLength = Len(cmbAuthor.Text)
End Sub

Private Sub cmbOffice_MouseUp(ByVal Button As Integer, ByVal Shift As Integer, ByVal X As Single, ByVal Y As Single)
    cmbOffice.SelStart = 0
    cmbOffice.SelLength = Len(cmbOffice.Text)
End Sub

Private Sub txtSubject_MouseUp(ByVal Button As Integer, ByVal Shift As Integer, ByVal X As Single, ByVal Y As Single)
    txtSubject.SelStart = 0
    txtSubject.SelLength = Len(txtSubject.Text)
End Sub

Private Sub txtPages_MouseUp(ByVal Button As Integer, ByVal Shift As Integer, ByVal X As Single, ByVal Y As Single)
    txtPages.SelStart = 0
    txtPages.SelLength = Len(txtPages.Text)
End Sub


Private Sub CancelButton_Click()
    On Error GoTo Err_CommandButtonCancel_Click
    Unload Me
    ActiveDocument.Close savechanges:=wdDoNotSaveChanges
    End
Exit_CommandButtonCancel_Click:
    Exit Sub
Err_CommandButtonCancel_Click:
    Select Case Err

        Case Else
            MsgBox "Unanticipated error number:" & Err.Number & " :" & Err.Description
    End Select
End Sub

Private Sub OkButton_Click()
    Dim faxDate, faxTo, faxNum, faxCc, faxSubject, faxNumPages, faxProjNum As String
    Dim faxAuthor, faxPosition, faxOffice, faxPhone, faxFax, faxSalutation As String
    Dim j, authorNum, OfficeNum As Integer
    Application.ScreenUpdating = False

    'Input Checking
    If txtDate = "" Then
        MsgBox("Date Cannot be Blank", vbExclamation)
        txtDate.SetFocus()
        Exit Sub
    End If

    If txtTo = "" Then
        MsgBox("Company Cannot be Blank", vbExclamation)
        txtTo.SetFocus()
        Exit Sub
    End If

    If txtFaxNum = "" Then
        MsgBox("Fax Number Cannot be Blank", vbExclamation)
        txtFaxNum.SetFocus()
        Exit Sub
    End If

    If txtAttn = "" Then
        MsgBox("Attention Cannot be Blank", vbExclamation)
        txtAttn.SetFocus()
        Exit Sub
    End If

    If cmbAuthor = "" Then
        MsgBox("Author Cannot be Blank", vbExclamation)
        cmbAuthor.SetFocus()
        Exit Sub
    End If

    If cmbOffice = "" Then
        MsgBox("Office Cannot be Blank", vbExclamation)
        cmbOffice.SetFocus()
        Exit Sub
    End If

    'Set Author according to form submission
    j = 0
    While j < numberStaff

        If cmbAuthor = strUserDetails(j, 1) Then
            authorNum = j
            j = numberStaff
        Else
            j = j + 1
        End If
    End While
    'Set office accoriding to form submission
    j = 0
    While j < numberOffices

        If cmbOffice = strOfficeDetails(j, 1) Then
            OfficeNum = j
            j = numberOffices
        Else
            j = j + 1
        End If

    End While

    'hide form
    Me.Hide()


    With ActiveDocument

        'Add Data to new Template
        'officeAddress
        '.Bookmarks("OfficeAddress").Range.Text = strOfficeDetails(OfficeNum, 2)
        .SelectContentControlsByTag("officeAddress").Item(1).LockContents = False
        .SelectContentControlsByTag("officeAddress").Item(1).Range.Text = strOfficeDetails(OfficeNum, 2)
        '.SelectContentControlsByTag("officeAddress").Item(1).LockContents = True

        'officePhone
        '.Bookmarks("OfficePhone").Range.Text = strOfficeDetails(OfficeNum, 6)
        .SelectContentControlsByTag("officePhone").Item(1).LockContents = False
        .SelectContentControlsByTag("officePhone").Item(1).Range.Text = strOfficeDetails(OfficeNum, 6)
        '.SelectContentControlsByTag("officePhone").Item(1).LockContents = True

        'officeFax
        '.Bookmarks("OfficeFax").Range.Text = strOfficeDetails(OfficeNum, 7)
        .SelectContentControlsByTag("officeFax").Item(1).LockContents = False
        .SelectContentControlsByTag("officeFax").Item(1).Range.Text = strOfficeDetails(OfficeNum, 7)
        '.SelectContentControlsByTag("officeFax").Item(1).LockContents = True

        'suburb
        '.Bookmarks("Suburb").Range.Text = strOfficeDetails(OfficeNum, 3)
        .SelectContentControlsByTag("suburb").Item(1).LockContents = False
        .SelectContentControlsByTag("suburb").Item(1).Range.Text = strOfficeDetails(OfficeNum, 3)
        '.SelectContentControlsByTag("suburb").Item(1).LockContents = True

        'state
        '.Bookmarks("State").Range.Text = strOfficeDetails(OfficeNum, 4)
        .SelectContentControlsByTag("state").Item(1).LockContents = False
        .SelectContentControlsByTag("state").Item(1).Range.Text = strOfficeDetails(OfficeNum, 4)
        '.SelectContentControlsByTag("state").Item(1).LockContents = True

        'postCode
        '.Bookmarks("PostCode").Range.Text = strOfficeDetails(OfficeNum, 5)
        .SelectContentControlsByTag("postCode").Item(1).LockContents = False
        .SelectContentControlsByTag("postCode").Item(1).Range.Text = strOfficeDetails(OfficeNum, 5)
        '.SelectContentControlsByTag("postCode").Item(1).LockContents = True

        'company
        '.Bookmarks("Company").Range.Text = txtTo.Value
        .SelectContentControlsByTag("company").Item(1).LockContents = False
        .SelectContentControlsByTag("company").Item(1).Range.Text = txtTo.Value
        '.SelectContentControlsByTag("company").Item(1).LockContents = True

        'authorHdr
        '.Bookmarks("AuthorHdr").Range.Text = strUserDetails(authorNum, 1)
        .SelectContentControlsByTag("authorHdr").Item(1).LockContents = False
        .SelectContentControlsByTag("authorHdr").Item(1).Range.Text = strUserDetails(authorNum, 1)
        '.SelectContentControlsByTag("authorHdr").Item(1).LockContents = True

        'attention
        '.Bookmarks("Attn").Range.Text = txtAttn.Value
        .SelectContentControlsByTag("attention").Item(1).LockContents = False
        .SelectContentControlsByTag("attention").Item(1).Range.Text = txtAttn.Value
        '.SelectContentControlsByTag("attention").Item(1).LockContents = True

        'projNum
        '.Bookmarks("ProjNum").Range.Text = txtProjNum.Value
        .SelectContentControlsByTag("projNum").Item(1).LockContents = False
        .SelectContentControlsByTag("projNum").Item(1).Range.Text = txtProjNum.Value
        '.SelectContentControlsByTag("projNum").Item(1).LockContents = True

        'toFaxNumber
        '.Bookmarks("ToFaxNum").Range.Text = txtFaxNum.Value
        .SelectContentControlsByTag("toFaxNumber").Item(1).LockContents = False
        .SelectContentControlsByTag("toFaxNumber").Item(1).Range.Text = txtFaxNum.Value
        '.SelectContentControlsByTag("toFaxNumber").Item(1).LockContents = True

        'date
        '.Bookmarks("date").Range.Text = txtDate.Value
        .SelectContentControlsByTag("date").Item(1).LockContents = False
        .SelectContentControlsByTag("date").Item(1).Range.Text = txtDate.Value
        '.SelectContentControlsByTag("date").Item(1).LockContents = True

        'cc
        '.Bookmarks("CC").Range.Text = txtcc.Value
        .SelectContentControlsByTag("cc").Item(1).LockContents = False
        .SelectContentControlsByTag("cc").Item(1).Range.Text = txtcc.Value
        '.SelectContentControlsByTag("cc").Item(1).LockContents = True

        'numOfPages
        '.Bookmarks("NumOfPages").Range.Text = txtPages.Value
        .SelectContentControlsByTag("numOfPages").Item(1).LockContents = False
        .SelectContentControlsByTag("numOfPages").Item(1).Range.Text = txtPages.Value
        '.SelectContentControlsByTag("numOfPages").Item(1).LockContents = True

        'subject
        '.Bookmarks("Subject").Range.Text = txtSubject.Value
        .SelectContentControlsByTag("subject").Item(1).LockContents = False
        .SelectContentControlsByTag("subject").Item(1).Range.Text = txtSubject.Value
        '.SelectContentControlsByTag("subject").Item(1).LockContents = True

        'salutation
        '.Bookmarks("Salutation").Range.Text = txtSalutation.Value
        .SelectContentControlsByTag("salutation").Item(1).LockContents = False
        .SelectContentControlsByTag("salutation").Item(1).Range.Text = txtSalutation.Value
        '.SelectContentControlsByTag("salutation").Item(1).LockContents = True

        'author
        '.Bookmarks("Author").Range.Text = strUserDetails(authorNum, 1)
        .SelectContentControlsByTag("author").Item(1).LockContents = False
        .SelectContentControlsByTag("author").Item(1).Range.Text = strUserDetails(authorNum, 1)
        '.SelectContentControlsByTag("author").Item(1).LockContents = True

        'position
        '.Bookmarks("Position").Range.Text = strUserDetails(authorNum, 2)
        .SelectContentControlsByTag("position").Item(1).LockContents = False
        .SelectContentControlsByTag("position").Item(1).Range.Text = strUserDetails(authorNum, 2)
        '.SelectContentControlsByTag("position").Item(1).LockContents = True

        'subjectHdr
        '.Bookmarks("SubjectHeader").Range.Text = txtSubject.Value
        '.SelectContentControlsByTag("subjectHdr").Item(1).LockContents = False
        '.SelectContentControlsByTag("subjectHdr").Item(1).Range.Text = txtSubject.Value
        '.SelectContentControlsByTag("subjectHdr").Item(1).LockContents = True

        'dateHdr
        '.Bookmarks("DateHeader").Range.Text = txtDate.Value
        '.SelectContentControlsByTag("dateHdr").Item(1).LockContents = False
        '.SelectContentControlsByTag("dateHdr").Item(1).Range.Text = txtDate.Value
        '.SelectContentControlsByTag("dateHdr").Item(1).LockContents = True


    End With

    Application.Run macroname:="UpdateAllFields"

    'Unload the form
    Unload Me
    'Focus the E3 Consult Ribbon
    SendKeys("%(Y)", False)
    SendKeys("{ESC}", False)
    SendKeys("{ESC}", False)

    'Error Handling

Exit_CommandButtonOK_Click:
    Exit Sub

Err_CommandButtonOK_Click:
    Select Case Err
        Case Is = 5941 'Bookmark not in document
            Resume Next
        Case Else
            MsgBox "Unanticipated error number:" & Err.Number & " :" & Err.Description
    End Select
End Sub



Private Sub optMadam_Click()
    Me.txtSalutation.Value = "Madam"
End Sub

Private Sub optName_Click()
    Me.txtSalutation.Value = txtAttn.Value
End Sub

Private Sub optSir_Click()
    Me.txtSalutation.Value = "Sir"
End Sub

Private Sub optSirMadam_Click()
    Me.txtSalutation.Value = "Sir / Madam"
End Sub

Private Sub UserForm_Initialize()
    Dim strFilePath, strFileOpen As String
    Dim i, commaPosition As Integer
    'Set Todays Date
    strFilePath = Options.DefaultFilePath(Path:=wdUserTemplatesPath)
    If ActiveDocument.SelectContentControlsByTag("date").Item(1).Range.Text = " " Then
    txtDate.Text = Format(Date, "d mmmm yyyy")
    Else
        txtDate.Value = ActiveDocument.SelectContentControlsByTag("date").Item(1).Range.Text
    End If

    txtTo.Value = ActiveDocument.SelectContentControlsByTag("company").Item(1).Range.Text
    txtAttn.Value = ActiveDocument.SelectContentControlsByTag("attention").Item(1).Range.Text
    txtFaxNum.Value = ActiveDocument.SelectContentControlsByTag("toFaxNumber").Item(1).Range.Text
    txtProjNum.Value = ActiveDocument.SelectContentControlsByTag("projNum").Item(1).Range.Text
    txtSalutation.Value = ActiveDocument.SelectContentControlsByTag("salutation").Item(1).Range.Text
    txtcc.Value = ActiveDocument.SelectContentControlsByTag("cc").Item(1).Range.Text
    txtSubject.Value = ActiveDocument.SelectContentControlsByTag("subject").Item(1).Range.Text
    txtPages.Value = ActiveDocument.SelectContentControlsByTag("numOfPages").Item(1).Range.Text


    'Open csv file for reading in variables
    strFileOpen = strFilePath + "\E3Staff.csv"

Open strFileOpen For Input As #1

    i = 0


    While Not EOF(1)
    Line Input #1, strUserDetails(i, 0)

        commaPosition = InStr(strUserDetails(i, 0), ",")
        strUserDetails(i, 1) = Mid(strUserDetails(i, 0), 1, commaPosition - 1)
        strUserDetails(i, 0) = Mid(strUserDetails(i, 0), commaPosition + 1)

        commaPosition = InStr(strUserDetails(i, 0), ",")
        strUserDetails(i, 2) = Mid(strUserDetails(i, 0), 1, commaPosition - 1)
        strUserDetails(i, 0) = Mid(strUserDetails(i, 0), commaPosition + 1)

        commaPosition = InStr(strUserDetails(i, 0), ",")
        strUserDetails(i, 3) = Mid(strUserDetails(i, 0), 1, commaPosition - 1)
        strUserDetails(i, 0) = Mid(strUserDetails(i, 0), commaPosition + 1)


        i = i + 1

    End While

    numberStaff = i
Close #1
    strFileOpen = strFilePath + "\E3Office.csv"

Open strFileOpen For Input As #1
    'Declare Array

    'Reset Counter
    i = 0

    While Not EOF(1)
    Line Input #1, strOfficeDetails(i, 0)

        commaPosition = InStr(strOfficeDetails(i, 0), ",")
        strOfficeDetails(i, 1) = Mid(strOfficeDetails(i, 0), 1, commaPosition - 1)
        strOfficeDetails(i, 0) = Mid(strOfficeDetails(i, 0), commaPosition + 1)

        commaPosition = InStr(strOfficeDetails(i, 0), ",")
        strOfficeDetails(i, 2) = Mid(strOfficeDetails(i, 0), 1, commaPosition - 1)
        strOfficeDetails(i, 0) = Mid(strOfficeDetails(i, 0), commaPosition + 1)

        commaPosition = InStr(strOfficeDetails(i, 0), ",")
        strOfficeDetails(i, 3) = Mid(strOfficeDetails(i, 0), 1, commaPosition - 1)
        strOfficeDetails(i, 0) = Mid(strOfficeDetails(i, 0), commaPosition + 1)

        commaPosition = InStr(strOfficeDetails(i, 0), ",")
        strOfficeDetails(i, 4) = Mid(strOfficeDetails(i, 0), 1, commaPosition - 1)
        strOfficeDetails(i, 0) = Mid(strOfficeDetails(i, 0), commaPosition + 1)

        commaPosition = InStr(strOfficeDetails(i, 0), ",")
        strOfficeDetails(i, 5) = Mid(strOfficeDetails(i, 0), 1, commaPosition - 1)
        strOfficeDetails(i, 0) = Mid(strOfficeDetails(i, 0), commaPosition + 1)

        commaPosition = InStr(strOfficeDetails(i, 0), ",")
        strOfficeDetails(i, 6) = Mid(strOfficeDetails(i, 0), 1, commaPosition - 1)
        strOfficeDetails(i, 0) = Mid(strOfficeDetails(i, 0), commaPosition + 1)

        commaPosition = InStr(strOfficeDetails(i, 0), ",")
        strOfficeDetails(i, 7) = Mid(strOfficeDetails(i, 0), 1, commaPosition - 1)
        strOfficeDetails(i, 0) = Mid(strOfficeDetails(i, 0), commaPosition + 1)

        i = i + 1

    End While
    numberOffices = i
Close #1

    Dim j As Integer
    j = 0
    While j < numberStaff

        Me.cmbAuthor.AddItem(strUserDetails(j, 1))
        Me.cmbAuthor.ListIndex = 0
        j = j + 1
    End While
    cmbAuthor.Value = ActiveDocument.SelectContentControlsByTag("author").Item(1).Range.Text
    j = 0
    While j < numberOffices

        Me.cmbOffice.AddItem(strOfficeDetails(j, 1))
        Me.cmbOffice.ListIndex = 0
        j = j + 1
    End While
    If ActiveDocument.SelectContentControlsByTag("postCode").Item(1).Range.Text = "4102" Then
        Me.cmbOffice.Value = "Brisbane"
    End If
    If ActiveDocument.SelectContentControlsByTag("postCode").Item(1).Range.Text = "2060" Then
        Me.cmbOffice.Value = "Sydney"
    End If

End Sub



﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Xml.Linq;
using Microsoft.Office.Tools.Word;
using Microsoft.VisualStudio.Tools.Applications.Runtime;
using Office = Microsoft.Office.Core;
using Word = Microsoft.Office.Interop.Word;

namespace Letter_CDMSmith
{
    public partial class ThisDocument
    {
        [CachedAttribute()]
        public string propertiesXMLPartID = string.Empty;
        private Office.CustomXMLPart propertiesXMLPart;
        private const string prefix = "xmlns:ns='CDMSmith'";

        private void ThisDocument_Startup(object sender, System.EventArgs e)
        {
            // Extract custom XML data and bind to content controls
            string xmlData = GetXML();

            if (xmlData != null)
            {
                AddCustomXMLPart(xmlData);
                BindControlsToXMLPart();
            }
           
        }

        private void ThisDocument_Shutdown(object sender, System.EventArgs e)
        {
        }
 
        /// <summary>
        /// Obtain XML Data from the Fax_CDMSmith.Properties embedded resource
        /// </summary>
        /// <returns>
        /// String variable containing entire XML Data from embedded resource
        /// </returns>
        private string GetXML()
        {
            System.Reflection.Assembly asm = System.Reflection.Assembly.GetExecutingAssembly();
            System.IO.Stream stream1 = asm.GetManifestResourceStream("Letter_CDMSmith.DocumentProperties.xml");

                using (System.IO.StreamReader resourceReader = new System.IO.StreamReader(stream1))
                {
                    if (resourceReader != null)
                    {
                        return resourceReader.ReadToEnd();
                    }
                }
            
            return null;

        }

        /// <summary>
        /// Adds custom XML Part to document if xmlData is not empty and the XML Part has already not been added
        /// </summary>
        /// <param name="xmlData"></param>
        private void AddCustomXMLPart(string xmlData)
        {
            if (xmlData != null)
            {
                propertiesXMLPart = this.CustomXMLParts.SelectByID(propertiesXMLPartID);
                if (propertiesXMLPart == null)
                {
                    propertiesXMLPart = this.CustomXMLParts.Add(xmlData);
                    propertiesXMLPart.NamespaceManager.AddNamespace("ns", "CDMSmith");
                    propertiesXMLPartID = propertiesXMLPart.Id;
                }
            }
        }
       
        /// <summary>
        /// Bind all Content Controls to respective XML Parts
        /// </summary>
        private void BindControlsToXMLPart()
        {

            string xPathTopLevel = "ns:CDMSmithProperties/";

            this.documentDate1.XMLMapping.SetMapping(xPathTopLevel + "ns:DocumentDate", prefix, propertiesXMLPart);
            this.documentDate2.XMLMapping.SetMapping(xPathTopLevel + "ns:DocumentDate", prefix, propertiesXMLPart);
            this.personalTitle.XMLMapping.SetMapping(xPathTopLevel + "ns:PersonalTitle", prefix, propertiesXMLPart);
            this.firstName.XMLMapping.SetMapping(xPathTopLevel + "ns:FirstName", prefix, propertiesXMLPart);
            this.surname.XMLMapping.SetMapping(xPathTopLevel + "ns:Surname", prefix, propertiesXMLPart);
            this.professionalTitle.XMLMapping.SetMapping(xPathTopLevel + "ns:ProfessionalTitle", prefix, propertiesXMLPart);
            this.companyName.XMLMapping.SetMapping(xPathTopLevel + "ns:CompanyName", prefix, propertiesXMLPart);
            this.companyNameHeader.XMLMapping.SetMapping(xPathTopLevel + "ns:CompanyName", prefix, propertiesXMLPart);
            this.companyAddress.XMLMapping.SetMapping(xPathTopLevel + "ns:CompanyAddress", prefix, propertiesXMLPart);
            this.enclosures.XMLMapping.SetMapping(xPathTopLevel + "ns:Enclosures", prefix, propertiesXMLPart);
            this.projectNum.XMLMapping.SetMapping(xPathTopLevel + "ns:ProjectNum", prefix, propertiesXMLPart);
            this.subject.XMLMapping.SetMapping(xPathTopLevel + "ns:Subject", prefix, propertiesXMLPart);
            this.subjectHeader.XMLMapping.SetMapping(xPathTopLevel + "ns:Subject", prefix, propertiesXMLPart);
            this.carbonCopy.XMLMapping.SetMapping(xPathTopLevel + "ns:CarbonCopy", prefix, propertiesXMLPart);
            this.salutation.XMLMapping.SetMapping(xPathTopLevel + "ns:Salutation", prefix, propertiesXMLPart);
            this.author.XMLMapping.SetMapping(xPathTopLevel + "ns:Author", prefix, propertiesXMLPart);
            this.authorTitle.XMLMapping.SetMapping(xPathTopLevel + "ns:Author/@Title", prefix, propertiesXMLPart);
            this.officeStreet.XMLMapping.SetMapping(xPathTopLevel + "ns:Office/@Street", prefix, propertiesXMLPart);
            this.officeSuburb.XMLMapping.SetMapping(xPathTopLevel + "ns:Office/@Suburb", prefix, propertiesXMLPart);
            this.officeState.XMLMapping.SetMapping(xPathTopLevel + "ns:Office/@State", prefix, propertiesXMLPart);
            this.officePostCode.XMLMapping.SetMapping(xPathTopLevel + "ns:Office/@PostCode", prefix, propertiesXMLPart);
            this.officePhone.XMLMapping.SetMapping(xPathTopLevel + "ns:Office/@Phone", prefix, propertiesXMLPart);
            this.officeFax.XMLMapping.SetMapping(xPathTopLevel + "ns:Office/@Fax", prefix, propertiesXMLPart);


        }


        #region VSTO Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InternalStartup()
        {
            this.Startup += new System.EventHandler(this.ThisDocument_Startup);
            this.Shutdown += new System.EventHandler(this.ThisDocument_Shutdown);

        }

        #endregion






  






    }
}

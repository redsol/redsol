﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Xml.Linq;
using Microsoft.Office.Tools.Word;
using Microsoft.VisualStudio.Tools.Applications.Runtime;
using Office = Microsoft.Office.Core;
using Microsoft.Office.Interop.Word;

namespace Fax_CDMSmith
{
    public partial class ThisDocument
    {
        [CachedAttribute()]
        public string propertiesXMLPartID = string.Empty;
        private Office.CustomXMLPart propertiesXMLPart;
        private const string prefix = "xmlns:ns='CDMSmith'";
        
        private void ThisDocument_Startup(object sender, System.EventArgs e)
        {

            // Extract custom XML data and bind to content controls
            string xmlData = GetXML();

            if (xmlData != null)
            {
                AddCustomXMLPart(xmlData);
                BindControlsToXMLPart();
            }
           
               
        }

        private void ThisDocument_Shutdown(object sender, System.EventArgs e)
        {
        }

        /// <summary>
        /// Obtain XML Data from the Fax_CDMSmith.Properties embedded resource
        /// </summary>
        /// <returns>
        /// String variable containing entire XML Data from embedded resource
        /// </returns>
        private string GetXML()
        {
            System.Reflection.Assembly asm = System.Reflection.Assembly.GetExecutingAssembly();
            System.IO.Stream stream1 = asm.GetManifestResourceStream("Fax_CDMSmith.DocumentProperties.xml");

                using (System.IO.StreamReader resourceReader = new System.IO.StreamReader(stream1))
                {
                    if (resourceReader != null)
                    {
                        return resourceReader.ReadToEnd();
                    }
                }
            
            return null;

        }

        /// <summary>
        /// Adds custom XML Part to document if xmlData is not empty and the XML Part has already not been added
        /// </summary>
        /// <param name="xmlData"></param>
        private void AddCustomXMLPart(string xmlData)
        {
            if (xmlData != null)
            {
                propertiesXMLPart = this.CustomXMLParts.SelectByID(propertiesXMLPartID);
                if (propertiesXMLPart == null)
                {
                    propertiesXMLPart = this.CustomXMLParts.Add(xmlData);
                    propertiesXMLPart.NamespaceManager.AddNamespace("ns", "CDMSmith");
                    propertiesXMLPartID = propertiesXMLPart.Id;
                }
            }
        }
        /// <summary>
        /// Bind all Content Controls to respective XML Parts
        /// </summary>
        private void BindControlsToXMLPart()
        {

            string xPathToPerson = "ns:CDMSmithProperties/ns:ToPerson";
            string xPathCarbonCopy = "ns:CDMSmithProperties/ns:CarbonCopy";
            string xPathCompany = "ns:CDMSmithProperties/ns:Company";
            string xPathFaxNum = "ns:CDMSmithProperties/ns:FaxNum";
            string xPathProjectNum = "ns:CDMSmithProperties/ns:ProjectNum";
            string xPathSubject = "ns:CDMSmithProperties/ns:Subject";
            string xPathPageCount = "ns:CDMSmithProperties/ns:PageCount";
            string xPathAuthor = "ns:CDMSmithProperties/ns:Author";
            string xPathTitle = "ns:CDMSmithProperties/ns:Author/@Title";
            string xPathOfficeAddress = "ns:CDMSmithProperties/ns:Office/@Street";
            string xPathSuburb = "ns:CDMSmithProperties/ns:Office/@Suburb";
            string xPathState = "ns:CDMSmithProperties/ns:Office/@State";
            string xPathPostCode = "ns:CDMSmithProperties/ns:Office/@PostCode";
            string xPathOfficePhone = "ns:CDMSmithProperties/ns:Office/@Phone";
            string xPathOfficeFax = "ns:CDMSmithProperties/ns:Office/@Fax";
            string xPathDocumentDate = "ns:CDMSmithProperties/ns:DocumentDate";

            
            this.toPersonHdr.XMLMapping.SetMapping(xPathToPerson, prefix, propertiesXMLPart);
            this.toPerson.XMLMapping.SetMapping(xPathToPerson, prefix, propertiesXMLPart);
            this.carbonCopy.XMLMapping.SetMapping(xPathCarbonCopy, prefix, propertiesXMLPart);
            this.company.XMLMapping.SetMapping(xPathCompany, prefix, propertiesXMLPart);
            this.faxNum.XMLMapping.SetMapping(xPathFaxNum, prefix, propertiesXMLPart);
            this.projectNum.XMLMapping.SetMapping(xPathProjectNum, prefix, propertiesXMLPart);
            this.subject.XMLMapping.SetMapping(xPathSubject, prefix, propertiesXMLPart);
            this.pageCount.XMLMapping.SetMapping(xPathPageCount, prefix, propertiesXMLPart);
            this.authorHdr.XMLMapping.SetMapping(xPathAuthor, prefix, propertiesXMLPart);
            this.author.XMLMapping.SetMapping(xPathAuthor, prefix, propertiesXMLPart);
            this.title.XMLMapping.SetMapping(xPathTitle, prefix, propertiesXMLPart);
            this.officeAddress.XMLMapping.SetMapping(xPathOfficeAddress, prefix, propertiesXMLPart);
            this.suburb.XMLMapping.SetMapping(xPathSuburb, prefix, propertiesXMLPart);
            this.state.XMLMapping.SetMapping(xPathState, prefix, propertiesXMLPart);
            this.postcode.XMLMapping.SetMapping(xPathPostCode, prefix, propertiesXMLPart);
            this.officePhone.XMLMapping.SetMapping(xPathOfficePhone, prefix, propertiesXMLPart);
            this.officeFax.XMLMapping.SetMapping(xPathOfficeFax, prefix, propertiesXMLPart);
            this.documentDate.XMLMapping.SetMapping(xPathDocumentDate, prefix, propertiesXMLPart);

        }

        #region VSTO Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InternalStartup()
        {
            this.Startup += new System.EventHandler(ThisDocument_Startup);
            this.Shutdown += new System.EventHandler(ThisDocument_Shutdown);
        }

        #endregion
    }
}

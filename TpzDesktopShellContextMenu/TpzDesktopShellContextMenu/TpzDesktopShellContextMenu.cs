﻿using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing;
using SharpShell.SharpContextMenu;
using System.Runtime.InteropServices;
using SharpShell.Attributes;
using System.IO;

namespace DesktopShellContext
{
    [ComVisible(true)]
    [COMServerAssociation(AssociationType.ClassOfExtension, ".pdf", ".tif")]
    public class DesktopShellContext : SharpContextMenu
    {
        //GLOBALS
        const string itemDesktopMergeName = "Merge Documents With Trapeze";
        const string installerLocation = @"C:\Program Files (x86)\Onstream Trapeze\";
        const string tpzDesktopMergeLocation = @"C:\Users\jay.sindorff\Git\TpzDesktopMerge\TpzDesktopMerge\bin\Debug\TpzDesktopMerge.exe";
        protected override bool CanShowMenu()
        {
            return true;
        }

        protected override ContextMenuStrip CreateMenu()
        {

            //Obtain the Icon for the context menu item from the Trapeze Application
            Icon tpzIcon = Icon.ExtractAssociatedIcon(installerLocation + "trapeze.exe");
            Image image = tpzIcon.ToBitmap();

            //create new context menu
            var menu = new ContextMenuStrip();
            
            //Create new item for context menu with icon
            var itemDesktopMerge = new ToolStripMenuItem(itemDesktopMergeName, image);
            
            // Set Action for when item is selected
            itemDesktopMerge.Click += (sender, args) => MergeDocuments();

            //Add item to menu
            menu.Items.Add(itemDesktopMerge);

            /* Submenu code - to do for future releases
            var submenu = new ToolStripMenuItem("Trapeze Helpers", image);
            var itemDesktopIni = new ToolStripMenuItem(itemDesktopIniName);
            itemDesktopIni.Click += (sender, args) => MergeDocuments();
            submenu.DropDownItems.Add(itemDesktopMerge);
            submenu.DropDownItems.Add(itemDesktopIni);
             */
 
            return menu;
        }

        // Merge Selected Documents using TpzDesktopMerge.exe
        private void MergeDocuments()
        {
            string folderPath = null;
            string fileName = null;
            string dstPath = null;
            string srcPaths = null;

            
            folderPath = Path.GetDirectoryName(SelectedItemPaths.ElementAt(0));

            // Select Option on where to save the Destination Merged File
            SaveFileDialog mergeOutputDialog = new SaveFileDialog();
            mergeOutputDialog.Filter = "PDF files (*.pdf)|*.pdf";
            mergeOutputDialog.OverwritePrompt = true;
            mergeOutputDialog.InitialDirectory = folderPath;
            mergeOutputDialog.RestoreDirectory = true;
            mergeOutputDialog.Title = "Destination of Merged PDF Document";

            // If destination File has not beed selected - stop.
            if (mergeOutputDialog.ShowDialog() == DialogResult.OK)
            {
                dstPath = mergeOutputDialog.FileName;
            }
            else
            {               
                return;
            }

            // Merge Source Paths into one string for passing as argument
            foreach (string filepath in SelectedItemPaths)
            {
                srcPaths += filepath + " ";
            }
            
            // FOR TESTING PURPOSES ONLY
            srcPaths += "c:\\test\\2.pdf";

            // Create Start Options for TpzMerge.exe
            ProcessStartInfo startInfo = new ProcessStartInfo();
            startInfo.CreateNoWindow = true;
            startInfo.UseShellExecute = true;
            startInfo.FileName = tpzDesktopMergeLocation;
            startInfo.WindowStyle = ProcessWindowStyle.Hidden;
           
            // FOR TESTING PURPOSES ONLY
            //startInfo.Arguments = "-debug " + dstPath + " " + srcPaths;
            // Set arguments to pass to tpzmerge.exe

            startInfo.Arguments = dstPath + " " + srcPaths;
            try
            {
                using (Process tpzDesktopMerge = Process.Start(startInfo))
                {
                    tpzDesktopMerge.WaitForExit();
                    MessageBox.Show("Merge Complete");
                }
            }
            catch
            {

            }

            
        }

        public Image Properties { get; set; }
    }
}

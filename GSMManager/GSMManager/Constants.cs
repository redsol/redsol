﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GSMManager
{
    class Constants
    {
        //Error Codes
        public const int GENERAL_ERROR_CODE = 1;
        public const int DESTINATION_EXISTS = 2;
        public const int SOURCE_DOESNT_EXIST = 3;
        public const int LICENSE_INVALID = 4;
        public const int LICENSE_NOT_FOUND = 5;

        //Reading and Writing
        public const uint GENERIC_READ = 0x80000000;
        public const uint GENERIC_WRITE = 0x40000000;



    }
}


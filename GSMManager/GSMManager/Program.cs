﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Diagnostics;
using System.IO;

namespace GSMManager
{
    class Program
    {
        static void Main(string[] args)
        {

            /* Command Line Parameters
            * args[0] Starting MailboxID
            * args[1] Finishing MailboxID
            * */
            Process lastInstance = new Process();
            // Trap Ctrl+C to cancel process
            Console.TreatControlCAsInput = false;
            Console.CancelKeyPress += new ConsoleCancelEventHandler(CancelPressed);
            Global.skipMailboxes = new List<int>();
            int skipFound;
            #region Testing and Debugging
            /*--FOR TESTING PURPOSES ONLY
            //Check for debug commandline Argument and LaunchDebugger if found
            if (args[0] == "-debug" && Debugger.IsAttached == false)
            {
                Debugger.Launch();
               for(int i = 0; i < args.Length - 1; i++)
               {
                   args[i] = args[i + 1];
               }
               args[args.Length - 1] = "";
            }*/
            #endregion

            //Check number of arguments
            if (args.Length < 2)
            {
                Console.WriteLine("Usage: GSMManager.exe StartMailboxID EndMailboxID MaxProcesses SkipMailboxID SkipMailboxID ... /n");
                System.Environment.Exit(Constants.GENERAL_ERROR_CODE);
            }
            Console.WriteLine("Setting Global ID's ");
            Global.lastMailbox = Convert.ToInt32(args[1]);
            Global.currentMailbox = Convert.ToInt32(args[0]);
            Global.firstMailbox = Convert.ToInt32(args[0]);
            Global.processMax = Convert.ToInt32(args[2]);
            for (int i = 3; i < args.Length;i++ )
            {
                Global.skipMailboxes.Add(Convert.ToInt32(args[i]));
            }

                while (Global.currentMailbox <= Global.lastMailbox)
                {
  

                    while (Global.processCount <= Global.processMax)
                    {
                        //If end of processing list is reached - don't create any more processes
                        if (Global.currentMailbox > Global.lastMailbox) break;
                        
                        skipFound = 0;
                        // If mailbox is on skiplist - skip processing
                        foreach (int skipMailbox in Global.skipMailboxes)
                        {
                            if (skipMailbox == Global.currentMailbox)
                            {
                                Console.WriteLine("Skipping Process Mailbox ID " + Global.currentMailbox);
                                skipFound = 1;
                                break;
                            }
                        }
                        if (skipFound == 1)
                        {
                            Global.currentMailbox++;
                            break;
                        }
                        try
                        {
                            Console.WriteLine("Starting Process for " + Global.currentMailbox + " in process slot #" + Global.processCount);
                            int mailboxID = Global.currentMailbox;
                            Global.currentMailbox++;
                            Global.processCount++;
                            Process gsmInstance = new Process();

                            gsmInstance.StartInfo.CreateNoWindow = true;
                            gsmInstance.StartInfo.UseShellExecute = false;
                            gsmInstance.StartInfo.RedirectStandardOutput = true;
                            gsmInstance.StartInfo.RedirectStandardError = true;
                            gsmInstance.StartInfo.FileName = @"C:\Program Files (x86)\Dell\Archive Manager\GroupWiseStoreManagerService.exe";
                            gsmInstance.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
                            gsmInstance.StartInfo.Arguments = "-console -singleinstance -id " + mailboxID;
                            gsmInstance.Exited += (sender, e) => gsmExited(sender, e, gsmInstance, mailboxID);
                            gsmInstance.OutputDataReceived += (sender, e) => e_OutputDataReceived(sender, e, mailboxID);
                            gsmInstance.ErrorDataReceived += (sender, e) => e_OutputErrorReceived(sender, e, mailboxID);
                            gsmInstance.EnableRaisingEvents = true;
                            //gsmInstance.Exited += (sender, e) => e_gsmExited(sender, e, gsmInstance);
                            gsmInstance.Start();
                            gsmInstance.BeginOutputReadLine();
                            gsmInstance.BeginErrorReadLine();
                            lastInstance = gsmInstance;
                            Console.WriteLine("Processing for " + mailboxID + " Started");


                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine("GSM MGR failed to start new process: " + ex.Message);
                            return;
                        }

                    }
                }
            //Wait for the last process to finish
            lastInstance.WaitForExit();
        }
        protected static void CancelPressed(object sender, ConsoleCancelEventArgs args)
        {
            args.Cancel = true;
        }
        protected static void gsmExited(object sender, EventArgs e, Process exitedProcess, int mailboxID)
        {
            
            //Calculate time running for mailbox
            TimeSpan overallRunningTime = exitedProcess.ExitTime - exitedProcess.StartTime;
            Console.WriteLine("Process Exited For " + mailboxID);
            Console.WriteLine("Process Overall Running Time: " + overallRunningTime.ToString());
            Console.WriteLine("Process Exit Code: " + exitedProcess.ExitCode);

            using (StreamWriter logStream = new StreamWriter(@"Log.csv", true))
            {
                logStream.WriteLine(DateTime.Now.ToString() + "," + mailboxID.ToString() + "," + overallRunningTime.ToString() + "," + exitedProcess.ExitCode.ToString());
            }

            //Reduce the processCount
            Global.processCount--;          
        }
        static void e_OutputErrorReceived(object sender, DataReceivedEventArgs e, int mailboxID)
        {
            using (StreamWriter outputStream = new StreamWriter(@"C:\logs\GSMMgr\" + mailboxID + "err.txt", true))
            {
                outputStream.WriteLine(e.Data);
            }
        }
        static void e_OutputDataReceived(object sender, DataReceivedEventArgs e, int mailboxID)
        {
           
            using (StreamWriter outputStream = new StreamWriter(@"C:\logs\GSMMgr\" + mailboxID + ".txt", true))
            {
                outputStream.WriteLine(e.Data);
            }
        }
        public class Global
        {
            public static int currentMailbox;
            public static int firstMailbox;
            public static int lastMailbox = 0;
            public static int processCount = 0;
            public static int processMax = 4;
            public static List<int> skipMailboxes;

        }
    

    }
}
